<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tools\DataGenerator;

use PDO;
use Phalcon\Exception;

/**
 * Class DataGenerator
 *
 * This stores Dummy Data into the Database. In addition, it adds Default Data for certain Tables.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Kendall Parks <kendall.parks@breaktech.com>
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class DataGenerator extends PDO
{

	public $result = null;


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateLogs($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		if ($truncateTables === true) {
			$this->truncate('log');
		}

		$nextAvailableId = $this->max('log', 'lid');

		$ids = new DataPool(range(1, 9999));
		$requestIds = new DataPool(range(1, 9999));
		$messages = new DataPool($this->buildMixedStrings('AUTO#', 6, range(1, $number)));

		$randomLogs = $this->buildInserts(
			$number,
			[
				'lid' => 'pk',
				'fk_tenant_id' => 1,
				'application_id' => $ids,
				'request_id' => $requestIds,
				'type' => 'UNKNOWN',
				'message' => $messages,
				'context' => 'DATE'
			],
			$nextAvailableId + 1
		);

		$this->save('log', $randomLogs);
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateStudents($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultStudents = [];
		if ($truncateTables === true) {
			$this->truncate('student');

			$defaultStudents = [
				[
					'sid' => 0,
					'fk_tenant_id' => 0,
					'personal_id' => 'NULL',
					'first_name' => 'Anonymous',
					'last_name' => 'Student',
					'date_of_birth'	=> 'NULL',
					'state' => 'NULL',
					'school_name' => 'NULL',
					'grade' => 'NULL',
					'active' => 1
				],
				[
					'sid' => 1,
					'fk_tenant_id' => 1,
					'personal_id' => '1234567890',
					'first_name' => 'John',
					'last_name' => 'Smith',
					'date_of_birth'	=> '2000-01-10',
					'state' => 'IL',
					'school_name' => 'Evanston Elementary',
					'grade' => '11',
					'active' => 1
				]
			];
		}

		$nextAvailableId = $this->max('student', 'sid');

		$ids = new DataPool(range(1, 9999));
		$firstNames = new DataPool($this->parseJson('first-names.json'));
		$lastNames = new DataPool($this->parseJson('last-names.json'));
		$states = new DataPool($this->parseJson('states.json'));
		$schools = new DataPool($this->parseJson('schools.json'));
		$grades = new DataPool($this->parseJson('grades.json'));

		$randomStudents = $this->buildInserts(
			$number,
			[
				'sid' => 'pk',
				'fk_tenant_id' => 1,
				'personal_id' => $ids,
				'first_name' => $firstNames,
				'last_name' => $lastNames,
				'date_of_birth'	=> 'DATE',
				'state' => $states,
				'school_name' => $schools,
				'grade' => $grades,
				'active' => 1
			],
			$nextAvailableId + count($defaultStudents) + 1
		);

		$this->save('student', array_merge($defaultStudents, $randomStudents));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTests($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultTests = [];
		if ($truncateTables === true) {
			$this->truncate('test');

			$defaultTests = [
				[
					'tid' => 1,
					'fk_tenant_id' => 1,
					'external_tid' => 'i1111111111',
					'name' => 'Math Test G11',
					'program' => 'Diagnostic Assessment',
					'score_report' => 'None',
					'subject' => 'Math',
					'grade' => '11',
					'item_selection_algorithm' => 'Fixed',
					'security' => 'Non-Secure',
					'multimedia' => 'OnDemand',
					'scoring' => 'Immediate',
					'permissions' => 'Non-Restricted',
					'privacy' => 'Public',
					'description' => 'This is a sample Math Test.',
					'active' => 1
				]
			];
		}

		$nextAvailableId = $this->max('test', 'tid');

		$externalTids = new DataPool($this->buildMixedStrings('i#', 17, range(890123456789, $number + 890123456789)));
		$names = new DataPool($this->buildMixedStrings('Test Name #', 5, range(1, $number)));
		$programs = new DataPool($this->parseJson('test-programs.json'));
		$scoreReports= new DataPool($this->parseJson('test-score-reports.json'));
		$subjects = new DataPool($this->parseJson('test-subjects.json'));
		$grades = new DataPool($this->parseJson('test-grades.json'));
		$selectionAlgorithms = new DataPool($this->parseJson('test-item-selection-algorithms.json'));
		$security = new DataPool($this->parseJson('test-security.json'));
		$multimedia = new DataPool($this->parseJson('test-multimedia.json'));
		$scoring = new DataPool($this->parseJson('test-scoring.json'));
		$permissions = new DataPool($this->parseJson('test-permissions.json'));
		$privacy = new DataPool($this->parseJson('test-privacy.json'));
		$descriptions = new DataPool($this->buildMixedStrings('Auto Description #', 6, range(1, $number)));

		$randomTests = $this->buildInserts(
			$number,
			[
				'tid' => 'pk',
				'fk_tenant_id' => 1,
				'external_tid' => $externalTids,
				'name' => $names,
				'program' => $programs,
				'score_report' => $scoreReports,
				'subject' => $subjects,
				'grade' => $grades,
				'item_selection_algorithm' => $selectionAlgorithms,
				'security' => $security,
				'multimedia' => $multimedia,
				'scoring' => $scoring,
				'permissions' => $permissions,
				'privacy' => $privacy,
				'description' => $descriptions,
				'active' => 1
			],
			$nextAvailableId + count($defaultTests) + 1
		);

		$this->save('test', array_merge($defaultTests, $randomTests));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestContents($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultTestContents = [];
		if ($truncateTables === true) {
			$this->truncate('test_content');

			$defaultTestContents = [
				[
					'cid' => 1,
					'fk_tenant_id' => 1,
					'fk_test_form_revision_id' => 1,
					'path' => 'test.json',
					'default' => 1,
					'active' => 1
				]
			];
		}

		$nextAvailableId = $this->max('test_content', 'cid');

		$tfrids = new DataPool(range(2, 9999));

		$randomTestContents = $this->buildInserts(
			$number,
			[
				'cid' => 'pk',
				'fk_tenant_id' => 1,
				'fk_test_form_revision_id' => $tfrids,
				'path' => 'test_key',
				'default' => 1,
				'active' => 1
			],
			$nextAvailableId + count($defaultTestContents) + 1
		);

		$this->save('test_content', array_merge($defaultTestContents, $randomTestContents));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestForms($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultTestForms = [];
		if ($truncateTables === true) {
			$this->truncate('test_form');

			$defaultTestForms = [
				[
					'fid' => 1,
					'fk_tenant_id' => 1,
					'external_fid' => 'i2222222222',
					'fk_test_id' => 1,
					'name' => 'A',
					'external_form_test_id' => 'i3333333333',
					'form_test_name' => 'Math G11',
					'active' => 1
				],
			];
		}

		$nextAvailableId = $this->max('test_form', 'fid');

		$externalFids = new DataPool($this->buildMixedStrings('i#', 17, range(4567890123, $number + 4567890123)));
		$fkTestIds = new DataPool(range(2, 9999));
		$names = new DataPool($this->buildMixedStrings('Auto Form Name #', 5, range(1, $number)));
		$externalFormTestIds = new DataPool(
			$this->buildMixedStrings('i#', 17, range(4567890123, $number + 4567890123))
		);
		$testNames = new DataPool($this->buildMixedStrings('Auto Test Name #', 5, range(1, $number)));

		$randomTestForms = $this->buildInserts(
			$number,
			[
				'fid' => 'pk',
				'fk_tenant_id' => 1,
				'external_fid' => $externalFids,
				'fk_test_id' => $fkTestIds,
				'name' => $names,
				'external_form_test_id' => $externalFormTestIds,
				'form_test_name' => $testNames,
				'active' => 1
			],
			$nextAvailableId + count($defaultTestForms) + 1
		);

		$this->save('test_form', array_merge($defaultTestForms, $randomTestForms));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestFormRevisions($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultFormRevisions = [];
		if ($truncateTables === true) {
			$this->truncate('test_form_revision');

			$defaultFormRevisions = [
				[
					'rid' => 1,
					'fk_tenant_id' => 1,
					'external_rid' => 'i4444444444',
					'fk_test_form_id' => 1,
					'version' => 1,
					'compiled' => 'DATETIME',
					'compiled_by' => 'John Smith',
					'published' => '2015-05-05 05:05:05',
					'published_by' => 'John Smith',
					'active' => 1
				],
			];
		}

		$nextAvailableId = $this->max('test_form_revision', 'rid');

		$externalRids = new DataPool($this->buildMixedStrings('i#', 17, range(1234567890, $number + 1234567890)));
		$fkTestFormIds = new DataPool(range(2, 9999));
		$firstNames = new DataPool($this->parseJson('first-names.json'));

		$randomFormRevisions = $this->buildInserts(
			$number,
			[
				'rid' => 'pk',
				'fk_tenant_id' => 1,
				'external_rid' => $externalRids,
				'fk_test_form_id' => $fkTestFormIds,
				'version' => 1,
				'compiled' => 'DATETIME',
				'compiled_by' => $firstNames,
				'published' => 'DATETIME',
				'published_by' => $firstNames,
				'active' => 1
			],
			$nextAvailableId + count($defaultFormRevisions) + 1
		);

		$this->save('test_form_revision', array_merge($defaultFormRevisions, $randomFormRevisions));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestResults($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultTestResults = [];
		if ($truncateTables === true) {
			$this->truncate('test_results');

			$defaultTestResults = [
				[
					'trid' => 1,
					'fk_tenant_id' => 1,
					'fk_test_session_id' => 1,
					'token' => '5abe2752-035d-11e5-b9d1-f0def1bd7269',
					'path' => '1/1.json',
					'active' => 1,
					'created' => 'DATETIME'
				],
				[
					'trid' => 2,
					'fk_tenant_id' => 1,
					'fk_test_session_id' => 2,
					'token' => 'a43e0d9c-0ad2-11e5-aff9-f0def1bd7269',
					'path' => '2/2.json',
					'active' => 1,
					'created' => 'DATETIME'
				]
			];
		}

		$nextAvailableId = $this->max('test_results', 'trid');

		$testSessionIds = new DataPool(range(3, 9999));
		$tokens = new DataPool($this->buildMixedStrings('b7202717-6b31-11e5-auto-#', 12, range(1, 999)));

		$randomTestResults = $this->buildInserts(
			$number,
			[
				'trid' => 'pk',
				'fk_tenant_id' => 1,
				'fk_test_session_id' => $testSessionIds,
				'token' => $tokens,
				'path' => '1/1.json',
				'active' => 1,
				'created' => 'DATETIME'
			],
			$nextAvailableId + count($defaultTestResults) + 1
		);

		$this->save('test_results', array_merge($defaultTestResults, $randomTestResults));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestResultArchives($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		if ($truncateTables === true) {
			$this->truncate('test_results_archive');
		}

		$nextAvailableId = $this->max('test_results_archive', 'trid');

		$testSessionIds = new DataPool(range(3, 9999));
		$tokens = new DataPool($this->buildMixedStrings('b7202717-6b31-11e5-auto-#', 12, range(1, 999)));

		$randomTestResults = $this->buildInserts(
			$number,
			[
				'trid' => 'pk',
				'fk_tenant_id' => 1,
				'fk_test_session_id' => $testSessionIds,
				'token' => $tokens,
				'path' => '1/1.json',
				'active' => 1,
				'created' => 'DATETIME'
			],
			$nextAvailableId + 1
		);

		$this->save('test_results_archive', $randomTestResults);
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestSessions($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultTestSessions = [];
		if ($truncateTables === true) {
			$this->truncate('test_session');

			$defaultTestSessions = [
				[
					'tsid' => 1,
					'fk_tenant_id' => 1,
					'external_tsid' => 123456,
					'fk_student_id' => 1,
					'fk_test_form_id' => 1,
					'fk_test_form_revision_id' => 1,
					'test_key' => 'DOCUMENTAT',
					'status' => 'Scheduled',
					'enable_line_reader' => 1,
					'enable_text_to_speech' => 1,
					'active' => 1
				],
				[
					'tsid' => 2,
					'fk_tenant_id' => 1,
					'external_tsid' => 123789,
					'fk_student_id' => 1,
					'fk_test_form_id' => 1,
					'fk_test_form_revision_id' => 1,
					'test_key' => 'AAAAAAAAAA',
					'status' => 'Scheduled',
					'enable_line_reader' => 1,
					'enable_text_to_speech' => 1,
					'active' => 1
				]
			];
		}

		$nextAvailableId = $this->max('test_session', 'tsid');

		$tsids = new DataPool(range(3, 9999));
		$studentIds = new DataPool(range(2, 9999));
		//$testKeys = new DataPool($this->buildMixedStrings('AUTO#', 6, range(1, $number)));
		//$testFormIds = new DataPool($this->get('test_form_revision', 'fk_test_form_id'), true);
		//$testFormRevisionIds = new DataPool($this->get('test_form_revision', 'rid'));

		$randomTestSessions = $this->buildInserts(
			$number,
			[
				'tsid' => 'pk',
				'fk_tenant_id' => 1,
				'external_tsid' => $tsids,
				'fk_student_id' => $studentIds,
				'fk_test_form_id' => 1,
				'fk_test_form_revision_id' => 'NULL',
				'test_key' => 'test_key',
				'status' => 'Scheduled',
				'enable_line_reader' => 1,
				'enable_text_to_speech' => 1,
				'active' => 1
			],
			$nextAvailableId + count($defaultTestSessions) + 1
		);

		$this->save('test_session', array_merge($defaultTestSessions, $randomTestSessions));
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateTestSessionArchives($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		if ($truncateTables === true) {
			$this->truncate('test_session_archive');
		}

		$nextAvailableId = $this->max('test_session_archive', 'tsid');

		$tsids = new DataPool(range(3, 9999));
		$studentIds = new DataPool(range(2, 9999));
		//$testKeys = new DataPool($this->buildMixedStrings('AUTO#', 6, range(1, $number)));
		//$testFormIds = new DataPool($this->get('test_form_revision', 'fk_test_form_id'), true);
		//$testFormRevisionIds = new DataPool($this->get('test_form_revision', 'rid'));

		$randomTestSessions = $this->buildInserts(
			$number,
			[
				'tsid' => 'pk',
				'fk_tenant_id' => 1,
				'external_tsid' => $tsids,
				'fk_student_id' => $studentIds,
				'fk_test_form_id' => 1,
				'fk_test_form_revision_id' => 'NULL',
				'test_key' => 'test_key',
				'status' => 'Scheduled',
				'enable_line_reader' => 1,
				'enable_text_to_speech' => 1,
				'active' => 1
			],
			$nextAvailableId + 1
		);

		$this->save('test_session_archive', $randomTestSessions);
	}


	/**
	 * @param  integer   $number (@default=null)
	 * @param  boolean   $truncateTables (@default=false)
	 * @throws Exception
	 */
	public function generateUsers($number = null, $truncateTables = false)
	{
		if (!is_int($number) && !is_null($number)) {
			throw new Exception('Expected integer, but received ' . gettype($number));

		} elseif ($number === 0) {
			return;
		}

		$defaultUsers = [];
		if ($truncateTables === true) {
			$this->truncate('user');

			$defaultUsers = [
				[
					'uid' => 0,
					'fk_tenant_id' => 0,
					'username' => 'Anonymous',
					'password' => 'NULL',
					'secret' => 'NULL',
					'type' => 'TD',
					'active' => 1
				],
				[
					'uid' => 1,
					'fk_tenant_id' => 1,
					'username' => 'DocumentationTD',
					'password' => '$2y$08$DRY.ntDq54GZ6JXiHFdoGODlhTKwcuG.7pT0WOJijkn7ut4zgiaeC',
					'secret' => '578cca1aa71f702c26cc81439392124c',
					'type' => 'TD',
					'active' => 1
				],
				[
					'uid' => 2,
					'fk_tenant_id' => 1,
					'username' => 'TestTD',
					'password' => '$2y$08$CMKRA7WA8hX/0StseV5MAO1AgjvpooyDX7Fyk8ghcGaV/e0oWKoG.',
					'secret' => '82cc94f866eb06b64ff535254ec42173',
					'type' => 'TD',
					'active' => 1
				]
			];
		}

		$nextAvailableId = $this->max('user', 'uid');

		$usernames = new DataPool($this->parseJson('first-names.json'));
		$passwords = new DataPool($this->buildMixedStrings('AUTO-PASSWORD#', 6, range(1, $number)));
		$secrets = new DataPool($this->buildMixedStrings('AUTO-SECRET#', 6, range(1, $number)));

		$randomUsers = $this->buildInserts(
			$number,
			[
				'uid' => 'pk',
				'fk_tenant_id' => 1,
				'username' => $usernames,
				'password' => $passwords,
				'secret' => $secrets,
				'type' => 'PRC',
				'active' => 1
			],
			$nextAvailableId + count($defaultUsers) + 1
		);

		$this->save('user', array_merge($defaultUsers, $randomUsers));
	}


	/**
	 * @param  integer   $number
	 * @param  array     $params
	 * @param  integer   $pkIndex
	 * @return array
	 * @throws Exception
	 */
	private function buildInserts($number, array $params, $pkIndex = 0)
	{
		// Pick out the primary key elements.
		$pks = array_filter($params, function ($v) {
			return (is_string($v) && $v === 'pk');
		});

		$testKeys = array_filter($params, function ($v) {
			return (is_string($v) && $v === 'test_key');
		});

		$strings = array_filter($params, function ($v) {
			return (is_string($v) && $v !== 'pk' && $v !== 'test_key' && $v !== 'DATETIME');
		});

		// Pick out the integer elements.
		$ints = array_filter($params, function ($v) {
			return is_int($v);
		});

		// Pick out the DATETIME elements.
		$dates = array_filter($params, function ($v) {
			return (is_string($v) && $v === 'DATE');
		});

		// Pick out the DATETIME elements.
		$datetimes = array_filter($params, function ($v) {
			return (is_string($v) && $v === 'DATETIME');
		});

		// Pick out the Random Data Pool elements.
		$dataPools = array_filter($params, function ($v) {
			return (is_object($v) && get_class($v) === 'DataPool');
		});

		// Build inserts.
		$inserts = [];
		for ($x = 0; $x < $number; $x++) {
			$record = [];

			foreach ($pks as $column => $value) {
				$record[$column] = $pkIndex++;
			}

			foreach ($testKeys as $column => $value) {
				$record[$column] = 'AUTO' . ($pkIndex - 1);
			}

			foreach ($strings as $column => $value) {
				$record[$column] = $value;
			}

			foreach ($ints as $column => $value) {
				$record[$column] = $value;
			}

			foreach ($dates as $column => $value) {
				$record[$column] = date('Y-m-d');
			}

			foreach ($datetimes as $column => $value) {
				$record[$column] = date('Y-m-d H:i:s');
			}

			/** @var DataPool $dataPool */
			foreach ($dataPools as $column => $dataPool) {
				$record[$column] = $dataPool->generate(false);
			}
			$inserts[] = $record;
		}

		return $inserts;
	}


	/**
	 * @param  string  $format
	 * @param  integer $decimals
	 * @param  array   $numbers
	 * @param  string  $intToken
	 * @return array
	 */
	private function buildMixedStrings($format, $decimals, array $numbers, $intToken = '#')
	{
		$array = [];
		foreach ($numbers as $number) {
			$array[] = str_replace($intToken, sprintf("%0{$decimals}d", $number), $format);
		}

		return $array;
	}


	/**
	 * Saves generated Data into Database.
	 *
	 * @param  string    $table
	 * @param  array     $data
	 * @throws Exception
	 */
	private function save($table, array $data)
	{
		$columns = array_keys(current($data));
		foreach ($columns as $key => $value) {
			$columns[$key] = '`' . $value . '`';
		}
		$columns = implode(', ', $columns);
		$values = [];

		// Build the VALUES parameters.
		array_walk($data, function ($record) use (&$values, $data) {
			$string = '';

			foreach (array_keys(current($data)) as $column) {
				if (is_string($record[$column])) {
					$string .= ($record[$column] === 'NULL' ? 'NULL, ' : "'{$record[$column]}', ");

				} else {
					$string .= "{$record[$column]}, ";
				}
			}

			$values[] = '(' . trim($string, ' ,') . ')';
		});

		$this->exec(
			"INSERT INTO `{$table}` ({$columns}) VALUES " . implode(', ', $values)
		);

		if (intval($this->errorCode()) > 0) {
			throw new Exception($this->errorInfo()[2]);
		}
	}


	/**
	 * @param  string $table
	 * @param  string $column
	 * @return array
	 */
	/*
	private function get($table, $column)
	{
		$result = $this->query("SELECT `{$column}` FROM `{$table}`")->fetchAll(PDO::FETCH_COLUMN);

		return $result;
	}
	*/


	/**
	 * Returns the Maximum Value for given Column from the given Table.
	 *
	 * @param  string  $table
	 * @param  string  $column
	 * @return integer
	 */
	private function max($table, $column)
	{
		$result = $this->query("SELECT MAX(`{$column}`) as max FROM `{$table}`");

		return intval($result->fetch(PDO::FETCH_ASSOC)['max']);
	}


	/**
	 * Parses Data from the given JSON File.
	 *
	 * @param  string    $filename
	 * @return mixed
	 * @throws Exception
	 */
	private function parseJson($filename)
	{
		$filePath = __DIR__ . '/data/' . $filename;

		if (file_exists($filePath)) {
			return json_decode(file_get_contents($filePath), true);

		} else {
			throw new Exception("Cannot load file '{$filePath}'.");
		}
	}


	/**
	 * Truncates given Table and deletes ALL Data from it.
	 *
	 * @param  string  $table
	 * @return integer
	 */
	private function truncate($table)
	{
		return $this->exec("TRUNCATE TABLE `{$table}`");
	}
}
