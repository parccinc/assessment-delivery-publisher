<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tools\DataGenerator;

use Phalcon\Exception;

/**
 * Class DataPool.
 *
 * This generates Data Pool with Dummy Data
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Kendall Parks <kendall.parks@breaktech.com>
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class DataPool
{

	protected $initDataPool;
	protected $dataPool;
	protected $linear;


	/**
	 * Data Pool Constructor.
	 *
	 * @param array   $items
	 * @param boolean $linear
	 */
	public function __construct(array $items, $linear = false)
	{
		$this->initDataPool = array_values($items);
		$this->dataPool = $this->initDataPool;
		$this->linear = $linear;
	}


	/**
	 * @param  boolean $replace
	 * @return mixed
	 * @throws Exception
	 */
	public function generate($replace = true)
	{
		if (empty($this->dataPool)) {
			$this->refill();
		}

		if ($this->linear === true) {
			$val = $this->dataPool[0];
			unset($this->dataPool[0]);
			$this->dataPool = array_values($this->dataPool);

			return $val;

		} else {
			$index = rand(1, count($this->dataPool)) - 1;

			if (array_key_exists($index, $this->dataPool)) {
				$value = $this->dataPool[$index];

			} else {
				throw new Exception("Random Data Pool index '{$index}' doesn't exist.");
			}

			if (!$replace) {
				unset($this->dataPool[$index]);
				$this->dataPool = array_values($this->dataPool);
			}

			return $value;
		}
	}

	private function refill()
	{
		$this->dataPool = $this->initDataPool;
	}
}
