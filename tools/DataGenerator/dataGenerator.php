<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// Extend the execution time to unlimited!
set_time_limit(0);

use PARCC\ADP\Tools\DataGenerator\DataGenerator;

/**
 * This is the Data Generator Tool. It generates Dummy Data and populates the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Kendall Parks <kendall.parks@breaktech.com>
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

// For security reasons limit access strictly to command line!
if (PHP_SAPI !== 'cli') {
	exit("This tool is only available from the command line!");
}

require_once 'dataGenerator/DataGenerator.php';
require_once 'dataGenerator/DataPool.php';


// Check if Configuration File exists.
if (file_exists(__DIR__ . '/config.php')) {
	// Load and parse Configuration File.
	$config = require __DIR__ . '/config.php';

} else {
	// Exit if Configuration File is missing.
	exit("Missing Configuration File: '" . __DIR__ . "/config.php'.");
}

// Instantiate DataGenerator.
$dataGenerator = new DataGenerator(
	'mysql:host=' . $config['dbHost'] . ';port=3306;dbname=' . $config['dbName'],
	$config['dbUsername'],
	$config['dbPassword']
);


// Generate Data.
$dataGenerator->generateLogs(
	$config['numberOfLogs'],
	$config['truncateTables']
);
$dataGenerator->generateStudents(
	$config['numberOfStudent'],
	$config['truncateTables']
);
$dataGenerator->generateTests(
	$config['numberOfTests'],
	$config['truncateTables']
);
$dataGenerator->generateTestContents(
	$config['numberOfTestContents'],
	$config['truncateTables']
);
$dataGenerator->generateTestForms(
	$config['numberOfTestForms'],
	$config['truncateTables']
);
$dataGenerator->generateTestFormRevisions(
	$config['numberOfTestFormRevisions'],
	$config['truncateTables']
);
$dataGenerator->generateTestResults(
	$config['numberOfTestResults'],
	$config['truncateTables']
);
$dataGenerator->generateTestResultArchives(
	$config['numberOfTestResultArchives'],
	$config['truncateTables']
);
$dataGenerator->generateTestSessions(
	$config['numberOfTestSessions'],
	$config['truncateTables']
);
$dataGenerator->generateTestSessionArchives(
	$config['numberOfTestSessionArchives'],
	$config['truncateTables']
);
$dataGenerator->generateUsers(
	$config['numberOfUsers'],
	$config['truncateTables']
);

// Print confirmation.
echo "Data Successfully generated.";
