#!/usr/bin/env bash
echo -e "Deleting ADP Cache...\n"
php -d apc.enable_cli=1 cacheWipe.php
echo -e "\n\n"
