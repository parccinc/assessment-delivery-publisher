@echo off
cls
title PARCC-ADP Code Documentation Generator

echo Generating ADP Code Documentation...
:: Generate PHPLOC Report.
echo.
php bin\phploc-2.0.4.phar --log-xml bin/data/phploc.xml --names *.php,*.phtml --exclude vendor ../../

:: Generate Code Documentation.
echo.
cd bin
php -dextension=/xampp/php/ext/php_fileinfo.dll -dextension=/xampp/php/ext/php_xsl.dll phpdox-0.8.1.1.phar
cd ..
php bin\data\patch.php

echo.
pause
