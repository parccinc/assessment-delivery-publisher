<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
// Extend the execution time to unlimited!
set_time_limit(0);


use Phalcon\Config;
use Phalcon\Loader;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlConnection;
use Phalcon\Di\FactoryDefault as DefaultDI;
use PARCC\ADP\Models\TestContent;
use PARCC\ADP\Models\TestResults;
use PARCC\ADP\Models\TestResultsArchive;
use PARCC\ADP\Models\TestFormRevision;
use PARCC\ADP\Tools\AWS\S3;
use PARCC\ADP\Tools\AWS\S3Exception;

/**
 * Data Wipe is used to delete ALL Data from Database and AWS S3.
 * Based on its Configuration certain Records can be skipped and all related Objects within AWS S3.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 * @author: Kendall Parks <kendall.parks@breaktech.com>
 */

// For security reasons limit access strictly to command line!
if (PHP_SAPI !== 'cli') {
	exit("This tool is only available from the command line!");
}

require_once 'include/dataWipeTools.php';


if (isset($argv[1]) && in_array($argv[1], ['--h', '-h', '-help', '--help', '?'])) {
	echo "\033[1mUsage:\033[0m" . PHP_EOL .
	     "\033[1m{$argv[0]}\033[0m [OPTION] [table_name...] " . PHP_EOL .
	     "This script will truncate all tables except user and system tables and the table list that will be " .
	     "specified as arguments." . PHP_EOL;

	exit();
}


echo "[Step 0] Loading Configuration Files & Initializing Used Namespaces." . PHP_EOL;

/**
 * Configuration Parameters need to have the below Default Configuration.
 *
 * 	return [
 * 	    'doNotTouchTables' => [
 *          'system',
 *          'user'
 *      ],
 * 	    'recordsToKeep' => [
 *          'students' => 1,
 *          'test' => 1,
 *          'test_content' => 16,
 *          'test_form' => 1,
 *          'test_form_revision' => 1,
 *          'test_results' => 2,
 *          'test_session' => 2
 *      ],
 * 	];
 */
if (!is_file(dirname(dirname(__DIR__)) . '/app/config/config.php')) {
	exit("Configuration file '" . dirname(dirname(__DIR__)) . "/app/config/config.php' is missing!");
}


// Try to load Configuration file.
if (file_exists('config.php')) {
	$defaultParameters = require 'config.php';
} else {
	// In case Configuration file is missing.
	exit("Configuration ERROR:\nFile 'config.php' is missing!");
}

if (!array_key_exists('recordsToKeep', $defaultParameters) || !is_array($defaultParameters['recordsToKeep']) ||
	!array_key_exists('doNotTouchTables', $defaultParameters) || !is_array($defaultParameters['doNotTouchTables'])
) {
	exit('defaultParameters file malformed!');
}


echo "[Step 1] Creating Loader and Registering Namespaces." . PHP_EOL;

$loader = new Loader();
$loader->registerNamespaces([
	'PARCC\ADP\Models' => dirname(dirname(__DIR__)) . '/app/models'
])->register();


echo "[Step 2] Instantiating DefaultDI." . PHP_EOL;

/**
 * @var DefaultDI $di Default Dependency Injector.
 */
$di = new DefaultDI();


echo "[Step 3] Loading the Configuration and Database." . PHP_EOL;

/**
 * Returns Application Configuration.
 *
 * @return Config Application Configuration.
 */
$di->setShared('config', function () {
	// Retrieve Application Configuration.
	return require_once dirname(dirname(__DIR__)) . '/app/config/config.php';
});

$di->setShared('db', function () use ($di) {
	// Create Database Connection. Warnings and Errors must be silenced, so application can handle them appropriately
	// and be able to recover without affecting the regular logical flow.
	$mysqlConnection = @new MysqlConnection(
		(array) $di['config']->database
	);
	return $mysqlConnection;
});


echo "[Step 4] Loading amazon/S3.php client." . PHP_EOL;

require_once dirname(dirname(__DIR__)) . '/tools/TestDriverDeployment/aws/S3.php';
require_once dirname(dirname(__DIR__)) . '/tools/TestDriverDeployment/aws/S3Exception.php';
require_once dirname(dirname(__DIR__)) . '/tools/TestDriverDeployment/aws/S3Request.php';


echo "[Step 5] Initializing Variables." . PHP_EOL;

$statistics = [];
$config = $di['config'];

// Initialize AWS S3 Service.
$s3 = new S3($config->s3->accessKeyId, $config->s3->secretAccessKey);

$contentBucket = $config->s3->contentBucket;
$contentArchiveBucket = $config->s3->contentArchiveBucket;
$resultsBucket = $config->s3->resultsBucket;
$resultsArchiveBucket = $config->s3->resultsArchiveBucket;


echo "[Step 6] Processing Deletion of Contents from AWS S3." . PHP_EOL;

$contents = TestContent::find();
if (!empty($contents)) {
	$statistics['deletedContentsCount'] = 0;
	$statistics['deletedGzippedContentsCount'] = 0;
	foreach ($contents as $content) {
		if ($content->parentTestFormRevisionId > $defaultParameters['recordsToKeep']['test_form_revision']) {
			// Delete Object from AWS S3.
			$key = $config->s3->contentPrefix . '/' . $content->parentTestFormRevisionId . '/' . $content->path;

			// Try to delete Object from AWS S3.
			try {
				$s3->deleteObject($contentBucket, $key);
				++$statistics['deletedContentsCount'];
				echo "Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $exception) {
				echo "[AWS] Unable to Delete: " . $contentBucket . $key . PHP_EOL;
			}

			try {
				// Try to delete GZip Version of the Object from AWS S3.
				$s3->deleteObject($contentBucket, $key . '.gz');
				++$statistics['deletedGzippedContentsCount'];
				echo "Successfully Deleted: {$key}.gz" . PHP_EOL;

			} catch (S3Exception $exception) {
				echo "[AWS] Unable to Delete: " . $contentBucket . $key . ".gz." . PHP_EOL;
			}

		} else {
			echo "Skipping Content Id: {$content->contentId} Test Form Revision Id: " .
			     "{$content->parentTestFormRevisionId}" . PHP_EOL;
		}
	}
}

$statistics['deleteContentsNotFoundInDatabaseCount'] = 0;
$statistics['deleteGzippedContentsNotFoundInDatabaseCount'] = 0;

// Check if there are any additional Objects inside AWS S3 that were not referenced inside Database.
$keys = [];
try {
	$keys = $s3->getObjectList(
		$contentBucket,
		$config->s3->contentPrefix . '/',
		$config->s3->contentPrefix . '/'
	);

} catch (S3Exception $ex) {
	// Do nothing.
}

$keys = array_keys($keys);
$keys = array_reverse($keys);

foreach ($keys as $key) {
	$match = [];

	$pattern = str_replace('//', '\//', $config->s3->contentPrefix);
	$pattern = ($pattern == '') ? '' : $pattern . '\/';
	preg_match('#' . $pattern . '([\d]+)\/.*$#', $key, $match);

	if (isset($match[1])) {
		if ($match[1] > $defaultParameters['recordsToKeep']['test_form_revision']) {
			try {
				$s3->deleteObject($contentBucket, $key);
				++$statistics['deleteContentsNotFoundInDatabaseCount'];
				echo "Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $ex) {
				echo "[AWS] Unable to Delete: " . $contentBucket . $key . PHP_EOL;
			}
		}

	} else {
		if ($defaultParameters['keepIrregularFiles']) {
			echo "Skipping Irregular Key: {$key}" . PHP_EOL;

		} else {
			try {
				$s3->deleteObject($contentBucket, $key);
				++$statistics['deleteContentsNotFoundInDatabaseCount'];
				echo "Irregular Key Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $ex) {
				echo "[AWS] Unable to Delete: " . $contentBucket . $key . PHP_EOL;
			}
		}
	}
}


echo "[Step 7] Processing Deletion of Form Revisions from AWS S3." . PHP_EOL;

$revisions = TestFormRevision::find();
if (!empty($revisions)) {
	$statistics['deletedRevisionPackagesCount'] = 0;
	foreach ($revisions as $revision) {
		// Delete Object from AWS S3.
		$key = $config->s3->contentArchivePrefix . '/' . $revision->revisionId . '/' .
		       $revision->externalRevisionId . ".zip";

		try {
			$s3->deleteObject($contentArchiveBucket, $key);
			++$statistics['deletedRevisionPackagesCount'];
			echo "Successfully Deleted: {$key}" . PHP_EOL;

		} catch (S3Exception $exception) {
			echo "[AWS] Unable to Delete: " . $contentArchiveBucket . $key . PHP_EOL;
		}
	}
}

$statistics['deletePackageNotFoundInDatabaseCount'] = 0;

try {
	$keys = $s3->getObjectList(
		$contentArchiveBucket,
		$config->s3->contentArchivePrefix . '/',
		$config->s3->contentArchivePrefix . '/'
	);

} catch (S3Exception $exception) {
	// Do nothing.
}

$keys = array_keys($keys);
$keys = array_reverse($keys);

foreach ($keys as $key) {
	try {
		$s3->deleteObject($contentArchiveBucket, $key);
		++$statistics['deletePackageNotFoundInDatabaseCount'];
		echo "Successfully Deleted: {$key}" . PHP_EOL;

	} catch (S3Exception $exception) {
		echo "[AWS] Unable to Delete: " . $contentArchiveBucket . $key . PHP_EOL;
	}
}


echo "[Step 8] Processing Deletion of Test Results from AWS S3." . PHP_EOL;

$results = TestResults::find();
if (!empty($results)) {
	$statistics['deletedResultsCount'] = 0;
	$statistics['deletedArchivedResultsPackagesCount'] = 0;

	foreach ($results as $result) {
		$key = $config->s3->resultsPrefix . '/' . $result->path;

		if ($result->parentTestSessionId > $defaultParameters['recordsToKeep']['test_session']) {
			// Delete Object from AWS S3.
			try {
				$s3->deleteObject($resultsBucket, $key);
				++$statistics['deletedResultsCount'];
				echo "Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $exception) {
				echo "[AWS] Unable to Delete: " . $resultsBucket . $key . PHP_EOL;
			}

		} else {
			echo "Skipping: {$key}" . PHP_EOL;
		}
	}
}

$statistics['deleteResultNotFoundInDatabaseCount'] = 0;
try {
	$keys = $s3->getObjectList(
		$resultsBucket,
		$config->s3->resultsPrefix . '/',
		$config->s3->resultsPrefix . '/'
	);

} catch (S3Exception $exception) {
	// Do nothing.
}

$keys = array_keys($keys);
$keys = array_reverse($keys);

foreach ($keys as $key) {
	$match = [];
	$pattern = str_replace('//', '\//', $config->s3->resultsPrefix);
	$pattern = ($pattern == '') ? '' : $pattern . '\/';
	preg_match('#' . $pattern . '([\d]+)\/.*#', $key, $match);

	if (isset($match[1])) {
		// This will handle cases where testSession has more than 1 digit test_sessions > 9
		if (strlen($match[1] > 1)) {
			$match[1] = strrev($match[1]);
		}

		if ($match[1] > $defaultParameters['recordsToKeep']['test_session']) {
			try {
				$s3->deleteObject($resultsBucket, $key);
				++$statistics['deleteResultNotFoundInDatabaseCount'];
				echo "Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $exception) {
				echo "[AWS] Unable to Delete: " . $resultsBucket . $key . PHP_EOL;
			}
		}

	} else {
		if ($defaultParameters['keepIrregularFiles']) {
			echo "Skipping Irregular Key: {$key}" . PHP_EOL;

		} else {
			try {
				$s3->deleteObject($resultsBucket, $key);
				++$statistics['deleteResultNotFoundInDatabaseCount'];
				echo "Irregular Key Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $ex) {
				echo "[AWS] Unable to Delete: " . $resultsBucket . $key . PHP_EOL;
			}
		}
	}
}

$results = TestResultsArchive::find();
$deletedArchivedResults = [];
if (!empty($results)) {
	$statistics['deletedResultsCount'] = 0;
	$statistics['deletedArchivedResultsPackagesCount'] = 0;

	foreach ($results as $result) {
		// Delete Object from AWS S3.
		if (!in_array($result->parentTestSessionId, $deletedArchivedResults)) {
			$deletedArchivedResults[] = $result->parentTestSessionId;
			$key = $config->s3->resultsArchivePrefix . '/' . $result->parentTestSessionId . '.zip';

			try {
				$s3->deleteObject($resultsArchiveBucket, $key);
				++$statistics['deletedArchivedResultsPackagesCount'];
				echo "Successfully Deleted: {$key}" . PHP_EOL;

			} catch (S3Exception $ex) {
				echo "[AWS] Unable to Delete: " . $resultsArchiveBucket . $key . PHP_EOL;
			}
		}
	}
}

$statistics['deleteResultArchiveNotFoundInDatabaseCount'] = 0;
try {
	$keys = $s3->getObjectList(
		$resultsArchiveBucket,
		$config->s3->resultsArchivePrefix . '/',
		$config->s3->resultsArchivePrefix . '/'
	);
} catch (S3Exception $exception) {
	// Do nothing.
}

$keys = array_keys($keys);
$keys = array_reverse($keys);

foreach ($keys as $key) {
	try {
		$s3->deleteObject($resultsArchiveBucket, $key);
		++$statistics['deleteResultArchiveNotFoundInDatabaseCount'];
		echo "Successfully Deleted: {$key}" . PHP_EOL;

	} catch (S3Exception $exception) {
		echo "[AWS] Unable to Delete: " . $resultsArchiveBucket . $key . PHP_EOL;
	}
}


echo "[Step 9] Displaying S3 file removal statistics.";

echo PHP_EOL . PHP_EOL;

var_dump(json_encode($statistics));

echo PHP_EOL;
echo memoryPeakUsage(true);


/**
 * This part Truncates Tables in a Database.
 *
 * How to use:
 * Run this script from the command line and pass it the Path to the Phalcon Configuration File. Any Additional
 * Parameters will be added to the list of Table Names to exclude from Truncation. The 'system' and 'user' Tables are
 * always excluded. This script will pull the Database Settings and Credentials from the config file's 'database'
 * Property. All non-excluded Tables will then be Truncated.
 *
 * @example:
 *     php truncate.php path/to/config/file.php excluded_table
 * ALL Tables except 'system', 'user' and 'excluded_table' will be Truncated!
 *
 * @author: Kendall Parks <kendall.parks@breaktech.com>
 */

echo "[Step 10] Start of Database Clean Up." . PHP_EOL;

foreach (['database', 'database2'] as $db) {
	// Validate Configuration Object.
	if (array_key_exists($db, $config)) {
		$requiredProperties = ['host', 'dbname', 'username', 'password'];

		foreach ($requiredProperties as $property) {
			if (!array_key_exists($property, $config->$db)) {
				exit("Cannot find property {$property} in Config File!");
			}
		}
	}

	$host = $config->$db->host;
	$dbname = $config->$db->dbname;

	// Connect to Database.
	try {
		$pdo = new PDO("mysql:dbname=$dbname;host=$host", $config->$db->username, $config->$db->password);

	} catch (Exception $exception) {
		echo "Database Connection Failed: " . $exception->getMessage();
	}

	// Get Table Names.
	$statement = $pdo->prepare("SELECT table_name FROM information_schema.tables WHERE table_schema = ?;");
	$statement->execute([$dbname]);
	$tables = $statement->fetchAll();

	// Begin Truncating Tables.
	$pdo->beginTransaction();

	$doNotTouchTables = $defaultParameters['doNotTouchTables'];
	$recordsToKeep = $defaultParameters['recordsToKeep'];

	foreach ($tables as $table) {
		$table = $table['table_name'];
		$fullName = "$dbname.$table";

		if (!in_array($table, $doNotTouchTables)) {
			$InsertQueries = [];
			$threshold = 0;
			if (in_array($table, array_keys($recordsToKeep))) {
				$threshold = $recordsToKeep[$table];
			}

			if ($threshold >= 0) {
				// Generic processing (Truncate and put Record back).
				$res = $pdo->query("SHOW KEYS FROM {$table} WHERE Key_name = 'PRIMARY'");
				$details = $res->fetch(PDO::FETCH_ASSOC);
				$id = $details['Column_name'];

				$statement = $pdo->query("SELECT * FROM `{$table}` WHERE {$id} <= {$threshold};");

				// some tables does not have any primary key (request_queue_history)
				if ($statement) {
					$records = $statement->fetchAll(PDO::FETCH_ASSOC);
					if ($records) {
						foreach ($records as $record) {
							/** @noinspection PhpUnusedParameterInspection */
							$values = "'" . implode("', '", array_filter($record, function ($key, $value) {
								return !is_null($key);
							}, ARRAY_FILTER_USE_BOTH)) . "'";

							/** @noinspection PhpUnusedParameterInspection */
							$columns = "`" . implode("`, `", array_keys(array_filter($record, function ($key, $value) {
								return !is_null($key);
							}, ARRAY_FILTER_USE_BOTH))) . '`';

							$InsertQueries[] = "INSERT INTO {$table} ({$columns}) VALUES ({$values});";
						}
					}
				}
			}

			echo "Truncating '{$fullName}'." . PHP_EOL;
			$query = "DELETE FROM `{$table}` ";

			$result = $pdo->exec($query);
			if ($result === false) {
				echo "Failed Truncating '{$fullName}'." . PHP_EOL;
				rollback($pdo);
			}

			// Put the Records back, if any!
			if (count($InsertQueries) > 0) {
				echo "Re-inserting Data to '{$table}'" . PHP_EOL;
				foreach ($InsertQueries as $insertQuery) {
					$insertResult = $pdo->exec($insertQuery);
					if ($insertResult === false || $insertResult === 0) {
						rollback($pdo);
					}
				}
			}
		}
	}
	$pdo->commit();
}

echo "[Step 11] End of Database Clean Up." . PHP_EOL;
echo memoryPeakUsage(true);
