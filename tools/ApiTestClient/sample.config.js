/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * API Test Client is used for testing web services and should be disabled in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author bojan.vulevic@breaktech.com (Bojan Vulevic)
 */


/**
 * Initialize API Test Client Configuration (User Credentials).
 *
 * This should NOT be used in Production!!!
 */
/*global $ */
$(document).ready(function() {
	'use strict';

	// Admin.
	$('#adminOperation').val(''); // @sample: 'cache/clear'.

	// Authentication for Login, Content and Results (Test Delivery).
	$('#username').val(''); // @sample: 'TD'.
	$('#password').val(''); // @sample: '3fb7521307800b7d363e75dae529bc9a'.
	$('#secret').val(''); // @sample: 'd56c60a81b9f2ae075c9a652a66c3823'.

	// Login.
	$('#testKey').val(''); // @sample: 'ABCDEFGHIJ'.
	$('#dateOfBirth').val(''); // @sample: '2000-01-01'.
	$('#studentId').val(''); // @sample: 'ABCD1234567890'.
	$('#state').val(''); // @sample: 'IL'.

	// Results.
	$('#testStatus').val(''); // @sample: 'pause'.

	// Assessment.
	$('#assessmentTestKey').val(''); // @sample: 'PRACTXXXXX'.

	// Test.
	$('#testUsername').val(''); // @sample: 'TD'.
	$('#testPassword').val(''); // @sample: '3fb7521307800b7d363e75dae529bc9a'.
	$('#testSecret').val(''); // @sample: 'd56c60a81b9f2ae075c9a652a66c3823'.
	$('#testFormRevisionId').val(''); // @sample: 'i123456789012'.
	$('#publishedBy').val(''); // @sample: 'John Smith'.
	$('#adpTestFormRevisionId').val(); // @sample: 1.
	$('#active').val(''); // @sample: 'true'.

	// Test Properties.
	$('#adpTestId').val(); // @sample: 1.
	$('#batteryId').val(''); // @sample: 'i123456789012'.
	$('#name').val(''); // @sample: 'Math Test'.
	$('#program').val(''); // @sample: 'Diagnostic Assessment'.
	$('#scoreReport').val(''); // @sample: 'Generic'.
	$('#subject').val(''); // @sample: 'Math'.
	$('#grade').val(''); // @sample: '11'.
	$('#itemSelectionAlgorithm').val(''); // @sample: 'Fixed'.
	$('#security').val(''); // @sample: 'Non-Secure'.
	$('#multimedia').val(''); // @sample: 'OnDemand'.
	$('#scoring').val(''); // @sample: 'Immediate'.
	$('#permissions').val(''); // @sample: 'Non-Restricted'.
	$('#privacy').val(''); // @sample: 'Public'.
	$('#description').val(''); // @sample: 'Sample Description.'.
	$('#isActive').val(''); // @sample: 'true'.

	// Assignment.
	$('#assignmentUsername').val(''); // @sample: 'TD'.
	$('#assignmentPassword').val(''); // @sample: '3fb7521307800b7d363e75dae529bc9a'.
	$('#assignmentSecret').val(''); // @sample: 'd56c60a81b9f2ae075c9a652a66c3823'.
	$('#assignmentTestAssignmentId').val(); // @sample: 1.
	$('#assignmentAdpTestAssignmentId').val(); // @sample: 1.
	$('#assignmentTestId').val(); // @sample: 1.
	$('#assignmentTestFormId').val(); // @sample: 1.
	$('#assignmentTestKey').val(''); // @sample: 'ABCDEFGHIJ'.
	$('#assignmentTestStatus').val(''); // @sample: 'Scheduled'.
	$('#assignmentEnableLineReader').val(''); // @sample: 'true'.
	$('#assignmentEnableTextToSpeech').val(''); // @sample: 'true'.
	$('#assignmentStudentId').val(); // @sample: 1.
	$('#assignmentPersonalId').val(''); // @sample: 'ABCD1234567890'.
	$('#assignmentFirstName').val(''); // @sample: 'John'.
	$('#assignmentLastName').val(''); // @sample: 'Smith'.
	$('#assignmentDateOfBirth').val(''); // @sample: '2000-01-01'.
	$('#assignmentState').val(''); // @sample: 'IL'.
	$('#assignmentSchoolName').val(''); // @sample: 'Evanston High School'.
	$('#assignmentGrade').val(''); // @sample: '1'.

	// Results Details.
	$('#resultsUsername').val(''); // @sample: 'TD'.
	$('#resultsPassword').val(''); // @sample: '3fb7521307800b7d363e75dae529bc9a'.
	$('#resultsSecret').val(''); // @sample: 'd56c60a81b9f2ae075c9a652a66c3823'.
	$('#resultsAdpTestAssignmentId').val(); // @sample: 1.
});
