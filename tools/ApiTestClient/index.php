<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * API Test Client is used for testing web services and should be disabled in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */


?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="author" content="Bojan Vulevic; Breakthrough Technologies, LLC © 2015"/>
<title>ADP API Test Client</title>
<link rel="icon" type="image/x-icon" href="favicon.ico"/>
<style type="text/css"><!--
body {font-family:Helvetica,Arial,sans-serif;font-size:13px}
fieldset {margin-bottom:5px;/*noinspection CssOverwrittenProperties*/padding:0;
          (;/*noinspection CssOverwrittenProperties*/padding:4px;);border:1px solid #ccc;border-radius:5px}
legend {margin:5px;padding:0 3px;color:#c00;font-weight:700;cursor:pointer}
div {display:none;margin:0 10px}
.wrapper {display:block;margin:0 10px 10px 180px}
label, b {width:170px;float:left;position:relative;top:2px;font-size:13px}
b {top:5px}
p {font-weight:700}
div p {margin-top:0}
label, input, select {margin:0 10px 10px 0}
input, button, select, pre, textarea {font-size:12px;line-height:12px;padding:3px;border:1px solid #ccc;
                                      border-radius:5px}
input:focus, select:focus, textarea:focus {padding:2px;background-color:#f2f8fb;border:2px solid #8bd;outline:0}
label.checkbox {width:0}
input[type="checkbox"] {display:none}
input[type="checkbox"] + label {display:inline-block;position:relative;left:2px;padding:7px;background-color:#eee;
                                border:1px solid #ccc;border-radius:3px;cursor:pointer}
input[type="checkbox"]:checked + label {color:#c00;background-color:#eee;border:1px solid #ccc}
input[type="checkbox"]:checked + label:after {position:absolute;top:-2px;left:2px;font-size:13px;content:'\2714'}
input[type="checkbox"] + label:focus, input[type="checkbox"]:checked + label:focus {padding:6px;border:2px solid #8bd;
                                                                                    background-color:#f2f8fb;outline:0}
input[type="checkbox"]:checked + label:focus:after {left:1px;font-size:12px}
input {width:230px}
input[type="checkbox"], label[for="healthCheck"], select {cursor:pointer}
textarea {width:100%;height:80px}
button {margin:0 0 10px 10px;padding:5px;color:#fff;background-color:#c00;outline:0;font-weight:700;cursor:pointer}
button:focus, button:hover {background-color:#900;outline:0}
#image, #audio, #video, #html {max-width:100%}
#html {width:100%;height:100%;border:0}
@-moz-document url-prefix() {#image, #audio, #video {margin-bottom:5px}}
.response {min-height: 20px}
pre {display:table;table-layout:fixed;width:100%;margin:0;padding:0 0 7px;border:0;word-wrap:break-word}
label.required:after {color:#c00;content:'*'}
.string {color:green}
.number {color:darkorange}
.boolean {color:blue}
.key {color:red}
.null {color:magenta}
//--></style>
<script type="text/javascript"><!--
	var serverTime = <?=time()?>;
//--></script>
<script type="text/javascript" src="jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="jquery.sha256.min.js"></script>
<script type="text/javascript" src="client.js"></script>
<script type="text/javascript" src="config.js"></script>
</head>
<body>
<p>Common:</p>
<fieldset>
	<legend>Admin:</legend>
	<div>
		<label for="adminOperation" class="required">Admin Operation:</label>
		<select id="adminOperation" required onkeypress="enter(event, admin, 'GET')">
			<option value="">&nbsp;</option>
			<option value="cache/clear">Clear Cache</option>
			<option value="maintenance/status">Maintenance Status</option>
			<option value="maintenance/enable">Maintenance Enable</option>
			<option value="maintenance/disable">Maintenance Disable</option>
			<option value="install">Install Application</option>
			<option value="update">Update Application</option>
		</select><br/>
		<b>Ping:</b>
		<button type="button" onclick="admin('GET')">GET</button>
		<button type="button" onclick="admin('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset();$('#adminOperation').val('')">Clear</button>
	</div>
</fieldset>
<fieldset>
	<legend>Ping:</legend>
	<div>
		<label for="healthCheck">Health Check:</label>
		<input type="checkbox" id="healthCheck"/>
		<label for="healthCheck" class="checkbox" tabindex="0" onkeypress="enter(event, ping, 'GET')"></label><br/><br/>
		<b>Ping:</b>
		<button type="button" onclick="ping('GET')">GET</button>
		<button type="button" onclick="ping('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset();$('#healthCheck').attr('checked', false)">Clear</button>
	</div>
</fieldset>
<p>Test Delivery:</p>
<fieldset>
	<legend>Authentication:</legend>
	<div>
		<p>Required for: Login &amp; Results.</p>
		<label for="username" class="required">Username:</label>
		<input id="username" pattern="^[0-9a-zA-Z]{1,20}$" placeholder="TD" required/><br/>
		<label for="password" class="required">Password:</label>
		<input id="password" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/><br/>
		<label for="secret" class="required">Secret:</label>
		<input id="secret" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/>
		<p>Required for: Content &amp; Results.</p>
		<label for="token" class="required">Token:</label>
		<input id="token" pattern="^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$" required
		       placeholder="12345678-abcd-abcd-abcd-abcdefghijkl"/><br/>
		<label for="testId" class="required">Test ID:</label>
		<input id="testId" pattern="^\d{1,10}$" placeholder="12345" required/><br/>
	</div>
</fieldset>
<fieldset>
	<legend>Login:</legend>
	<div>
		<label for="testKey" class="required">Test Key:</label>
		<input id="testKey" pattern="^[A-Z]{10}$" placeholder="ABCDEFGHIJ" required
		       onkeypress="enter(event, login, 'POST')"/><br/>
		<label for="dateOfBirth" class="required">Date Of Birth:</label>
		<input id="dateOfBirth" pattern="^(199\d|20\d\d)-(0\d|10|11|12)-(0\d|1\d|2\d|30|31)$" placeholder="2000-01-01"
		       onkeypress="enter(event, login, 'POST')"/>
		<br/>
		<label for="studentId" class="required">Student ID:</label>
		<input id="studentId" pattern="^[0-9a-zA-Z]{0,30}$" placeholder="1234567890abcdefghijklmnopqrst"
		       onkeypress="enter(event, login, 'POST')"/><br/>
		<label for="state" class="required">State:</label>
		<input id="state" pattern="^((A[LKZR])|(C[AOT])|(D[ECD])|(FL)|(GA)|(HI)|(I[DLNA])|(K[SY])|(LA)|(M[EDAINSOT])|\
		       (N[EVHJMYCD])|(O[HKR])|(P[AT])|(RI)|(S[CD])|(T[NX])|(UT)|(V[TA])|(W[AVIY]))$" placeholder="IL" required
		       onkeypress="enter(event, login, 'POST')"/>
		<br/>
		<b>Login:</b>
		<button type="button" onclick="login('POST')">POST</button>
		<button type="button" onclick="login('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset();$('#token').val('');$('#testId').val('')">Clear</button>
	</div>
</fieldset>
<fieldset>
	<legend>Content:</legend>
	<div>
		<label for="contentFile" class="required">Content File:</label>
		<input id="contentFile" pattern="^[^\|\*\?\\:<>/$\x22]*[^\.\|\*\?\\:<>/$\x22]+$" placeholder="test.json"
		       onkeypress="enter(event, content, 'GET')"/><br/>
		<b>Content:</b>
		<button type="button" onclick="content('GET')">GET</button>
		<button type="button" onclick="content('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset();$('#contentFile').val('')">Clear</button>
	</div>
</fieldset>
<fieldset>
	<legend>Results:</legend>
	<div>
		<label for="resultsFile" class="required">Results:</label>
		<div class="wrapper">
			<textarea id="resultsFile" placeholder="{}"></textarea>
		</div>
		<label for="testStatus" class="required">Test Status:</label>
		<select id="testStatus" onkeypress="enter(event, results, 'POST')">
			<option value="invalid">&nbsp;</option>
			<option value="">InProgress</option>
			<option value="pause">Paused</option>
			<option value="submit">Submitted</option>
		</select><br/>
		<b>Results:</b>
		<button type="button" onclick="results('GET')">GET</button>
		<button type="button" onclick="results('POST')">POST</button>
		<button type="button" onclick="results('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset();$('#resultsFile').val('');$('#testStatus').val('invalid')">Clear</button>
	</div>
</fieldset>
<fieldset>
	<legend>Assessment:</legend>
	<div>
		<label for="assessmentTestKey" class="required">Test Key:</label>
		<input id="assessmentTestKey" pattern="^[A-Z]{10}$" placeholder="PRACTXXXXX" required
		       onkeypress="enter(event, assessment, 'POST')"/><br/>
		<label for="assessmentName" class="required">Name:</label>
		<input id="assessmentName" pattern="^$" onkeypress="enter(event, assessment, 'POST')"/><br/>
		<b>Assessment:</b>
		<button type="button" onclick="assessment('GET')">GET</button>
		<button type="button" onclick="assessment('POST')">POST</button>
		<button type="button" onclick="assessment('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset()">Clear</button>
	</div>
</fieldset>
<p>Publisher:</p>
<fieldset>
	<legend>Test:</legend>
	<div>
		<p>Authentication:</p>
		<label for="testUsername" class="required">Username:</label>
		<input id="testUsername" pattern="^[0-9a-zA-Z]{1,20}$" placeholder="ACR" required/><br/>
		<label for="testPassword" class="required">Password:</label>
		<input id="testPassword" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/><br/>
		<label for="testSecret" class="required">Secret:</label>
		<input id="testSecret" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/>
		<p>Test:</p>
		<label for="testFormRevisionId" class="required">Test Form Revision ID:</label>
		<input id="testFormRevisionId" pattern="^i[0-9]{13,25}$" placeholder="i123456789012" required/><br/>
		<label for="publishedBy" class="required">Published By:</label>
		<input id="publishedBy" pattern="^.{1,100}$" placeholder="John Smith" required/><br/>
		<label for="adpTestFormRevisionId">ADP Test Form Revision ID:</label>
		<input id="adpTestFormRevisionId" pattern="^[0-9]{1,10}$" placeholder="12345" required/><br/>
		<label for="active">Test Status:</label>
		<select id="active">
			<option>&nbsp;</option>
			<option value="true">Active</option>
			<option value="false">Inactive</option>
		</select><br/>
		<b>Test:</b>
		<button type="button" onclick="test('POST')">POST</button>
		<button type="button" onclick="test('PUT')">PUT</button>
		<button type="button" onclick="test('PATCH')">PATCH</button>
		<button type="button" onclick="test('DELETE')">DELETE</button>
		<button type="button" onclick="test('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset();$('#active').val('')">Clear</button><br/><br/>
		<p>Test Properties:</p>
		<label for="adpTestId" class="required">ADP Test Battery ID: </label>
		<input id="adpTestId" pattern="^[0-9]{1,10}$" placeholder="12345" required/><br/>
		<label for="batteryId" class="required">Test Battery ID:</label>
		<input id="batteryId" pattern="^i[0-9]{13,25}$" placeholder="i123456789012" required/><br/>
		<label for="name">Test Battery Name:</label>
		<input id="name" pattern="^.{1,100}$" placeholder="Math Test"/><br/>
		<label for="program">Test Battery Program:</label>
		<select id="program">
			<option>&nbsp;</option>
			<option value="Diagnostic Assessment">Diagnostic Assessment</option>
			<option value="K2 Formative">K2 Formative</option>
			<option value="Mid-Year/Interim">Mid-Year/Interim</option>
			<option value="Practice Test">Practice Test</option>
			<option value="Quiz Test">Quiz Test</option>
			<option value="Speaking & Listening">Speaking & Listening</option>
		</select><br/>
		<label for="scoreReport">Test Battery Score Report:</label>
		<select id="scoreReport">
			<option>&nbsp;</option>
			<option value="None">None</option>
			<option value="Generic">Generic</option>
			<option value="ELA Decoding">ELA Decoding</option>
			<option value="ELA Reader Motivation Survey">ELA Reader Motivation Survey</option>
			<option value="ELA Reader Comprehension">ELA Reader Comprehension</option>
			<option value="ELA Vocabulary">ELA Vocabulary</option>
			<option value="Math Comprehension">Math Comprehension</option>
			<option value="Math Fluency">Math Fluency</option>
		</select><br/>
		<label for="subject">Test Battery Subject:</label>
		<select id="subject">
			<option>&nbsp;</option>
			<option value="ELA">ELA</option>
			<option value="Math">Math</option>
			<option value="Science">Science</option>
			<option value="N/A">N/A</option>
		</select><br/>
		<label for="grade">Test Battery Grade:</label>
		<select id="grade">
			<option>&nbsp;</option>
			<option value="K">K</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="Multi-level">Multi-level</option>
		</select><br/>
		<label for="itemSelectionAlgorithm">Item Selection Algorithm:</label>
		<select id="itemSelectionAlgorithm">
			<option>&nbsp;</option>
			<option value="None">None</option>
			<option value="Fixed">Fixed</option>
			<option value="Adaptive">Adaptive</option>
		</select><br/>
		<label for="security">Test Battery Security:</label>
		<select id="security">
			<option>&nbsp;</option>
			<option value="Non-Secure">Non-Secure</option>
			<option value="Secure">Secure</option>
		</select><br/>
		<label for="multimedia">Test Battery Multimedia:</label>
		<select id="multimedia">
			<option>&nbsp;</option>
			<option value="OnDemand">OnDemand</option>
			<option value="PreDownload">PreDownload</option>
			<option value="Embedded">Embedded</option>
		</select><br/>
		<label for="scoring">Test Battery Scoring:</label>
		<select id="scoring">
			<option>&nbsp;</option>
			<option value="Immediate">Immediate</option>
			<option value="Delayed">Delayed</option>
		</select><br/>
		<label for="permissions">Test Battery Permissions:</label>
		<select id="permissions">
			<option>&nbsp;</option>
			<option value="Non-Restricted">Non-Restricted</option>
			<option value="Restricted">Restricted</option>
		</select><br/>
		<label for="privacy">Test Battery Privacy:</label>
		<select id="privacy">
			<option>&nbsp;</option>
			<option value="Public">Public</option>
			<option value="Private">Private</option>
		</select><br/>
		<label for="description">Test Battery Description:</label>
		<div class="wrapper">
			<textarea id="description" placeholder="Test description..."></textarea><br/>
		</div>
		<label for="isActive">Test Battery is Active:</label>
		<select id="isActive">
			<option>&nbsp;</option>
			<option value="true">Active</option>
			<option value="false">Inactive</option>
		</select><br/>
		<b>Test Properties:</b>
		<button type="button" onclick="testProperties('PATCH')">PATCH</button>
		<button type="button" onclick="testProperties('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset()">Clear</button><br/>
	</div>
</fieldset>
<fieldset>
	<legend>Assignment:</legend>
	<div>
		<p>Authentication:</p>
		<label for="assignmentUsername" class="required">Username:</label>
		<input id="assignmentUsername" pattern="^[0-9a-zA-Z]{1,20}$" placeholder="LDR" required/><br/>
		<label for="assignmentPassword" class="required">Password:</label>
		<input id="assignmentPassword" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv"
		       required/><br/>
		<label for="assignmentSecret" class="required">Secret:</label>
		<input id="assignmentSecret" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/>
		<p>Assignment Details:</p>
		<label for="assignmentTestAssignmentId" class="required">Test Assignment ID:</label>
		<input id="assignmentTestAssignmentId" pattern="^[0-9]{1,10}$" placeholder="12345" required/><br/>
		<label for="assignmentAdpTestAssignmentId">ADP Test Assignment ID:</label>
		<input id="assignmentAdpTestAssignmentId" pattern="^[0-9]{1,10}$" placeholder="12345"/>
		<p>Test Details:</p>
		<label for="assignmentTestId">Test ID:</label>
		<input id="assignmentTestId" pattern="^[0-9]{1,10}$" placeholder="12345"/><br/>
		<label for="assignmentTestFormId">Test Form ID:</label>
		<input id="assignmentTestFormId" pattern="^[0-9]{1,10}$" placeholder="12345"/><br/>
		<label for="assignmentTestKey">Test Key:</label>
		<input id="assignmentTestKey" pattern="^[A-Z]{10}$" placeholder="ABCDEFGHIJ"/><br/>
		<label for="assignmentTestStatus">Test Status:</label>
		<select id="assignmentTestStatus">
			<option>&nbsp;</option>
			<option>Scheduled</option>
			<option>InProgress</option>
			<option>Paused</option>
			<option>Submitted</option>
			<option>Completed</option>
			<option>Canceled</option>
		</select><br/>
		<label for="assignmentEnableLineReader">Enable Line Reader:</label>
		<select id="assignmentEnableLineReader">
			<option>&nbsp;</option>
			<option value="true">Enabled</option>
			<option value="false">Disabled</option>
		</select><br/>
		<label for="assignmentEnableTextToSpeech">Enable Text-to-Speech:</label>
		<select id="assignmentEnableTextToSpeech">
			<option>&nbsp;</option>
			<option value="true">Enabled</option>
			<option value="false">Disabled</option>
		</select><br/>
		<p>Student Details:</p>
		<label for="assignmentStudentId">Student ID:</label>
		<input id="assignmentStudentId" pattern="^[0-9]{1,10}$" placeholder="12345"/><br/>
		<label for="assignmentPersonalId">Personal ID:</label>
		<input id="assignmentPersonalId" pattern="^[0-9a-zA-Z]{0,30}$" placeholder="1234567890abcdefghijklmnopqrst"/>
		<br/>
		<label for="assignmentFirstName">First Name:</label>
		<input id="assignmentFirstName" pattern="^.{1,35}$" placeholder="John"/><br/>
		<label for="assignmentLastName">Last Name:</label>
		<input id="assignmentLastName" pattern="^.{1,35}$" placeholder="Smith"/><br/>
		<label for="assignmentDateOfBirth">Date of Birth:</label>
		<input id="assignmentDateOfBirth" pattern="^(199\d|20\d\d)-(0\d|10|11|12)-(0\d|1\d|2\d|30|31)$"
		       placeholder="2000-01-01"/><br/>
		<label for="assignmentState">State:</label>
		<input id="assignmentState" pattern="^((A[LKZR])|(C[AOT])|(D[ECD])|(FL)|(GA)|(HI)|(I[DLNA])|(K[SY])|(LA)|\
		       (M[EDAINSOT])|(N[EVHJMYCD])|(O[HKR])|(P[AT])|(RI)|(S[CD])|(T[NX])|(UT)|(V[TA])|(W[AVIY]))$"
		       placeholder="IL"/><br/>
		<label for="assignmentSchoolName">School Name:</label>
		<input id="assignmentSchoolName" pattern="^.{1,60}$" placeholder="Evanston Elementary"/><br/>
		<label for="assignmentGrade">Grade:</label>
		<select id="assignmentGrade">
			<option>&nbsp;</option>
			<option value="K">K</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
		</select><br/>
		<b>Assignment:</b>
		<button type="button" onclick="assignment('POST')">POST</button>
		<button type="button" onclick="assignment('PATCH')">PATCH</button>
		<button type="button" onclick="assignment('DELETE')">DELETE</button>
		<button type="button" onclick="assignment('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset()">Clear</button>
	</div>
</fieldset>
<fieldset>
	<legend>Results Details:</legend>
	<div>
		<p>Authentication:</p>
		<label for="resultsUsername" class="required">Username:</label>
		<input id="resultsUsername" pattern="^[0-9a-zA-Z]{1,20}$" placeholder="LDR" required/><br/>
		<label for="resultsPassword" class="required">Password:</label>
		<input id="resultsPassword" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/>
		<br/>
		<label for="resultsSecret" class="required">Secret:</label>
		<input id="resultsSecret" pattern="^[0-9a-z]{32}$" placeholder="1234567890abcdefghijklmnopqrstuv" required/>
		<p>Assignment Details:</p>
		<label for="resultsAdpTestAssignmentId" class="required">ADP Test Assignment ID:</label>
		<input id="resultsAdpTestAssignmentId" pattern="^[0-9]{1,10}$" placeholder="12345" required
		       onkeypress="enter(event, resultsDetails, 'GET')"/><br/>
		<b>Results:</b>
		<button type="button" onclick="resultsDetails('GET')">GET</button>
		<button type="button" onclick="resultsDetails('OPTIONS')">OPTIONS</button>
		<button type="button" onclick="reset()">Clear</button>
	</div>
</fieldset>
<p>Response:</p>
<fieldset>
	<legend>Response Headers:</legend>
	<div class="response">
		<pre id="responseHeaders"></pre>
	</div>
</fieldset>
<fieldset>
	<legend>Response Body:</legend>
	<div class="response">
		<img id="image" src="#" alt=""/>
		<audio id="audio" preload="auto" controls></audio>
		<video id="video" controls></video>
		<iframe id="html" width="0" height="0"></iframe>
		<pre id="responseBody"></pre>
	</div>
</fieldset>
</body>
</html>
