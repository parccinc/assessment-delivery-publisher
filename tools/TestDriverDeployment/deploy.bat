@echo off
cls
title PARCC-ADP Test Driver Deployment
echo Deploying ADP Test Driver to AWS S3...
echo.
php testDriverDeployment.php
echo.
echo.
pause
