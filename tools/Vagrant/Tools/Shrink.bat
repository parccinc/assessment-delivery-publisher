@echo off
cls
title Compacting free space on PARCC-Vagrant Virtual Machine

:: Shrink all VMDK disk images.
for /r %%i in (*.vmdk) do (
	echo Shrinking: "%%i"
	echo.
	VDiskManager -k "%%i"
	echo.
)

:: Shrink all VDI disk images.
for /r %%i in (*.vdi) do (
	echo Shrinking: "%%i"
	echo.
	VBoxManage modifyhd "%%i" --compact
	echo.
)

pause
