<?php
/**
 * @apiDefine ACR_PRC User Authentication includes:
 *                    <ol>
 *                      <li>Basic Authentication</li>
 *                      <li>HMAC Authentication</li>
 *                    </ol>
 */
/**
 * @apiGroup ADP Publisher
 * @api {POST} /api/test Test Publishing
 * @apiName Test Publishing
 * @apiPermission ACR_PRC
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>ACR</b> and <b>PRC</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates
 *                 multiple parameters before it allows client to login into ADP Publisher application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 For Practice Tests and Quizzes, it also automatically generates Test Assignment with Test Key and
 *                 Token.<br/>
 *                 Once Test Publishing Request is received, if correct, ADP Publisher will create a Request Job in its
 *                 Job Queue and will process it as soon as possible depending on its priority, and number and priority
 *                 of other pending Request Jobs in its Job Queue.<br/><br/>
 *                 In case of successful request, it returns a Request Job ID which is required in case Requested Job
 *                 needs to be canceled. It also returns Date and Time when that Request Job was created and current
 *                 Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started
 *                 yet.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 If subsequent request for publishing the same Test Package is sent while the previous one is still
 *                 being processed, ADP Publisher will simply return the same request as the first time and will not
 *                 create new Request Job. If that Test Package was already been successfully published, it will return
 *                 ADP Test Delivery's Test Battery Form Revision ID, Date and Time when that Test Package was
 *                 published, and its current Status.<br/><br/>
 *                 Once actual Test Publishing is completed, ADP Publisher will send back a notification with details
 *                 about the outcome of Test Publishing.<br/>
 *                 In case of successful publishing, it will provide Test Battery Form Revision ID, Date and Time when
 *                 Test Package was published. In case of Practice Test or Quiz, it will also return generated Test Key.
 *                 If successfully published Test is a Diagnostic Test, it will also notify LDR about successful
 *                 outcome, and in case of successfully published Practice Test or Quiz it will also notify PRC.<br/>
 *                 <br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Publishing specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                   <tr>
 *                      <td>ADP-1703</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case of a validation error.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1707</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error</td>
 *                      <td>Returns JSON error that will contain a custom error message.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1708</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if the combination of name, grade and subject was already
 *                          published.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1709</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case the form name already exist for the actual battery.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Publishing:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testFormRevisionId = ...<br/>
 *                   $publishedBy = ...<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/test";<br/>
 *                   $username = "DocumentationACR";<br/>
 *                   $password = "635fbb063cbc28e8bf03ed12ca4cca00";<br/>
 *                   $secret = "51f48327961f6cb0da7d584a664cafba";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>
 *                   &nbsp;  'testFormRevisionId' => $testFormRevisionId,<br/>
 *                   &nbsp;  'publishedBy' => $publishedBy<br/>
 *                   ]));<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationACR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="635fbb063cbc28e8bf03ed12ca4cca00" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="51f48327961f6cb0da7d584a664cafba" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * POST /api/test HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {String} testFormRevisionId="i123456789012" Test Battery Form Revision ID is an ID used as a reference for
 *                    Test Package being published which is unique in a system requesting publishing and is used inside
 *                    ADP only as an external reference to the source where Test Package came from.<br/>
 *                    It's 26 characters long string.
 * @apiParam {String} publishedBy="John Smith" First and/or Last Name of the Content Author initiating Test Package
 *                    publishing. If neither is available, this can also be an email or a username of the Content
 *                    Author.<br/>
 *                    It's 100 characters long string.
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "testFormRevisionId": "i123456789012",
 *     "publishedBy": "John Smith"
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was
 *                         created.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "requestId": 123,
 *     "publishedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "requestId": 123,
 *       "publishedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 * @apiSuccessExample Successful Notification:
 *   HTTP/1.1 200 OK
 *   {
 *     "testFormRevisionId": 10,
 *     "publishedTimestamp": "2015-05-05 06:06:06",
 *     "status": "Published",
 *     "testKey": "PRACT-ABCDE"
 *   }
 *
 * @apiSuccessExample Successful Notification with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 4
 *     },
 *     "result": {
 *       "testFormRevisionId": 10,
 *       "publishedTimestamp": "2015-05-05 06:06:06",
 *       "status": "Published",
 *       "testKey": "PRACT-ABCDE"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "statusCode": 403,
 *     "statusDescription": "Forbidden",
 *     "errorCode": "ADP-1703",
 *     "errorDescription": "Access Denied.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 403,
 *       "statusDescription": "Forbidden",
 *       "errorCode": "ADP-1703",
 *       "errorDescription": "Access Denied.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function testPublish()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Publisher
 * @api {PUT} /api/test/{adpTestFormRevisionId} Test Re-Publishing
 * @apiName Test Re-Publishing
 * @apiPermission ACR_PRC
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>ACR</b> and <b>PRC</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates
 *                 multiple parameters before it allows client to login into ADP Publisher application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 For Practice Tests and Quizzes, it also automatically generates Test Assignment with Test Key and
 *                 Token.<br/>
 *                 Once Test Re-Publishing Request is received, it is processed in almost identical way as Test
 *                 Publishing Request. The only difference is that the referenced Test Package already exists in ADP (or
 *                 at least some parts of it), so Re-Publishing will simply repeat the whole publishing process in order
 *                 to re-create any potentially missing data references inside ADP database and/or re-deploy any
 *                 potentially missing files.<br/><br/>
 *                 In case of successful request, it returns a Request Job ID which is required in case Requested Job
 *                 needs to be canceled. It also returns Date and Time when that Request Job was created and current
 *                 Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started
 *                 yet.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 If subsequent request for re-publishing the same Test Package is sent while the previous one is still
 *                 being processed, ADP Publisher will simply return the same request as the first time and will not
 *                 create new Request Job. If that Test Package was already been successfully re-published, it will
 *                 return ADP Test Delivery's Test Battery Form Revision ID, Date and Time when that Test Package was
 *                 updated, and its current Status.<br/><br/>
 *                 Once actual Test Re-Publishing is completed, ADP Publisher will send back a notification with details
 *                 about the outcome of Test Re-Publishing.<br/>
 *                 In case of successful re-publishing, it will provide Test Battery Form Revision ID, Date and Time
 *                 when Test Package was re-published. In case of Practice Test or Quiz, it will also return previously
 *                 generated Test Key (it will not generate a new one). If successfully re-published Test is a
 *                 Diagnostic Test, it will also notify LDR about successful outcome, and in case of successfully
 *                 re-published Practice Test or Quiz it will also notify PRC.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Re-Publishing specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                   <tr>
 *                   <tr>
 *                      <td>ADP-1703</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case of a validation error.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1707</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error</td>
 *                      <td>Returns JSON error that will contain a custom error message.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1708</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if the combination of name, grade and subject was already
 *                          published.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1709</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case the form name already exist for the actual battery.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Re-Publishing:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testFormRevisionId = ...<br/>
 *                   $publishedBy = ...<br/>
 *                   $adpTestFormRevisionId = ...; // ADP's Test Battery Form Revision ID that was provided when it was
 *                                                    published the first time.<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/test/" . $adpTestFormRevisionId;<br/>
 *                   $username = "DocumentationACR";<br/>
 *                   $password = "635fbb063cbc28e8bf03ed12ca4cca00";<br/>
 *                   $secret = "51f48327961f6cb0da7d584a664cafba";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>
 *                   &nbsp;  'testFormRevisionId' => $testFormRevisionId,<br/>
 *                   &nbsp;  'publishedBy' => $publishedBy<br/>
 *                   ]));<br/>
 *                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationACR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="635fbb063cbc28e8bf03ed12ca4cca00" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="51f48327961f6cb0da7d584a664cafba" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * PUT /api/test/10 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {String}  testFormRevisionId="i123456789012" Test Battery Form Revision ID is an ID used as a reference for
 *                     Test Package being published which is unique in a system requesting publishing and is used inside
 *                     ADP only as an external reference to the source where Test Package came from.<br/>
 *                     It's 26 characters long string.
 * @apiParam {String}  publishedBy="John Smith" First and/or Last Name of the Content Author initiating Test Package
 *                     publishing. If neither is available, this can also be an email or a username of the Content
 *                     Author.<br/>
 *                     It's 100 characters long string.
 * @apiParam {Integer} adpTestFormRevisionId="10" ADP Test Battery Form Revision ID is an unique ID assigned by ADP when
 *                     the Test Package was originally published. As multiple systems can publish their own Test
 *                     Packages, ADP has to use its own IDs in order to ensure they are unique.<br/>
 *                     It's 32-bit unsigned unique integer.
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "testFormRevisionId": "i123456789012",
 *     "publishedBy": "John Smith"
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was
 *                         created.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "requestId": 456,
 *     "publishedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "requestId": 456,
 *       "publishedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 * @apiSuccessExample Successful Notification:
 *   HTTP/1.1 200 OK
 *   {
 *     "testFormRevisionId": 10,
 *     "publishedTimestamp": "2015-05-05 06:06:06",
 *     "status": "Published",
 *     "testKey": "PRACT-ABCDE"
 *   }
 *
 * @apiSuccessExample Successful Notification with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 4
 *     },
 *     "result": {
 *       "testFormRevisionId": 10,
 *       "publishedTimestamp": "2015-05-05 06:06:06",
 *       "status": "Published",
 *       "testKey": "PRACT-ABCDE"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "statusCode": 403,
 *     "statusDescription": "Forbidden",
 *     "errorCode": "ADP-1703",
 *     "errorDescription": "Access Denied.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 403,
 *       "statusDescription": "Forbidden",
 *       "errorCode": "ADP-1703",
 *       "errorDescription": "Access Denied.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function testRePublish()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Publisher
 * @api {PATCH} /api/test/properties/{adpTestId} Test Properties Update
 * @apiName Test Properties Update
 * @apiPermission ACR_PRC
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>ACR</b> and <b>PRC</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates
 *                 multiple parameters before it allows client to login into ADP Publisher application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 Once Test is Published, it is automatically made Active (Online) which means that Test Assignments
 *                 can be created, if the Test's permission is set to 'Restricted' the Test will not be viewable and
 *                 available to teachers and will only be available to the content team, once content team check and
 *                 make sure everything look fine, then they update that permission value to 'Non-Restricted' which
 *                 makes the test available for teachers.<br/>
 *                 Similar to publishing, a Request Job for Test Properties Update is created first, and is processed as
 *                 soon as possible according to its priority and other pending Jobs in the ADP Publisher's Job
 *                 Queue.<br/><br/>
 *                 In case of successful request, it returns a Request Job ID which is required in case Requested Job
 *                 needs to be canceled. It also returns Date and Time when that Request Job was created and current
 *                 Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started
 *                 yet.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 If subsequent request for patching the same Test is sent while the previous one is still being
 *                 processed, ADP Publisher will simply return the same request as the first time and will not create
 *                 new Request Job.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 This web service could be extended to update the Test Battery Form and the Test Battery Form Revision
 *                 properties in the future.<br/><br/>
 *                 These are possible Test Properties Update specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                   <tr>
 *                      <td>ADP-1760</td>
 *                      <td>Invalid Request.</td>
 *                      <td>412</td>
 *                      <td>Precondition Failed</td>
 *                      <td>Returns JSON error in case if the data sent to the server is not a valid JSON object.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1761</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if the batteryId property is missing.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1762</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if the batteryId property is missing.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1763</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case if battery is not found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1764</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if isActive property has an invalid value.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1765</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case If one of the not allowed properties is being updated./td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1766</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if a value is not in the expected range.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1767</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case Battery name or description does not meet the validation
 *                          requirements.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1769</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error</td>
 *                      <td>Returns JSON error in case the system is not able to send the new test details to LDR.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Activate/Deactivate:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $adpTestId = ...<br/>
 *                   $batteryId = ...<br/>
 *                   $name = ...<br/>
 *                   $program = ...<br/>
 *                   $scoreReport = ...<br/>
 *                   $subject = ...<br/>
 *                   $grade = ...<br/>
 *                   $itemSelectionAlgorithm = ...<br/>
 *                   $security = ...<br/>
 *                   $multimedia = ...<br/>
 *                   $scoring = ...<br/>
 *                   $permissions = ...<br/>
 *                   $privacy = ...<br/>
 *                   $description = ...<br/>
 *                   $isActive = ...<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/test/properties/" . $adpTestId;<br/>
 *                   $username = "DocumentationACR";<br/>
 *                   $password = "635fbb063cbc28e8bf03ed12ca4cca00";<br/>
 *                   $secret = "51f48327961f6cb0da7d584a664cafba";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>
 *                   &nbsp;  'batteryId' => $batteryId,<br/>
 *                   &nbsp;  'name' => $name,<br/>
 *                   &nbsp;  'program' => $program,<br/>
 *                   &nbsp;  'scoreReport' => $scoreReport,<br/>
 *                   &nbsp;  'subject' => $subject,<br/>
 *                   &nbsp;  'grade' => $grade,<br/>
 *                   &nbsp;  'itemSelectionAlgorithm' => $itemSelectionAlgorithm,<br/>
 *                   &nbsp;  'security' => $security,<br/>
 *                   &nbsp;  'multimedia' => $multimedia,<br/>
 *                   &nbsp;  'scoring' => $scoring,<br/>
 *                   &nbsp;  'permissions' => $permissions,<br/>
 *                   &nbsp;  'privacy' => $privacy,<br/>
 *                   &nbsp;  'description' => $description,<br/>
 *                   &nbsp;  'isActive' => $isActive<br/>
 *                   ]));<br/>
 *                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationACR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="635fbb063cbc28e8bf03ed12ca4cca00" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="51f48327961f6cb0da7d584a664cafba" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * PATCH /api/test/properties/10 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {Integer} adpTestId="1" ADP Test Battery ID is an unique ID assigned by ADP when the Test Package was
 *                     originally published. As multiple systems can publish their own Test Packages, ADP has to use its
 *                     own IDs in order to ensure they are unique.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  batteryId="i123456789012" Test Battery ID is an ID used as a reference for the published Test
 *                     which is unique in a system requesting publishing and is used inside ADP only as an external
 *                     reference to the source where Test came from.<br/>
 *                     It's 26 characters long string.
 * @apiParam {String}  [name="Math Test"] Test Battery's Name.<br/>
 *                     It's 100 characters long string.
 * @apiParam {String}  [program="'Diagnostic Assessment','K2 Formative','Mid-Year/Interim','Practice Test','Quiz Test','Speaking & Listening','Summative'"]
 *                     Test Battery's Program.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [scoreReport="'None','Generic','ELA Decoding','ELA Reader Motivation Survey','ELA Reader Comprehension','ELA Vocabulary','Math Comprehension','Math Fluency'"]
 *                     Test Battery's Score Report Type.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [subject="'ELA','Math','Science','N/A'"] Test Battery's Subject.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [grade="'K','1','2','3','4','5','6','7','8','9','10','11','12','Multi-level'"] Test Battery's
 *                     Grade.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [itemSelectionAlgorithm="'Fixed','Adaptive'"] Test Battery's Item Selection Algorithm.</br>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [security="'Non-Secure','Secure'"] Indicator if Secure Browser is required for delivery of all
 *                     Test Battery Form Revisions of the Test Battery. ’Non-Secure’ Test Batteries can be delivered in
 *                     any Browser, whereas ’Secure’ can only be delivered inside the Secure Browser!<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [multimedia="'OnDemand','PreDownload','Embedded'"] The Item Selection Algorithm this property is
 *                     not allowed to be updated at the moment, and updating it would trigger an error.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [scoring="'Immediate','Delayed'"] Test Battery’s Scoring Methods. ’Immediate’ means that Test
 *                     Driver will perform all required Scoring for the Test Battery. ’Delayed’ means that Test Driver
 *                     will perform as much as possible of automated Scoring, but some additional Scoring is required
 *                     after Test Delivery (this may be either Automated Delayed Scoring, Hand Scoring, or combination
 *                     of both).<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [permissions="'Non-Restricted','Restricted'"] Indicator if Test Battery requires additional
 *                     Permissions for Test Battery Form Assignments. ’Non-Restricted’ means that the Test Battery is
 *                     available to all Rostering Users and all of them can create Test Battery Form Assignments for the
 *                     related Test Battery. ’Restricted’ on the other hand requires special permissions, and only
 *                     Rostering Users with such permissions can create Test Battery Form Assignments for the related
 *                     Test Battery. This Indicator is only used by external Rostering System and does not affect System
 *                     in any way.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [privacy="'Public','Private'"] Indicator if Test Battery’s Content has special Privacy, License,
 *                     Copyright or any other restrictions. This Indicator is only used by external Rostering System and
 *                     does not affect System in any way.<br/>
 *                     It's one of the the above predefined strings.
 * @apiParam {String}  [description="Test Description..."] Test Battery’s Description.<br/>
 *                     It's 4096 characters long string.
 * @apiParam {Boolean} [isActive="true, false"] Indicates if Test Battery should be Active or Inactive (Online or
 *                     Offline).<br/>
 *                     It's boolean.
 *
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "batteryId": "i1446833225635899",
 *     "name": "Math Test",
 *     "program": "Diagnostic Assessment",
 *     "scoreReport": "Generic",
 *     "subject": "Math",
 *     "grade": "3",
 *     "itemSelectionAlgorithm": "Fixed",
 *     "security": "Non-Secure",
 *     "multimedia": "Embedded",
 *     "scoring": "Delayed",
 *     "permissions": "Non-Restricted",
 *     "privacy": "Private",
 *     "description": "This is a simple Math Test.",
 *     "isActive": true
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was
 *                         created.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "requestId": 789,
 *     "publishedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "requestId": 789,
 *       "publishedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "statusCode": 403,
 *     "statusDescription": "Forbidden",
 *     "errorCode": "ADP-1703",
 *     "errorDescription": "Access Denied.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 403,
 *       "statusDescription": "Forbidden",
 *       "errorCode": "ADP-1703",
 *       "errorDescription": "Access Denied.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function testPropertiesUpdate()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Publisher
 * @api {PATCH} /api/test/{adpTestFormRevisionId} Test Activate/Deactivate
 * @apiName Test Activate/Deactivate
 * @apiPermission ACR_PRC
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>ACR</b> and <b>PRC</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates
 *                 multiple parameters before it allows client to login into ADP Publisher application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 Once Test is Published, it is automatically made Active (Online) which means that Test Assignments
 *                 can be created for it and it can be taken. However, if any issue is discovered with Published Test,
 *                 it can be Deactivated (updated by making it Inactive/Offline). Once issue is resolved, it can be
 *                 Activated (updated by making it Active/Online) again.<br/>
 *                 Similar to publishing, a Request Job for Activating and Deactivating is created first, and is
 *                 processed as soon as possible according to its priority and other pending Jobs in the ADP Publisher's
 *                 Job Queue.<br/><br/>
 *                 In case of successful request, it returns a Request Job ID which is required in case Requested Job
 *                 needs to be canceled. It also returns Date and Time when that Request Job was created and current
 *                 Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started
 *                 yet.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 If subsequent request for Activating/Deactivating the same Test Package is sent while the previous
 *                 one is still being processed, ADP Publisher will simply return the same request as the first time and
 *                 will not create new Request Job. If that Test Package was already been successfully
 *                 activated/deactivated, it will return ADP Test Delivery's Test Battery Form Revision ID, Date and
 *                 Time when that Test Package was updated, and its current Status.<br/><br/>
 *                 Once actual Test Activation/Deactivation is completed, ADP Publisher will send back a notification
 *                 with details about the outcome of Test Activation/Deactivation.<br/>
 *                 In case of successful Activation/Deactivation, it will provide Test Battery Form Revision ID, Date
 *                 and Time when Test Package was updated. In case of Practice Test or Quiz, it will also return
 *                 previously generated Test Key. If successfully activated/deactivated Test is a Diagnostic Test, it
 *                 will also notify LDR about successful outcome, and in case of successfully activated/deactivated
 *                 Practice Test or Quiz it will also notify PRC.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Activate/Deactivate specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                   <tr>
 *                      <td>ADP-1753</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case of validation errors.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1756</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case of a not found testFormRevision.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1757</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case if user is unauthorized.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Properties Update:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testFormRevisionId = ...<br/>
 *                   $publishedBy = ...<br/>
 *                   $adpTestFormRevisionId = ...; // ADP's Test Battery Form Revision ID that was provided when it was
 *                                                    published the first time.<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/test/" . $adpTestFormRevisionId;<br/>
 *                   $username = "DocumentationACR";<br/>
 *                   $password = "635fbb063cbc28e8bf03ed12ca4cca00";<br/>
 *                   $secret = "51f48327961f6cb0da7d584a664cafba";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>
 *                   &nbsp;  'testFormRevisionId' => $testFormRevisionId,<br/>
 *                   &nbsp;  'publishedBy' => $publishedBy,<br/>
 *                   &nbsp;  'active' => false,<br/>
 *                   ]));<br/>
 *                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationACR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="635fbb063cbc28e8bf03ed12ca4cca00" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="51f48327961f6cb0da7d584a664cafba" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt
 *                     for SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * PATCH /api/test/10 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {String}  testFormRevisionId="i123456789012" Test Battery Form Revision ID is an ID used as a reference for
 *                     Test Package being published which is unique in a system requesting publishing and is used inside
 *                     ADP only as an external reference to the source where Test Package came from.<br/>
 *                     It's 26 characters long string.
 * @apiParam {String}  publishedBy="John Smith" First and/or Last Name of the Content Author initiating Test Package
 *                     publishing. If neither is available, this can also be an email or a username of the Content
 *                     Author.<br/>
 *                     It's 100 characters long string.
 * @apiParam {Integer} adpTestFormRevisionId="10" ADP Test Battery Form Revision ID is an unique ID assigned by ADP when
 *                     the Test Package was originally published. As multiple systems can publish their own Test
 *                     Packages, ADP has to use its own IDs in order to ensure they are unique.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Boolean} active="true, false" Active indicates if Published Test Battery Form Revision should be Activate
 *                     or Deactivated (Online or Offline).<br/>
 *                     It's boolean.
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "testFormRevisionId": "i123456789012",
 *     "publishedBy": "John Smith",
 *     "active": false
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was
 *                         created.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "requestId": 789,
 *     "publishedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "requestId": 789,
 *       "publishedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 * @apiSuccessExample Successful Notification:
 *   HTTP/1.1 200 OK
 *   {
 *     "testFormRevisionId": 10,
 *     "publishedTimestamp": "2015-05-05 06:06:06",
 *     "status": "Inactive",
 *     "testKey": "PRACT-ABCDE"
 *   }
 *
 * @apiSuccessExample Successful Notification with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 4
 *     },
 *     "result": {
 *       "testFormRevisionId": 10,
 *       "publishedTimestamp": "2015-05-05 06:06:06",
 *       "status": "Inactive",
 *       "testKey": "PRACT-ABCDE"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 403 Forbidden
 *   {
 *     "statusCode": 403,
 *     "statusDescription": "Forbidden",
 *     "errorCode": "ADP-1753",
 *     "errorDescription": "Access Denied.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 400 Bad Request
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 403,
 *       "statusDescription": "Forbidden",
 *       "errorCode": "ADP-1753",
 *       "errorDescription": "Access Denied.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function testActivateDeactivate()
{
}



/**********************************************************************************************************************/



/**
 * @apiGroup ADP Publisher
 * @api {DELETE} /api/test/{adpTestFormRevisionId} Test Un-Publishing
 * @apiName Test Un-Publishing
 * @apiPermission ACR_PRC
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>ACR</b> and <b>PRC</b><br/>
 *                 Status: <b style="color:#c00">TBD</b><br/><br/>
 *                 Test web service is exclusively used by ACR and PRC for Test Package Publishing. It validates
 *                 multiple parameters before it allows client to login into ADP Publisher application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 Similar to publishing, a Request Job for Un-Publishing is created first, and is processed as soon as
 *                 possible according to its priority and other pending Jobs in the ADP Publisher's Job Queue. If a
 *                 Request Job ID is given, then instead of Un-Publishing ADP Publisher will just cancel previously
 *                 created Request Job.<br/><br/>
 *                 In case of successful request, it returns a Request Job ID which is required in case Requested Job
 *                 needs to be canceled. It also returns Date and Time when that Request Job was created and current
 *                 Status of the Requested Job. However, a Job can be canceled only if its processing hasn't started
 *                 yet.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 If subsequent request for Un-Publishing the same Test Package is sent while the previous one is still
 *                 being processed, ADP Publisher will simply return the same request as the first time and will not
 *                 create new Request Job. If that Test Package was already been successfully un-published, it will
 *                 return an error as ADP Publisher won't be able to find any references for such Test Package as all of
 *                 them were already deleted.<br/><br/>
 *                 Once actual Test Un-Publishing is completed, ADP Publisher will send back a notification with details
 *                 about the outcome of Test Un-Publishing.<br/>
 *                 In case of successful Un-Publishing, it will provide Test Battery Form Revision ID, Date and Time
 *                 when Test Package was un-published. If successfully un-published test is a Diagnostic Test, it will
 *                 also notify LDR about successful outcome, and in case of successfully un-published Practice Test or
 *                 Quiz it will also notify PRC.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Test Un-Publishing specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1700</td>
 *                     <td>Invalid Request.</td>
 *                     <td>412</td>
 *                     <td>Precondition Failed</td>
 *                     <td>Returns JSON error in case of missing or invalid format of the provided Test Battery Form
 *                         Revision details.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for Test Un-Publishing:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testFormRevisionId = ...<br/>
 *                   $publishedBy = ...<br/>
 *                   $adpRequestId = ...; // ADP's Publishing Request Job ID that was provided when it was requested the
 *                                           first time.<br/>
 *                   $adpTestFormRevisionId = ...; // ADP's Test Battery Form Revision ID that was provided when it was
 *                                                    published the first time.<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/test/" . $adpTestFormRevisionId;<br/>
 *                   $username = "DocumentationACR";<br/>
 *                   $password = "635fbb063cbc28e8bf03ed12ca4cca00";<br/>
 *                   $secret = "51f48327961f6cb0da7d584a664cafba";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([<br/>
 *                   &nbsp;  'testFormRevisionId' => $testFormRevisionId,<br/>
 *                   &nbsp;  'publishedBy' => $publishedBy,<br/>
 *                   &nbsp;  'requestId' => $adpRequestId // This is optional: If provided, instead of Un-Publishing,
 *                                                           ADP Publisher will just cancel the<br/>
 *                   &nbsp;                               // previous request (if it isn't already started or
 *                                                           completed).<br/>
 *                   ]));<br/>
 *                   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationACR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="635fbb063cbc28e8bf03ed12ca4cca00" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in
 *                     the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="51f48327961f6cb0da7d584a664cafba" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the
 *                     form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt for
 *                     SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * DELETE /api/test/10 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {String}  testFormRevisionId="i123456789012" Test Battery Form Revision ID is an ID used as a reference for
 *                     Test Package being published which is unique in a system requesting publishing and is used inside
 *                     ADP only as an external reference to the source where Test Package came from.
 *                     <br/>
 *                     It's 26 characters long string.
 * @apiParam {String}  publishedBy="John Smith" First and/or Last Name of the Content Author initiating Test Package
 *                     publishing. If neither is available, this can also be an email or a username of the Content
 *                     Author.<br/>
 *                     It's 100 characters long string.
 * @apiParam {Integer} adpTestFormRevisionId="10" ADP Test Battery Form Revision ID is an unique ID assigned by ADP when
 *                     the Test Package was originally published. As multiple systems can publish their own Test
 *                     Packages, ADP has to use its own IDs in order to ensure they are unique.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {Integer} [requestId="789"] ADP Publisher's Request Job ID is an unique ID assigned by ADP Publisher when
 *                     the original Request was created.<br/>
 *                     <b>NOTE:</b> It must be provided in case original Request needs to be canceled (deleted) which is
 *                     only possible in case its processing hasn't started yet.<br/>
 *                     It's 32-bit unsigned unique integer.
 *
 * @apiParamExample {JSON} Request Body Example
 *   {
 *     "testFormRevisionId": "i123456789012",
 *     "publishedBy": "John Smith",
 *     "requestId": 789
 *   }
 *
 *
 * @apiSuccess {JSON} data Dataset includes ADP Publisher's Request Job ID, Date and Time when Request Job was
 *                         created.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "requestId": 789,
 *     "publishedTimestamp": "2015-05-05 05:05:05",
 *     "status": "Scheduled"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "requestId": 789,
 *       "publishedTimestamp": "2015-05-05 05:05:05",
 *       "status": "Scheduled"
 *     }
 *   }
 *
 * @apiSuccessExample Successful Notification:
 *   HTTP/1.1 200 OK
 *   {
 *     "testFormRevisionId": 10,
 *     "publishedTimestamp": "2015-05-05 06:06:06",
 *     "status": "Unpublished"
 *   }
 *
 * @apiSuccessExample Successful Notification with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 3
 *     },
 *     "result": {
 *       "testFormRevisionId": 10,
 *       "publishedTimestamp": "2015-05-05 06:06:06",
 *       "status": "Unpublished"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 400 Bad Request
 *   {
 *     "statusCode": 400,
 *     "statusDescription": "Bad Request",
 *     "errorCode": "ADP-1701",
 *     "errorDescription": "User is not defined.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 400 Bad Request
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 400,
 *       "statusDescription": "Bad Request",
 *       "errorCode": "ADP-1701",
 *       "errorDescription": "User is not defined.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function testUnPublish()
{
}
