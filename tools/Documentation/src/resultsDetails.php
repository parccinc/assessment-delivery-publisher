<?php
/**
 * @apiDefine LDR User Authentication includes:
 *                <ol>
 *                  <li>Basic Authentication</li>
 *                  <li>HMAC Authentication</li>
 *                </ol>
 */
/**
 * @apiGroup ADP Publisher
 * @api {GET} /api/results/{adpTestAssignmentId} Results Details
 * @apiName Results Details
 * @apiPermission LDR
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>LDR</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Results Details web service is exclusively used by LDR for getting details for Final Test Results
 *                 once Test is being Submitted or Canceled. It validates multiple parameters before it allows client to
 *                 login into ADP Publisher application.<br/>
 *                 Authentication includes: Basic Authentication and HMAC Authentication.<br/><br/>
 *                 In order to get details for Final Test Results, only Test Assignment ID is required. Test must be
 *                 already Submitted or Canceled.<br/><br/>
 *                 Once Results Details Request is received, if correct, ADP Publisher will create a Request Job in its
 *                 Job Queue and will process it immediately.<br/><br/>
 *                 In case of successful request, it returns LDR Test Assignment ID, ADP Test Results ID, Student ID,
 *                 Test ID, Test Form ID, Test Form Revision ID, Date and Time when Test was started, Date and Time when
 *                 Test was either Submitted or Canceled, and its current Status.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server.<br/><br/>
 *                 This means that LDR should be able to handle exception if ADP doesn't return expected Test Results
 *                 Details and be able to make the same request later, if needed. In addition, ADP Publisher will not
 *                 check if actual Test Results file is physically present inside AWS S3 and if Test Results are valid.
 *                 This check was already performed previously, and it is expected that Test Results are still in place
 *                 where they were initially saved. If subsequent request for getting Test Results Details is sent, ADP
 *                 Publisher will return the same response.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Results Details specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1901</td>
 *                      <td>Internal Error.</td>
 *                      <td>500</td>
 *                      <td>Internal Server Error</td>
 *                      <td>Returns JSON error in case of error while saving the job details.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1902</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case validation error.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1903</td>
 *                      <td>Data Not Found</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case Test Assignment is not found.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1904</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case referenced Test Assignment status is 'Scheduled'.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1911</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case of no results found for a Test Session.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1912</td>
 *                      <td>Access Denied.</td>
 *                      <td>403</td>
 *                      <td>Forbidden</td>
 *                      <td>Returns JSON error in case web service is called for a Test Assignment that is not
 *                          'submitted', 'completed' or 'canceled'.</td>
 *                   </tr>
 *                   <tr>
 *                      <td>ADP-1913</td>
 *                      <td>Data Not Found.</td>
 *                      <td>404</td>
 *                      <td>Not Found</td>
 *                      <td>Returns JSON error in case there is no form details for a Test Session.</td>
 *                   </tr>
 *                 </table>
 *                 PHP Code for getting Test Results Details:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $testAssignmentId = ...<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/results/" . $testAssignmentId;<br/>
 *                   $username = "DocumentationLDR";<br/>
 *                   $password = "97f0ca00610888a0e9e58883dabc813b";<br/>
 *                   $secret = "6d393cf3b856c34ee5c55f7c50dfeb49";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication<br/>
 *                   ]);<br/>
 *                   <br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre><br/>
 *
 *
 * @apiHeader {String} username="DocumentationLDR" NOTE: This is a dummy field and is used only as a placeholder, so
 *                     Username can be entered in the form.<br/>
 *                     It's 20 characters long unique string.
 * @apiHeader {String} password="97f0ca00610888a0e9e58883dabc813b" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Password can be entered in the form.<br/>
 *                     It's 60 characters long string.
 * @apiHeader {String} secret="6d393cf3b856c34ee5c55f7c50dfeb49" NOTE: This is a dummy field and is used only as a
 *                     placeholder, so Secret can be entered in the form.<br/>
 *                     It's 32 characters long MD5 hash string.
 * @apiHeader {String} Authentication="Base64EncodedStringXXXXXXXXXX==" HMAC Authentication using Secret key as salt
 *                     for SHA256 hashing of the request.
 * @apiHeader {String} Authorization="Basic Base64EncodedStringYYYYYYYYYY==" Basic Authentication using Username and
 *                     Password.
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/results/1 HTTP/1.1
 * Host: adp.parcc.dev
 * User-Agent: curl/7.40.0
 * Accept: * / *
 * Content-Type: application/json; charset=UTF-8
 * Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==
 * Authentication: MTQzMTY1MTAxMTMwNjo1MTkxMDpOMkZpWVRjMVltUmpNMkpoWXpBNFpERXlPRGhtTjJZMVky
 * Content-Length: 89
 *
 *
 * @apiParam {Integer} adpTestAssignmentId="1" Test Assignment ID is an ID used as a reference to LDR's Test Assignment
 *                     ID which is unique inside LDR, but may not be the same as those Test Assignments created by PRC
 *                     for Practice Tests and Quizzes.<br/>
 *                     It's 32-bit unsigned unique integer.
 *
 *
 * @apiSuccess {JSON} data Dataset includes LDR Test Assignment ID, ADP Test Results ID, Student ID, Test ID, Test Form
 *                         ID, Test Form Revision ID, Date and Time when Test was started, Date and Time when Test was
 *                         either Submitted or Canceled, and its Status.<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "testAssignmentId": 2,
 *     "studentId": 3,
 *     "testId": 1,
 *     "testFormId": 3,
 *     "testFormRevisionId": 5,
 *     "testResultsId": 100,
 *     "startTimestamp": "2015-05-05 05:05:05",
 *     "endTimestamp": "2015-05-05 06:05:05",
 *     "status": "Submitted"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 9
 *     },
 *     "result": {
 *       "testAssignmentId": 2,
 *       "studentId": 3,
 *       "testId": 1,
 *       "testFormId": 3,
 *       "testFormRevisionId": 5,
 *       "testResultsId": 100,
 *       "startTimestamp": "2015-05-05 05:05:05",
 *       "endTimestamp": "2015-05-05 06:05:05",
 *       "status": "Submitted"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "statusCode": 500,
 *     "statusDescription": "Internal Server Error",
 *     "errorCode": "ADP-1901",
 *     "errorDescription": "Internal Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 500 Internal Server Error
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 500,
 *       "statusDescription": "Internal Server Error",
 *       "errorCode": "ADP-1901",
 *       "errorDescription": "Internal Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function assignmentCreate()
{
}
