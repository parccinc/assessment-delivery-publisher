<?php
/**
 * @apiGroup ADP Common
 * @api / Authentication
 * @apiName Authentication
 * @apiVersion 1.9.3
 *
 * @apiDescription Client: <b>ACR</b>, <b>LDR</b>, <b>PRC</b> and <b>Test Driver</b><br/><br/>
 *                 All ADP web service use Core Authentication and the only exception is Content web service. Some web
 *                 services have additional steps for authentication and require additional details (i.e. Login and
 *                 Results).<br/>
 *                 Core ADP Authentication consists of:
 *                 <ol>
 *                   <li>Basic Authentication</li>
 *                   <li>HMAC Authentication</li>
 *                 </ol><br/>
 *                 <b>Basic Authentication</b>:<br/><br/>
 *                 Basic Authentication is a simple authentication based on username and password sent in
 *                 "Authorization" request header. All passwords are 32-characters long lowercase alphanumeric strings
 *                 generated as random MD5 hashes. This can be further strengthened in the future to increase security,
 *                 if necessary. The value in the header is string "Basic " followed by Base64 encoded digest
 *                 (concatenated username and password separated by single colon):
 *                 <pre class="pseudo-code">"Authorization": "Basic " + Base64Encode(Username + ":" + Password)</pre>
 *                 Example:
 *                 <pre>Authorization: Basic VGVzdERyaXZlcjo2NTJmNWQwM2ViZjdhZWU5MmMxZjZjZTcxYTE5Njk5OQ==</pre><br/>
 *                 <b>HMAC Authentication</b>:<br/><br/>
 *                 HMAC Authentication is more complex authentication based Keyed Hash Message Authentication Code that
 *                 uses Secret as salt and is signed with a timestamp, so once generated it is valid only for a short
 *                 period of time.<br/><br/>
 *                 It requires calculation of Message Authentication Code (MAC) which involves cryptographic hash
 *                 function with combination with a secret cryptographic key. As with any MAC, it is used to
 *                 simultaneously verify both the data integrity and the authentication of a message. SHA-256
 *                 cryptographic hash function is used in the calculation of the HMAC. The cryptographic strength of the
 *                 HMAC depends upon the cryptographic strength of the underlying hash function, the size of its hash
 *                 output, and on the size and quality of the key, so if necessary, the security can be further
 *                 increased in the future by replacing SHA-256 with a stronger cryptographic hash function and/or by
 *                 increasing the complexity of the HMAC algorithm which generates the input digest.<br/><br/>
 *                 An iterative hash function breaks up a message into blocks of a fixed size and iterates over them
 *                 with a compression function. SHA-256 cryptographic hash function operates on 512-bit blocks, and the
 *                 size of the output (HMAC Hash) is the same and is fixed to 256 bits which means it produces a
 *                 64-characters long lowercase alphanumeric string. This string can further be truncated at certain
 *                 position in order to increase complexity of HMAC Authentication algorithm in the future (if
 *                 necessary), so there is a lot of space for further increasing security levels in the future for
 *                 adding support for High-Stakes Testing, if needed.<br/><br/>
 *                 The current implementation of HMAC relies on SHA-256 hashing using Secret as salt. Secret is
 *                 32-characters long lowercase alphanumeric string generated as random MD5 hash which should be stored
 *                 on the client side in a secure place. As Test Driver can run in a regular browser and its Secret
 *                 cannot be stored in a highly secured manner, it is the only exception, so it has to provide
 *                 additional authentication details for the extended authentication algorithm which include one or more
 *                 of the following depending on the web service: Token, Test Key, Personal ID, Date of Birth and/or
 *                 State. This only applies to Login, Content and Results web services which are further explained
 *                 within their sections.<br/>
 *                 <br/>
 *                 In addition, HMAC Authentication uses Unix timestamp to sign the request, so it's valid only for a
 *                 short period of time.<br/>
 *                 As all timestamps in ADS must be in UTC timezone, timestamp is a 13-digit Unix timestamp in a UTC
 *                 timezone (frequently called micro-time). In
 *                 addition, HMAC Authentication uses Nonce which is a random integer in the range [0, 99999):<br/>
 *                 <pre class="pseudo-code">
 *                   Nonce = Floor(Random() * 99999)<br/>
 *                   Message = apiURI + Round(Microtime('UTC') / 1000) + Nonce<br/>
 *                   Hash = SHA-256-HMAC(Secret, Message)<br/><br/>
 *                   "Authentication": Base64Encode(Microtime('UTC')+ ":" + Nonce + ":" + Base64Encode(Hash))
 *                 </pre>
 *                 Example:
 *                 <pre>Authentication: MTQzMTg3ODczMzYzOTo3MjQyNjpPRGN3TXpoak16Um1ZMlJoWXpoak1ESTFaR1JpWXpneVlqSmxNbVl6
 *                                      TkRabFlUWmlZMkZqWTJFM01HRTBaR0U1TVdZNE0ySTNPVEZsWWpreVl6Sm1NUT09</pre>
 *                 JavaScript Code for Authentication:
 *                 <pre class="pseudo-code">
 *                   // Time Offset represent time difference between server and client in seconds. It is important that
 *                      all timestamps coming from the<br/>
 *                   // client side are synced with the server in UTC time zone as client can be in any (possibly wrong)
 *                      timezone, and client clock can<br/>
 *                   // be off or can be tampered with. Therefore, anything related to time and date must be relative to
 *                      a timestamp received form the<br/>
 *                   // server which is always synchronized with Atomic Clock Server using NTP protocol.<br/>
 *                   // Server time is always provided as 10-digit Unit timestamp in UTC timezone.<br/>
 *                   var timeOffset = 1000 * serverTime - new Date().getTime();<br/>
 *                   <br/>
 *                   var apiHost = "https://adp.parcc.dev";<br/>
 *                   var apiURI = "/api/{api_route}";<br/>
 *                   var username = "DocumentationTestDriver";<br/>
 *                   var password = "53c5de17b5488f1a8549e6db1786ca81";<br/>
 *                   var secret = "578cca1aa71f702c26cc81439392124c";<br/>
 *                   <br/>
 *                   var timestamp = new Date().getTime() + timeOffset; // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   var nonce = Math.floor(Math.random() * 99999); // 1-5 digit long random integer in [0, 99999)
 *                                                                     range.<br/>
 *                   var message = apiURI + Math.round(timestamp / 1000) + nonce; // Message uses rounded 10-digit Unix
 *                                                                                   timestamp.<br/>
 *                   var hash = $.sha256hmac(secret, message);<br/>
 *                   var authentication = btoa(timestamp + ':' + nonce + ':' + btoa(hash));<br/>
 *                   <br/>
 *                   $.ajax({<br/>
 *                   &nbsp;  url: apiHost + apiURI,<br/>
 *                   &nbsp;  type: requestMethod,<br/>
 *                   &nbsp;  headers: {<br/>
 *                   &nbsp;    'Content-Type': "application/json; charset=UTF-8",<br/>
 *                   &nbsp;    'Authorization': "Basic " + btoa(username + ":" + password),<br/>
 *                   &nbsp;    'Authentication': authentication,<br/>
 *                   &nbsp;    ...<br/>
 *                   &nbsp;  },<br/>
 *                   &nbsp;  data: data,<br/>
 *                   &nbsp;  ...<br/>
 *                   })
 *                 </pre>
 *                 PHP Code for Authentication:
 *                 <pre class="pseudo-code">
 *                   date_default_timezone_set('UTC'); // Make sure to use timestamps in UTC timezone and that server
 *                                                        uses NTP service.<br/>
 *                   <br/>
 *                   $apiHost = "https://adp.parcc.dev";<br/>
 *                   $apiURI = "/api/{api_route}";<br/>
 *                   $username = "DocumentationTestDriver";<br/>
 *                   $password = "53c5de17b5488f1a8549e6db1786ca81";<br/>
 *                   $secret = "578cca1aa71f702c26cc81439392124c";<br/>
 *                   <br/>
 *                   $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.<br/>
 *                   $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.<br/>
 *                   $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix
 *                                                                              timestamp.<br/>
 *                   $hash = hash_hmac('sha256', $message, $secret);<br/>
 *                   $authentication = base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));<br/>
 *                   <br/>
 *                   $ch = curl_init($apiHost . $apiURI);<br/>
 *                   curl_setopt($ch, CURLOPT_HTTPHEADER, [<br/>
 *                   &nbsp;  "Content-Type": "application/json; charset=UTF-8",<br/>
 *                   &nbsp;  "Authorization: Basic " . base64_encode($username . ":" . $password),<br/>
 *                   &nbsp;  "Authentication:" . $authentication,<br/>
 *                   &nbsp;  ...<br/>
 *                   ]);<br/>
 *                   ...<br/>
 *                   $result = curl_exec($ch);<br/>
 *                   curl_close($ch);<br/>
 *                   ...
 *                 </pre>
 */
function authentication()
{
}
