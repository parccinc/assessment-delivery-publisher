<?php
/**
 * @apiDefine TestDriver_Content User Authentication includes:
 *                               <ol>
 *                                <li>Token</li>
 *                                <li>Test Battery Form Revision ID</li>
 *                               </ol>
 */
/**
 * @apiGroup ADP Test Delivery
 * @api {GET} /api/content/{token}/{testId}/{contentFile='test.json'} Content Download
 * @apiName Content
 * @apiPermission TestDriver_Content
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>Test Driver</b><br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Content web service is exclusively used by Test Delivery for getting test content. It validates
 *                 multiple parameters before it allows client to login into ADP Test Delivery application.<br/><br/>
 *                 In case of successful authentication, it returns a 302 Redirect with a URL that will return the
 *                 actual requested content file resource.<br/>
 *                 In case of failure, it returns an error with error code and error description, HTTP status code and
 *                 HTTP status description, and current time on the server. These details are returned within response
 *                 header, but the response body is omitted.<br/><br/>
 *                 More details about HMAC Authentication can be found under Authentication section.<br/>
 *                 All possible generic API Errors can be found under Errors section.<br/><br/>
 *                 These are possible Content specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1300</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns error in response header without response body in case Token doesn't belong to any
 *                         Test Assignment.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1301</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns error in response header without response body in case Test Assignment is
 *                         Deactivated.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1302</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns error in response header without response body in case Test Form Revision doesn't
 *                         match Test Assignment.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1303</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns error in response header without response body in case referenced Test is already
 *                         Submitted.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1304</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns error in response header without response body in case referenced Test is already
 *                         Completed.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1305</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns error in response header without response body in case referenced Test is
 *                         Canceled.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1306</td>
 *                     <td>Authorization Failed.</td>
 *                     <td>401</td>
 *                     <td>Unauthorized</td>
 *                     <td>Returns error in response header without response body in case Token has expired.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1307</td>
 *                     <td>Data Not Found.</td>
 *                     <td>404</td>
 *                     <td>Not Found</td>
 *                     <td>Returns error in response header without response body in case requested File Resource was
 *                         not found.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1308</td>
 *                     <td>Access Denied.</td>
 *                     <td>403</td>
 *                     <td>Forbidden</td>
 *                     <td>Returns error in response header without response body in case Test Content is
 *                         Deactivated.</td>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-16xx</td>
 *                     <td>Internal Error.</td>
 *                     <td>500</td>
 *                     <td>Internal Server Error</td>
 *                     <td>Returns error in response header without response body in case of failure to generate AWS
 *                         CloudFront Signed URL. Refer to S3 Errors for more details about possible errors.</td>
 *                   </tr>
 *                 </table>
 *                 JavaScript Code for Content:
 *                 <pre class="pseudo-code">
 *                   var apiHost = "https://adp.parcc.dev";<br/>
 *                   var apiURI = "/api/content";<br/>
 *                   <br/>
 *		             var token = $('#token').val();<br/>
 *		             var testId = $('#testId').val();<br/>
 *                   <br/>
 *                   $.ajax({<br/>
 *                   &nbsp;  url: apiHost + apiURI + "/" + token + "/" + testId +
 *                                (contentFile <span class="exclamation-mark"/>== "" ? ("/" + contentFile) : ""),<br/>
 *                   &nbsp;  type: 'GET',<br/>
 *                   &nbsp;  ...<br/>
 *                   })
 *                 </pre><br/>
 *
 *
 * @apiParam {String}  token="3c564954-0096-11e5-3123-10def1bd7269" Token is unique per test assignment, including
 *                     practice tests and quizzes. It is received as part of successful Login response. It also has an
 *                     expiration timestamp and once expired it will not be valid anymore.<br/>
 *                     It's 36 characters long unique GUID (lowercase only alphanumeric string with dashes).
 * @apiParam {Integer} testId="3" Test Battery Form Revision ID currently being taken by the Student. This is returned
 *                     by Login web service and is part of the Test Assignment.<br/>
 *                     It's 32-bit unsigned unique integer.
 * @apiParam {String}  [contentFile="sample.jpg, sample.png, sample.mp3, sample.mp4, sample.webm"] Content File Resource
 *                     being requested. This is optional parameter and if omitted, by default "test.json" will be
 *                     returned which is the test definition file which includes references to all other Content File
 *                     Resources. All Content File Resource files are prefixed with 3 indexes separated by underscores
 *                     (i.e. 0_0_0_) which indicate Test Part, Test Section and Test Item respectively &ndash; these are
 *                     the sequence numbers within the current Test.<br/>
 *                     It's 256 characters long string.
 *
 *
 * @apiParamExample {URL} Request URL Example
 *   GET /api/content/3c564954-0096-11e5-3123-10def1bd7269/3/0_0_0_flower.jpg
 *
 *
 * @apiSuccess {URL} N/A URL pointing to the requested Content File Resource. This is a signed URL for AWS CloudFront
 *                       and is pointing to AWS S3 bucket which contains the actual file. Signed URL has expiration
 *                       timestamp after which it is not valid anymore. Upon successful Login, ADP Test Delivery will
 *                       generate AWS CloudFront signature for all URLs that apply to the related Test Battery Form
 *                       Revision and those will be valid as long as related Token is valid. Once Token and URL
 *                       Signature expire, a new Login is necessary in order to generate new ones.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 302 Found
 *   Location: https://d111111abcdef8.cloudfront.net/flower.jpg?Policy=eyANCiAgICEXAMPLEW1lbnQiOiBbeyANCiAgICAgICJgTf
 *   sZXNvdXJjZSI6Imh0dHA6Ly9kemJlc3FtN3VuMW0wLmNsb3VkZnJvbnQubmV0L2RlbW8ucGhwIiwgDQogICAgICAiQ29uZGl0aW9uIjp7IA0KtR3
 *   iCAgICAgICAgIklwQWRkcmVzcyI6eyJBV1M6U291cmNlSXAiOiIyMDcuMTcxLjE4MC4xMDEvMzIifSwNCiAgICAgICAgICJEYXRlR3JlYXRlc4Fd
 *   lRoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTI5Njg2MDE3Nn0sDQogICAgICAgICAiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjEyOT2eF
 *   Y4NjAyMjZ9DQogICAgICB9IA0KICAgfV0gDQp9DQo&Signature=nitfHRCrtziwO2HwPfWwyYDhUF5EwRunQAj19DzZrvDh6hQ73lDxar3UoufG
 *   cvvRQVw6EkCGdpGQyyOSKQimTxAnW7d8F5Kkai9HVx0FIu5jcQb0UEmatEXAMPLE3ReXySpLSMj0yCd3ZAB4UcBCAqEijkytL6f3fVYNGQI62RfT
 *   &Key-Pair-Id=APKA9ONS7QCOWEXAMPLE
 *
 *
 * @apiError {Header} N/A Response Header contains Status Code, Status Description, Error Code and Error Description,
 *                        and current time on the server. Response Body is omitted.
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 401 Unauthorized
 *   X-Error-Code: ADP-1300
 *   X-Error-Description: Access Denied.
 *   X-Time: 1426619032
 *
 */
function downloadContent()
{
}
