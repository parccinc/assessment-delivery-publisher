<?php
/**
 * @apiDefine ELB User Authentication:
 *                Not needed.
 */
/**
 * @apiGroup ADP Common
 * @api {GET} /api/ping Ping
 * @apiName Ping
 * @apiPermission ELB
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>ELB</b> - Health Check<br/>
 *                 Status: <b style="color:green">Completed</b><br/><br/>
 *                 Ping web service is exclusively used by ELB (Elastic Load Balancer) for Health Check. It doesn't use
 *                 any authentication and is only used to confirm that the server is up and running and that ADP is
 *                 responsive. However, this doesn't mean that it's fully functional.<br/>
 *                 It always returns successful response and should never return errors.<br/><br/>
 *                 More details about ADP Status Health Check can be found under Status.<br/><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/ping HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {HTML} N/A It always returns "OK" as ELB only accepts responses with "200 OK" HTTP Status.<br/>
 *                        Anything else is considered as an error.
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   OK
 *
 */
function ping()
{
}



/**********************************************************************************************************************/



/**
 * @apiDefine IT_Dev User Authentication:
 *                   Not needed.
 */
/**
 * @apiGroup ADP Common
 * @api {GET} /api/ping/status Status
 * @apiName Status
 * @apiPermission IT_Dev
 * @apiVersion 1.26.2
 *
 * @apiDescription Client: <b>IT</b>/<b>Developers</b> - Health Check<br/>
 *                 Status: <b style="color:#c00">In Development</b><br/><br/>
 *                 Status web service is exclusively used by IT and Developers for Status Health Check. It doesn't use
 *                 any authentication and is only used to confirm that the server is up and running and that ADP is
 *                 fully functional. It validates connections to MySQL Databases, Memcached instances, presence and
 *                 correctness of the configuration file etc.<br/><br/>
 *                 In case of successful request, it returns OK as the status if everything if working fine. Otherwise,
 *                 it returns details which failed during
 *                 Status Health Check.<br/><br/>
 *                 These are possible Status specific errors:<br/>
 *                 <table>
 *                   <tr>
 *                     <th>Error Code</th>
 *                     <th>Error Description</th>
 *                     <th>HTTP Code</th>
 *                     <th>HTTP Status</th>
 *                     <th>Note</th>
 *                   </tr>
 *                   <tr>
 *                     <td>ADP-1080</td>
 *                     <td>Database Connection Error.</td>
 *                     <td>503</td>
 *                     <td>Service Unavailable</td>
 *                     <td>Returns JSON error in case a Database Connection failed to be opened.</td>
 *                   </tr>
 *                 </table><br/>
 *
 *
 * @apiHeaderExample {String} Request Headers Example
 *
 * GET /api/ping/status HTTP/1.1
 * Host: adp.parcc.dev
 * Origin: https://adp.parcc.dev
 * User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0
 * Accept: application/json, text/javascript; q=0.01
 * Accept-Language: en-US,en;q=0.5
 * Accept-Encoding: gzip, deflate
 * Referer: https://adp.parcc.dev/
 * Content-Length: 89
 * Connection: keep-alive
 * Pragma: no-cache
 * Cache-Control: no-cache
 *
 *
 * @apiSuccess {JSON} data Dataset includes simple "OK" as Status, API Version with Date and Database Schema Version
 *                         with Date. Dates are Last Modified Dates of Version Files which is NOT a guarantee that it
 *                         matches the actual Date of the actual version as the File(s) could be manually modified at
 *                         any time!<br/>
 *                         It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                         response and its status (in this case SUCCESS).
 *
 * @apiSuccessExample Successful Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "status": "OK",
 *     "apiVersion": "1.19.2 [11/16/2015]",
 *     "dbVersion": "1.19.2 [11/17/2015]",
 *     "apiVersionInstalled": "1.19.2 [11/16/2015]",
 *     "dbVersionInstalled": "1.19.2 [11/17/2015]"
 *   }
 *
 * @apiSuccessExample Successful Response with Envelope:
 *   HTTP/1.1 200 OK
 *   {
 *     "meta": {
 *       "status": "SUCCESS",
 *       "count": 5
 *     },
 *     "result": {
 *       "status": "OK"
 *       "apiVersion": "1.19.2 [11/16/2015]",
 *       "dbVersion": "1.19.2 [11/17/2015]",
 *       "apiVersionInstalled": "1.19.2 [11/16/2015]",
 *       "dbVersionInstalled": "1.19.2 [11/17/2015]"
 *     }
 *   }
 *
 *
 * @apiError {JSON} data Dataset includes statusCode, statusDescription, errorCode and errorDescription, and current
 *                       time on the server.<br/>
 *                       It may contain ENVELOPE in which case it will also indicate number of child nodes in the
 *                       response and its status (in this case ERROR).
 *
 * @apiErrorExample Error Response:
 *   HTTP/1.1 503 Service Unavailable
 *   {
 *     "statusCode": 503,
 *     "statusDescription": "Service Unavailable",
 *     "errorCode": "ADP-1080",
 *     "errorDescription": "Database Connection Error.",
 *     "time": 1426619032
 *   }
 *
 * @apiErrorExample Error Response with Envelope:
 *   HTTP/1.1 503 Service Unavailable
 *   {
 *     "meta": {
 *       "status": "ERROR",
 *       "count": 5
 *     },
 *     "result": {
 *       "statusCode": 503,
 *       "statusDescription": "Service Unavailable",
 *       "errorCode": "ADP-1080",
 *       "errorDescription": "Database Connection Error.",
 *       "time": 1426619032
 *     }
 *   }
 *
 */
function pingStatus()
{
}
