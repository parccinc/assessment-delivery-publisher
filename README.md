PARCC-ADP-TDS
=============

PARCC ADP (Assessment Delivery Platform) Server is a server side of PARCC ADS Learn system.  
It provides REST APIs for Test Delivery and Test Publishing.

### Version
**v1.26.2**

### License
GNU GPL v2
Copyright © 2015 Breakthrough Technologies, LLC

### Standards
ADP complies with following PSR Standards:
- PSR-1: Basic Coding Standard (100%)
- PSR-2: Coding Style Guide (except use of tabs)
- PSR-3: Logger Interface (100%)
- PSR-4: Autoloading Standard (100%)
- PSR-5: PHPDoc Standard (100%)
- PSR-6: Caching Interface (80%)
- PSR-7: HTTP Message Interface (80%)
- PSR-11: Container Interface (100%)
- PSR-12: Extended Coding Style Guide (100%)

### Documentation
Complete interactive API Documentation can be found under **`/tools/Documentation`**.

### System Requirements
- Apache 2.2.15+ or NGINX 1.8.1+ with PHP-FPM 5.6.20+
- MySQL 5.6.30+
- PHP 5.6.20+
- CURL 7.19.7+
- OpenSSL 1.0.1e+
- PHP-Phalcon 2.0.9+
- PHP-Common 5.6.20+ (Ctype, Date, Filter, Hash, JSON, Mhash, PCRE, SPL)
- PHP-Cli 5.6.20+
- PHP-MySQLND 5.6.20+
- PHP-PDO 5.6.20+
- PHP-PDO_MySQL 5.6.20+
- PHP-OpCache 5.6.20+
- PHP-PECL-APCu 4.0.10+
- PHP-PECL-IGBinary 1.2.1+
- PHP-PECL-JSONC 1.3.9+
- PHP-PECL-ZIP 1.13.1+

### Installation
1. Checkout code from Github.
2. Create database from **`/install/database.sql`** file.
3. Create database user with following permissions for created database:  
   Select  
   Insert  
   Update  
   Delete  
   Create  
   Index  
   Alter  
   Lock Tables
4. Copy **`/app/config/sample.config.php`** to **`/app/config/config.php`**.
5. Enter details for Database access, S3, CloudFront and other Application Configuration as desired.
6. Configure web server and point document root to **`/html`** folder.
7. Open **`/api/ping/status`** in a Browser and if everything works fine it should return:  
   ```json
   {"status": "OK"}
   ```  
   Anything else is an error.

### Client Authentication
To configure authentication credentials for API Clients, use Password Generator tool located at
`/tools/PasswordGenerator/generate.bat`. Copy generated Password Hash and Secret, and insert them into a Database. Also
write down matching Password and hand it together with the Secret to the API Client to be used for their authentication.

### Login Configuration
To change the default Login Configuration follow these steps:  
1. Copy `/tools/LoginConfiguration/sample.loginConfiguration.json` to
   `/tools/LoginConfiguration/loginConfiguration.json`  
2. Edit State Login Rules within copied file `/tools/LoginConfiguration/loginConfiguration.json`.  
3. Run `/tools/LoginConfiguration/import.bat` to import new Login Configuration.

### Test Driver Configuration
To change the default Test Driver Configuration follow these steps:  
1. Copy file `/tools/TestDriverConfiguration/sample.testDriverConfiguration.json` to
   `/tools/TestDriverConfiguration/testDriverConfiguration.json`  
2. Edit Test Driver Configuration within copied file `/tools/TestDriverConfiguration/testDriverConfiguration.json`.  
3. Run `/tools/TestDriverConfiguration/import.bat` to import new Test Driver Configuration.

### UDP Socket Logging
ADP supports logging to an UDP Socket. Once enabled, Socket Listener console can be used to get a live feed of API
Logs.  
Socket Listener is located at `/tools/SocketListener/listen.bat` and can accept additional switch to use different
colors based on Log Entry Severity (-c).

### Database Upgrade
* To upgrade Database simply import all `update.sql` scripts that belong to versions that are newer than the one
  currently installed. Update scripts are located in a folders named by version they belong to at:  
  **`/install/update/`**

### Test Driver Installation
1. Checkout code from Github.
2. Run Grunt to inside **`/TD`** folder to compile production ready build.
3. Copy all files from the **`/TD/release`** folder except `index.html` and `testDriver.phtml` into **`/html/TD`**
   folder for testing purposes only (this step is not necessary for the production).
4. If necessary, replace `/app/views/testDriver.phtml` and `/app/views/testDriverDebug.phtml` templates with the new
   versions.
5. Configure **`/tools/TestDriverDeployment/config.php`** file based on
   **`/tools/TestDriverDeployment/sample.config.php`** sample config file.
6. Run **`/tools/TestDriverDeployment/deploy.bat`** or **`/tools/TestDriverDeployment/deploy.sh`** script to deploy Test
   Driver files to AWS CloudFront.

### Testing
For testing purposes use API Test Client under **`/tools/APITestClient`**.

### Tools
The following Tools can be found within **`/tools`** folder:
 1. API Test Client (used for API testing).
 2. Cache Wipe (clears Application Cache).
 3. Code Documentation.
 4. Data Generator (populates database with generated dummy data).
 5. Data Wipe (wipes data from database and S3).
 6. Documentation (API Documentation).
 7. Login Configuration (used for configuring and importing custom state rules for Login).
 8. Password Generator (generates user credentials).
 9. Socket Listener (traces live Log stream from the application).
10. Test Driver Configuration (used for customizing and importing Test Driver configuration).
11. Test Driver Deployment (automated Test Driver deployment).
12. Vagrant (Vagrant Box).


### Vagrant Box
Vagrant Box is based on CentOS 6.7 x64 (minimal install) and has the following pre-installed:
* Apache 2.2.15
* NGINX 1.9.4 + PHP-FPM 5.6.12
* MySQL 5.6.26
* PHP 5.6.12
* Memcached 1.4.22
* Git 1.7.1
* Phalcon 2.0.7
* various additional tools (Xdebug, WebGrind, APC Control Panel, Memcached Control Panel etc.)

It is configured to work with **PARCC-ADP-TDS**, **PARCC-ADP-PS** and **PARCC-ACR** and uses virtual hosts.  
DocumentRoot for virtual hosts is configured to point to the shared folders, so all the source is deployed on the Host
Computer.

###### Vagrant Box Installation
1. Install Vagrant and VirtualBox.
2. Checkout Vagrant Box.
3. Checkout **PARCC-ADP-TDS**, **PARCC-ADP-PS** and **PARCC-ACR** to the following paths:  
   **`C:\xampp\htdocs\PARCC-ADP-TDS`**  
   **`C:\xampp\htdocs\PARCC-ADP-PS`**  
   **`C:\xampp\htdocs\PARCC-ACR`**
4. Make sure the following ports are available (are not used by other application):  
   SSH: **22**  
   HTTP: **80**  
   HTTPS: **443**  
   MySQL: **3306**
5. If desired, all shared folders, their paths, forwarded ports, VM names and other details can be configured as
   desired.
   This must be done before proceeding to the next step by editing the provided **`Vagrantfile`** file and/or
   **`Batch`** files.
6. Install Vagrant Box by running the **`Install.bat`** script.
7. Recommended: SSH to Vagrant Box and run YUM update to upgrade LAMP environment:  
   **`yum update -y`**  
   Once completed, reboot the Vagrant Box.

###### Vagrant Box Usage
* To start Vagrant Box simply run the **`Start.bat`** script.
* To stop Vagrant Box simply run the **`Stop.bat`** script.
* To access Vagrant Box over SSH simply run the **`Connect.bat`** script.  
  You can also use PuTTY with the following credentials:  
  username: **admin**  
  password: **vagrant**

  Alternatively, you can also use SSH key without providing any credentials.  
  During the installation Vagrant will generate the SSH key and will place it inside the folder where Vagrant Box was
  checked out. It will be located inside the following folder:  
  `/.vagrant/machines/PARCC-Vagrant/virtualbox/private_key`

  For access to MySQL use the following credentials:  
  username: **parcc**  
  password: **vagrant**

* Vagrant Box comes with **NGINX** configured to listen on port **80** and **Apache** on port **443**.  
  As TAO strongly relies on usage of Apache's .htaccess files it has limited support for NGINX, PARCC-ACR is therefore
  only available over HTTPS.  
  However, PARCC-ADP-TDS and PARCC-ADP-PS can be accessed either over HTTP or HTTPS.

  These are the URLs that should be used to access PARCC applications:
  1. **PARCC-ADP-TDS**:  
     `http://adp.parcc.dev`  
     `https://adp.parcc.dev`
  2. **PARCC-ADP-PS**:  
     `http://ps.parcc.dev`  
     `https://ps.parcc.dev`
  3. **PARCC-ACR**:  
     `https://acr.parcc.dev`

  In addition, several Dev Tools are available at the following URLs:  
  `http://localhost`  
  `https://localhost`

###### Vagrant Box Tools
As Vagrant Box is used its virtual hard drive starts growing over time and it becomes fragmented.  
In order to compact the virtual hard drive copy the files located inside **`Tools`** subfolder to the place where
Vagrant deployed VirtualBox VM.  
By default, VirtualBox deploys VMs to user's profile like:  
`C:\Users\<username>\VirtualBox VMs\PARCC-Vagrant\`  
Once files are copied into this folder simply run the **`Shrink.bat`** script.

###### Vagrant Box Uninstallation
* To uninstall Vagrant Box simply run the **`Uninstall.bat`** script.
