<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests\Services\Logger;

use Phalcon\Test\UnitTestCase;
use PARCC\ADP\Services\Logger\Socket as SocketLogger;
use PARCC\ADP\Tests\Stubs\SocketListenerStub;

/**
 * Class SocketTest.
 *
 * @package PARCC\ADP\Tests
 */
class SocketTest extends UnitTestCase
{

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @covers PARCC\ADP\Services\Logger\Socket::__construct
	 */
	public function testConstructorLocalhostReturnsSocket()
	{
		$this->assertInstanceOf(
			'PARCC\ADP\Services\Logger\Socket',
			new SocketLogger('http://localhost'),
			'Asserting Socket was created.'
		);
	}

	/**
	 * @covers PARCC\ADP\Services\Logger\Socket::__construct
	 */
	public function testConstructorLocalhost8083ReturnsSocket()
	{
		$this->assertInstanceOf(
			'PARCC\ADP\Services\Logger\Socket',
			new SocketLogger('http://localhost', ['port' => 8083]),
			'Asserting Socket was created.'
		);
	}

	/**
	 * @covers PARCC\ADP\Services\Logger\Socket::getFormatter
	 */
	public function testGetFormatterNoFormatterSetReturnsFormatter()
	{
		$socketLogger = new SocketLogger('http://localhost');

		$this->assertInstanceOf(
			'Phalcon\Logger\Formatter\Line',
			$socketLogger->getFormatter(),
			'Asserting Socket returns Formatter.'
		);

		return $socketLogger;
	}

	/**
	 * @covers  PARCC\ADP\Services\Logger\Socket::getFormatter
	 * @depends testGetFormatterNoFormatterSetReturnsFormatter
	 * @param   SocketLogger $socketLogger
	 */
	public function testGetFormatterFormatterSetReturnsFormatter(SocketLogger $socketLogger)
	{
		// Code coverage skips line 79.
		$this->assertInstanceOf(
			'Phalcon\Logger\Formatter\Line',
			$socketLogger->getFormatter(),
			'Asserting Socket returns Formatter.'
		);
	}

	/**
	 * @covers  PARCC\ADP\Services\Logger\Socket::logInternal
	 */
	public function testLogInternalShortMessage()
	{
		$this->markTestSkipped();
		$stub = new SocketListenerStub();
		$stub->listen();

		$socketLogger = new SocketLogger('http://localhost', ['port' => 0]);
		$socketLogger->logInternal('New log entry.', 1, time());

		$this->assertEquals('', $stub->read());
		unset($stub);
	}
}
