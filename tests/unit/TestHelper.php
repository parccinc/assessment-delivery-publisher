<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests;

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Loader;

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


// Define File Paths.
const ROOT_PATH = __DIR__;
const PATH_LIBRARY = __DIR__ . '/../../app/library/';
const PATH_SERVICES = __DIR__ . '/../../app/services/';
const PATH_RESOURCES = __DIR__ . '/../../app/resources/';
const INCUBATOR_PATH = __DIR__ . '/../../vendor/phalcon/incubator/Library/Phalcon';


// Load Configuration.
$config = require_once 'config.php';

set_include_path(ROOT_PATH . PATH_SEPARATOR . get_include_path());

// Use the Application Autoloader to autoload the Classes.
// Autoload the dependencies found in Composer.
$loader = new Loader();

// Register Namespaces.
$loader->registerNamespaces([
	'Phalcon' => INCUBATOR_PATH,
	'PARCC\ADP\Controllers' => '../../app/controllers',
	'PARCC\ADP\Exception' => '../../app/exceptions',
	'PARCC\ADP\Models' => '../../app/models',
	'PARCC\ADP\Responses' => '../../app/responses',
	'PARCC\ADP\Services' => '../../app/services',
	'PARCC\ADP\Services\AWS' => '../../app/services/aws',
	'PARCC\ADP\Services\JSV' => '../../app/services/jsv',
	'PARCC\ADP\Services\Logger' => '../../app/services/logger',
	'PARCC\ADP\Tests' => '.',
	'PARCC\ADP\Tests\Interfaces' => 'interfaces',
	'PARCC\ADP\Tests\Interfaces\Implementations' => 'interfaces/implementations',
	'PARCC\ADP\Tests\Services\Logger' => 'tests/services/logger',
]);

$loader->registerDirs([ROOT_PATH]);

// Check if Debugging is enabled.
if ($config['DEBUG_UNIT_TESTS']) {
	$eventsManager = new EventsManager();
	$eventsManager->attach(
		'loader',
		function ($event, $loader) {
			/** @var \Phalcon\Events\Event $event */
			if ($event->getType() == 'beforeCheckPath') {
				/** @var Loader $loader */
				echo PHP_EOL . 'Checked path: ' . $loader->getCheckedPath() . PHP_EOL;
			}
		}
	);
	$loader->setEventsManager($eventsManager);
}

$loader->register();

$di = new FactoryDefault();
Di::reset();

// Add any needed Services to the DI here.
Di::setDefault($di);
