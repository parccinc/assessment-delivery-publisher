<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests\Stubs;

use Phalcon\Exception;

/**
 * Class SocketListenerStub.
 *
 * @package PARCC\ADP\Tests
 */
class SocketListenerStub
{
	protected $client;
	protected $socket;

	public function listen()
	{
		$sock = socket_create(AF_INET, SOCK_STREAM, 0);

		// Bind the socket to an address/port.
		if (!socket_bind($sock, 'localhost', 0)) {
			throw new Exception('Could not bind to address');
		}

		// Start listening for connections.
		socket_listen($sock);

		socket_set_nonblock($sock);

		// Accept incoming requests and handle them as child processes.
		$this->client = socket_accept($sock);
		$this->socket = $sock;
	}

	/**
	 * @return string
	 */
	public function read()
	{
		if ($this->client === false) {
			return socket_strerror(socket_last_error($this->socket));
		}
		// Read the input from the client 1024 Bytes.
		$input = socket_read($this->client, 1024);

		return $input;
	}
}
