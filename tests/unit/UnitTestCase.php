<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests;

use Phalcon\Config;
use Phalcon\Di;
use Phalcon\Test\UnitTestCase as PhalconTestCase;

/**
 * Class UnitTestCase.
 *
 * @package PARCC\ADP\Tests
 */
abstract class UnitTestCase extends PhalconTestCase
{
	/**
	 * @var mixed $now
	 */
	public static $now;
	/**
	 * @var \Phalcon\Config $config
	 */
	protected $config;
	/**
	 * @var boolean $loaded
	 */
	private $loaded = false;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->config = require 'config.php';
	}

	/**
	 * Setup.
	 */
	public function setUp()
	{

		// get any DI components here. If you have a config, be sure to pass it to the parent
		parent::setUp();

		$this->loaded = true;
	}

	/**
	 * Check if the test case is setup properly.
	 *
	 * @throws \PHPUnit_Framework_IncompleteTestError
	 */
	public function __destruct()
	{
		if (!$this->loaded) {
			throw new \PHPUnit_Framework_IncompleteTestError('Please run parent::setUp().');
		}
	}

	/**
	 * Tear Down.
	 */
	public function tearDown()
	{
		self::$now = null;

		parent::tearDown();
	}

	/**
	 * Call a private or protected method of an object.
	 *
	 * @param  object $object
	 * @param  string $methodName
	 * @param  array $parameters
	 * @return mixed
	 */
	public function invokeMethod(&$object, $methodName, array $parameters = [])
	{
		$reflection = new \ReflectionClass(get_class($object));
		$method = $reflection->getMethod($methodName);
		$method->setAccessible(true);

		return $method->invokeArgs($object, $parameters);
	}
}
