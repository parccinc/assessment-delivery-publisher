<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Tests\Interfaces;

/**
 * Interface HttpRequest.
 *
 * @package PARCC\ADP\Tests
 */
interface HttpRequest
{
	/**
	 * @param  mixed $name
	 * @param  mixed $value
	 * @return mixed
	 */
	public function setOption($name, $value);

	public function execute();

	public function getError();

	/**
	 * @param  mixed $name
	 * @return mixed
	 */
	public function getInfo($name);

	public function close();
}
