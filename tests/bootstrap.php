<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

// Report on ALL Errors!
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


use Phalcon\Di;
use Phalcon\Di\FactoryDefault;

/**
 * Bootstrap initializes the Application and provides various Application Services which are all lazy loaded.
 *
 * @package PARCC\ADP
 * @version v1.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */

define('ROOT_PATH', __DIR__);
define('PATH_LIBRARY', __DIR__ . '/../app/library/');
define('PATH_SERVICES', __DIR__ . '/../app/services/');
define('PATH_RESOURCES', __DIR__ . '/../app/resources/');

set_include_path(
	ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// Required for Phalcon/Incubator.
include __DIR__ . '/../vendor/autoload.php';

// Use the application autoloader to autoload the classes.
// Autoload the dependencies found in composer.
$loader = new Phalcon\Loader();

$loader->registerDirs([ROOT_PATH]);

$loader->register();

$di = new FactoryDefault();
Di::reset();

// Add any needed services to the DI here.

Di::setDefault($di);
