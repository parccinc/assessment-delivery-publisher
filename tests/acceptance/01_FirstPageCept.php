<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Codeception acceptance test for testing TAO start page
 *
 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
 * @test
 */
$I = new AcceptanceTester($scenario);
$I->wantTo('1 - Run TAO start page test');

$I->comment("\n--- Hit TAO start page ---\n");
$I->amOnPage('index.php');
$I->see('tao');
$I->seeElement('#tao-main-logo');

$I->comment("\n--- All done ---\n");
?>
