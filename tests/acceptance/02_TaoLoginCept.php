<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Codeception acceptance test for testing TAO login
 *
 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
 * @test
 */
if (!defined('TAO_USERNAME')) { define('TAO_USERNAME', 'tao_admin'); }
if (!defined('TAO_PASSWORD')) { define('TAO_PASSWORD', 'tao_admin_pass'); }
if (!defined('TAO_NAME'))     { define('TAO_NAME', 'Vlad Karpenko'); }

$I = new AcceptanceTester($scenario);
$I->wantTo('2 - Run TAO User login test');
$I->amOnPage('index.php');

$I->comment("\n--- Trying to login  to TAO ---\n");
$I->see('Connect to the TAO platform');
$I->seeInCurrentUrl('login');
$I->fillField('login', TAO_USERNAME);
$I->fillField('password', TAO_PASSWORD);
$I->click('connect');

$I->comment("\n--- Looking for my UserName in the header ---\n");
$I->see(TAO_NAME, '.username');

$I->comment("\n--- All done ---\n");
?>
