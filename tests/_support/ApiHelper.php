<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace Codeception\Module;

class ApiHelper extends \Codeception\Module
{
	/**
	 * CONVERT KEY NAMES
	 * Converts key names from database field names to ADP-defined variable names.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param array $arr
	 * @param array $names
	 * @return array
	 */
	public function convertKeyNames(array $arr, array $names) {
		foreach($names as $oldKey => $newKey) {
			if(array_key_exists($oldKey, $arr)) {
				$arr[$newKey] = $arr[$oldKey];
				unset($arr[$oldKey]);
			}
		}
		return $arr;
	}

	/**
	 * GENERATE HEADERS
	 * Creates headers necessary for ADP calls.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $username
	 * @param string $password
	 * @param string $signature
	 * @param string $testKey
	 * @return array
	 */
	public function generateHeaders($username, $password, $signature, $testKey) {
		$headers = [];

		$headers['Authorization'] = 'Basic ' . base64_encode("$username:$password");
		$headers['Content-Type'] = 'application/json; charset=UTF-8';
		$headers['Authentication'] = $signature;
		$headers['X-Requested-With'] = base64_encode($testKey);

		return $headers;
	}

	/**
	 * MAKE JSON ASSERTIONS
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param ApiHelper $I
	 * @param array $params
	 * @param int $adpError
	 * @param int httpStatus
	 */
	public function makeAssertions($I, array $params = [], $adpError = null, $httpStatus = null) {
		// If schema is provided, the response is expected to be JSON.
		if(array_key_exists('schema', $params)) {
			if(!is_object($params['schema'])) {
				throw new \Exception("Couldn't load schema.");
			}

			$I->seeResponseIsJson();
			$response = json_decode($I->grabResponse());
			$jsonValidation = \Jsv4\Validator::validate($response, $params['schema']);
			$jsonError = $jsonValidation->valid ? "" : "JSON Error: '{$jsonValidation->errors[0]->message}' - Data path: '{$jsonValidation->errors[0]->dataPath}' - Schema path: '{$jsonValidation->errors[0]->schemaPath}'";

			$status =
				(property_exists($response, 'meta') && property_exists($response->meta, 'status')) ?
				$response->meta->status :
				null;

			// Make ADP error code assertions.
			$I->dontSeeHttpHeader('X-Error-Code');

			if (is_null($adpError)) {
				$I->assertEquals('success', strtolower($status), 'Expecting SUCCESS');
				$I->assertTrue((property_exists($response, 'result') && !property_exists($response->result, 'errorCode')) || !property_exists($response, 'result'));
				$I->assertTrue($jsonValidation->valid, $jsonError, 'Expecting no JSON validation errors.');
			} else {
				$I->assertTrue(property_exists($response, 'result') && property_exists($response->result, 'errorCode'), 'Asserting an error code exists.');
				$I->assertEquals("ADP-$adpError", $response->result->errorCode);
			}
		} elseif(array_key_exists('is_json', $params) && $params['is_json'] === true) {
			$I->seeResponseIsJson();
		} else {
			if(is_null($adpError)) {
				$I->dontSeeHttpHeader('X-Error-Code');
			} else {
				$I->assertEquals("ADP-$adpError", $I->grabHttpHeader('X-Error-Code'));
			}

			$containsAssertions = array_filter($params, function($key) {
				return preg_match("/contains/", $key);
			}, ARRAY_FILTER_USE_KEY);

			foreach($containsAssertions as $assertion) {
				$I->seeResponseContains($assertion);
			}
		}
		
		// Make HTTP status code assertion.
		$I->seeResponseCodeIs(is_null($httpStatus) ? HTTP_SUCCESS : intval($httpStatus));
	}

	/**
	 * get HMAC signature
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $apiUri
	 * @param string $secret
	 * @param
	 * @return string $signature
	 */
	public function getHmacSignature($apiUri, $secret, $microtime = null) {
		if(is_null($microtime)) {
			$microtime = round(microtime(true) * 1000);
		}

		$nonce = rand(0, 99999);
		$hash = $apiUri . round(($microtime / 1000)) . $nonce;

		// Authentication header value
		$signature = base64_encode($microtime . ':' . $nonce . ':' . base64_encode(hash_hmac('sha256', $hash, $secret)));

		return $signature;
	}

	/**
	 * is response in envelope format?
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param object ApiTester $I
	 * @return boolean $res
	 */
	public function findIfEnvelopeSet($I) {
		$response = $I->grabResponse();
		$res = false;

		if (stripos($response, '{"meta":') === true) {
			$res = true;
		}

		return $res;
	}

	/**
	 * convert any (envelope or not envelope mode) response to envelope array format
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param object ApiTester $I
	 * @param string returnFormat {'array' | 'json'} default 'array'
	 * @return {array | json} $normResponse
	 */
	public function normalizeResponse($I, $returnFormat = 'array') {
		$normResponse = [];

		// tricky way to convert CC response json into array
		$response = json_decode(json_encode(json_decode($I->grabResponse())), true);

		// envelope mode
		if (array_key_exists('meta', $response)) {
			$normResponse = $response;
		}
		// not envelope mode
		else {
			$normResponse = ['meta' => [], 'result' => $response];

			if (array_key_exists('errorCode', $response)) {
				$normResponse['meta']['status'] = 'ERROR';
			}
			else {
				$normResponse['meta']['status'] = 'SUCCESS';
			}

			$normResponse['meta']['count'] = count($response);
		}

		if ($returnFormat == 'json') {
			$normResponse = json_encode($normResponse);
		}

		return $normResponse;
	}

	/**
	 * validate response structure against supplied pattern
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param array $response
	 * @param array $pattern
	 * @return boolean {true | false} on {valid | error}
	 */
	public function validateResponse($response, $pattern) {
		$valid = false;

		if (!empty($response) && !empty($pattern)) {
			$pattern = $this->_fillKeysAssoc($pattern);
			$valid = !$this->_isKeyDiff($response, $pattern);
		}

		return $valid;
	}

	/**
	 * hidden utility function to recursively look
	 * if keys are differ in supplied arrays
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param array $test
	 * @param array $model
	 * @return boolean {true | false} on {differ | same}
	 */
	public function _isKeyDiff($test, $model) {
		$is = false;

		foreach ($model as $key => $value) {
            if (is_array($value) && isset($test[$key]) && is_array($test[$key])) {
				$is = $this->_isKeyDiff($test[$key], $value);

                if ($is) { break; }
            }
            else {
                if (!array_key_exists($key, $test)) {
                    $is = true;
                    break;
                }
            }

			unset($test[$key]);
		}

		if (!empty($test)) { $is = true; }

		return $is;
	}

	/**
	 * hidden utility function to recursively fill supplied array
	 * with passed filler value (default is empty string) using array elements as keys
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param array $array
	 * @param mixed $filler
	 * @return array $data
	 */
	public function _fillKeysAssoc($array, $filler = '') {
		$data = [];

		foreach ($array as $key => $value) {
			if (is_array($value)) {
				if (count($value) == count($value, COUNT_RECURSIVE)) {
					$data[$key] = array_fill_keys($value, $filler);
				}
				else {
					$data[$key] = $this->_fillKeysAssoc($value, $filler);
				}
			}
			else {
				$data[$value] = $filler;
			}
		}

		return $data;
	}

	/**
	 * Performs standard login of a student.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param ApiTester $I
	 * @param Object $testCase
	 * @return Object $student
	 */
	public function login($I, $testCase, $student) {
		$I->wantTo('Login Test Case ' . $testCase->tsid);

		// Combine data from the two tables into a request body, removing any null values.
		$data = array_filter([
			'testKey'		=> $testCase->test_key,
			'dateOfBirth'	=> $student->date_of_birth,
			'state'			=> $student->state,
			'studentId'		=> $student->personal_id
		], function($value) {
			return !is_null($value);
		});

		$headers = $I->generateHeaders(TD_USERNAME, TD_PASSWORD, $I->getHmacSignature(API_URI . API_LOGIN_URI, TD_SECRET), $data['testKey']);

		foreach ($headers as $name => $value) {
			$I->haveHttpHeader($name, $value);
		}

		// We must login to retrieve the token and test ID.
		$I->sendPOST(API_HOST . API_URI . API_LOGIN_URI, json_encode($data));
		$I->makeAssertions($I);
		return json_decode($I->grabResponse());
	}

	/**
	 * Returns a new Authentication token, replacing the specified component with the new value.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param String $token
	 * @param Int $index
	 * @param Mixed $value
	 * @return string
	 */
	public function replaceAuthenticationComponent($token, $index, $value) {
		$hmacKeys = explode(':', base64_decode($token));
		$hmacKeys[$index] = $value;
		return base64_encode(implode(':', $hmacKeys));
	}
}
