<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace Codeception\Module;

use PDO;

class DbHelper extends \Codeception\Module
{
	/**
	 * ACTIVATE/DEACTIVATE TEST SESSION
	 * Sets the 'active' flag in the test_session table.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param bool $activate
	 * @param string $testKey
	 * @return int | false (if failed)
	 */
	public function activateDeactivateTestSession($activate, $testKey) {
		$db = $this->getModule('Db')->dbh;
		return $db->exec("UPDATE `test_session` SET active = " . (int)$activate . " WHERE test_key = '$testKey' LIMIT 1");
	}

	/**
	 * COUNT RECORDS
	 * Counts the records in a table.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $table
	 * @return int
	 */
	public function countRecords($table) {
		$db = $this->getModule('Db')->dbh;
		$result = $db->query("SELECT COUNT(*) AS count FROM $table")->fetch(5); // 2 is PDO::FETCH_ASSOC - 5 is PDO::FETCH_OBJ
		return intval($result->count);
	}

	/**
	 * ERASE TOKEN
	 * Removes a test session's token.
	 *
	 * @param string $testKey
	 * @return int | false (if failed)
	 */
	public function eraseToken($testKey) {
		$db = $this->getModule('Db')->dbh;
		return $db->exec("UPDATE `test_session` SET token = NULL WHERE test_key = '$testKey' LIMIT 1");
	}

	/**
	 * Loads and executes SQL from test case file which sits in _data dir.
	 *
	 * author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $fileName
	 * @return boolean $result
	 */
	public function execSqlScript($fileName) {
		$result = 0;
		$file = substr(__DIR__, 0, strrpos(__DIR__, "\\")) . "\\_data\\" . $fileName;

		if (!empty($fileName) && file_exists($file)) {
			$sql = file_get_contents($file);
			$db = $this->getModule('Db')->dbh;
			$result = $db->exec($sql);
		}

		return $result;
	}

	/**
	 * Drops test case data tables
	 *
	 * author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string array $tables
	 * @return void
	 */
	public function dropTestCaseData(array $tables) {
		if (!empty($tables)) {
			$db = $this->getModule('Db')->dbh;

			foreach ($tables as $table) {
				$db->exec("DROP TABLE IF EXISTS $table");
			}
		}
	}

	/**
	 * EXPIRE TOKEN
	 * Sets a test_session's token expiration to one.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $testKey
	 * @return int | false (if failed)
	 */
	public function expireToken($testKey) {
		return $this->setTokenExpiration(1, $testKey);
	}

	/**
	 * RENEW TOKEN
	 * Sets a test_session's token expiration to be very high.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $testKey
	 * @return int | false (if failed)
	 */
	public function renewToken($testKey, $token = null, $tokenExpiration = null) {
		if(!is_null($token)) {
			$this->setToken($token, $testKey);
	}

		return $this->setTokenExpiration($tokenExpiration, $testKey);
	}

	/**
	 * GRAB ALL FROM DATABASE
	 * Grabs all records from a table.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $table
	 * @param int PDO Fetch Mode
	 * @return array[object]
	 */
	public function grabAllFromDatabase($table, $fetchMode = PDO::FETCH_OBJ) {
		$db = $this->getModule('Db')->dbh;
		return  $db->query("SELECT * FROM $table")->fetchAll($fetchMode); // 2 is PDO::FETCH_ASSOC - 5 is PDO::FETCH_OBJ
	}

	/**
	 * GRAB RECORD FROM DATABASE
	 * Grabs a record from a table with the given criteria in key-value pairs.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $table
	 * @param array $criteria
	 * @return object
	 */
	public function grabRecordFromDatabase($table, $criteria) {
		$db = $this->getModule('Db')->dbh;

		if(is_array($criteria) && !empty($criteria)) {
			$arr = [];
			foreach($criteria as $key => $value) {
				$arr[] = "$key=" . (intval($value) ? : "'$value'");
			}
			$string = implode(" AND ", $arr);
		} elseif(is_string($criteria) && strlen($criteria) > 0) {
			$string = $criteria;
		} else {
			return null;
		}

		return  $db->query("SELECT * FROM $table WHERE $string LIMIT 1")->fetch(5); // 2 is PDO::FETCH_ASSOC - 5 is PDO::FETCH_OBJ
	}

	/**
	 * RESET TEST SESSION
	 * Resets a test session's token and changes the status back to 'Scheduled'.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $testKey
	 * @return int | false (if failed)
	 */
	public function resetTestSession($testKey) {
		if($this->eraseToken($testKey) !== false && $this->activateDeactivateTestSession(true, $testKey) !== false) {
			return $this->setTestStatus('Scheduled', $testKey);
		} else {
			return false;
		}
	}

	/**
	 * SET TEST STATUS
	 * Sets the status in the 'test_session' table for the given test key.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $status
	 * @param string $testKey
	 * @return int | false (if failed)
	 */
	public function setTestStatus($status, $testKey) {
		$db = $this->getModule('Db')->dbh;
		return $db->exec("UPDATE `test_session` SET status = '$status' WHERE test_key = '$testKey' LIMIT 1");
	}

	/**
	 * SHOW/HIDE LOGIN CONFIGURATION
	 * Shows or hides the loginConfiguration record in the `system` table.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $showHide
	 * @return int | false (if failed)
	 */
	public function showHideLoginConfiguration($showHide) {
		$newName = ($showHide === 'show' ? 'loginConfiguration' : 'hiddenLoginConfiguration');
		$oldName = ($showHide === 'show' ? 'hiddenLoginConfiguration' : 'loginConfiguration');

		$db = $this->getModule('Db')->dbh;
		return $db->exec("UPDATE `system` SET name = '$newName' WHERE name = '$oldName' LIMIT 1");
	}

	/**
	 * UPDATE FIELD IN DATABASE
	 * Updates a field in the given table.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $table
	 * @param array | string $criteria
	 * @param string $field
	 * @param mixed $newValue
	 * @return int
	 */
	public function updateFieldInDatabase($table, $criteria, $field, $newValue) {
		$db = $this->getModule('Db')->dbh;

		if(is_array($criteria) && !empty($criteria)) {
			$arr = [];
			foreach($criteria as $key => $value) {
				$arr[] = "$key=" . (intval($value) ? : "'$value'");
			}
			$string = implode(" AND ", $arr);
		} elseif(is_string($criteria) && strlen($criteria) > 0) {
			$string = $criteria;
		} else {
			return null;
		}

		$newValue = is_string($newValue) ? "'$newValue'" : $newValue;

		return  $db->exec("UPDATE $table SET $field = $newValue WHERE $string LIMIT 1");
	}

	private function setToken($token, $testKey) {
		$db = $this->getModule('Db')->dbh;
		return $db->exec("UPDATE `test_session` SET token = '$token' WHERE test_key = '$testKey' LIMIT 1");
	}

	private function setTokenExpiration($timestamp, $testKey) {
		if(!is_int($timestamp) && !is_string($testKey) && !is_float($timestamp)) {
			return false;
		}

		$db = $this->getModule('Db')->dbh;
		return $db->exec("UPDATE `test_session` SET token_expiration = $timestamp WHERE test_key = '$testKey' LIMIT 1");
	}
}
