<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

return serialize([
	'USERNAME'			=> 'TestTD',
	'PASSWORD'			=> '8c1e3e155057f95c0802e025ef84faa4',
	'SECRET'			=> '82cc94f866eb06b64ff535254ec42173',
	
	'nameConversion'	=> [
		'test_key'		=> 'testKey',
		'personal_id'	=> 'studentId',
		'date_of_birth'	=> 'dateOfBirth'
	]
]);