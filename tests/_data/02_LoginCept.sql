--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright © 2015 Breakthrough Technologies, LLC
--

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.0.0.4865
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table adp_tds_test.test_session_tc
DROP TABLE IF EXISTS `test_session_tc`;
CREATE TABLE IF NOT EXISTS `test_session_tc` (
  `ADP_ERROR` int(11) DEFAULT NULL,
  `HTTP_STATUS` int(11) DEFAULT NULL,
  `tsid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique Test Battery Form Revision Session’s ID generated during Test Battery Form Assignment Import.',
  `fk_student_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key: Unique Student’s ID that the Test Battery Form Revision Session belongs to. It is created during Test Battery Form Assignment Import.',
  `test_key` char(10) COLLATE utf8_bin DEFAULT NULL COMMENT 'Unique Test Battery Form Revision Session’s Test Key used to identify particular Test Battery Form Revision Assignment. Practice Tests and Quizzes use a predefined pattern (i.e. PRACT-XXXXX) which indicates to Test Driver the type of the test being launched! Test Battery Form Revision Session’s Test Key must have 10 alpha only uppercase characters! These are generated outside of the System and are imported either during Test Battery Form Revision Publishing of Practice Tests and Quizzes, or during Test Battery Form Assignment Import of all other types of Tests!',
  PRIMARY KEY (`tsid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Battery Form Revision Session details of assigned Test Battery Form Revision to a particular Student.';

-- Dumping data for table adp_tds_test.test_session_tc: ~14 rows (approximately)
/*!40000 ALTER TABLE `test_session_tc` DISABLE KEYS */;
INSERT INTO `test_session_tc` (`ADP_ERROR`, `HTTP_STATUS`, `tsid`, `fk_student_id`, `test_key`) VALUES
	(NULL, NULL, 1, 1, 'AAAAAAAAAA'),
	(1200, 401, 2, 1, 'AAAAAAAAAA'),
	(1201, 401, 3, 1, 'AAAAAAAAAA'),
	(1202, 401, 4, 1, 'AAAAAAAAAA'),
	(1203, 401, 5, 1, 'AAAAAAAAAA'),
	(1204, 401, 6, 1, 'AAAAAAAAAA'),
	(1205, 401, 7, 1, 'AAAAAAAAAA'),
	(1206, 401, 8, 1, 'AAAAAAAAAA'),
	(1207, 401, 9, 1, 'AAAAAAAAAA'),
	(1208, 500, 10, 1, 'JGGJBPBOBR'),
	(1209, 500, 11, 1, 'MCNAVKNWOO'),
	(1210, 401, 12, 1, 'AAAAAAAAAA'),
	(1211, 401, 13, 1, 'AAAAAAAAAA'),
	(1212, 401, 14, 38, 'BABABABABA'),
	(1213, 401, 15, 1, 'AAAAAAAAAA'),
	(1214, 401, 16, 1, 'AAAAAAAAAA'),
	(1215, 401, 17, 38, 'BABABABABA'),
	(1216, 403, 19, 5, 'FAFAFAFCDA'),
	(1217, 401, 20, 1, 'AAAAAAAAAA'),
	(1218, 500, 21, 1, 'GODKENDALL'),
	(1219, 403, 22, 1, 'CDCDCDCDCD'),
-- 	(1220, 500, 23, 1, 'YESKENDALL'), -- Unreachable error: The check for 'testId' (ADP-1219) must come after the
-- check for 'testForm' (ADP-1221), as the test must be retrieved in order to retrieve the form.
	(1221, 403, 24, 1, 'CDCDCDCDCE'),
	(1222, 500, 25, 1, 'ABCDABCDAB'),
	(1223, 500, 26, 1, 'XDXDXDXDXD'),
	(1224, 403, 27, 1, 'XDXDXDXDXE'),
	(1225, 403, 28, 1, 'AAAAAAAAAA'),
	(1226, 403, 29, 1, 'AAAAAAAAAA'),
	(1227, 403, 30, 1, 'AAAAAAAAAA'),
	(1228, 403, 31, 1, 'AAAAAAAAAA'),
	(1229, 403, 32, 1, 'AAAAAAAAAA')
-- 	(1230, 500, 33, 1, 'AAAAAAAAAA') -- Can't force database query to fail.
;
/*!40000 ALTER TABLE `test_session_tc` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
