--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright © 2015 Breakthrough Technologies, LLC
--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table adp_tds_test.test_session_tc
CREATE TABLE IF NOT EXISTS `test_session_tc` (
  `ADP_ERROR` int(11) DEFAULT NULL,
  `HTTP_STATUS` int(11) DEFAULT NULL,
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique Test Battery’s ID generated during Test
    Battery Form Revision Publishing.',
  `batteryId` varchar(26) DEFAULT NULL COMMENT 'External Unique Test Battery’s ID used only for manual
  cross-linking of the data with the external Authoring System.',
  `name` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Test Battery’s Name.' COLLATE 'utf8_bin',
	`program` ENUM('Diagnostic Assessment','K2 Formative','Mid-Year/Interim','Practice Test','Quiz Test','Speaking &
	Listening') NULL DEFAULT NULL COMMENT 'Test Battery’s Program. Practice Tests and Quizzes are all assigned to and
	taken by the Anonymous Student!' COLLATE 'utf8_bin',
	`scoreReport` ENUM('None','Generic','ELA Decoding','ELA Reader Motivation Survey','ELA Reader Comprehension',
	'ELA Vocabulary','Math Comprehension','Math Fluency') NULL DEFAULT NULL COMMENT 'Type of Score Report being
	generated at the end of Test Battery’s Test. This affects the structure of Test Package and once first Test Package
	is generated it cannot be changed anymore!' COLLATE 'utf8_bin',
	`subject` ENUM('ELA','Math','N/A') NULL DEFAULT NULL COMMENT 'Test Battery’s Subject.' COLLATE 'utf8_bin',
	`grade` VARCHAR(40) NULL DEFAULT NULL COMMENT 'Test
	Battery’s Grade.' COLLATE 'utf8_bin',
	`itemSelectionAlgorithm` ENUM('Fixed','Adaptive') NULL DEFAULT NULL COMMENT 'Test Battery’s Item Selection
	Algorithm. Fixed applies to Fixed Form Tests and delivers Items in the order they are configured within a Test
	whereas Adaptive Engine determines based on previous answers what Item should be delivered from the available Item
	 Pool.' COLLATE 'utf8_bin',
	`security` ENUM('Non-Secure','Secure') NULL DEFAULT NULL COMMENT 'Indicator if Secure Browser is required for
	delivery of all Test Battery Form Revisions of the Test Battery. ’Non-Secure’ Test Batteries can be delivered in
	any Browser, whereas ’Secure’ can only be delivered inside the Secure Browser!' COLLATE 'utf8_bin',
	`multimedia` ENUM('OnDemand','PreDownload','Embedded') NULL DEFAULT NULL COMMENT 'Test Battery’s Multimedia
	Delivery Method. This indicates when/how Test Battery Form Revision Content is being downloaded. ’OnDemand’ is the
	default method and each File Resource is downloaded only if/when needed. ’PreDownload’ method requires all File
	Resources to be pre-downloaded before the Test is started. ’Embedded’ method is similar to ’PreDownload’ method
	with a difference that all File Resources are embedded within the Test Definition JSON file and is limited to
	images only.' COLLATE 'utf8_bin',
	`scoring` ENUM('Immediate','Delayed') NULL DEFAULT NULL COMMENT 'Test Battery’s Scoring Methods. ’Immediate’ means
	 that Test Driver will perform all required Scoring for the Test Battery. ’Delayed’ means that Test Driver will
	 perform as much as possible of automated Scoring, but some additional Scoring is required after Test Delivery
	 (this may be either Automated Delayed Scoring, Hand Scoring, or combination of both).' COLLATE 'utf8_bin',
	`permissions` ENUM('Non-Restricted','Restricted') NULL DEFAULT NULL COMMENT 'Indicator if Test Battery requires
	additional Permissions for Test Battery Form Assignments. ’Non-Restricted’ means that the Test Battery is available
	to all Rostering Users and all of them can create Test Battery Form Assignments for the related Test Battery.
	’Restricted’ on the other hand requires special permissions, and only Rostering Users with such permissions can
	create Test Battery Form Assignments for the related Test Battery. This Indicator is only used by external
	Rostering System and does not affect System in any way.' COLLATE 'utf8_bin',
	`privacy` ENUM('Public','Private') NULL DEFAULT NULL COMMENT 'Indicator if Test Battery’s Content has special
	Privacy, License, Copyright or any other restrictions. This Indicator is only used by external Rostering System
	and does not affect System in any way.' COLLATE 'utf8_bin',
	`description` VARCHAR(4096) NULL DEFAULT NULL COMMENT 'Test Battery’s Description.' COLLATE 'utf8_bin',
	`isActive` VARCHAR (10) NULL COMMENT 'Indicator if Test Battery is Active. Inactive Test Battery is not
	allowed to be taken, or assigned!',

  PRIMARY KEY (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Battery details.';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Dumping data for table adp_tds_test.test_session_tc: ~5 rows (approximately)
/*!40000 ALTER TABLE `test_session_tc` DISABLE KEYS */;
INSERT INTO `test_session_tc` (`ADP_ERROR`, `HTTP_STATUS`, `tid`, `batteryId`, `name`, `program`, `scoreReport`,
`subject`, `grade`, `itemSelectionAlgorithm`, `security`, `multimedia`, `scoring`, `permissions`, `privacy`,
 `description`, `isActive`) VALUES
	(1765, 403, 9, 'i14459635542695946', NULL, 'Diagnostic Assessment', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL),
	(1765, 403, 11, 'i14459635542676546', NULL, NULL, 'Math Comprehension', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL,
	 NULL, NULL),
	(1765, 403, 12, 'i14459635542695876', NULL, NULL, NULL, NULL, NULL, 'Fixed',  NULL, NULL, NULL, NULL, NULL, NULL,
	NULL),
	(1763, 404, 13, 'i14555444333222111', 'Practice Test 3', 'Practice Test', NULL, NULL, NULL, 'Fixed',  NULL, NULL,
	NULL, NULL, NULL, NULL,	NULL),
	(1764, 403, 14, 'i14459635542695444', 'Practice Test 4', NULL, NULL, NULL, NULL, NULL,  NULL, NULL, NULL, NULL, NULL,
	NULL,	'blabla'),
	(1766, 403, 15, 'i14459635542695333', 'Practice Test 5', NULL, NULL, NULL, 'NewGrade', NULL,  NULL, NULL, NULL,
	NULL,	NULL, NULL,	NULL),
	(1708, 403, 16, 'i14459635542695222', 'Practice Test 5', NULL, NULL, NULL, NULL, NULL,  NULL, NULL, NULL,
	NULL,	NULL, NULL,	NULL),
	(NULL, NULL, 17, 'i14459635542695111', 'Practice Test 8', NULL, NULL, 'Math', '1', NULL,
	'Secure', 'OnDemand', 'Immediate', 'Restricted', 'Public', 'This is a description', 'false'),
	(1761, 403, 18, NULL, 'Practice Test 5', NULL, NULL, NULL, NULL, NULL,  NULL, NULL, NULL,
	NULL,	NULL, NULL,	NULL),
	(1762, 403, 19, 'q14459635542695111', 'Practice Test 5', NULL, NULL, NULL, NULL, NULL,  NULL, NULL, NULL,
	NULL,	NULL, NULL,	NULL)
	;


	--,
	--(1767, 403, 20, 'i14459635542695000', 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
	-- Lorem Ipsum Lorem Ipsum Lorem Ipsum', NULL, NULL, NULL, NULL, NULL,  NULL, NULL, NULL, NULL,	NULL, NULL,	NULL)
	-- --validation works manually but does not work with API Test.



