--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright © 2015 Breakthrough Technologies, LLC
--

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.0.0.4865
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table adp_tds_test.test_session_tc
DROP TABLE IF EXISTS `test_session_tc`;
CREATE TABLE IF NOT EXISTS `test_session_tc` (
  `ADP_ERROR` int(11) DEFAULT NULL,
  `HTTP_STATUS` int(11) DEFAULT NULL,
  `tsid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique Test Battery Form Revision Session’s ID generated during Test Battery Form Assignment Import.',
  `fk_student_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key: Unique Student’s ID that the Test Battery Form Revision Session belongs to. It is created during Test Battery Form Assignment Import.',
  `test_key` char(10) COLLATE utf8_bin DEFAULT NULL COMMENT 'Unique Test Battery Form Revision Session’s Test Key used to identify particular Test Battery Form Revision Assignment. Practice Tests and Quizzes use a predefined pattern (i.e. PRACT-XXXXX) which indicates to Test Driver the type of the test being launched! Test Battery Form Revision Session’s Test Key must have 10 alpha only uppercase characters! These are generated outside of the System and are imported either during Test Battery Form Revision Publishing of Practice Tests and Quizzes, or during Test Battery Form Assignment Import of all other types of Tests!',
  PRIMARY KEY (`tsid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Battery Form Revision Session details of assigned Test Battery Form Revision to a particular Student.';

-- Dumping data for table adp_tds_test.test_session_tc: ~14 rows (approximately)
/*!40000 ALTER TABLE `test_session_tc` DISABLE KEYS */;
INSERT INTO `test_session_tc` (`ADP_ERROR`, `HTTP_STATUS`, `tsid`, `fk_student_id`, `test_key`) VALUES
-- 	(1000, 503, 1, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1001, 503, 2, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1002, 200, 3, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1003, 503, 4, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1004, 503, 5, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1005, 503, 6, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1006, 503, 7, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
-- 	(1007, 401, 8, 1, 'AAAAAAAAAA'), -- Response contains additional HTML, caused by a Phalcon error.
-- 	(1008, 401, 9, 1, 'AAAAAAAAAA'), -- Response contains additional HTML, caused by a Phalcon error.
	(1009, 401, 10, 1, 'AAAAAAAAAA'),
-- 	(1010, 401, 11, 1, 'AAAAAAAAAA'), -- Can't force returned User to be invalid JSON
	(1011, 401, 12, 1, 'AAAAAAAAAA'),
	(1012, 401, 13, 1, 'AAAAAAAAAA'),
	(1013, 401, 14, 1, 'AAAAAAAAAA'),
	(1014, 401, 15, 1, 'AAAAAAAAAA'),
	(1015, 401, 16, 1, 'AAAAAAAAAA'),
	(1016, 401, 17, 1, 'AAAAAAAAAA'),
	(1017, 401, 18, 1, 'AAAAAAAAAA'),
	(1018, 401, 19, 1, 'AAAAAAAAAA'),
	(1019, 408, 20, 1, 'AAAAAAAAAA'),
# 	(1060, 503, 21, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
# 	(1061, 200, 22, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
# 	(1062, 503, 23, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
# 	(1063, 503, 24, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
# 	(1064, 503, 25, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
# 	(1065, 503, 26, 1, 'AAAAAAAAAA'), -- Can't force maintenance mode to be enabled
	(1066, 404, 27, 1, 'AAAAAAAAAA'),
	(1067, 200, 28, 1, 'AAAAAAAAAA'),
	(1068, 404, 29, 1, 'AAAAAAAAAA'),
	(1069, 404, 30, 1, 'AAAAAAAAAA'),
	(1070, 404, 31, 1, 'AAAAAAAAAA'),
	(1071, 404, 32, 1, 'AAAAAAAAAA'),
# 	(1080, 503, 33, 1, 'AAAAAAAAAA'), -- Can't force a database connection error.
# 	(1080, 500, 34, 1, 'AAAAAAAAAA'), -- Can't force a MySQL error.
# 	(1100, 403, 35, 1, 'AAAAAAAAAA'), -- ADP config must be modified to test this.
# 	(1101, 403, 36, 1, 'AAAAAAAAAA'), -- ADP config must be modified to test this.
# 	(1102, 500, 37, 1, 'AAAAAAAAAA'), -- Not sure how to test this.
# 	(1103, 500, 38, 1, 'AAAAAAAAAA'), -- Not sure how to test this.
# 	(1104, 403, 39, 1, 'AAAAAAAAAA'), -- Not sure how to test this.
# 	(1105, 403, 40, 1, 'AAAAAAAAAA'), -- Not sure how to test this.
	(1106, 400, 41, 1, 'AAAAAAAAAA'),
	(1107, 401, 42, 1, 'AAAAAAAAAA'),
	(1108, 401, 43, 1, 'AAAAAAAAAA'),
	(1109, 403, 44, 1, 'KPAAAAAAKP')
# 	(1110, 500, 45, 1, 'AAAAAAAAAA'), -- Untestable.
;
