--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright © 2015 Breakthrough Technologies, LLC
--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table adp_tds_test.test_session_tc
CREATE TABLE IF NOT EXISTS `test_session_tc` (
  `ADP_ERROR` int(11) DEFAULT NULL,
  `HTTP_STATUS` int(11) DEFAULT NULL,
  `tsid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique Test Battery Form Revision Session’s ID generated during Test Battery Form Assignment Import.',
  `fk_student_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key: Unique Student’s ID that the Test Battery Form Revision Session belongs to. It is created during Test Battery Form Assignment Import.',
  `test_key` char(10) COLLATE utf8_bin DEFAULT NULL COMMENT 'Unique Test Battery Form Revision Session’s Test Key used to identify particular Test Battery Form Revision Assignment. Practice Tests and Quizzes use a predefined pattern (i.e. PRACT-XXXXX) which indicates to Test Driver the type of the test being launched! Test Battery Form Revision Session’s Test Key must have 10 alpha only uppercase characters! These are generated outside of the System and are imported either during Test Battery Form Revision Publishing of Practice Tests and Quizzes, or during Test Battery Form Assignment Import of all other types of Tests!',
  PRIMARY KEY (`tsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Battery Form Revision Session details of assigned Test Battery Form Revision to a particular Student.';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Dumping data for table adp_tds_test.test_session_tc: ~5 rows (approximately)
/*!40000 ALTER TABLE `test_session_tc` DISABLE KEYS */;
INSERT INTO `test_session_tc` (`ADP_ERROR`, `HTTP_STATUS`, `tsid`, `fk_student_id`, `test_key`) VALUES
	(NULL, NULL, 1, 1, 'AAAAAAAAAB'),
	(NULL, NULL, 2, 1, 'AAAAAAAAAD'),
-- 	(1901, 500, 3, 1, 'AAAAAAAAAB'), -- Skipped. Unable to force ADP to fail saving job.
	(1902, 403, 4, 1, 'AAAAAAAAAB'),
	(1903, 404, 5, 1, 'AAAAAAAAAB'),
	(1904, 403, 6, 1, 'BABABABABA'),
	(1911, 404, 7, 1, 'ZVWEWDZEXN'), -- Documentation should read "Returns JSON error in case Test Session status is
	-- 'Completed'".
	(1912, 403, 8, 1, 'NRUOLRKJFT'),
	(1913, 404, 9, 1, 'AAAAAAAAAC')
;
