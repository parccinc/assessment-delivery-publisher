--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright © 2015 Breakthrough Technologies, LLC
--

USE `adp_tds_test`;

CREATE TABLE IF NOT EXISTS `test_publish_test` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `revisionId` varchar(20) DEFAULT '',
  `publishedBy` varchar(20) DEFAULT '',
  `http_code` int(10) unsigned DEFAULT NULL,
  `adp_code` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`tid`));

INSERT INTO `test_publish_test` (`tid`, `revisionId`, `publishedBy`, `http_code`, `adp_code`) VALUES
	(1, 'i1444333461941154', 'acr_user', 200, null);
