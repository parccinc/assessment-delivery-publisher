--
-- This file is part of ADP.
--
-- ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
--
-- ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with ADP. If not, see
-- <http://www.gnu.org/licenses/>.
--
-- Copyright © 2015 Breakthrough Technologies, LLC
--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table adp_tds_test.test_session_tc
CREATE TABLE IF NOT EXISTS `test_session_tc` (
  `UPDATE` char(10) DEFAULT NULL,
  `ADP_ERROR` int(11) DEFAULT NULL,
  `HTTP_STATUS` int(11) DEFAULT NULL,
  `tsid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique Test Battery Form Revision Session’s ID generated during Test Battery Form Assignment Import.',
  `fk_student_id` int(10) unsigned DEFAULT NULL COMMENT 'Foreign Key: Unique Student’s ID that the Test Battery Form Revision Session belongs to. It is created during Test Battery Form Assignment Import.',
  `test_key` char(10) COLLATE utf8_bin DEFAULT NULL COMMENT 'Unique Test Battery Form Revision Session’s Test Key used to identify particular Test Battery Form Revision Assignment. Practice Tests and Quizzes use a predefined pattern (i.e. PRACT-XXXXX) which indicates to Test Driver the type of the test being launched! Test Battery Form Revision Session’s Test Key must have 10 alpha only uppercase characters! These are generated outside of the System and are imported either during Test Battery Form Revision Publishing of Practice Tests and Quizzes, or during Test Battery Form Assignment Import of all other types of Tests!',
  PRIMARY KEY (`tsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Test Battery Form Revision Session details of assigned Test Battery Form Revision to a particular Student.';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Dumping data for table adp_tds_test.test_session_tc: ~5 rows (approximately)
/*!40000 ALTER TABLE `test_session_tc` DISABLE KEYS */;
INSERT INTO `test_session_tc` (`UPDATE`, `ADP_ERROR`, `HTTP_STATUS`, `tsid`, `fk_student_id`, `test_key`) VALUES
	('status',		NULL, NULL, 1, 1, 'ABCDABCDEF'),
	('student',		NULL, NULL, 2, 1, 'ABCDABCDEF'),
	('test',		NULL, NULL, 3, 1, 'ABCDABCDEF'),
	('everything',	NULL, NULL, 4, 1, 'ABCDABCDEF'),
	('status', 1803, 412, 7, 1, 'ABCDABCDEF'),
	('status', 1804, 412, 8, 1, 'ABCDABCDEF'),
	('status', 1805, 412, 9, 1, 'ABCDABCDEF'),
	('status', 1806, 403, 10, 1, 'ABCDABCDEF'),
	('status', 1807, 404, 11, 1, 'ABCDABCDEF'),
-- 	('status', 1809, 412, 13, 1, 'ABCDABCDEF') -- Skipped. Unsure how to test this.
	('student', 1810, 412, 14, 1, 'ABCDABCDEF'),
	('student', 1811, 403, 16, 1, 'ABCDABCDEF'),
	('student', 1812, 403, 17, 1, 'ABCDABCDEF'),
	('student', 1813, 403, 18, 1, 'ABCDABCDEF'),
	('student', 1814, 403, 19, 1, 'ABCDABCDEF'),
	('student', 1815, 403, 20, 1, 'ABCDABCDEF'),
	('student', 1816, 403, 21, 1, 'ABCDABCDEF'),
	('student', 1817, 403, 22, 1, 'ABCDABCDEF'),
	('student', 1818, 403, 23, 1, 'ABCDABCDEF'),
	('status', 1819, 404, 24, 1, 'ABCDABCDEF'),
	('test', 1820, 412, 25, 1, 'ABCDABCDEF'),
	('test', 1821, 403, 26, 1, 'ABCDABCDEF'),
	('test', 1822, 403, 27, 1, 'ABCDABCDEF'),
	('test', 1823, 403, 28, 1, 'ABCDABCDEF'),
	('test', 1824, 412, 29, 1, 'ABCDABCDEF'),
	('test', 1825, 404, 30, 1, 'ABCDABCDEF'),
-- 	('test', 1826, 412, 31, 1, 'ABCDABCDEF'), -- Skipped. Does not exist.
	('test', 1827, 500, 32, 1, 'ABCDABCDEF'),
--	('status', 1828, 412, 33, 1, 'ABCDABCDEF'), -- Unreachable error. Gets picked up by ADP-1819
-- 	('status', 1829, 400, 34, 1, 'ABCDABCDEF'), -- Skipped. Does not exist. Line 349 should be updated to remove this code from its reference.
	('test', 1830, 403, 35, 1, 'ABCDABCDEF'),
	('test', 1831, 403, 36, 1, 'ABCDABCDEF'),
	('test', 1832, 403, 37, 1, 'ABCDABCDEF')
;
