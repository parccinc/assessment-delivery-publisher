<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Common API tests constants
 *
 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
 */

// HTTP status codes used by Web services
define('HTTP_SUCCESS',               200);
define('HTTP_REDIRECT',              302);
define('HTTP_BAD_REQUEST',           400);
define('HTTP_UNAUTHORIZED',          401);
define('HTTP_FORBIDDEN',             403);
define('HTTP_NOT_FOUND',             404);
define('HTTP_REQUEST_TIMEOUT',       408);
define('HTTP_CONFLICT',              409);
define('HTTP_PRECONDITION_FAILED',   412);
define('HTTP_INTERNAL_SERVER_ERROR', 500);
define('HTTP_SERVICE_UNAVAILABLE',   503);

// API URIs parts
define('API_HOST',           'https://ps.parcc.dev');
define('API_URI',            '/api');
define('API_LOGIN_URI',      '/login');
define('API_RESULTS_URI',    '/results');
define('API_TEST_PROPERTIES_URI',    '/test/properties');
define('API_ASSESSMENT_URI', '/assessment');
define('API_CONTENT_URI',    '/content');
define('API_PUBLISH_URI',    '/test');
define('API_ASSIGNMENT_URI', '/assignment');

// Authentication/authorization/login data
define('DOCUMENTATION_USERNAME', 'DocumentationACR');
define('DOCUMENTATION_PASSWORD',  '635fbb063cbc28e8bf03ed12ca4cca00');
define('DOCUMENTATION_SECRET',    '51f48327961f6cb0da7d584a664cafba');

define('ACR_USERNAME',	'TestACR');
define('ACR_PASSWORD',	'caec1256e3127d94cd9859b5f79308f0');
define('ACR_SECRET',	'f43ee8c76e7ed03e6316705c48c21592');

define('LDR_USERNAME', 'DocumentationLDR');
define('LDR_PASSWORD',  '97f0ca00610888a0e9e58883dabc813b');
define('LDR_SECRET',    '6d393cf3b856c34ee5c55f7c50dfeb49');

define('TD_USERNAME',	'TestTD');
define('TD_PASSWORD',	'8c1e3e155057f95c0802e025ef84faa4');
define('TD_SECRET',		'82cc94f866eb06b64ff535254ec42173');
