<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

//use \ApiTester;

class ApiCest {
	protected $config;
	protected $schemas;
	protected $codes;

	public function __construct() {
		// Load config
		$this->config = unserialize(require __DIR__ . '/../_support/CCHelpers/config.php');

		// Load schemas
		$this->loadSchemas();

		$this->codes = new stdClass();
	}

	public function _before(ApiTester $I) {
		// Reset database
		$I->execSqlScript("adp_tds-dump.sql");
	}

	public function _after(ApiTester $I) {
	}

	public function RunGenericErrorsTest(ApiTester $I) {
		$loginUri = API_URI . API_LOGIN_URI;

		$I->execSqlScript("01_GenericCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo("Test Generic Error " . $testCase->tsid);
			$student = $I->grabRecordFromDatabase('student', ['sid' => $testCase->fk_student_id]);

			// Combine data from the two tables into a request body, removing any null values.
			$data = array_filter([
					'testKey'		=> $testCase->test_key,
					'dateOfBirth'	=> $student->date_of_birth,
					'state'		=> $student->state,
					'studentId'	=> $student->personal_id
			], function($value) {
				return !is_null($value);
			});

			$username = TD_USERNAME;
			$password = TD_PASSWORD;
			$secret = TD_SECRET;
			$signature = $I->getHmacSignature($loginUri, $secret);
			$requestUri = API_HOST . $loginUri;
			$assertionParams = ['schema' => $this->schemas->get('response_schema')];

			// Make header changes based on test case.
			switch($testCase->ADP_ERROR) {
				case '1007':
					$username = '';
					break;
				case '1008':
					$username = 'doesNotExist';
					break;
				case '1009':
					$username = 'APITest';
					break;
				case '1010':
					$password = '';
					break;
				case '1011':
					$password = 'invalidPasswordLength';
					break;
				case '1012':
					$password = '32characterbutincorrectpassword0';
					break;
				case '1013':
					$signature = '';
					break;
				case '1014':
					$signature = $I->replaceAuthenticationComponent($signature, 2, 'aaa:fourthKey');
					break;
				case '1015':
					$signature = $I->replaceAuthenticationComponent($signature, 0, '321');
					break;
				case '1016':
					$signature = $I->replaceAuthenticationComponent($signature, 1, '123456');
					break;
				case '1017':
					$signature = $I->replaceAuthenticationComponent($signature, 2, 'ABC');
					break;
				case '1018':
					$signature = $I->replaceAuthenticationComponent($signature, 2, $I->getHmacSignature('something', $secret));
					break;
				case '1019':
					$microtime = round(microtime(true) * 1000 - (1000 * 60 * 60));
					$signature = $I->getHmacSignature($loginUri, $secret, $microtime);
					break;
				case '1066':
					$requestUri .= '/something';
					break;
				case '1067':
					$requestUri .= '/something';
					$assertionParams = [];
					break;
				case '1068':
					$requestUri = API_HOST . API_URI . API_CONTENT_URI . '/something';
					$assertionParams = [];
					break;
				case '1069':
					$requestUri = API_HOST . API_URI . API_RESULTS_URI . '/something';
					break;
				case '1070':
					$assertionParams = ['contains_1' =>
							'<body><div><div><div><h1>Oops!</h1><h2>404 Not Found</h2>
Sorry, requested page was not found.</div></div></div></body>'];
					break;
				case '1071':
					$assertionParams = [];
					break;
				case '1108':
					$data['testKey'] = 'ABCKENDALL';
					break;
			}

			$headers = $I->generateHeaders($username, $password, $signature, $data['testKey']);

			foreach ($headers as $name => $value) {
				if($testCase->ADP_ERROR === '1107' && $name === 'X-Requested-With') {
					$I->haveHttpHeader($name, 'numb3r2');
				} else {
					$I->haveHttpHeader($name, $value);
				}
			}

			$data = json_encode($data);

			if($testCase->ADP_ERROR === '1106') {
				$data .= 'something';
			}

			switch($testCase->ADP_ERROR) {
				case '1067':
					$I->sendOPTIONS($requestUri, $data);
					break;
				case '1068':
				case '1069':
				case '1070':
					$I->sendGET($requestUri);
					break;
				case '1071':
					$I->sendHEAD($requestUri);
					break;
				default:
					// Login
					$I->sendPOST($requestUri, $data);
					break;
			}

			$I->makeAssertions($I, $assertionParams, $testCase->ADP_ERROR, $testCase->HTTP_STATUS);

			$data = json_decode($data, true);
			if(!is_null($data) && array_key_exists('testKey', $data)) {
				$I->comment("Resetting token: " . $I->resetTestSession($data['testKey']));
			}
		}
	}

	public function RunLoginTest(ApiTester $I) {
		$loginUri = API_URI . API_LOGIN_URI;

		// Setup DB
		$I->execSqlScript("02_LoginCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Authenticate User ' . $testCase->tsid);

			$student = $I->grabRecordFromDatabase('student', ['sid' => $testCase->fk_student_id]);

			// Combine data from the two tables into a request body, removing any null values.
			$data = array_filter([
					'testKey'		=> $testCase->test_key,
					'dateOfBirth'	=> $student->date_of_birth,
					'state'			=> $student->state,
					'studentId'		=> $student->personal_id
			], function($value) {
				return !is_null($value);
			});

			if($student->state === 'AR') {
				$data['studentId'] = '';
				$data['dateOfBirth'] = '';
			}

			$headers = $I->generateHeaders(TD_USERNAME, TD_PASSWORD, $I->getHmacSignature($loginUri, TD_SECRET), $data['testKey']);

			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			$this->loadStatusCodes($testCase);

			switch($this->codes->adp) {
				case '1200':
					$data['testKey'] = '';
					break;
				case '1201':
					unset($data['state']);
					break;
				case '1202':
					unset($data['studentId']);
					break;
				case '1203':
					unset($data['dateOfBirth']);
					break;
				case '1204':
					$data['testKey'] = '~ Wild and tilde ~';
					break;
				case '1205':
					$data['state'] = 'XR';
					break;
				case '1206':
					$data['testKey'] = 'BBBBBBBBBB';
					break;
				case '1207':
					$data['state'] = 'OK';
					break;
				case '1210':
					$data['studentId'] = '1234567890123456789012345678901';
					break;
				case '1211':
					$data['studentId'] = $data['studentId'] . '1';
					break;
				case '1212':
					$data['studentId'] = $student->personal_id;
					break;
				case '1213':
					$data['dateOfBirth'] = '02-18-1987';
					break;
				case '1214':
				case '1215':
					$data['dateOfBirth'] = '2010-01-10';
					break;
				case '1217':
					$data['state'] = 'NY';
					break;
				case '1225':
					$I->setTestStatus('Submitted', $data['testKey']);
					break;
				case '1226':
					$I->setTestStatus('Completed', $data['testKey']);
					break;
				case '1227':
					$I->setTestStatus('Canceled', $data['testKey']);
					break;
				case '1228':
					$I->setTestStatus('InProgress', $data['testKey']);
					break;
				case '1229':
					$I->setTestStatus('Scheduled', $data['testKey']);
					$I->renewToken($data['testKey'], '11c9fe56-8c8b-11e5-88a7-5c260a8199a6', (time() + (60 * 60 * 24)));
					break;
			}

			// Login
			$I->sendPOST(API_HOST . $loginUri, json_encode($data));

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);
			$I->comment("Resetting token: " . $I->resetTestSession($data['testKey']));
		}
	}

	public function RunAssessmentTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("03_AssessmentCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		// Send GET request
		$I->sendGET(API_HOST . API_ASSESSMENT_URI);

		// GET Assertions
		$I->makeAssertions($I, [
			// Do not indent the following text, or the assertion will fail!
				'contains_1' => '<div><div id="f"><h1>Welcome!</h1><div></div>Today you will have an opportunity to show what you
know and<br/>have learned. To begin, you will need to log in. If you do not have your<br/>log-in information ready,
please ask your teacher for help.<h2>Select the "Start" button when you are ready to log in.</h2></div></div>'
		], null, HTTP_SUCCESS);

		// POST Requests
		for($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Launch Test Driver Assessment Test ' . $testCase->tsid);

			$this->loadStatusCodes($testCase);

			$I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');

			$name = $this->codes->adp === '1502' ? 'FAIL' : '';
			$testKey = $this->codes->adp === '1503' ? 'FAIL' : $testCase->test_key;

			if ($this->codes->adp == '1501') {
				$I->sendPOST(API_HOST . API_ASSESSMENT_URI, ['testKey' => $testKey, 'name' => $name, 'blab' => 'bla']);
			} else {
				$I->sendPOST(API_HOST . API_ASSESSMENT_URI, ['testKey' => $testKey, 'name' => $name]);
			}

			if(is_null($this->codes->http) || $this->codes->http === 200) {
				$I->makeAssertions($I, ['contains_1' => 'token:"Vm0xMGExWXlWblJWYTFwVFlURktWMWxXVWtkVVZsWTJVVzV3YUUxRVJqRlpWRW93Vkd4S1NHUklTbHBpUm5CeVdWWmFhMUl4YkRaVWJGWk9UVzVvTmxkcldtdGliVkY0Vkc1S1UySlhhSEpVVjNCWFVrWndWMVZzV2s1aVZrcFlWMnRvVDFSc1RraGxTR3hYVTBoQ1lWUlZXbmRUVjA1R1RWVTVUbUV4Y0c5V2EyTjRaV3N3ZUZKdVJsWmlSbkJ3VkZjeGJrMXNiRFpVYlVaWFlsWktTRlZ0Tld0WlZURklWR3BDV0dKWVFsTmFWM2gzVjBaYVZWVnNjR3hpVlRFMVZqSjRVMkV5U2tkalJWWlNWMGhDYUZaclduTk9iRkpYVld4d1RrMUVSa1pXYlRGM1ZERktTR0ZJUmxWV00wSkhWRlpWTlZaV1RsVk5SREE5"'], null, HTTP_SUCCESS);
			} else {
				$I->makeAssertions($I, [], $this->codes->adp, $this->codes->http);
			}

			$I->showHideLoginConfiguration('show');
		}

	}

	public function RunContentTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("04_ContentCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Download Test Content ' . $testCase->tsid);
			$student = $I->grabRecordFromDatabase('student', ['sid' => $testCase->fk_student_id]);

			$this->loadStatusCodes($testCase);

			// We must login to retrieve the token and test ID.
			$response = $I->login($I, $testCase, $student);
			$token = $response->result->api->token;
			$testId = $response->result->test->id;

			// Configure failure cases.
			$content = '/test.json';
			switch ($this->codes->adp) {
				case '1300':
					$token = 'a9182528-78ba-11e5-a079-00155d9c760c';
					break;
				case '1301':
					$I->activateDeactivateTestSession(false, $testCase->test_key);
					break;
				case '1302':
					$token = '33857be3-76bd-11e5-a079-00155d9c7602';
					break;
				case '1303':
					$I->setTestStatus('Submitted', $testCase->test_key);
					break;
				case '1304':
					$I->setTestStatus('Completed', $testCase->test_key);
					break;
				case '1305':
					$I->setTestStatus('Canceled', $testCase->test_key);
					break;
				case '1306':
					$I->expireToken($testCase->test_key);
					break;
				case '1307':
					$content = '/test2.json';
					break;
				case '1308':
					$content = '/deactivated_test.json';
					break;
			}

			$contentUrl = API_HOST . API_URI . API_CONTENT_URI . "/{$token}/{$testId}{$content}";

			$I->sendGET($contentUrl);

			$I->makeAssertions($I, [], $this->codes->adp, $this->codes->http);
			$I->comment("Resetting token: " . $I->resetTestSession($testCase->test_key));
		}
	}

	public function RunResultsUploadTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("05_ResultsUploadCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo("Upload results " . $testCase->tsid);
			$student = $I->grabRecordFromDatabase('student', ['sid' => $testCase->fk_student_id]);

			$this->loadStatusCodes($testCase);

			// We must login to retrieve the token and test ID.
			$response = $I->login($I, $testCase, $student);
			$token = $response->result->api->token;
			$testId = $response->result->test->id;

			$testStatus = '';

			// Get results file.
			$resultsContent = file_get_contents(__DIR__ . '\..\_data\sample_results.json');

			switch ($this->codes->adp) {
				case '1450':
					$token = '43aceece-76ab-11e5-a079-00155d9c7602';
					break;
				case '1451':
					$testId += 1;
					break;
				case '1452':
					$I->setTestStatus('Submitted', $testCase->test_key);
					break;
				case '1453':
					$I->setTestStatus('Completed', $testCase->test_key);
					break;
				case '1454':
					$I->setTestStatus('Canceled', $testCase->test_key);
					break;
				case '1455':
					$testStatus = '/pause';
					break;
				case '1456':
					$testStatus = '/submit';
					break;
				case '1457':
					$testStatus = '/junk';
					break;
				case '1458':
					$I->expireToken($testCase->test_key);
					$I->setTestStatus('Scheduled', $testCase->test_key);
					break;
				case '1459':
					$I->expireToken($testCase->test_key);
					$I->setTestStatus('Paused', $testCase->test_key);
					break;
				case '1463':
					$resultsContent = preg_replace('/dynamic/', 'dynamic2', $resultsContent);
					break;
				case '1466':
					$I->expireToken($testCase->test_key);
					$I->setTestStatus('InProgress', $testCase->test_key);
					break;
			}

			$authUri = API_URI . API_RESULTS_URI . "/$token/$testId$testStatus";

			$headers = $I->generateHeaders(TD_USERNAME, TD_PASSWORD, $I->getHmacSignature($authUri, TD_SECRET), $testCase->test_key);

			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			// POST Upload Request
			$I->sendPOST(API_HOST . $authUri, $resultsContent);

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);
			$I->comment("Resetting token: " . $I->resetTestSession($testCase->test_key));
		}
	}

	public function RunResultsDownloadTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("06_ResultsDownloadCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Download Results ' . $testCase->tsid);

			$student = $I->grabRecordFromDatabase('student', ['sid' => $testCase->fk_student_id]);

			$this->loadStatusCodes($testCase);

			// We must login to retrieve the token and test ID.
			$response = $I->login($I, $testCase, $student);
			$token = $response->result->api->token;
			$testId = $response->result->test->id;

			switch($this->codes->adp) {
				case '1400':
					$token = '46162599-8173-11e5-ab0c-5c260a8199a7';
					break;
				case '1401':
					$testId += 1;
					break;
				case '1402':
					$I->setTestStatus('Submitted', $testCase->test_key);
					break;
				case '1403':
					$I->setTestStatus('Completed', $testCase->test_key);
					break;
				case '1404':
					$I->setTestStatus('Canceled', $testCase->test_key);
					break;
				case '1405':
					$I->expireToken($testCase->test_key);
					break;
			}

			$authUri = API_URI . API_RESULTS_URI . "/$token/$testId";

			$headers = $I->generateHeaders(TD_USERNAME, TD_PASSWORD, $I->getHmacSignature($authUri, TD_SECRET), $testCase->test_key);

			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			// GET results file
			$I->sendGET(API_HOST . API_URI . API_RESULTS_URI . "/$token/$testId", []);

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);
			$I->comment("Resetting token: " . $I->resetTestSession($testCase->test_key));
		}
	}

	public function RunPublishingSchedulingTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("07_PublishRequestCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for ($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Publish Job ' . $testCase->tsid);

			$this->loadStatusCodes($testCase);

			$testSession = $I->grabRecordFromDatabase('test_session', ['test_key' => $testCase->test_key]);
			$testFormRevision = $I->grabRecordFromDatabase('test_form_revision', ['rid' => $testSession->fk_test_form_revision_id]);

			// Set up body
			$data = json_encode([
					'testFormRevisionId'	=> $testFormRevision->external_rid,
					'publishedBy'			=> "ApiPublishingTest $testCase->tsid"
			]);

			$username = ACR_USERNAME;
			$password = ACR_PASSWORD;

			switch($this->codes->adp) {
				case '1703':
					$data = preg_replace('/i[0-9]+/', '12345', $data);
					break;
			}

			$publishUri = $publishUri = API_URI . API_PUBLISH_URI;
			// Set up headers
			$headers = $I->generateHeaders($username, $password, $I->getHmacSignature($publishUri, ACR_SECRET), $testCase->test_key);
			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			$I->sendPOST(API_HOST . $publishUri, $data);
			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);
		}
	}

	public function RunActivateDeactivateTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("09_ActivateDeactivateCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for ($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Activate Test ' . $testCase->tsid);

			$this->loadStatusCodes($testCase);

			$activate = intval($testCase->ACTIVATE);
			$testSession = $I->grabRecordFromDatabase('test_session', ['test_key' => $testCase->test_key]);
			$testFormRevision = $I->grabRecordFromDatabase('test_form_revision', ['rid' => $testSession->fk_test_form_revision_id]);

			$testFormRevisionId = $testFormRevision->external_rid;
			$publishedBy = '09ActivateRequest';

			switch($this->codes->adp) {
				case '1753':
					$publishedBy = '~';
					break;
				case '1756':
					$testFormRevisionId = 'i9999999999';
					break;
			}

			// Set up body
			$data = json_encode([
					'testFormRevisionId'	=> $testFormRevisionId,
					'publishedBy'			=> $publishedBy,
					'active'				=> (bool) $activate
			]);

			$username = ACR_USERNAME;
			$password = ACR_PASSWORD;
			$secret = ACR_SECRET;

			$activateUri = API_URI . API_PUBLISH_URI . "/$testFormRevision->rid";

			switch($this->codes->adp) {
				case '1751':
					$username = 'x';
					break;
				case '1752':
					$data .= 'x';
					break;
				case '1757':
					$username = TD_USERNAME;
					$password = TD_PASSWORD;
					$secret = TD_SECRET;
					break;
			}

			// Set up headers
			$headers = $I->generateHeaders($username, $password, $I->getHmacSignature($activateUri, $secret), $testCase->test_key);
			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			// Verify data is correct
			$I->seeInDatabase('test_form_revision', [
					'active'	=> (int) !$activate,
					'rid'		=> $testFormRevision->rid
			]);

			// Send request
			$I->sendPATCH(API_HOST . $activateUri, $data);

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);

			if(is_null($this->codes->adp) && is_null($this->codes->http)) {
				// Assert data changed
				$I->seeInDatabase('test_form_revision', [
						'active'	=> $activate,
						'rid'		=> $testFormRevision->rid
				]);

				// Change data back and make assertion
				$data = json_encode([
						'testFormRevisionId'	=> $testFormRevision->external_rid,
						'publishedBy'			=> $publishedBy,
						'active'				=> !$activate
				]);
				$I->sendPATCH(API_HOST . $activateUri, $data);
				$I->seeInDatabase('test_form_revision', [
						'active'	=> (int) !$activate,
						'rid'		=> $testFormRevision->rid
				]);
			}
		}
	}

	public function RunAssignmentUpdateTest(ApiTester $I) {
		$newSchoolName = 'NEW SCHOOL NAME';

		// Setup DB
		$I->execSqlScript("10_AssignmentUpdateCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for ($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Update Test ' . $testCase->UPDATE . ' ' . $testCase->tsid);

			$this->loadStatusCodes($testCase);

			$testSession = $I->grabRecordFromDatabase('test_session', ['test_key' => $testCase->test_key]);
			$testForm = $I->grabRecordFromDatabase('test_form', ['fid' => $testSession->fk_test_form_id]);
			$test = $I->grabRecordFromDatabase('test', ['tid' => $testForm->fk_test_id]);
			$ldrTestAssignmentId = $testSession->external_tsid;
			$newStatus = '';
			$newLineReader = false;
			$testData = [];
			$studentData = [
					'studentId'	=> $testSession->fk_student_id
			];

			switch($testCase->UPDATE) {
				case 'status':
					$newStatus = ($testSession->status === 'Scheduled') ? 'Canceled' : 'Scheduled';
					break;
				case 'student':
					$studentData['schoolName'] = $newSchoolName;
					break;
				case 'test':
					$testData['testId'] = $test->tid;
					$testData['testFormId'] = $testForm->fid;
					$testData['testKey'] = $testSession->test_key;
					$testData['enableLineReader'] = $newLineReader;
					break;
				case 'everything':
					$testData['testId'] = $test->tid;
					$testData['testFormId'] = $testForm->fid;
					$testData['testKey'] = $testSession->test_key;
					$testData['enableLineReader'] = $newLineReader;
					$newStatus = ($testSession->status === 'Scheduled') ? 'Canceled' : 'Scheduled';
					$studentData['schoolName'] = $newSchoolName;
					break;
			}

			switch($this->codes->adp) {
				case '1805':
					$studentData = [];
					break;
				case '1806':
					$ldrTestAssignmentId = -1;
					break;
				case '1807':
					$ldrTestAssignmentId = 987654;
					break;
				case '1810':
					unset($studentData['studentId']);
					break;
				case '1811':
					$studentData['studentId'] = -5;
					break;
				case '1812':
					$studentData['state'] = 'OZ';
					break;
				case '1813':
					$studentData['grade'] = '15';
					break;
				case '1814':
					$studentData['dateOfBirth'] = '2015-44-44';
					break;
				case '1815':
					$studentData['dateOfBirth'] = '2015X44X44';
					break;
				case '1816':
					$studentData['schoolName'] = '~ Wild and tilde! ~';
					break;
				case '1817':
					$studentData['firstName'] = '~ Wild and tilde! ~';
					break;
				case '1818':
					$studentData['personalId'] = '~ Wild and tilde! ~';
					break;
				case '1819':
					$studentData['studentId'] = 19999974;
					break;
				case '1820':
					unset($testData['testFormId']);
					break;
				case '1821':
					$testData['testId'] = -5;
					break;
				case '1822':
					$testData['testFormId'] = -5;
					break;
				case '1823':
					$testData['testKey'] = 'ABCDEFGHIJKLM';
					break;
				case '1825':
					$ldrTestAssignmentId = 575757;
					break;
				case '1827':
					$testData['testFormId'] = 42;
					break;
				case '1828':
					$studentData['studentId'] = 58422;
					break;
				case '1830':
					$newStatus = 'What';
					break;
				case '1831':
					$testData['enableLineReader'] = 'blabla';
					break;
				case '1832':
					$testData['enableTextToSpeech'] = 'blabla';
					break;
			}

			// Set up body
			$data = json_encode(array_filter([
					'testAssignmentId'	=> $ldrTestAssignmentId,
					'testStatus'		=> $newStatus,
					'test'				=> $testData,
					'student'			=> $studentData
			], function($v) {
				return !empty($v);
			}));

			$username = LDR_USERNAME;
			$password = LDR_PASSWORD;
			$secret = LDR_SECRET;

			$adpTestAssignmentId = $testSession->tsid;

			switch($this->codes->adp) {
				case '1801':
					$username .= 'x';
					break;
				case '1802':
					$data .= 'x';
					break;
				case '1803':
				case '1824':
				case '1826':
					$adpTestAssignmentId = '';
					break;
				case '1804':
					$adpTestAssignmentId = '-5';
					break;
			}

			$assignmentUri = API_URI . API_ASSIGNMENT_URI . (strlen($adpTestAssignmentId) > 0 ? "/$adpTestAssignmentId" : '');

			// Set up headers
			$headers = $I->generateHeaders($username, $password, $I->getHmacSignature($assignmentUri, $secret), $testCase->test_key);
			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			// Ensure data is correct before making request
			switch($testCase->UPDATE) {
				case 'status':
					$I->dontSeeInDatabase('test_session', [
							'tsid'		=> $testSession->tsid,
							'status'	=> $newStatus
					]);
					$oldStatus = $I->grabFromDatabase('test_session', 'status', ['tsid' => $testSession->tsid]);
					break;
				case 'student':
					$I->dontSeeInDatabase('student', ['school_name'	=> $newSchoolName]);
					$oldSchoolName = $I->grabFromDatabase('student', 'school_name', ['sid' => $testSession->fk_student_id]);
					break;
				case 'test':
					$I->dontSeeInDatabase('test_session', [
							'test_key'				=> $testSession->test_key,
							'enable_line_reader'	=> $newLineReader
					]);
					$oldLineReader = $I->grabFromDatabase('test_session', 'enable_line_reader', ['tsid' => $testSession->tsid]);
					break;
				case 'everything':
					$I->dontSeeInDatabase('test_session', [
							'tsid'		=> $testSession->tsid,
							'status'	=> $newStatus
					]);
					$I->dontSeeInDatabase('test_session', [
							'test_key'				=> $testSession->test_key,
							'enable_line_reader'	=> $newLineReader
					]);
					$I->dontSeeInDatabase('student', ['school_name'	=> $newSchoolName]);
					$I->dontSeeInDatabase('test_session', ['test_key' => $newLineReader]);
					$oldStatus = $I->grabFromDatabase('test_session', 'status', ['tsid' => $testSession->tsid]);
					$oldSchoolName = $I->grabFromDatabase('student', 'school_name', ['sid' => $testSession->fk_student_id]);
					$oldLineReader = $I->grabFromDatabase('test_session', 'enable_line_reader', ['tsid' => $testSession->tsid]);
					break;
			}

			// Send request
			if(strlen($adpTestAssignmentId) > 0) {
				$I->sendPATCH(API_HOST . $assignmentUri, $data);
			} else {
				$I->sendPOST(API_HOST . $assignmentUri, $data);
			}

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);

			// Success cases only
			if(is_null($this->codes->adp)) {
				// Ensure data was changed correctly
				switch($testCase->UPDATE) {
					case 'status':
						$I->seeInDatabase('test_session', [
								'tsid'		=> $testSession->tsid,
								'status'	=> $newStatus
						]);
						break;
					case 'student':
						$I->seeInDatabase('student', [
								'sid'			=> $testSession->fk_student_id,
								'school_name'	=> $newSchoolName
						]);
						break;
					case 'test':
						$I->seeInDatabase('test_session', [
								'test_key'				=> $testSession->test_key,
								'enable_line_reader'	=> $newLineReader
						]);
						break;
					case 'everything':
						$I->seeInDatabase('test_session', [
								'tsid'		=> $testSession->tsid,
								'status'	=> $newStatus
						]);
						$I->seeInDatabase('test_session', [
								'test_key'				=> $testSession->test_key,
								'enable_line_reader'	=> $newLineReader
						]);
						$I->seeInDatabase('student', [
								'sid'			=> $testSession->fk_student_id,
								'school_name'	=> $newSchoolName
						]);
						break;
				}
			}

			// Put data back the way it was
			switch($testCase->UPDATE) {
				case 'status':
					$I->updateFieldInDatabase('test_session', ['tsid' => $testSession->tsid], 'status', $oldStatus);
					break;
				case 'student':
					$I->updateFieldInDatabase('student', ['sid' => $testSession->fk_student_id], 'school_name', $oldSchoolName);
					break;
				case 'test':
					$I->updateFieldInDatabase('test_session', ['tsid' => $testSession->tsid], 'enable_line_reader', $oldLineReader);
					break;
				case 'everything':
					$I->updateFieldInDatabase('test_session', ['tsid' => $testSession->tsid], 'status', $oldStatus);
					$I->updateFieldInDatabase('student', ['sid' => $testSession->fk_student_id], 'school_name', $oldSchoolName);
					$I->updateFieldInDatabase('test_session', ['tsid' => $testSession->tsid], 'enable_line_reader', $oldLineReader);
					break;
			}
		}
	}

	public function RunResultsDetailsTest(ApiTester $I) {
		// Setup DB
		$I->execSqlScript("11_ResultsDetailsCept.sql");

		$testCases = $I->grabAllFromDatabase('test_session_tc');

		for ($x = 0; $x < count($testCases); $x++) {
			$testCase = $testCases[$x];
			$I->wantTo('Get Test Results ' . $testCase->tsid);

			$this->loadStatusCodes($testCase);

			$testSession = $I->grabRecordFromDatabase('test_session', ['test_key' => $testCase->test_key]);

			$testSessionId = $testSession->tsid;

			$username = LDR_USERNAME;
			$password = LDR_PASSWORD;
			$secret = LDR_SECRET;

			switch($this->codes->adp) {
				case '1902':
					$testSessionId = 4294967296;
					break;
				case '1903':
					$testSessionId = 5999;
					break;
			}

			$resultsUri = API_URI . API_RESULTS_URI . '/' . $testSessionId;

			// Set up headers
			$headers = $I->generateHeaders($username, $password, $I->getHmacSignature($resultsUri, $secret), $testCase->test_key);
			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			// Send request
			$I->sendGET(API_HOST . $resultsUri);

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp, $this->codes->http);
		}
	}

	public function RunTestPropertiesUpdateTest(ApiTester $I)
	{
		// Setup DB
		$I->execSqlScript("12_PropertiesUpdateCept.sql");

		$adpTestId = '';

//		$notAllowedAttributes = ['program', 'scoreReport', 'itemSelectionAlgorithm'];
//		$allowedAttributes = ["name", "subject", "grade", "security", "multimedia", "scoring",
//				"permissions", "privacy", "isActive", "description"];

		$testCases = $I->grabAllFromDatabase('test_session_tc', PDO::FETCH_ASSOC);

		foreach ($testCases as $testCase) {
			$I->wantTo('Get Test Details for Test: ' . $testCase['tid']);

			$adpTestId = $testCase['tid'];

			$this->loadStatusCodes((object)$testCase);

			$username = ACR_USERNAME;
			$password = ACR_PASSWORD;
			$secret = ACR_SECRET;

			$data = [];

			foreach ($testCase as $column => $newValue) {
				if (!in_array($column, ['ADP_ERROR', 'HTTP_STATUS', 'tid']) && $newValue != null) {
					$data[$column] = $newValue;
				}
			}

			$testPropertiesUpdateUri = API_URI . API_TEST_PROPERTIES_URI . '/' . $adpTestId;

			// Set up headers
			$headers = $I->generateHeaders($username, $password, $I->getHmacSignature($testPropertiesUpdateUri, $secret),
					'');

			foreach ($headers as $name => $value) {
				$I->haveHttpHeader($name, $value);
			}

			// Send request
			$I->sendPATCH(API_HOST . $testPropertiesUpdateUri, json_encode($data));

			$I->makeAssertions($I, ['schema' => $this->schemas->get('response_schema')], $this->codes->adp,
					$this->codes->http);

			if ($testCase['tid'] == 17) {

				// Verify data is correct
				$I->seeInDatabase('test',
						[
								'tid' => 17,
								'external_tid' => $data['batteryId'],
								'name' => $data['name'],
								'subject' => $data['subject'],
								'grade' => $data['grade'],
								'security' => $data['security'],
								'multimedia' => $data['multimedia'],
								'scoring' => $data['scoring'],
								'permissions' => $data['permissions'],
								'privacy' => $data['privacy'],
								'description' => $data['description'],
								'active' => filter_var($data['isActive'], FILTER_VALIDATE_BOOLEAN)
						]
				);
			}
		}
	}

	private function loadSchemas() {
		$this->schemas = new \Jsv4\SchemaStore();

		// Load response schema.
		$schema = json_decode(file_get_contents(__DIR__ . '/schema/Response_Schema.json'));
		$this->schemas->add("response_schema", $schema);
	}

	private function loadStatusCodes($testCase) {
		$this->codes->adp = property_exists($testCase, 'ADP_ERROR') ? $testCase->ADP_ERROR : null;
		$this->codes->http = property_exists($testCase, 'HTTP_STATUS') ? $testCase->HTTP_STATUS : null;
	}
}
