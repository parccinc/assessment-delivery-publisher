<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Config;

/**
 * Application Settings for particular environment.
 *
 * Supported Log Levels:
 * 0: EMERGENCY
 * 1: CRITICAL
 * 2: ALERT
 * 3: ERROR
 * 4: WARNING
 * 5: NOTICE
 * 6: INFO
 * 7: DEBUG
 * 8: CUSTOM
 * 9: SPECIAL (Default)
 *
 * @package PARCC\ADP
 * @version v1.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
return new Config([
	// General Application Configuration.
	'app' => [
		// Path to the Controllers folder, relative to the Application root. Do NOT change!
		'controllersDir'        => '/app/controllers/',
		// Path to the Models folder, relative to the Application root. Do NOT change!
		'modelsDir'             => '/app/models/',
		// Path to the Views folder, relative to the Application root. Do NOT change!
		'viewsDir'              => '/app/views/',
		// Application Instance Identifier (@default=1).
		'applicationId'         => 0,
		// Flag if Application is allowed to operate in Test Delivery mode (@default=false).
		'allowTestDelivery'     => true,
		// Flag if Application is allowed to operate in Test Publishing mode (@default=false).
		'allowTestPublishing'   => true,
		// Flag if Envelope should be used in JSON Responses (@default=true).
		'useEnvelope'	          => true,
		// Flag if Status Code and Status Description should be included in the Response Header for all Response Types
		// (@default=true).
		'useHeaderStatus'       => true,
		// Flag if ETags should be used as hash for the Responses (@default=false).
		'useETag'               => false,
		// Duration in seconds for how long HMAC generated Authentication Hash will be considered as valid. However, the
		// Timestamp when it was generated is on the Client side and its Clock may be out of sync with Application
		// Clock! Network Lag and Processing Time should also be considered, so this value should not be too low
		// (@default=300).
		'hmacTimeTolerance'     => 300,
		// Flag if Maintenance Mode should be enabled. Once enabled, only access to Ping web service is permitted to
		// allow Health Check required for Load Balancer (@default=false).
		'enableMaintenanceMode' => false,
		// Flag to turn On/Off Application Debugging/Profiling tools (@default=false).
		'debug'                 => true,
		// Specifies an alternate environment to load configuration options for.
		'environment'	=> 'API_Testing'
	],
	// Database Configuration.
	'database' => [
		// Database Adapter/Driver {MySQL|PostgreSQL|SQLite|Oracle} (@default='MySQL').
		'adapter'               => 'MySQL',
		// Database Hostname.
		'host'                  => '127.0.0.1',
		// Database Username.
		'username'              => 'root',
		// Database Password.
		'password'              => '',
		// Database Name.
		//'dbname'            => 'adp_tds',
		'dbname'            => 'adp_tds_test',
		// Default Character Set used for every Database Connection. It also enforces use of default Collation for
		// selected Character Set. Default Collation for 'utf8' Character Set is 'utf8_general_ci'!
		'charset'           => 'utf8',
		// Switch for using Persistent Connections to Database.
		'persistent'        => true,
		// Flag to turn On/Off Database Logging, so all SQL Queries being executed are saved into a Log File on the
		// configured "logPath", and is only available if Debugging is enabled! (@default=false).
		'useSqlLogging'        => true,
		// Path to the Database Log, relative to the Application root. This is used only when Debugging is turned on and
		// it contains all SQL queries being executed by the Application. "{date}" will be replaced with current date.
		'sqlLogPath'           => '/db_{date}.log'
	],
	// Database2 configuration.
	'database2' => [
		// Database Adapter/Driver {MySQL|PostgreSQL|SQLite|Oracle} (@default='MySQL').
		'adapter'              => 'MySQL',
		// Database Hostname.
		'host'                 => '',
		// Database Username.
		'username'          => 'root',
		// Database Password.
		'password'             => '',
		// Database Name.
		// 'dbname'         => 'adp_ps',
		// 'dbname'			=> 'adp_tds',
		'dbname'            => 'adp_tds_test',
		// Default Character Set used for every Database Connection. It also enforces use of default Collation for
		// selected Character Set. Default Collation for 'utf8' Character Set is 'utf8_general_ci'! (@default=utf8).
		'charset'              => 'utf8',
		// Switch for using Persistent Connections to Database (@default=true).
		'persistent'           => true,
		// Flag to turn On/Off Database Logging, so all SQL Queries being executed are saved into a Log File on the
		// configured "logPath", and is only available if Debugging is enabled! (@default=false).
		'useSqlLogging'        => true,
		// Path to the Database Log, relative to the Application root. This is used only when Debugging is turned on and
		// it contains all SQL queries being executed by the Application. "{date}" will be replaced with current date.
		'sqlLogPath'           => '/db2_{date}.log'
	],

	// APCu configuration.
	'apc' => [
		// Duration in seconds for how long Application Configuration and System Configuration should be cached before
		// Application reloads the Config file and System Table from the Database, and updates the Cache (0 means
		// forever in which case either a Server Restart is needed, OR APCu Cache has to be flushed manually!)
		// (@default=0).
		'configCacheLifetime'   => 0,
		// Duration in seconds for how long API Routes should be cached before Application re-scans all available Routes
		// and updates the Routes Cache (0 means forever in which case either a Server Restart is needed, OR APCu Cache
		// has to be flushed manually!) (@default=0).
		'routesCacheLifetime'   => 0,
		// Duration in seconds for how long Database Schema should be cached before Application re-scans the Database
		// and updates the Model Cache (0 means forever in which case either a Server Restart is needed, OR APCu Cache
		// has to be flushed manually!) (@default=0).
		'dbSchemaCacheLifetime' => 0,
		// Duration in seconds for how long AWS IAM Role should be cached before Application retries to retrieve it from
		// AWS Instance Metadata and User Data Service (0 means forever in which case either a Server Restart is needed,
		// or APCu Cache has to be flushed manually!) (@default=0).
		'iamRoleCacheLifetime'  => 0
	],
	// Memcached Configuration.
	'memcached' => [
		// Memcached Adapter/Driver (@default='Memcached').
		'adapter'               => 'Memcached',
		// Memcached Hostname.
		'host'                  => '',
		// Memcached Username.
		'username'              => '',
		// Memcached Password.
		'password'              => '',
		// Memcached Database Name.
		'dbname'                => ''
	],
	// Remote peer configurations
	'acr' => [
		// Remote ACR HOST (without trailing slash!).
		'host'	=> 'https://localhost',
		// Remote ACR username (used for HMAC authentication)
		'username' => 'ADPPUBLISHER',
		// Remote ACR password (used for HMAC authentication)
		'password' => 'ADPPUBLISHERPASSWORD',
		// Remote ACR secret (used for HMAC authentication)
		'secret' => 'SECRET_ADP'
	],
	'ldr' => [
		// Remote LDR HOST (without trailing slash!).
		'host' => 'https://int.ldr.parcc-ads.breaktech.org/ldr_svcs',
		// Remote LDR USERNAME
		'username' => 'parcc_adp',
		// Remote LDR PASSWORD
		'password' => 'm2ca59qnej3rsck',
		// Remote LDR SECRET
		'secret' => '',
		// Token LifeTime
		'tokenLifetime' => 1200,
		// Maximum Number of attempts
		'maxAttempts' => 3
	],
	// AWS S3 and CloudFront configuration.
	's3' => [
		// AWS S3 Hostname (without "http://" or "https://" scheme and trailing slash!) (@default='s3.amazonaws.com').
		'host'                  => 's3.amazonaws.com',
		// AWS STS (Security Token Service) Hostname (without trailing slash!). Currently, AWS STS uses only one IP
		// Address, doesn't provide Hostname and is only available over HTTP (@default='http://169.254.169.254').
		'stsHost'               => 'http://169.254.169.254',
		// Flag if AWS S3 should be accessed using HTTPS Protocol (@default=true).
		'useHTTPS'              => true,
		// AWS S3 Access Key ID (redundant if AWS IAM Roles are assigned to the server!) (@default=null).
		'accessKeyId'          => "AKIAJMH2OFGVQU3GWLBA",
		// AWS S3 Secret Access Key (redundant if AWS IAM Roles are assigned to the server@) (@default=null).
		'secretAccessKey'      => "BroS4I9s7gAPAz3pvRyHmH1wgJxtTEM0QiwbCf5B",
		// AWS S3 Bucket Name holding Test Content files.
		'contentBucket'        => 'parcc-ads',
		// AWS S3 Bucket Name holding Test Content Archived (Package) files.
		'contentArchiveBucket' => 'parcc-ads',
		// AWS S3 Bucket Name holding Test Results files.
		'resultsBucket'        => 'parcc-ads',
		// AWS S3 Bucket Name holding Test Results Archived files.
		'resultsArchiveBucket' => 'parcc-ads',
		// AWS S3 Bucket Key (subfolder) holding Test Content files.
		'contentPrefix'        => 'dev-mehdi-adp/content',
		// AWS S3 Bucket Key (subfolder) holding Test Package files.
		'contentArchivePrefix' => 'dev-mehdi-adp/content-archive',
		// AWS S3 Bucket Key (subfolder) holding Test Results files.
		'resultsPrefix'        => 'dev-mehdi-adp/results',
		// AWS S3 Bucket Key (subfolder) holding Test Results Archived files.
		'resultsArchivePrefix' => 'dev-mehdi-adp/results-archive',
		// List of File Extensions for which both Original and GZipped (compressed) version of the file should be
		// uploaded to AWS S3, so Browser can download compressed version if it supports GZip. Do NOT change!
		'gzipFileExtensions'    => ['htm', 'html', 'xhtml', 'txt', 'js', 'json', 'css', 'xml', 'ttf', 'otf', 'eot',
		  'woff', 'svg', 'ico'],
		////// EXTRA \\\\\\

		// AWS CloudFront URL (without trailing slash!).
		'cloudFrontURL'        => 'http://dolzog5bqrn3.cloudfront.net',
		//		'cloudFrontURL'        => 'https://localhost/TD_Content',
		// AWS S3 Bucket Key (subfolder) holding Test Package files.
		'packagesPrefix'       => 'adp-packages'
	],
	// Application Logging Configuration.
	'logger' => [
		// Logging Level [0-9] (@default=6).
		'logLevel'             => 7,
		// Flag to turn On/Off Logger Transactions (all Log Entries are saved together at the end) (@default=true).
		'useLogTransactions'   => true,
		// Flag to turn On/Off Database Profiles Logs being included into Application Log. This is similar to Database
		// Log with additional performance details, and is only available if both Debugging and Database SQL Logging are
		// enabled! (@default=false).
		'includeDbProfilerLog'  => true,
		// Flag to turn On/Off logging of Request URLs (@default=true).
		'logRequestUrl'         => true,
		// Flag to turn On/Off logging of Request Bodies. It depends on Request URLs logging (@default=true).
		'logRequestBody'        => true,
		// Amount of Bytes to be logged if Request Bodies logging is enabled. This should be reasonably small!
		// (@default=1024).
		'logRequestBodyMaxSize' => 1024,
		// Flag to turn On/Off logging of Responses (@default=true).
		'logResponse'           => true,
		// Amount of Bytes to be logged if Responses logging is enabled. This is only used for JSON Responses and should
		// be reasonably small! (@default=1024).
		'logResponseMaxSize'   => 1024,
		// Flag to turn On/Off File Logger, so all Log Entries are saved into a Log File on the configured "logPath".
		// This is the default Logger Adapter (@default=true).
		'useFileLogger'        => true,
		// Path to the Application Log, relative to the Application root. "{date}" will be replaced with current date.
		'fileLoggerPath'       => '/app_{date}.log',
		// Flag to turn On/Off Database Logger, so all Log Entries are also saved into centralized Application Database
		// (in case there are multiple Application instances behind the Load Balancer) (@default=true).
		'useDatabaseLogger'    => true,
		// Flag to turn On/Off Socket Logger, so all Log Entries are also sent to Socket Connection over Data Stream.
		// This should only be used for debugging, and should NOT be used in Production! It requires Debugging to be
		// enabled (@default=false).
		'useSocketLogger'      => true,
		// Socket Logger Hostname (@default='127.0.0.1').
		'socketLoggerHost'     => '127.0.0.1',
		// Socket Logger Port (@default=9999).
		'socketLoggerPort'     => 9999
	],
	// Assessment Web Service specific Configuration.
	'Assessment' => [
		// API Hostname (without "http://" or "https://" scheme and trailing slash!) (@default='').
		'apiHost'               => '',
		// Username used by Test Driver for Basic and HMAC Authentication.
		'testDriverUsername'   => 'TestACR',
		// Password used by Test Driver for Basic and HMAC Authentication.
		'testDriverPassword'   => 'KKK$2y$09$mWbXW7Oyluh0JAawnBy56uWl0ykO64dzL7aZNk7B4UohzsvUtQpgG',
		// Secret used by Test Driver for Basic and HMAC Authentication.
		'testDriverSecret'     => 'f43ee8c76e7ed03e6316705c48c21592',
		// AWS CloudFront Hostname for Production version of Test Driver Assets (Images, JavaScript, CSS, Fonts etc.)
		// (without "http://" or "https://" scheme and trailing slash!) (@default='*.cloudfront.net').
		'cloudFrontHost'        => '*.cloudfront.net',
		// Relative Path to Release version of Test Driver Assets (Images, JavaScript, CSS, Fonts etc.). This should NOT
		// be used in Production and should be empty! (@default='').
		'releaseAssetPath'      => '/TD/releaseDebug',
		// Relative Path to Debug version of Test Driver Assets (Images, JavaScript, CSS, Fonts etc.). This should NOT
		// be used in Production and should be empty! (@default='').
		'debugAssetPath'        => '/TD'
	],
	// Login Web Service specific Configuration.
	'Login' => [
		// Number of consecutive retries to generate a unique Test Session Token in case of previous failure before
		// quitting and triggering an Error (@default=3).
		'tokenRetryCount'       => 3,
		// Duration in seconds for how long Test Session Token will be valid once generated/refreshed. It gets reset
		// each time Content or Results Web Services are called after a successful Authentication and validation
		// (@default=3600).
		'tokenLifetime'         => 3600,
		// Flag to turn On/Off validation if Test is already in progress, and is only available if Debugging is enabled!
		// (@default=true).
		'useInProgressCheck'    => true,
		// Pearson CAT/DCM URL (without trailing slash!).
		'catUrl'			   => false
	],
	// Content Web Service specific Configuration.
	'Content' => [
		// AWS CloudFront Hostname (without "http://" or "https://" scheme and trailing slash!)
		// (@default='*.cloudfront.net').
		'cloudFrontHost'        => 'dolzog5bqrn3.cloudfront.net',
		// AWS CloudFront Key Pair ID (indicates AWS which RSA Key Pair to use to validate URL signature).
		'sshKeyPairId'          => 'APKAJE5ASOFF5HU6LEPQ',
		// AWS S3 Private RSA (SSH-2) Key for signing CloudFront URLs by trusted signer (relative from the Application
		// root folder) (@default='/app/config/private_key.pem').
		'sshPrivateKey'         => '/app/config/private_key.pem',
		// Flag if AWS CloudFront Signed ULR should be locked to a single IP Address (@default=true).
		'signedUrlLockToIP'     => false,
		// Duration in seconds for how long AWS CloudFront Signed URLs will be valid once generated (@default=300).
		'signedUrlLifetime'     => 60
	],
	// Results Web Service specific Configuration.
	'Results' => [
		// Path to Test Results JSON Schema used for Results validation (relative from the Application root folder). Do
		// NOT change!
		'resultsSchema'         => '/app/include/resultsSchema.json',
		// Flag to turn On/Off Test Results JSON validation against Test Results JSON Schema (@default=true).
		'useResultsValidation'  => true
	],
	// Test Publishing web service specific configuration.
	'Test' => [
		// Path to Test JSON Schema used for Test validation (relative from the Application root folder).
		'testSchema' => '/app/include/testSchema.json',
		// Flag to turn On/Off Test JSON validation against Test JSON Schema (@default=true).
		'useTestValidation' => true,
		// Gzip compression level (@default=9).
		'gzipCompressionLevel' => 9,
		// Chunk size that we use to gz-encode files. default value is 0.5MB (@default=524288)
		'gzipChunkSize' => 524288,
		// Flag to check and make sure all the resource in pub_resourceManifest are found,
		// if that flag is set to false an error will be logged and publishing would continue
		// if it is set to true and a resource is missing, an error would be logged and publishing would fail.
		'strictFileManifestCheck' => true,
		// Resource Prefix
		'resourcePrefix' => '[[ADP]]'
	],
	'testSession' => [
		'isEnabledLineReader' => true,
		'isEnabledTextToSpeech' => true
	]
]);
