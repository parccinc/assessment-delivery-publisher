<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Mvc\Micro\Collection;

/**
 * Collections let us define groups of Routes that will all use the same Controller. We can also set the Handler to be
 * lazy loaded. Collections can share a common Prefix.
 * This is an immediately invoked function in PHP. The return value of the anonymous function will be returned to any
 * file that "includes" it. e.g. $collection = include('example.php');
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return array $resultsCollection Collection of routes.
 */
return call_user_func(function () {

	$resultsCollection = new Collection();

	/**
	 * ALL ROUTES SHOULD HAVE THE SAME "api" PREFIX, ALWAYS!
	 *
	 * @var string  $token                    Authentication Token (lowercase GUID).
	 * @var integer $parentTestFormRevisionId Reference to the Test Form Revision to which the referenced Results belong
	 *                                        to.
	 */
	$resultsCollection->setPrefix('/api/results/{token:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}}/' .
	                              '{parentTestFormRevisionId:[0-9]+}')
	                  // Must be a string in order to support lazy loading!
	                  ->setHandler('PARCC\ADP\Controllers\ResultsController')
	                  ->setLazy(true);

	/**
	 * First parameter is the Route; Second parameter is the name of the Handling Method of the Controller;
	 * Third parameter is the name of the Route.
	 * Third parameter is only supported by Phalcon v2.x.x and will cause Phalcon v1.x.x to crash!!!
	 *
	 * $var string $testStatus Optional Test Status of the referenced Results {'InProgress'|'Pause'|'Submit'}
	 *                         (@default='InProgress').
	 */
	/** @noinspection PhpUndefinedCallbackInspection */
	$resultsCollection->get('/', 'downloadResults', 'Results');
	/** @noinspection PhpUndefinedCallbackInspection */
	$resultsCollection->post('/', 'uploadResults', 'Results');
	/** @noinspection PhpUndefinedCallbackInspection */
	$resultsCollection->post('/{testStatus}', 'uploadResults', 'Results');
	// Allow Clients to set CORS Headers using Pre-flight Request.
	/** @noinspection PhpUndefinedCallbackInspection */
	$resultsCollection->options('/', 'preflightHandler', 'Results');
	/** @noinspection PhpUndefinedCallbackInspection */
	$resultsCollection->options('/{testStatus}', 'preflightHandler', 'Results');

	return $resultsCollection;
});
