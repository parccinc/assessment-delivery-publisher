<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\User;

/**
 * Application Request Dispatch (Before Route Execution), Authentication and Authorization Handler.
 *
 * Before any Request gets processed, make sure to Log the Request details, assign appropriate Response Type, Set CORS
 * policies and Authenticate User first!
 * This supports Basic Authentication and HMAC Authorization. It also allows all OPTIONS Requests without Authentication
 * as those tend to not come with Cookies or any Authentication details (credentials in general), and are used for
 * Pre-flight Requests which are not implemented the same way in all Browsers.
 *
 * @throws HttpException In case User is not authenticated for any reason.
 */
// Retrieve Request details.
/** @var Phalcon\Mvc\Micro $app */
$routeName = $app->router->getMatchedRoute()->getName();
$requestURI = $app->request->getURI();


// Check if Request should be logged.
/** @var object $app->config */
if ($app->config->logger->logRequestUrl === true) {
	// Generate Log entry with the Request details.
	$request = $routeName . ': ' . $app->request->getMethod() . ' ' . $app->request->getScheme() . '://' .
	           $app->request->getHttpHost() . $requestURI .
	           ' [Tenant ID: ' . (isset($app['tenant']) ? $app['tenant']->tenantId : 'N/A') . ']';
	// Add Request Body to the Request details for POST, PUT, PATCH and DELETE Requests, if needed.
	if (!$app->request->isGet() && !$app->request->isHead() && !$app->request->isOptions() &&
	    $app->config->logger->logRequestBody === true
	) {
		// Include Request Body only if smaller than configured maximum size!
		// Test Results for example can be enormous and would consume a lot of memory and cause Logs to be huge.
		if ($_SERVER['CONTENT_LENGTH'] < $app->config->logger->logRequestBodyMaxSize) {
			/** @var Phalcon\Mvc\Micro $app */
			$requestBody = $app->request->getJsonRawBody();
			// Check if Request Body is empty.
			if (empty($requestBody)) {
				// Check if Request contains Form POST Data.
				if (count($_POST) > 0) {
					// Retrieve Form POST Data.
					$requestBody = 'Form Data: ' . json_encode($_POST);
				}
			} else {
				$requestBody = json_encode($requestBody);
			}
			$request .= (empty($requestBody) ? '' : (":\n" . $requestBody));
		} else {
			$request .= ":\n*** Size Limit Exceeded ***";
		}
	}
	// Log Request details.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
	$app->logger->info($request);
}


// Skip Maintenance Mode and Authentication for any Ping and Admin Requests.
if ($routeName === 'Ping' || $routeName === 'Admin') {
	// Make sure to set the CORS Policy as soon as possible. This is very important!!!
	/** @var object $app->setCors */
	$app->setCors;

	return true;
}


/*
 * Maintenance Mode.
 */
// Check if Maintenance Mode is turned On for any other Request.
// Ping Request must always return expected Response because of AWS ELB Health Check!
// Admin Request should ignore Maintenance Mode in order to be able to perform Administration tasks!
/** @var object $app->config */
if ($app->config->app->enableMaintenanceMode === true) {
	// Make sure to set the CORS Policy as soon as possible. This is very important!!!
	/** @var object $app->setCors */
	$app->setCors;

	// Throw Maintenance Exception using appropriate Response Type based on the Controller handling the Request.
	/** @var Phalcon\Mvc\Micro $app */
	if ($routeName === 'Assessment') {
		// Throw Maintenance Exception using HTML Response Type for Assessment Request.
		throw new HttpException(
			'ADP-1000',
			503,
			'The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/>Please try ' .
			'again later.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HtmlResponse'
		);

	} elseif (!$app->request->isGet() && !$app->request->isHead() && !$app->request->isOptions()) {
		// Throw Maintenance Exception using JSON Response Type for all POST, PUT, PATCH and DELETE Requests.
		throw new HttpException('ADP-1001', 503, 'System is currently undergoing maintenance.', 'System Maintenance.');

	} elseif ($app->request->isOptions()) {
		// Throw Maintenance Exception using Header Response Type for all OPTIONS Requests.
		// Per CORS Policy, all Pre-flight (OPTIONS) Requests must always receive 200 "OK" as a Response, so this is an
		// exception from other Maintenance cases.
		throw new HttpException(
			'ADP-1002',
			200,
			'System is currently undergoing maintenance.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HeaderResponse'
		);

	} elseif (mb_strpos($requestURI, '/api/content') === 0) {
		// Throw Maintenance Exception using Header Response Type for all Content Requests.
		throw new HttpException(
			'ADP-1003',
			503,
			'System is currently undergoing maintenance.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HeaderResponse'
		);

	} elseif (mb_strpos($requestURI, '/api/results') === 0) {
		// Throw Maintenance Exception using JSON Response Type for all Results Requests.
		throw new HttpException('ADP-1004', 503, 'System is currently undergoing maintenance.', 'System Maintenance.');

	} elseif ($app->request->isGet()) {
		// Throw Maintenance Exception using HTML Response Type for all other GET Requests.
		throw new HttpException(
			'ADP-1005',
			503,
			'The system is currently undergoing maintenance. We are sorry for the inconvenience.<br/>Please try ' .
			'again later.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HtmlResponse'
		);

	} else {
		// Throw Maintenance Exception using Header Response Type for all other Requests.
		throw new HttpException(
			'ADP-1006',
			503,
			'System is currently undergoing maintenance.',
			'System Maintenance.',
			'PARCC\ADP\Responses\HeaderResponse'
		);
	}
}


/*
 * Response Type.
 */
// Set Response Type based on the Controller handling the Request. HtmlResponse is only used for special scenarios
// (i.e. 404 for Homepage, 403 for Test Driver Launch or 503 for Maintenance Mode).
/** @var Phalcon\Mvc\Micro $app */
if ($routeName === 'Content' && $app->request->isGet()) {
	$app->setService('response', 'PARCC\ADP\Responses\RedirectResponse', true);
} elseif ($routeName === 'Assessment') {
	$app->setService('response', 'PARCC\ADP\Responses\HtmlResponse', true);
} elseif ($app->request->isOptions()) {
	$app->setService('response', 'PARCC\ADP\Responses\HeaderResponse', true);
} else {
	$app->setService('response', 'PARCC\ADP\Responses\JsonResponse', true);
}


/*
 * CORS (Cross-Origin Resource Sharing).
 */
// Make sure to set the CORS Policy as soon as possible. This is very important!!!
/** @var object $app->setCors */
$app->setCors;

// Skip Authentication for All OPTIONS Requests, and let Base Controller handle them. This, however, requires that ALL
// Controllers have OPTIONS Route added in their Route Collection, and is used for Pre-flight Requests only!
/** @var Phalcon\Mvc\Micro $app */
if ($app->request->isOptions()) {
	return true;
}


/*
 * Check if Current Tenant was successfully retrieved.
 */
/** @var object $app->setTenant */
if ($app->setTenant !== 1) {
	// Throw Tenant Exception in case Tenant was not successfully retrieved. This is a dummy Exception which will be
	// overridden with previously triggered Tenant Exception.
	throw new HttpException('ADP-0000', 500, 'Internal Error', 'Tenant Exception!', get_class($app->response));
}


// Skip Authentication for All Assessment and Content Requests, and let Base Controller handle them.
// Content Authentication will be validated directly within the Controller and it's the only exception from other
// Controllers.
/** @var Phalcon\Mvc\Micro $app */
if ($routeName === 'Assessment' || $routeName === 'Content') {
	return true;
}


/*
 * Basic Authentication.
 */
// Try to retrieve the Username of the User calling API and trying to Authenticate.
$username = filter_var($app->request->getServer('PHP_AUTH_USER'), FILTER_SANITIZE_STRING);

// Check if Username was successfully retrieved from Basic Authentication.
if (empty($username)) {
	// Throw Authentication Exception in case Basic Authentication is missing.
	throw new HttpException('ADP-1007', 401, 'Authorization Failed.', 'User Missing.');
}

// Try to retrieve referenced User from APCu Cache.
/** @var object $app->apcCache */
$user = $app->apcCache->get('user-' . $username . '-' . $app->tenant->tenantId);

// Check if referenced User was successfully loaded (if it was previously cached).
if (empty($user)) {
	// Try to retrieve referenced User first.
	/** @var User $user */
	$user = User::findFirst([
		'columns' => 'userId, username, password, secret, type, isActive',
		'conditions' => 'tenantId = ?1 AND username = ?2',
		'bind' => [
			1 => $app->tenant->tenantId,
			2 => $username
		]
	]);

	// Check if User was found and is Active.
	if ($user === false) {
		// Throw Authentication Exception if no matching User was found.
		throw new HttpException('ADP-1008', 401, 'Authorization Failed.', 'User Not Found.');
	} elseif ($user->isActive === false) {
		// Throw Authentication Exception if User is Deactivated.
		throw new HttpException('ADP-1009', 401, 'Authorization Failed.', 'User is Deactivated.');
	}

	// Encode User details as JSON.
	$user = json_encode($user);

	// Cache User details into APCu Cache.
	$app->apcCache->save(
		'user-' . $username . '-' . $app->tenant->tenantId,
		$user,
		$app->config->apc->configCacheLifetime
	);
}

// Parse retrieved User details.
$user = json_decode($user);

// Check if User is a valid JSON Object.
if (!is_object($user)) {
	// Throw Authentication Exception if User is not a valid JSON Object.
	throw new HttpException('ADP-1010', 401, 'Authorization Failed.', 'Invalid User Details.');
}

// Set the User as DI's Service, so it can be used within Controllers.
$app->getDI()->setShared('user', $user)->resolve();
// Log User details.
/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
$app->logger->debug("User Authentication: {$user->username}");

// Validate Basic Authentication Password.
/** @var Phalcon\Mvc\Micro $app */
$password = filter_var($app->request->getServer('PHP_AUTH_PW'), FILTER_SANITIZE_STRING);
// Check if retrieved Password is in expected format first.
// Password is 32-characters long lowercase alphanumeric string (MD5 Hash).
if (!preg_match('/^[a-z0-9]{32}$/', $password)) {
	// Throw Authentication Exception if Password is missing or is in invalid format.
	throw new HttpException('ADP-1011', 401, 'Authorization Failed.', 'Invalid Password format.');
}
// Check if retrieved Password matches the one found in Database.
if (password_verify($password, $user->password) === false) {
	// Throw Authentication Exception if Password doesn't match.
	throw new HttpException('ADP-1012', 401, 'Authorization Failed.', 'Invalid Password.');
}


/*
 * HMAC Authorization.
 */
// Try to retrieve HMAC (keyed Hash Message Authentication Code) Authorization Hash.
$hmacHash = filter_var($app->request->getHeader('AUTHENTICATION'), FILTER_SANITIZE_STRING);

// Check if HMAC Hash was successfully retrieved from HMAC Authorization.
if (empty($hmacHash)) {
	// Throw Authorization Exception in case HMAC Authorization is missing or is invalid.
	throw new HttpException('ADP-1013', 401, 'Authorization Failed.', 'HMAC Hash Missing.');
}

// Retrieve HMAC details from HMAC Hash.
$hmacKeys = explode(':', base64_decode($hmacHash));
// Check if HMAC Authorization contains expected number of Keys.
if (count($hmacKeys) !== 3) {
	// Throw Authorization Exception if HMAC Authorization Hash does not have all expected Keys.
	throw new HttpException('ADP-1014', 401, 'Authorization Failed.', 'Invalid Number of HMAC Keys.');
}

// Check if retrieved HMAC elements are in expected format first.
// HMAC Timestamp is micro-time (13-digit integer).
$hmacTimestamp = filter_var($hmacKeys[0], FILTER_SANITIZE_NUMBER_INT);
if (!preg_match('/^[0-9]{13}$/', $hmacTimestamp)) {
	// Throw Authorization Exception if HMAC Timestamp is missing or is invalid.
	throw new HttpException('ADP-1015', 401, 'Authorization failed.', 'Invalid HMAC Timestamp format.');
}
// Convert HMAC Timestamp from milliseconds into seconds.
$hmacTimestamp = round($hmacTimestamp / 1000);
// HMAC Nonce is up to 5-digit integer in the range [0, 99999).
$hmacNonce = filter_var($hmacKeys[1], FILTER_SANITIZE_NUMBER_INT);
if (!preg_match('/^([0-9]|[1-9][0-9]{0,4})$/', $hmacNonce)) {
	// Throw Authorization Exception if HMAC Nonce is missing or is invalid.
	throw new HttpException('ADP-1016', 401, 'Authorization failed.', 'Invalid HMAC Nonce format.');
}
// HMAC Digest is Base64-encoded HMAC Hash (64-characters long lowercase alphanumeric string) generated using Secret as
// a Salt, which is a string concatenated from:
// 1. Request URI
// 2. Timestamp when the Request was generated on the Client side, rounded to 10 digits
// 3. Nonce
$hmacDigest = filter_var($hmacKeys[2], FILTER_SANITIZE_STRING);
if (!preg_match('/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/', $hmacDigest)) {
	// Throw Authorization Exception if HMAC Digest is missing or is invalid.
	throw new HttpException('ADP-1017', 401, 'Authorization failed.', 'Invalid HMAC Digest format.');
}

// Strip out Xdebug Query from the Request, if present and only if Debugging is enabled.
// This should NOT be used in Production!
/** @var object $app->config */
if ($app->config->app->debug === true) {
	if (mb_strpos($requestURI, 'XDEBUG_SESSION_START') !== false) {
		$requestURI = substr($requestURI, 0, mb_strpos($requestURI, '?XDEBUG_SESSION_START'));
	}
	// Log HMAC Authorization details.
	/** @var PARCC\ADP\Services\Logger\Multiple $app->logger */
	$app->logger->debug('HMAC Timestamp: ' . $hmacTimestamp . ', Server Timestamp: ' . time() .
	                    ', Timestamp Difference: ' . (time() - $hmacTimestamp));
}

// Calculate HMAC Digest.
$digest = base64_encode(hash_hmac('sha256', $requestURI . $hmacTimestamp . $hmacNonce, $user->secret));

// Check if HMAC Authorization Hash is valid.
if ($hmacDigest !== $digest) {
	// Throw Authorization Exception if HMAC Authorization Hash does not match.
	throw new HttpException('ADP-1018', 401, 'Authorization Failed.', 'Mismatched HMAC Digest.');
} elseif (abs(time() - $hmacTimestamp) > $app->config->app->hmacTimeTolerance) {
	// Throw Authorization Exception if the time elapsed since the moment HMAC Authorization Hash was generated is
	// greater than what is allowed by API (time difference in general as Client's clock can be either behind or ahead
	// of the Server's clock).
	throw new HttpException('ADP-1019', 408, 'Authorization Failed.', 'HMAC Timestamp has Expired.');
} else {
	// Pass the Request to the appropriate Controller handling the Request in case User is successfully Authenticated
	// and Authorized. Any additional validation of the Request will be performed inside the Controller handling the
	// Request.
	return true;
}
