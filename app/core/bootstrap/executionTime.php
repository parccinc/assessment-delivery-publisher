<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Application Execution Time Service.
 *
 * Calculates Application Execution Time in milliseconds. Only used for debugging purposes.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return null|string Returns Application Execution Time in milliseconds, but only if Debugging is enabled. Otherwise,
 *                     it returns NULL.
 */
// Check if Application Start Time exists.
if ($di->has('startTimestamp')) {

	// Return Application Execution Time in milliseconds.
	return '[Execution Time: ' . round(1000 * (microtime(true) - $di['startTimestamp'])) . ' ms]';

} else {

	// Return Empty Application Execution Time if not available.
	return '[Execution Time: N/A]';
}
