<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

use Phalcon\Db\Adapter\Pdo\Mysql as MysqlConnection;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger\Adapter\File as FileLogger;

/**
 * Bootstrap Primary Database Service.
 *
 * Primary Database Connection Service. This is Test Delivery Database and is exclusively used for Test Delivery. If
 * Application is exclusively used for Test Delivery this is the only Database Connection Service it will use - it will
 * use Users from this Database and will save Logs into this Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  MysqlConnection $mysqlConnection Database Connection with enabled Profiling and Logging.
 */
// Try to create Primary Database Connection. Warnings and Errors must be silenced, so Application can handle them
// appropriately and be able to recover without affecting the regular logical flow.
try {
	$mysqlConnection = @new MysqlConnection((array) $di['config']->database);
} catch (PDOException $exception) {
	// Log Primary Database Exception to the PHP Log, so we can track how frequently it occurs.
	error_log("Application Startup Error: Primary Database Server not available!");
	// Exit Application as Primary Database Server is not available and there is no point to continue as Application is
	// not fully initialized at this point!
	exit("Database Server Error!");
}

// Check if Primary Database Connection was successfully established (in case Persistent Connections are used).
try {
	// Depending on its configuration for 'wait_timeout' and 'interactive_timeout' variables, MySQL will close any
	// established inactive Persistent Connection. Due to the way PHP and PDO handle Persistent Connections, they might
	// not detect when existing Connection is closed on the other end and are assuming previously established Connection
	// is still available. This results in a failure to execute any SQL Query, and throws a Warning "MySQL server has
	// gone away." which cannot be intercepted as an Exception. A general Error Handler can be defined at the global
	// level, but it only catches Irrecoverable Errors which prohibits any recovery procedures and ends up with
	// Application exiting the default process flow.
	// In addition, PDO has a gap and cannot ping MySQL Server to validate if Connection was closed on the other side,
	// and automatically reconnect if necessary.
	// This can be handled though by configuring all PDO Errors to throw a PDOException, and the only way to do so is to
	// pass additional configuration to PDO while establishing a Database Connection:
	//   $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// Unfortunately, Phalcon does not support this attribute and does not allow passing any custom values. Further, it
	// automatically connects to the Database as part of instantiation of the MySQL Object, and as this is all in
	// private scope of its core, we have zero control over the way Phalcon handles PDO Connections.
	// Therefore, the only way to handle Persistent Connections getting disconnected is to use a lame solution and
	// execute a simplest possible SQL Query each time a Connection is expected to be established or reused.
	// This solution is widely adopted in many PHP frameworks (i.e. Doctrine DBAL) as there is no other, more elegant
	// solution for this problem.
	// Warnings and Errors must be silenced, so Application can handle them appropriately and be able to recover without
	// affecting the regular logical flow.
	@$mysqlConnection->query("SELECT 1");
} catch (PDOException $exception) {
	// Create DB Error Service with the Error, so it can be logged by Application. This is the only way to keep track of
	// the Log Entry as at this point Application hasn't instantiated any Controllers yet, and our options are quite
	// limited. The only way to redefine a Shared Service is to remove it first and re-create again.
	$di->setShared('dbError', function () {
		return "Primary MySQL server has gone away. Trying to reconnect.";
	})->resolve();
	// Log Database Exception to the PHP Log, so we can track how frequently it occurs.
	error_log("Primary MySQL server has gone away. Trying to reconnect.");
	// Try to reconnect to Primary Database Server.
	try {
		$mysqlConnection->connect();
	} catch (PDOException $exception) {
		// Log Primary Database Exception to the PHP Log, so we can track how frequently it occurs.
		error_log("Application Startup Error: Trying to reconnect to Primary Database Server failed!");
		// Exit Application as Primary Database Server is not available and there is no point to continue as Application
		// is not fully initialized at this point!
		exit("Database Server Error!");
	}
}

// Check if Debugging is turned On.
if ($di['config']->app->debug === true) {
	// Check if Primary Database SQL Logging is enabled.
	if ($di['config']->database->useSqlLogging === true) {
		// Configure Primary Database Logger.
		$dbLogger = new FileLogger(
			dirname(dirname(dirname(__DIR__))) . strtr($di['config']->database->sqlLogPath, ['{date}' => date('Ymd')])
		);
		// Get a shared instance of the Primary Database Profiler.
		$dbProfiler = $di['dbProfiler'];

		// Listen for all Primary Database Events.
		$eventsManager = new EventsManager();
		$eventsManager->attach(
			'db',
			function (Event $event, MysqlConnection $mysqlConnection) use ($di, $dbLogger, $dbProfiler) {
				if ($event->getType() === 'beforeQuery') {
					$dbLogger->setLogLevel($di['config']->logger->logLevel)
					         ->debug($mysqlConnection->getSQLStatement());
					$dbProfiler->startProfile($mysqlConnection->getSQLStatement());
				}
				if ($event->getType() === 'afterQuery') {
					$dbProfiler->stopProfile();
				}
			}
		);
		// Assign Event Manager to the Primary Database Connection instance.
		$mysqlConnection->setEventsManager($eventsManager);
	}
}

return $mysqlConnection;
