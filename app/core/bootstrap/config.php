<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

/**
 * Bootstrap Configuration Service.
 *
 * Returns Application Configuration. It also caches it inside APCu Cache. If not already cached, it reads Configuration
 * from the Config file.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @return  Phalcon\Config Application Configuration.
 */
// Try to retrieve Application Configuration from APCu Cache.
$config = $di['apcCache']->get('config');

// Check if Application Configuration was successfully loaded (if it was previously cached).
if (empty($config)) {
	// Load and parse Application Configuration.
	$config = require_once dirname(dirname(__DIR__)) . '/config/config.php';

	// Cache retrieved Application Configuration as JSON into APCu Cache.
	$di['apcCache']->save('config', json_encode($config), $config->apc->configCacheLifetime);

} else {
	// Decode JSON Configuration.
	$config = json_decode($config);
}

// Return Application Configuration.
return $config;
