<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services\AWS;

use Phalcon\Di;

/**
 * Class S3
 *
 * This is the AWS S3 Client. It communicates with AWS S3 API in order to administer data inside AWS S3 Bucket. All
 * File Resources are organized by AWS S3 Bucket Prefixes ("subfolders") where each Prefix has its own Access
 * Permissions.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 * @see     http://docs.aws.amazon.com/AmazonS3/latest/API/APIRest.html AWS S3 API Documentation.
 */
final class S3
{

	/**
	 * AWS S3 Access Control List (ACL) properties (@default=ACL_PRIVATE).
	 */
	const ACL_PRIVATE = 'private';
	const ACL_AUTHENTICATED_READ = 'authenticated-read';
	const ACL_PUBLIC_READ = 'public-read';
	const ACL_PUBLIC_READ_WRITE = 'public-read-write';
	/**
	 * AWS S3 Storage Type properties (@default=STORAGE_CLASS_STANDARD).
	 */
	const STORAGE_CLASS_STANDARD = 'STANDARD';
	const STORAGE_CLASS_RRS = 'REDUCED_REDUNDANCY';
	/**
	 * AWS S3 Server Side Encryption properties (@default=SSE_AES256).
	 */
	const SSE_AES256 = 'AES256';
	const SSE_NONE = '';

	/**
	 * AWS S3 Protocol for securing HTTPS Connections.
	 * By explicitly setting TLS v1.2 as protocol for securing HTTPS Connections, it decreases time required to perform
	 * handshake with AWS S3 API and also confirms with AWS security policies. However, this requires a recent version
	 * of CURL library to be installed. Otherwise, 'CURL_SSLVERSION_TLSv1' should be used as a generic value.
	 *
	 * AWS announcement about discontinuing insecure SSLv3 protocol:
	 * "As of 12:00 AM PDT April 30, 2015, AWS will discontinue support of SSLv3 for securing connections to S3 buckets.
	 * Security research published late last year demonstrated that SSLv3 contained weaknesses that weakened its
	 * ability to protect and secure communications. These weaknesses have been addressed in the replacement for SSL,
	 * TLS. Since then, major Browser software vendors have been disabling support for SSLv3 and their work is largely
	 * complete. Consistent with our top priority to protect AWS customers, AWS will only support versions of the more
	 * modern Transport Layer Security (TLS) rather than SSLv3."
	 */
	const USE_SSL_VERSION = CURL_SSLVERSION_TLSv1;

	/**
	 * @var string $endpoint AWS S3 URL (@default='s3.amazonaws.com').
	 */
	private static $endpoint = 's3.amazonaws.com';
	/**
	 * @var string $stsEndpoint AWS STS (Security Token Service) URL (@default='http://169.254.169.254').
	 */
	private static $stsEndpoint = 'http://169.254.169.254';
	/**
	 * @var string $accessKeyId AWS S3 Access Key ID is used together with Secret Access Key in order to get a
	 *                          permission for access to related AWS S3 Bucket. It's considered as a Username whereas
	 *                          Secret Access Key is considered as a Password. These are set dynamically if Server has
	 *                          assigned an Identity and Access Management (IAM) Role with adequate ACL Permissions.
	 *                          However, IAM Role can only be assigned to an AWS EC2 Server instance, and only at the
	 *                          moment when it's provisioned which means it cannot be assigned to any server outside AWS
	 *                          Virtual Private Cloud (VPC). Therefore, any Server outside AWS' internal Network (or in
	 *                          general, any non-AWS Server) must use Access Key ID and Secret Access Key in order to
	 *                          access AWS S3 Storage.
	 */
	public static $accessKeyId;
	/**
	 * @var string $secretAccessKey AWS S3 Secret Access Key is used together with Access Key ID in order to get a
	 *                              permission for access to related AWS S3 Bucket. It's considered as a Password
	 *                              whereas Access Key ID is considered as a Username. These are set dynamically if
	 *                              Server has assigned an Identity and Access Management (IAM) Role with adequate ACL
	 *                              Permissions.
	 */
	public static $secretAccessKey;
	/**
	 * @var string $securityToken AWS IAM Role Security Token is used together with  Access Key ID and Secret Access Key
	 *                            when AWS IAM Role is used in which case AWS STS (Security Token Service) is providing
	 *                            temporary credentials. Otherwise, if Access Key ID and Secret Access Key are used
	 *                            (hard-coded in the configuration file), Security Token is not used.
	 */
	public static $securityToken;
	/**
	 * @var boolean $useHttps Indicates if Connection to AWS S3 API should be done over HTTPS (@default=true).
	 */
	public static $useHttps = true;
	/**
	 * @var boolean $useSslHostValidation Indicates if HTTPS Connection should verify AWS S3's SSL Certificate Name
	 *                                    against its Host (verifies if requested Host and its Certificate's Host Name
	 *                                    match) (@default=true).
	 */
	public static $useSslHostValidation = true;
	/**
	 * @var boolean $useSslPeerValidation Indicates if HTTPS Connection should verify AWS S3's Peer's SSL Certificate
	 *                                    (verifies Identity with Certification Authority (CA) that issues SSL
	 *                                    Certificate). Currently AWS does NOT support SSL Identity validation
	 *                                    (@default=false).
	 */
	public static $useSslPeerValidation = false;


	/**
	 * Constructor.
	 *
	 * Sets all AWS S3 and CloudFront required configuration parameters. It doesn't have to be instantiated as it
	 * already exists as a Service unless non-default parameters need to be used.
	 *
	 * @param string   $accessKeyId          AWS S3 Access Key ID.
	 * @param string   $secretAccessKey      AWS S3 Secret Access Key.
	 * @param string   $endpoint             AWS S3 URL (@default='s3.amazonaws.com').
	 * @param string   $stsEndpoint          AWS STS (Security Token Service) URL (@default='http://169.254.169.254').
	 * @param boolean  $useHttps Indicates   if HTTPS Connection should be used to access AWS S3 API
	 *                                       (@default=true).
	 * @param boolean  $useSslHostValidation Indicates if validation of AWS S3 SSL Certificate Name against its Host
	 *                                       should be performed (@default=true).
	 * @param boolean  $useSslPeerValidation Indicates if validation of AWS S3 SSL Certificate Identity against its CA
	 *                                       (Certificate Authority) should be performed (@default=false).
	 */
	public function __construct(
		$accessKeyId = '',
		$secretAccessKey = '',
		$endpoint = 's3.amazonaws.com',
		$stsEndpoint = 'http://169.254.169.254',
		$useHttps = true,
		$useSslHostValidation = true,
		$useSslPeerValidation = false
	) {
		// Check if S3 Access Key ID and/or S3 Secret Access Key are provided. If absent, the assumption is that IAM
		// Role is assigned to the Server and will try to be automatically retrieved during Authentication.
		if (!empty($accessKeyId) && !empty($secretAccessKey)) {
			// Set AWS S3 Access Key ID and Secret Access Key in case Access Keys are used for S3 API Authentication.
			self::$accessKeyId = $accessKeyId;
			self::$secretAccessKey = $secretAccessKey;
		}

		// Set AWS S3 default configuration parameters.
		self::$useHttps = $useHttps;
		self::$useSslHostValidation = $useSslHostValidation;
		self::$useSslPeerValidation = $useSslPeerValidation;

		// Set AWS S3 and AWS STS Endpoints.
		self::$endpoint = $endpoint;
		self::$stsEndpoint = $stsEndpoint;
	}


	/**
	 * Performs Authentication against AWS STS (Security Token Service) in order to assume IAM Role for EC2 Instance in
	 * case IAM Role is used for Authentication for S3 API. It also caches received temporary credentials (Access Key ID
	 * Secret Access Key and Security Token) for as long as they are valid (default is 12h).
	 *
	 * @throws S3Exception Throws S3Exception in case of STS API Error.
	 */
	public static function authenticate()
	{
		// Check if IAM Role should be used for Authentication (in case either of S3 Access Keys is missing).
		if (!empty(self::$accessKeyId) && !empty(self::$secretAccessKey)) {
			// In case both AWS Access Key ID and AWS Secret Access Key are provided, skip IAM Role Authentication.
			return;
		}

		// Get default Dependency Injector in order to get access to Application Services.
		$di = Di::getDefault();

		// Try to retrieve temporary S3 Credentials from APCu Cache and assume assigned IAM Role.
		$credentials = $di['apcCache']->get('s3-keys');

		// Check if temporary S3 Credentials were successfully loaded (if they were previously cached).
		if (empty($credentials)) {
			// Try to retrieve AWS IAM (Identity and Access Management) Role Name from APCu Cache used to provide ACL
			// Permissions to EC2 Server Instance.
			$iamRole = $di['apcCache']->get('iam-role');

			// Check if IAM Role was successfully loaded (if it was previously cached).
			if (empty($iamRole)) {
				/*
				 * "If there is an IAM role associated with the instance at launch, contains information about the last
				 * time the instance profile was updated, including the instance's LastUpdated date, InstanceProfileArn,
				 * and InstanceProfileId. Otherwise, not present."
				 *
				 * @example: The following command retrieves the IAM Info including IAM Role:
				 * curl http://169.254.169.254/latest/meta-data/iam/info
				 * {
				 *   "Code" : "Success",
				 *   "LastUpdated" : "2012-04-26T16:39:16Z",
				 *   "InstanceProfileArn" : "arn:aws:iam::483537475129:instance-profile/IAM-ROLE",
				 *   "InstanceProfileId" : "AKIAIOSFODNN7EXAMPLE"
				 * }
				 *
				 * @see: AWS Documentation
				 *       http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html
				 */
				// Initialize Instance Metadata and User Data Service Request to retrieve IAM Role.
				$metadataRequest = new S3Request(
					'GET',
					self::$stsEndpoint . '/latest/meta-data/iam/info',
					'',
					'',
					false
				);

				// Send Metadata Request and process the Response.
				$metadataRequest->send();

				// Check if there was an Error while making a Request.
				if ($metadataRequest->response->error === false && $metadataRequest->response->code !== 200) {
					// Throw S3Exception if unexpected HTTP Status Code was received.
					throw new S3Exception(
						"S3 S3Authenticate Error: [{$metadataRequest->response->code}] Received unexpected HTTP " .
						"Status.",
						1600
					);

				} elseif ($metadataRequest->response->error !== false) {
					// Throw S3Exception in case S3 API returned an Error.
					throw new S3Exception(
						"S3 S3Authenticate Error: [{$metadataRequest->response->error['code']}] " .
						"{$metadataRequest->response->error['message']}.",
						1601
					);
				}

				// Parse received IAM Info.
				$iamInfo = json_decode($metadataRequest->response->body, true);

				// Check if Metadata Request was successful.
				if (isset($iamInfo['Code'])) {
					if ($iamInfo['Code'] !== 'Success') {
						// Throw S3Exception in case Metadata Request was not successful.
						throw new S3Exception(
							"S3 S3Authenticate Error: [{$iamInfo['Code']}] Unexpected Instance Profile Response.",
							1602
						);
					}

				} else {
					// Throw S3Exception in case Metadata Request was not successful.
					throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1603);
				}

				// Check if Instance Profile ARN (Amazon Resource Name) was received.
				if (isset($iamInfo['InstanceProfileArn'])) {
					// Parse IAM Role from the EC2 Instance Profile ARN.
					$iamInfo = explode('/', $iamInfo['InstanceProfileArn']);

					// Check if IAM Role was successfully retrieved from the Instance Profile ARN.
					if (isset($iamInfo[1])) {
						// Retrieve IAM Role.
						$iamRole = $iamInfo[1];

					} else {
						// Throw S3Exception in case IAM Role was not successfully retrieved.
						throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1604);
					}

				} else {
					// Throw S3Exception in case IAM Role was not successfully retrieved.
					throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1605);
				}

				// Cache retrieved IAM Role into APCu Cache (by default with permanent Cache Lifetime as IAM Role can
				// only be assigned to EC2 Instance when it's created and cannot be changed after that, so assigned IAM
				// Role remains permanently assigned to the EC2 Server).
				$di['apcCache']->save('iam-role', $iamRole, $di['config']->apc->iamRoleCacheLifetime);
			}

			/*
			 * "An application on the instance retrieves the security credentials provided by the role from the instance
			 * metadata item iam/security-credentials/role-name. The application is granted the permissions for the
			 * actions and resources that you've defined for the role through the security credentials associated with
			 * the role. These security credentials are temporary and we rotate them automatically. We make new
			 * credentials available at least five minutes prior to the expiration of the old credentials."
			 *
			 * @example: The following command retrieves the security credentials for an IAM role named s3access:
			 * curl http://169.254.169.254/latest/meta-data/iam/security-credentials/s3access
			 * {
			 *   "Code" : "Success",
			 *   "LastUpdated" : "2012-04-26T16:39:16Z",
			 *   "Type" : "AWS-HMAC",
			 *   "AccessKeyId" : "AKIAIOSFODNN7EXAMPLE",
			 *   "SecretAccessKey" : "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY",
			 *   "Token" : "token",
			 *   "Expiration" : "2012-04-27T22:39:16Z"
			 * }
			 *
			 * @see: AWS Documentation
			 *       http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html
			 */
			// Initialize STS Request.
			$stsRequest = new S3Request(
				'GET',
				self::$stsEndpoint . '/latest/meta-data/iam/security-credentials/' . $iamRole,
				'',
				'',
				false
			);

			// Send STS Request and process the Response.
			$stsRequest->send();

			// Check if there was an Error while making a Request.
			if ($stsRequest->response->error === false && $stsRequest->response->code !== 200) {
				// Throw S3Exception if unexpected HTTP Status Code was received.
				throw new S3Exception(
					"S3 S3Authenticate Error: [{$stsRequest->response->code}] Received unexpected HTTP Status.",
					1606
				);

			} elseif ($stsRequest->response->error !== false) {
				// Throw S3Exception in case S3 API returned an Error.
				throw new S3Exception(
					"S3 S3Authenticate Error: [{$stsRequest->response->error['code']}] " .
					"{$stsRequest->response->error['message']}.",
					1607
				);
			}

			// Parse received S3 Credentials.
			$credentials = json_decode($stsRequest->response->body, true);

			// Check if STS Request was successful.
			if (isset($credentials['Code'])) {
				if ($credentials['Code'] !== 'Success') {
					// Throw S3Exception in case STS Request was not successful.
					throw new S3Exception(
						"S3 S3Authenticate Error: [{$credentials['Code']}] Unexpected Instance Profile Response.",
						1608
					);
				}

			} else {
				// Throw S3Exception in case STS Request was not successful.
				throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1609);
			}

			// Check if S3 Credentials Expiration Timestamp was received.
			if (isset($credentials['Expiration'])) {
				// Parse S3 Credentials Expiration Timestamp (received UTC Date and Time is in ISO 8601 format).
				$expiration = strtotime((string) $credentials['Expiration']);

			} else {
				// Throw S3Exception in case S3 API returned an Error.
				throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1610);
			}

			// Check if Access Key ID was received.
			if (!isset($credentials['AccessKeyId'])) {
				// Throw S3Exception in case Access Key ID was not successfully retrieved.
				throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1611);
			}

			// Check if Secret Access Key was received.
			if (!isset($credentials['SecretAccessKey'])) {
				// Throw S3Exception in case Secret Access Key was not successfully retrieved.
				throw new S3Exception("S3 S3Authenticate Error: Unexpected Instance Profile Response.", 1612);
			}

			// Cache generated list of S3 Credentials into APCu Cache with Cache Lifetime that matches received
			// Expiration Timestamp less 4 minutes to avoid using expired Security Token. Having in mind that AWS STS
			// will generate new Authentication Credentials at least 5min prior to the expiration of the previous ones
			// and to avoid risk of previous Authentication Credentials expiring while the request is being processed,
			// new set of Authentication Credentials should be requested 4 minutes prior to their expiration!
			$di['apcCache']->save(
				's3-keys',
				base64_encode(json_encode($credentials)),
				(empty($expiration) ? null : ($expiration - time() - 240))
			);

		} else {
			$credentials = json_decode(base64_decode($credentials), true);
		}

		// Set Access Key ID, if received.
		if (isset($credentials['AccessKeyId'])) {
			self::$accessKeyId = (string) $credentials['AccessKeyId'];
		}
		// Set Secret Access Key, if received.
		if (isset($credentials['SecretAccessKey'])) {
			self::$secretAccessKey = (string) $credentials['SecretAccessKey'];
		}
		// Set Security Token, if received.
		if (isset($credentials['Token'])) {
			self::$securityToken = (string) $credentials['Token'];
		}
	}


	/**
	 * Gets List of Objects inside given AWS S3 Bucket (List all Objects in alphabetical order).
	 *
	 * @see    http://docs.aws.amazon.com/AmazonS3/latest/API/RESTBucketGET.html AWS S3 API Documentation.
	 * @param  string      $bucket               S3 Bucket Name.
	 * @param  string      $prefix               Key Prefix returns only Keys that begin with it.
	 * @param  string      $marker               Marker specifies the Key to start with.
	 * @param  string      $maxKeys              Maximum Number of Keys to return. By default, S3 API returns 1000 Keys!
	 *                                           BUT if $maxKeys is NULL, this method will loop through truncated result
	 *                                           sets!
	 * @param  string      $delimiter            Delimiter is a character used to group Keys. All Keys that contain the
	 *                                           same string between the Prefix and the first occurrence of the
	 *                                           Delimiter after the Prefix are grouped under CommonPrefixes.
	 * @param  boolean     $returnCommonPrefixes Indicates if CommonPrefixes should be returned or not (@default=false).
	 * @return array                             Return an Array of Objects stored within given S3 Bucket.
	 * @throws S3Exception                       Throws S3Exception in case of S3 API Error.
	 */
	public function getObjectList(
		$bucket,
		$prefix = null,
		$marker = null,
		$maxKeys = null,
		$delimiter = null,
		$returnCommonPrefixes = false
	) {
		// Initialize Request.
		$s3Request = new S3Request('GET', self::$endpoint, $bucket);

		// Set Request Parameters.
		if (!empty($prefix)) {
			// Remove leading slash, if necessary.
			if ($prefix[0] === '/') {
				$prefix = substr($prefix, 1);
			}
			// Check if Prefix is an empty string.
			if (!empty($prefix)) {
				$s3Request->setParameter('prefix', $prefix);
			}
		}
		if (!empty($marker)) {
			$s3Request->setParameter('marker', $marker);
		}
		if (!empty($maxKeys)) {
			$s3Request->setParameter('max-keys', $maxKeys);
		}
		if (!empty($delimiter)) {
			$s3Request->setParameter('delimiter', $delimiter);
		}

		// Send S3 Request and process the Response.
		$s3Request->send();

		// Check if there was an Error while making a Request.
		if ($s3Request->response->error === false && $s3Request->response->code !== 200) {
			// Throw S3Exception if unexpected HTTP Status Code was received.
			throw new S3Exception(
				"S3 GetObjectList Error: [{$s3Request->response->code}] Received unexpected HTTP Status.",
				1613
			);
		} elseif ($s3Request->response->error !== false) {
			// Throw S3Exception in case S3 API returned an Error.
			throw new S3Exception(
				"S3 GetObjectList Error: [{$s3Request->response->error['code']}] " .
				"{$s3Request->response->error['message']}.",
				1614
			);
		}

		// Construct the Results.
		$results = [];
		// Keep tracking what the next Marker is.
		$nextMarker = null;

		// Parse Content (Objects), if found.
		if (isset($s3Request->response->body, $s3Request->response->body->Contents)) {
			// Loop through all Objects.
			foreach ($s3Request->response->body->Contents as $content) {
				$results[(string) $content->Key] = [
					'name' => (string) $content->Key,
					'time' => strtotime((string) $content->LastModified),
					'size' => (int) $content->Size,
					'hash' => mb_substr((string) $content->ETag, 1, -1)
				];

				// Set the next Marker.
				$nextMarker = (string) $content->Key;
			}
		}

		// Parse CommonPrefixes, if found, and if requested as a call argument.
		if ($returnCommonPrefixes === true &&
			isset($s3Request->response->body, $s3Request->response->body->CommonPrefixes)
		) {
			// Loop through all CommonPrefixes.
			foreach ($s3Request->response->body->CommonPrefixes as $commonPrefix) {
				$results[(string) $commonPrefix->Prefix] = [
					'prefix' => (string) $commonPrefix->Prefix
				];
			}
		}

		// Return Results if Response is not truncated.
		if (isset($s3Request->response->body, $s3Request->response->body->IsTruncated) &&
			(string) $s3Request->response->body->IsTruncated === 'false'
		) {
			return $results;
		}

		// Set the next Marker, if provided.
		if (isset($s3Request->response->body, $s3Request->response->body->NextMarker)) {
			$nextMarker = (string) $s3Request->response->body->NextMarker;
		}

		// Loop through truncated results if $maxKeys is NOT specified (is NULL).
		if ($maxKeys === null && $nextMarker !== null && (string) $s3Request->response->body->IsTruncated === 'true') {
			// Iteratively call getBucket API until ALL Keys are retrieved.
			// This can take a long time if S3 Bucket has a lot of Keys!
			// In case S3 Bucket has reasonably small number of Keys, it would be more elegant to recursively call the
			// method, BUT if S3 Bucket has a large number of Keys recursion can require high memory consumption as each
			// time a recursive call happens it creates a stack frame. In addition, if S3 Bucket has really large number
			// of Keys, it can reach Recursion Limit and trigger a segfault (Segmentation Fault which is basically
			// Memory Access Violation)!
			do {
				// Initialize new Request.
				$s3Request = new S3Request('GET', self::$endpoint, $bucket);

				// Set Request parameters.
				if (!empty($prefix)) {
					$s3Request->setParameter('prefix', $prefix);
				}
				$s3Request->setParameter('marker', $nextMarker);
				if (!empty($delimiter)) {
					$s3Request->setParameter('delimiter', $delimiter);
				}

				// Send new S3 Request and process the Response.
				$s3Request->send();

				// Check if there was an Error while making a Request, OR if S3 API returned an Error.
				if ($s3Request->response->error !== false || $s3Request->response->code !== 200) {
					break;
				}

				// Parse Content (Objects), if found.
				if (isset($s3Request->response->body, $s3Request->response->body->Contents)) {
					foreach ($s3Request->response->body->Contents as $content) {
						$results[(string) $content->Key] = [
							'name' => (string) $content->Key,
							'time' => strtotime((string) $content->LastModified),
							'size' => (int) $content->Size,
							'hash' => mb_substr((string) $content->ETag, 1, -1)
						];

						// Set the next Marker.
						$nextMarker = (string) $content->Key;
					}
				}

				// Parse CommonPrefixes, if found, and if requested as a call argument.
				if ($returnCommonPrefixes === true &&
					isset($s3Request->response->body, $s3Request->response->body->CommonPrefixes)
				) {
					foreach ($s3Request->response->body->CommonPrefixes as $commonPrefix) {
						$results[(string) $commonPrefix->Prefix] = [
							'prefix' => (string) $commonPrefix->Prefix
						];
					}
				}

				// Set the next Marker, if provided.
				if (isset($s3Request->response->body, $s3Request->response->body->NextMarker)) {
					$nextMarker = (string) $s3Request->response->body->NextMarker;
				}

			} while ($s3Request->response !== false && (string) $s3Request->response->body->IsTruncated === 'true');
		}

		// Return list of S3 Bucket Content.
		return $results;
	}


	/**
	 * Downloads an Object from AWS S3 Bucket.
	 *
	 * @see    http://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectGET.html AWS S3 API Documentation.
	 * @param  string      $bucket   S3 Bucket Name.
	 * @param  string      $key      Object Key (URI).
	 * @param  resource    $savePath Optional File Path or Resource to write downloaded S3 Object to.
	 * @return object                Returns the Data Object downloaded from S3 with its Metadata, OR just its Metadata
	 *                               if it's saved to provided $savePath.
	 * @throws S3Exception           Throws S3Exception in case of S3 API Error.
	 */
	public function downloadObject($bucket, $key, $savePath = null)
	{
		// Initialize Request.
		$s3Request = new S3Request('GET', self::$endpoint, $bucket, $key);

		// Check if downloaded Object should also be saved to a File Path or to a Resource.
		if (!empty($savePath)) {
			if (is_resource($savePath)) {
				// In case downloaded Object should be saved to a Resource.
				$s3Request->fileHandler =& $savePath;
			} elseif (($s3Request->fileHandler = @fopen($savePath, 'wb')) !== false) {
				// In case downloaded Object should be saved to a File. Warnings and Errors must be silenced, so
				// Application can handle them appropriately and be able to recover without affecting the regular
				// logical flow.
			} else {
				// Throw S3Exception if given File Path cannot be open for writing.
				throw new S3Exception("S3 DownloadObject Error: Unable to open file {$savePath} for writing.", 1615);
			}
		}

		// Send S3 Request and process the Response.
		$s3Request->send();

		// Check if there was an Error while making a Request.
		if ($s3Request->response->error === false && $s3Request->response->code !== 200 &&
			$s3Request->response->code !== 404
		) {
			// Throw S3Exception if unexpected HTTP Status Code was received.
			throw new S3Exception(
				"S3 DownloadObject Error: [{$s3Request->response->code}] Received unexpected HTTP Status.",
				1616
			);
		} elseif ($s3Request->response->error !== false) {
			// Throw S3Exception in case S3 API returned an Error.
			throw new S3Exception(
				"S3 DownloadObject Error: [{$s3Request->response->error['code']}] " .
				"{$s3Request->response->error['message']}.",
				1617
			);
		}

		// Return requested Object, OR its Metadata if $savePath was provided.
		return $s3Request->response;
	}


	/**
	 * Gets Object Details from AWS S3 Bucket.
	 *
	 * @see    http://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectHEAD.html AWS S3 API Documentation.
	 * @param  string      $bucket        S3 Bucket Name.
	 * @param  string      $key           Object Key (URI).
	 * @param  boolean     $returnDetails Indicates if Object Details should be returned, OR a simple confirmation if
	 *                                    referenced Object exists inside the S3 Bucket (@default=true).
	 * @return array|true                 Returns the Data Object Details from S3 with its Metadata, OR TRUE as a
	 *                                    confirmation that Object exists if Details are not requested (@default=true).
	 * @throws S3Exception                Throws S3Exception in case of S3 API Error.
	 */
	public function getObjectDetails($bucket, $key, $returnDetails = true)
	{
		// Initialize Request.
		$s3Request = new S3Request('HEAD', self::$endpoint, $bucket, $key);

		// Send S3 Request and process the Response.
		$s3Request->send();

		// Check if there was an Error while making a Request.
		if ($s3Request->response->error === false && $s3Request->response->code !== 200 &&
			$s3Request->response->code !== 404
		) {
			// Throw S3Exception if unexpected HTTP Status Code was received.
			throw new S3Exception(
				"S3 GetObjectDetails Error: [{$s3Request->response->code}] Received unexpected HTTP Status.",
				1618
			);
		} elseif ($s3Request->response->error !== false) {
			// Throw S3Exception in case S3 API returned an Error.
			throw new S3Exception(
				"S3 GetObjectDetails Error: [{$s3Request->response->error['code']}] " .
				"{$s3Request->response->error['message']}.",
				1619
			);
		}

		// Check if requested Object Details need to be included in the Response.
		if ($returnDetails) {
			// Return requested Object Details.
			return $s3Request->response;
		} else {
			// Return simple confirmation that referenced Object exists inside the S3 Bucket.
			return true;
		}
	}


	/**
	 * Get MIME Type for a given File based on its File Extension.
	 *
	 * @param  string|resource $file File Path, OR a File Resource.
	 * @return string                Returns identified MIME Type.
	 */
	private function getMimeType($file)
	{
		// List of known MIME Types.
		$mimeType = [
			// Text Files.
			'txt' => 'text/plain',
			'csv' => 'text/csv',
			'htm' => 'text/html',
			'html' => 'text/html',
			'xhtml' => 'text/html+xml',
			'xml' => 'text/xml',
			'css' => 'text/css',
			'js' => 'text/javascript',
			'json' => 'application/json',
			// Image Files.
			'jpg' => 'image/jpeg',
			'jpe' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'gif' => 'image/gif',
			'png' => 'image/png',
			'tif' => 'image/tiff',
			'tiff' => 'image/tiff',
			'svg' => 'image/svg+xml',
			'svgz' => 'image/svg+xml',
			'ico' => 'image/x-icon',
			// Object/Document Files.
			'swf' => 'application/x-shockwave-flash',
			'pdf' => 'application/pdf',
			'zip' => 'application/zip',
			// Audio Files.
			'wav' => 'audio/x-wav',
			'mp3' => 'audio/mpeg',
			'acc' => 'audio/x-acc',
			'ogg' => 'audio/ogg',
			'oga' => 'audio/ogg',
			// Video Files.
			'flv' => 'video/x-flv',
			'avi' => 'video/x-msvideo',
			'mpg' => 'video/mpeg',
			'mpeg' => 'video/mpeg',
			'mp4' => 'video/mp4',
			'mov' => 'video/quicktime',
			'ogv' => 'video/ogg',
			'webm' => 'video/webm',
			// Font Files.
			'ttf' => 'application/x-font-ttf',
			'otf' => 'application/x-font-otf',
			'eot' => 'application/vnd.ms-fontobject',
			'woff' => 'application/font-woff'
		];

		// Get File Extension.
		$fileExtension = mb_strtolower(pathinfo($file, PATHINFO_EXTENSION));

		// Check if MIME Type is known for the File.
		if ($fileExtension === 'gz') {
			// In case File is GZipped version of the Original File, return the MIME Type of the Original File.
			return $this->getMimeType(pathinfo($file, PATHINFO_FILENAME));
		} elseif (isset($mimeType[$fileExtension])) {
			// return identified MIME Type.
			return $mimeType[$fileExtension];
		} else {
			// Return the default AWS S3 Object MIME Type.
			return 'application/octet-stream';
		}
	}


	/**
	 * Uploads an Object to AWS S3 Bucket.
	 *
	 * @see    http://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectPUT.html AWS S3 API Documentation.
	 * @param  array|string $object               Data Object to be uploaded to S3. Can be either an Object Array, OR a
	 *                                            String.
	 * @param  string       $bucket               S3 Bucket Name.
	 * @param  string       $key                  Object Key (URI).
	 * @param  array|string $headers              Array of Response Headers, OR "Content-Type" as a string.
	 * @param  array        $metaHeaders          Array of AMZ Metadata Headers ("x-amz-meta-*" Headers).
	 * @param  string       $acl                  S3 Permission for the Object (@default=ACL_PRIVATE).
	 * @param  string       $storageClass         S3 Storage Type (@default=STORAGE_CLASS_STANDARD).
	 * @param  string       $serverSideEncryption Indicator if Object should be encrypted on the Server-side.
	 * @return void                               If AWS S3 API returns an Error, a S3Exception gets triggered with
	 *                                            Error details. Otherwise, nothing is returned as Metadata for uploaded
	 *                                            file is contained within S3 Response. Returned Metadata contains:
	 *                                            "x-amz-id-2", "x-amz-request-id", "Date",
	 *                                            "x-amz-server-side-encryption", "ETag", "Content-Length" and
	 *                                            "Server: Amazon S3" Response Headers only.
	 * @throws S3Exception                        Throws S3Exception in case of S3 API Error.
	 */
	public function uploadObject(
		$object,
		$bucket,
		$key,
		$headers = [],
		$metaHeaders = [],
		$acl = self::ACL_PRIVATE,
		$storageClass = self::STORAGE_CLASS_STANDARD,
		$serverSideEncryption = self::SSE_AES256
	) {
		// Check if Object was provided.
		if ($object === false) {
			// Throw S3Exception if Object is not valid.
			throw new S3Exception("S3 UploadObject Error: Invalid Object.", 1620);
		}

		// Construct the Object, if necessary.
		if (!is_array($object)) {
			$object = [
				'data' => $object,
				'size' => strlen($object),
				'md5' => base64_encode(md5($object, true))
			];
		}

		// Initialize Request.
		$s3Request = new S3Request('PUT', self::$endpoint, $bucket, $key);

		// Set Object Data.
		if (isset($object['fileHandler'])) {
			$s3Request->fileHandler =& $object['fileHandler'];
		} elseif (isset($object['file'])) {
			// Warnings and Errors must be silenced, so Application can handle them appropriately and be able to recover
			// without affecting the regular logical flow.
			$s3Request->fileHandler = @fopen($object['file'], 'rb');
		} elseif (isset($object['data'])) {
			$s3Request->data = $object['data'];
		}

		// Set Object "Content-Length" (required).
		if (isset($object['size']) && $object['size'] >= 0) {
			$s3Request->size = $object['size'];
		} elseif (isset($object['file'])) {
			// Clear File status cache.
			clearstatcache(false, $object['file']);

			$s3Request->size = filesize($object['file']);
		} elseif (isset($object['data'])) {
			$s3Request->size = strlen($object['data']);
		}

		// Check if provided Object is valid.
		if (!($s3Request->size >= 0 && ($s3Request->fileHandler !== false || $s3Request->data !== false))) {
			// Throw S3Exception if Object is not valid.
			throw new S3Exception("S3 UploadObject Error: Missing Object parameters.", 1621);
		}

		// Set Object "Content-Type", if not set already.
		if (!isset($object['type'])) {
			// Check if Object is a File.
			if (isset($object['file'])) {
				// Try to identify Content-Type based on the File MIME Type (determined based on the File Extension).
				$s3Request->setHeader('Content-Type', $this->getMimeType($object['file']));
				// Add "Content-Encoding" for SVGZ images.
				if (mb_strtolower(pathinfo($object['file'], PATHINFO_EXTENSION)) === 'svgz') {
					$s3Request->setHeader('Content-Encoding', 'gzip');
				}
			} elseif (isset($object['fileHandler'])) {
				// Try to identify Content-Type based on the Key MIME Type when using File Resource (determined based on
				// the File Extension, if available within the Key).
				$s3Request->setHeader('Content-Type', $this->getMimeType($key));
				// Add "Content-Encoding" for SVGZ images.
				if (mb_strtolower(pathinfo($key, PATHINFO_EXTENSION)) === 'svgz') {
					$s3Request->setHeader('Content-Encoding', 'gzip');
				}
			} else {
				// For any other type of Object use default AWS S3 Object MIME Type.
				$s3Request->setHeader('Content-Type', 'application/octet-stream');
			}
		}

		// Set Object Custom Response Headers (i.e. "Content-Type", "Content-Disposition", "Content-Encoding" etc.).
		if (is_array($headers)) {
			foreach ($headers as $header => $headerValue) {
				mb_strpos($header, 'x-amz-') === 0 ?
					$s3Request->setMetaHeader($header, $headerValue) :
					$s3Request->setHeader($header, $headerValue);
			}
		} elseif (is_string($headers)) {
			// Set Object "Content-Type", if provided as a single string.
			$s3Request->setHeader('Content-Type', $headers);
		}

		// Set Object Custom AMZ Metadata Headers ("x-amz-meta-*").
		foreach ($metaHeaders as $metaHeader => $metaHeaderValue) {
			$s3Request->setMetaHeader('x-amz-meta-' . $metaHeader, $metaHeaderValue);
		}

		// Set Object Permissions.
		$s3Request->setMetaHeader('x-amz-acl', $acl);

		// Set Object Storage Type, if necessary.
		if ($storageClass !== self::STORAGE_CLASS_STANDARD) {
			$s3Request->setMetaHeader('x-amz-storage-class', $storageClass);
		}

		// Set Object Server-side Encryption, if necessary.
		if ($serverSideEncryption !== self::SSE_NONE) {
			$s3Request->setMetaHeader('x-amz-server-side-encryption', $serverSideEncryption);
		}

		// Set Object MD5 Hash to be used as ETag, if necessary (otherwise AWS S3 API will automatically calculate it).
		if (isset($object['md5'])) {
			$s3Request->setHeader('Content-MD5', $object['md5']);
		}


		// Send S3 Request and process the Response.
		$s3Request->send();

		// Check if there was an Error while making a Request.
		if ($s3Request->response->error === false && $s3Request->response->code !== 200) {
			// Throw S3Exception if unexpected HTTP Status Code was received.
			throw new S3Exception(
				"S3 UploadObject Error: [{$s3Request->response->code}] Received unexpected HTTP Status.",
				1622
			);
		} elseif ($s3Request->response->error !== false) {
			// Throw S3Exception in case S3 API returned an Error.
			throw new S3Exception(
				"S3 UploadObject Error: [{$s3Request->response->error['code']}] " .
				"{$s3Request->response->error['message']}.",
				1623
			);
		}
	}


	/**
	 * Uploads a File to AWS S3 Bucket (File wrapper method around putObject).
	 *
	 * @param  string       $filePath             File Path of Object to be uploaded to S3.
	 * @param  string       $bucket               S3 Bucket Name.
	 * @param  string       $key                  Object Key (URI).
	 * @param  array|string $headers              Array of Response Headers, OR "Content-Type" Header as a string.
	 * @param  array        $metaHeaders          Array of AMZ Metadata Headers ("x-amz-meta-*" Headers).
	 * @param  string       $acl                  S3 Permission for the Object (@default=ACL_PRIVATE).
	 * @param  string       $storageClass         S3 Storage Type (@default=STORAGE_CLASS_STANDARD).
	 * @param  string       $serverSideEncryption Indicates if Object should be encrypted on the Server-side
	 *                                            (@default=SSE_AES256).
	 * @return void                               If AWS S3 API returns an Error, a S3Exception gets triggered with
	 *                                            Error details. Otherwise, nothing is returned as Metadata for uploaded
	 *                                            file is contained within S3 Response.
	 * @throws S3Exception                        Throws S3Exception in case of S3 API Error.
	 */
	public function uploadObjectFile(
		$filePath,
		$bucket,
		$key,
		$headers = [],
		$metaHeaders = [],
		$acl = self::ACL_PRIVATE,
		$storageClass = self::STORAGE_CLASS_STANDARD,
		$serverSideEncryption = self::SSE_AES256
	) {
		// Check if File Path points to a valid File.
		if (!file_exists($filePath) || !is_file($filePath) || !is_readable($filePath)) {
			// Throw S3Exception if File Path is not valid.
			throw new S3Exception("S3 UploadObjectFile Error: Unable to open file {$filePath}.", 1624);
		}
		// Clear File status cache.
		clearstatcache(false, $filePath);

		// Construct File Object.
		$object = [
			'file' => $filePath,
			'size' => filesize($filePath),
			'md5' => base64_encode(md5_file($filePath, true))
		];

		// Upload the File to S3 Bucket.
		$this->uploadObject(
			$object,
			$bucket,
			$key,
			$headers,
			$metaHeaders,
			$acl,
			$storageClass,
			$serverSideEncryption
		);
	}


	/**
	 * Uploads a Resource to AWS S3 Bucket (Resource wrapper method around putObject).
	 *
	 * @param  resource     $resource             Resource Object to be uploaded to S3.
	 * @param  string       $bucket               S3 Bucket Name.
	 * @param  string       $key                  Object Key (URI).
	 * @param  array|string $headers              Array of Response Headers, OR "Content-Type" Header as a string.
	 * @param  array        $metaHeaders          Array of AMZ Metadata Headers ("x-amz-meta-*" Headers).
	 * @param  string       $acl                  S3 Permission for the Object (@default=ACL_PRIVATE).
	 * @param  string       $storageClass         S3 Storage Type (@default=STORAGE_CLASS_STANDARD).
	 * @param  string       $serverSideEncryption Indicator if Object should be encrypted on the Server-side
	 *                                            (@default=SSE_AES256).
	 * @return void                               If AWS S3 API returns an Error, a S3Exception gets triggered with
	 *                                            Error details. Otherwise, nothing is returned as Metadata for uploaded
	 *                                            file is contained within S3 Response.
	 * @throws S3Exception                        Throws S3Exception in case of S3 API Error.
	 */
	public function uploadObjectResource(
		&$resource,
		$bucket,
		$key,
		$headers = [],
		$metaHeaders = [],
		$acl = self::ACL_PRIVATE,
		$storageClass = self::STORAGE_CLASS_STANDARD,
		$serverSideEncryption = self::SSE_AES256
	) {
		// Check if referenced Resource Object is valid.
		if (!is_resource($resource)) {
			// Throw S3Exception if referenced Resource Object is not valid.
			throw new S3Exception("S3 UploadObjectResource Error: Invalid File Resource.", 1625);
		}

		// Try to calculate Buffer Size of the given Resource.
		if (fseek($resource, 0, SEEK_END) < 0 || ($bufferSize = ftell($resource)) === false) {
			// Throw S3Exception if not able to calculate Buffer Size of the given Resource.
			throw new S3Exception(
				"S3 UploadObjectResource Error: Unable to obtain Resource Size for File Resource.",
				1626
			);
		}
		// Reset File Pointer to the beginning of the File Resource.
		fseek($resource, 0);

		// Calculate MD5 Checksum for referenced File Resource if custom MD5 is not provided.
		$hashContext = hash_init('md5');
		hash_update_stream($hashContext, $resource);
		$md5 = base64_encode(hash_final($hashContext, true));
		// Reset File Pointer to the beginning of the File Resource.
		fseek($resource, 0);

		// Construct Resource Object.
		$object = [
			'size' => $bufferSize,
			'md5' => $md5
		];
		$object['fileHandler'] =& $resource;

		// Upload the File Resource to S3 Bucket.
		$this->uploadObject(
			$object,
			$bucket,
			$key,
			$headers,
			$metaHeaders,
			$acl,
			$storageClass,
			$serverSideEncryption
		);
	}


	/**
	 * Deletes an Object from AWS S3 Bucket.
	 *
	 * @see    http://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectDELETE.html AWS S3 API Documentation.
	 * @param  string      $bucket S3 Bucket Name.
	 * @param  string      $key    Object Key (URI).
	 * @return void                AWS S3 API does NOT return any Error if referenced Object doesn't exist inside S3
	 *                             Bucket and it always returns the same Response unless other Exception was triggered
	 *                             (i.e. Authentication and/or Authorization).
	 * @throws S3Exception         Throws S3Exception in case of S3 API Error.
	 */
	public function deleteObject($bucket, $key)
	{
		// Initialize Request.
		$s3Request = new S3Request('DELETE', self::$endpoint, $bucket, $key);

		// Send S3 Request and process the Response.
		$s3Request->send();

		// Check if there was an Error while making a Request.
		if ($s3Request->response->error === false && $s3Request->response->code !== 204) {
			// Throw S3Exception if unexpected HTTP Status Code was received.
			throw new S3Exception(
				"S3 DeleteObject Error: [{$s3Request->response->code}] Received unexpected HTTP Status.",
				1627
			);
		} elseif ($s3Request->response->error !== false) {
			// Throw S3Exception in case S3 API returned an Error.
			throw new S3Exception(
				"S3 DeleteObject Error: [{$s3Request->response->error['code']}] " .
				"{$s3Request->response->error['message']}.",
				1628
			);
		}
	}


	/**
	 * Encodes a String with Base64 Encoding and replaced characters that are invalid for URL with the safe ones.
	 *
	 * @param  string $value A string that needs to be Base64 encoded.
	 * @return string        Base64 Encoded String with invalid URL character being replaced with the safe ones.
	 */
	private function urlSafeBase64Encode($value)
	{
		// Replace unsafe characters "+", "=" and "/" with the safe characters "-", "_" and "~" respectively.
		return strtr(base64_encode($value), '+=/', '-_~');
	}


	/**
	 * Generates AWS CloudFront Signed URL using Custom Policy Statement to allow access to referenced File Resource.
	 * It should NOT contain S3 Bucket Prefix as that should be automatically handled by AWS CloudFront Distribution
	 * configuration!!!
	 * AWS CloudFront Distribution should be configured to point to a particular Prefix (Key), so only those files are
	 * available to AWS CloudFront Distribution. For security reasons, all other files must be explicitly excluded, so
	 * nobody can get unauthorized access to any of them!
	 *
	 * This Signed URL is valid only for requested File for a duration of provided Lifetime for CloudFront Signed URLs.
	 * If requested, it will be locked to the Public IP Address of the Client that requested Signed URL for the very
	 * first time. If a consecutive Request is made, the Signed URL has not expired yet and the Request is originating
	 * from the same Public IP Address (if Signed URL is locked to an IP Address), AWS CloudFront will permit access to
	 * the requested File Resource. Otherwise, in any other scenario, the Request will be denied with a following Access
	 * Denied (403 Forbidden) Error message:
	 *   <Error>
	 *     <Code>AccessDenied</Code>
	 *     <Message>Access denied</Message>
	 *   </Error>
	 * The same Response will also be returned if a requested File Resource is not found!
	 *
	 * @see    http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/
	 *                private-content-creating-signed-url-custom-policy.html AWS S3 API Documentation.
	 * @param  string      $fileUrl           Absolute File Resource URL in CloudFront Distribution (it should also
	 *                                        contain the URL of the CloudFront Distribution).
	 * @param  string      $sshKeyPairId      AWS CloudFront Key Pair ID (indicates AWS which RSA Key Pair to use to
	 *                                        validate URL signature).
	 * @param  string      $sshPrivateKey     AWS S3 Private RSA (SSH-2) Key for signing CloudFront URLs by trusted
	 *                                        signer.
	 * @param  integer     $signedUrlLifetime Duration in seconds for how long AWS CloudFront Signed URLs will be valid
	 *                                        once generated (@default=60).
	 * @param  string      $lockedIpAddress   Optional IP Address to which AWS CloudFront Signed ULR should be locked
	 *                                        to, so it's not valid for any Request coming from a different IP Address.
	 * @return string                         Returns AWS CloudFront Signed URL for requested File Resource.
	 * @throws S3Exception                    Throws S3Exception in case of any Error.
	 */
	public function getCloudFrontSignedUrl(
		$fileUrl,
		$sshPrivateKey,
		$sshKeyPairId,
		$signedUrlLifetime = 60,
		$lockedIpAddress = null
	) {
		// Load AWS CloudFront SSH Private Key to generate an URL Signature. Warnings and Errors must be silenced, so
		// Application can handle them appropriately and be able to recover without affecting the regular logical flow.
		$fileHandler = @fopen($sshPrivateKey, 'r');
		// Check if AWS CloudFront SSH Private Key was successfully loaded.
		if ($fileHandler === false) {
			// Throw S3Exception if AWS CloudFront SSH Private Key can't be opened or found.
			throw new S3Exception(
				"S3 GetCloudFrontSignedURL Error: Unable to open AWS CloudFront SSH Private Key.",
				1629
			);
		}
		// Read AWS CloudFront SSH Private Key. Warnings and Errors must be silenced, so Application can handle them
		// appropriately and be able to recover without affecting the regular logical flow.
		$privateKey = @fread($fileHandler, filesize($sshPrivateKey));
		// Check if AWS CloudFront SSH Private Key was successfully read.
		if ($privateKey === false) {
			// Throw S3Exception if AWS CloudFront SSH Private Key can't be read.
			throw new S3Exception(
				"S3 GetCloudFrontSignedURL Error: Unable to read AWS CloudFront SSH Private Key.",
				1630
			);
		}
		// Close AWS CloudFront SSH Private Key. Warnings and Errors must be silenced, so Application can handle them
		// appropriately and be able to recover without affecting the regular logical flow.
		@fclose($fileHandler);
		// Parse AWS CloudFront SSH Private Key. Warnings and Errors must be silenced, so Application can handle them
		// appropriately and be able to recover without affecting the regular logical flow.
		$privateKeyId = @openssl_get_privatekey($privateKey);
		// Check if AWS CloudFront SSH Private Key was successfully parsed.
		if ($privateKeyId === false) {
			// Throw S3Exception if AWS CloudFront SSH Private Key can't be parsed.
			throw new S3Exception(
				"S3 GetCloudFrontSignedURL Error: Unable to parse AWS CloudFront SSH Private Key.",
				1631
			);
		}

		// Generate AWS CloudFront Custom Policy Statement to sign requested File Resource URL.
		$policyStatement = [
			'Statement' => [
				(object) [
					'Resource' => $fileUrl,
					'Condition' => (object) [
						'DateLessThan' => (object) [
							'AWS:EpochTime' => time() + (int) $signedUrlLifetime
						]
					]
				]
			]
		];

		// Check if generated Signed URL should be locked to a particular IP Address.
		if (!empty($lockedIpAddress)) {
			// Lock Signed URL to a single IP Address (what appears to be Public IP Address of the Client making the
			// Request).
			$policyStatement['Statement'][0]->Condition->{'IpAddress'} = (object) [
				'AWS:SourceIp' => $lockedIpAddress . '/32'
			];
		}

		// Encode AWS CloudFront Custom Policy Statement as JSON.
		$policyStatement = json_encode($policyStatement, JSON_UNESCAPED_SLASHES);

		// Generate AWS CloudFront Signature to sign requested File Resource URL.
		$signature = '';
		// Check if signing CloudFront Custom Policy was successful.
		if (openssl_sign($policyStatement, $signature, $privateKeyId) === false) {
			// Throw S3Exception if not able to sign CloudFront Custom Policy Statement for requested File Resource.
			throw new S3Exception(
				"S3 GetCloudFrontSignedURL Error: Unable to sign CloudFront Custom Policy Statement.",
				1632
			);
		}

		// Free the Private Key from Memory. Otherwise, this can cause a memory leak!
		openssl_free_key($privateKeyId);

		// Return generated AWS CloudFront Signed URL for requested File Resource. It does NOT include Prefix for the
		// AWS S3 Bucket as that should be automatically handled by AWS CloudFront Distribution configuration!!!
		// That should be automatically included by AWS CloudFront Distribution as AWS CloudFront Distribution
		// should be configured to point to a particular S3 Bucket Prefix (Key) for security reasons to limit
		// access only to certain files within the S3 Bucket instead any of them!
		return $fileUrl . '?' .
		       'Policy=' . $this->urlSafeBase64Encode($policyStatement) .
		       '&Signature=' . $this->urlSafeBase64Encode($signature) .
		       '&Key-Pair-Id=' . $sshKeyPairId;
	}


	/**
	 * Generates a HMAC-SHA1 Hash required to sign sensitive data being passed to AWS S3 API (i.e. for Authentication
	 * using Access Keys or for signing Authenticated S3 URL).
	 *
	 * @param  string $value String that should be signed with HMAC Hash using SHA1 Encryption.
	 * @return string        Returns HMAC-SHA1 signed string.
	 */
	public static function getHmacHash($value)
	{
		// Return HMAC-SHA1 signed string.
		return base64_encode(hash_hmac('sha1', $value, self::$secretAccessKey, true));
	}


	/**
	 * Get a S3 Authenticated URL for direct access to an Object in AWS S3 Bucket using Access Keys for Authentication
	 * in a URL Query. This should be used only for debugging purposes, and should NOT be used in Production as it's NOT
	 * safe!!!
	 *
	 * @see    http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#RESTAuthenticationQueryStringAuth
	 *                                       AWS S3 API Documentation.
	 * @param  string  $bucket               S3 Bucket Name.
	 * @param  string  $key                  Object Key (URI).
	 * @param  integer $urlLifetime          Duration in seconds for how long Authenticated URL will be valid once
	 *                                       generated (@default=60).
	 * @param  boolean $useVirtualHostBucket Indicates if S3 Bucket should be accessed as a Virtual Host
	 *                                       (@default=true). @example: "http(s)://{$bucket}.s3.amazonaws.com/"
	 *                                       Otherwise, S3 Bucket is accessed using Path-style URL.
	 *                                       @example: "http(s)://s3.amazonaws.com/{$bucket}/"
	 *                                       However, in this case AWS S3 API will return PermanentRedirect Error for
	 *                                       most regions with 301 Permanent Redirect providing details how exactly that
	 *                                       S3 Bucket must be access due to DNS settings being put in place and
	 *                                       indicating that for Path-style access an endpoint with the region must be
	 *                                       used instead (@example: "http(s)://us-east-1.amazonaws.com/{$bucket}/").
	 * @param  boolean $useHttps             Indicates if Connection to AWS S3 should be done over HTTPS
	 *                                       (@default=true).
	 * @return string
	 */
	/*
	public function getAuthenticatedURL(
		$bucket,
		$key,
		$urlLifetime = 60,
		$useVirtualHostBucket = true,
		$useHttps = true
	) {
		// Calculate the Expiration Timestamp (UTC Timestamp until which generated URL will be valid).
		$expires = time() + $urlLifetime;

		// Return generated Authenticated URL.
		return ($useHttps ? 'https://' : 'http://') .
		       ($useVirtualHostBucket ? ($bucket . '.' . self::$endpoint) : (self::$endpoint . '/' . $bucket)) . '/' .
		       strtr(rawurlencode($key), ['%2F' => '/', '%2B' => '+']) .
		       '?AWSAccessKeyId=' . self::$accessKeyId .
		       '&Expires=' . $expires .
		       '&Signature=' . urlencode(self::getHmacHash("GET\n\n\n{$expires}\n/{$bucket}/{$key}"));
	}
	*/
}
