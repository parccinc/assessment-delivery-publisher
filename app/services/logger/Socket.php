<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services\Logger;

use Phalcon\Logger\Adapter as LoggerAdapter;
use Phalcon\Logger\AdapterInterface as LoggerAdapterInterface;
use Phalcon\Logger\Formatter\Line as LoggerFormatter;

/**
 * Class Socket
 *
 * This is Socket Logger Adapter which sends Log Entries to Socket Stream. This should be used for debugging purposes
 * only and should NOT be used in Production!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class Socket extends LoggerAdapter implements LoggerAdapterInterface
{

	/**
	 * @const integer MAXIMUM_DATA_PACKAGE_SIZE Maximum supported Data Packet Size allowed by UDP Protocol.
	 */
	const MAXIMUM_DATA_PACKAGE_SIZE = 65507;
	/**
	 * @var string $host Socket Host/URL it will be broadcasting data stream to.
	 */
	protected $host;
	/**
	 * @var integer $port Socket Port URL it will be broadcasting data stream to (@default=9999).
	 */
	protected $port = 9999;
	/**
	 * @var resource|false $socket Socket Stream, OR FALSE on Error.
	 */
	protected $socket;


	/**
	 * Class constructor.
	 *
	 * Initializes Socket Logger by opening Socket Stream (Endpoint for communication).
	 *
	 * @param string $host    Socket Host/URL it will be broadcasting data stream to.
	 * @param array  $options Optional additional options may include Socket Port to override the default one.
	 */
	public function __construct($host, $options = [])
	{
		// Set Socket Host and custom Port, if provided.
		$this->host = $host;
		if (isset($options['port'])) {
			$this->port = $options['port'];
		}

		// Create a Socket and set Non-Blocking Mode for it (sets O_NONBLOCK flag on the Socket).
		$this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
		socket_set_nonblock($this->socket);
	}


	/**
	 * Getter for Configured Logger Formatter.
	 *
	 * @return LoggerFormatter $this->_formatter Returns Logger Formatter which is set by the parent Class.
	 */
	public function getFormatter()
	{
		// Instantiate Logger Formatter if not already set.
		if (empty($this->_formatter)) {
			$this->_formatter = new LoggerFormatter();
		}

		return $this->_formatter;
	}


	/**
	 * Formats the Log Entry with Logger Formatter based on the given format.
	 *
	 * @param string  $message Log Entry Message.
	 * @param integer $type    Log Entry Type (Severity).
	 * @param integer $time    Log Entry Timestamp.
	 * @param array   $context Optional Log Entry Context (debug backtrace, if provided).
	 * @return string $data    JSON encoded Log Entry.
	 */
	private function format($message, $type, $time, $context)
	{
		// Generate data with Log Entry Type (Severity) in addition to formatted Log Entry Message, so based on that
		// Socket Listener can apply additional styling and/or filtering.
		$data = json_encode([
			't' => $type,
			'm' => $this->getFormatter()->format($message, $type, $time, $context)
		]);

		return $data;
	}


	/**
	 * Generates Log Entry with Logger Formatter based on the given format and sends it to the Socket Stream.
	 *
	 * @param string  $message Log Entry Message.
	 * @param integer $type    Log Entry Type (Severity).
	 * @param integer $time    Log Entry Timestamp.
	 * @param array   $context Optional Log Entry Context (debug backtrace, if provided).
	 */
	public function logInternal($message, $type, $time, $context = [])
	{
		// Check if Socket was successfully opened.
		if ($this->socket !== false) {
			// Generate Log Entry.
			$data = $this->format($message, $type, $time, $context);

			// Shorten the Log Entry, if too long, by trimming it from the end.
			while (isset($data[self::MAXIMUM_DATA_PACKAGE_SIZE])) {
				if (isset($message[self::MAXIMUM_DATA_PACKAGE_SIZE])) {
					$message = mb_substr($message, 0, self::MAXIMUM_DATA_PACKAGE_SIZE - 4) . "...";
				} else {
					$message = mb_substr($message, 0, mb_strlen($message) - 4) . "...";
				}

				// Generate Log Entry.
				$data = $this->format($message, $type, $time, $context);
			}

			// Try to send the Log Entry over Socket Connection. Warnings and Errors must be silenced as Socket might be
			// closed at this point, or TCP/IP stack on the Server might be configured restrictively and might not allow
			// UDP Protocol to use the Maximum Possible Packet Size, so this is a safety measure.
			if (@socket_sendto($this->socket, $data, strlen($data), 0, $this->host, $this->port) === false) {
				// Try to send an Error Message in case Data Packet failed to be sent because of the size limit.
				$data = $this->format(
					"PHP Warning: socket_sendto() not able to write to Socket. A Message sent on a Datagram Socket " .
					"was larger than the Internal Message Buffer or some other Network Limit, or the Buffer used to " .
					"receive a Datagram into was smaller than the Datagram itself.",
					0,
					$time,
					$context
				);
				@socket_sendto($this->socket, $data, strlen($data), 0, $this->host, $this->port);
			}
		}
	}


	/**
	 * Closes the Socket Logger.
	 */
	public function close()
	{
		if ($this->socket !== false) {
			socket_close($this->socket);
		}
	}
}
