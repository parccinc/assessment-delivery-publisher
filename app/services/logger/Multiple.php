<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services\Logger;

use Phalcon\Logger\Multiple as MultipleLogger;

/**
 * Class Multiple
 *
 * This is the Logger Aggregator. It allows Log Entries to be persisted to multiple destinations at the same time, each
 * having it's own rules and logic, and using it's own custom Log Formatter.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property Multiple logger
 *
 * @method   mixed getLoggers()
 */
class Multiple extends MultipleLogger
{

	/**
	 * @var string $useTransactions Indicates if Log Transactions should be used.
	 */
	protected $useTransactions;


	/**
	 * Constructor.
	 *
	 * Sets the indicator if Log Transactions should be used or not. When Log Transactions are used, all Log Entries are
	 * saved at the end of the Request as part of the Shut Down process.
	 *
	 * @param boolean $useTransactions Indicates if Log Transactions should be used.
	 */
	public function __construct($useTransactions = true)
	{
		$this->useTransactions = $useTransactions;
	}


	/**
	 * Starts a Logger Transaction, if necessary.
	 *
	 * @return \Phalcon\Logger\Adapter Logger Adapter.
	 * @method mixed begin()
	 */
	public function begin()
	{
		// Check if Log Transactions should be used.
		if ($this->useTransactions) {
			// Loop through all Logger Adapters.
			$loggers = $this->getLoggers();
			if (count($loggers) > 0) {
				/** @var \Phalcon\Logger\AdapterInterface $logger */
				foreach ($loggers as $logger) {
					if (is_callable([$logger, 'begin'])) {
						// Start a Logger Transaction, if supported by the current Logger Adapter.
						$logger->begin();
					}
				}
			}
		}
	}


	/**
	 * Commits the Internal Logger Transaction, if necessary.
	 *
	 * @return \Phalcon\Logger\Adapter Logger Adapter.
	 */
	public function commit()
	{
		// Check if Log Transactions should be used.
		if ($this->useTransactions) {
			// Loop through all Logger Adapters.
			$loggers = $this->getLoggers();
			if (count($loggers) > 0) {
				/** @var \Phalcon\Logger\AdapterInterface $logger */
				foreach ($loggers as $logger) {
					if (is_callable([$logger, 'commit'])) {
						// Commit the Logger Transaction, if supported by the current Logger Adapter.
						$logger->commit();
					}
				}
			}
		}
	}


	/**
	 * Rollbacks the Internal Logger Transaction, if necessary.
	 *
	 * @return \Phalcon\Logger\Adapter Logger Adapter.
	 */
	public function rollback()
	{
		// Check if Log Transactions should be used.
		if ($this->useTransactions) {
			// Loop through all Logger Adapters.
			$loggers = $this->getLoggers();
			if (count($loggers) > 0) {
				/** @var \Phalcon\Logger\AdapterInterface $logger */
				foreach ($loggers as $logger) {
					if (is_callable([$logger, 'rollback'])) {
						// Rollback the Logger Transaction, if supported by the current Logger Adapter.
						$logger->rollback();
					}
				}
			}
		}
	}


	/**
	 * Closes the Logger Adapter.
	 *
	 * @return boolean Indicates if Logger was successfully closed.
	 */
	public function close()
	{
		// Loop through all Logger Adapters.
		$loggers = $this->getLoggers();
		if (count($loggers) > 0) {
			/** @var \Phalcon\Logger\AdapterInterface $logger */
			foreach ($loggers as $logger) {
				if (is_callable([$logger, 'close'])) {
					// Close the current Logger Adapter, if supported.
					$logger->close();
				}
			}
		}
	}
}
