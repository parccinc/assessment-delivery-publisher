<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services\Logger;

use PDOException;
use Phalcon\Logger\Adapter as LoggerAdapter;
use Phalcon\Logger\AdapterInterface as LoggerAdapterInterface;
use PARCC\ADP\Models\Log;

/**
 * Class Database
 *
 * This is Database Logger Adapter which stores Log Entries into Database. This is used for centralized logging from
 * multiple Application instances in a highly scalable Production environment (i.e. being behind the Load Balancer).
 * However, it adds some overhead to the Database and therefore impacts its performance!
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class Database extends LoggerAdapter implements LoggerAdapterInterface
{

	/**
	 * @const integer MAXIMUM_DATA_PACKAGE_SIZE Maximum supported Data Size for Text Field allowed in MySQL Database.
	 */
	const MAXIMUM_DATA_PACKAGE_SIZE = 65535;
	/**
	 * @var mixed $databaseConnection Database Connection Pointer, OR NULL in case of failure to Connect to Database.
	 */
	protected $databaseConnection;
	/**
	 * @var string $applicationId Application Instance ID (Unique Identifier in case of multiple Application instances
	 *                            are used concurrently behind the Load Balancer).
	 */
	protected $applicationId;
	/**
	 * @var string $requestId HTTP Request ID (Process ID and a Random Number between 1 and 9999 separated by a hyphen).
	 */
	protected $requestId;


	/**
	 * Class constructor.
	 *
	 * Initializes Database Logger by setting Database Connection Properties.
	 *
	 * @param mixed $databaseConnection Database Connection Pointer, OR NULL in case of failure to Connect to Database.
	 * @param array $options            Optional Application Instance Id and HTTP Request ID.
	 */
	public function __construct($databaseConnection, $options = [])
	{
		// Set Database Connection.
		$this->databaseConnection = $databaseConnection;

		// Set Instance ID, if provided.
		if (isset($options['applicationId'])) {
			$this->applicationId = $options['applicationId'];
		}
		// Set Request ID, if provided.
		if (isset($options['requestId'])) {
			$this->requestId = $options['requestId'];
		}
	}


	/**
	 * Getter for configured Logger Formatter.
	 * This is not used, but is needed as it's defined as abstract in the LoggerAdapterInterface.
	 *
	 * @return \Phalcon\Logger\FormatterInterface Logger Formatter Interface.
	 */
	public function getFormatter()
	{
		// Dummy Method placeholder.
	}


	/**
	 * Constructs and prepares single Log Entry to be stored into the Database.
	 *
	 * @param  string  $message  Log Entry Message.
	 * @param  integer $type     Log Entry Type (Severity).
	 * @param  integer $time     Log Entry Timestamp (Not used. Local Database Server UTC Timestamp is used instead as
	 *                           it is more reliable to rely on a single clock in case of multiple Servers being used
	 *                           concurrently).
	 * @param  array   $context  Optional Log Entry Context (Debug Backtrace, if provided).
	 * @return Log               Returns prepared Log Entry.
	 */
	public function prepareLog($message, $type, $time, $context = [])
	{

		// Redefine Log Entry Type (Severity) to be in a human readable format.
		switch ($type) {
			case 0:
				$type = 'EMERGENCY';
				break;
			case 1:
				$type = 'CRITICAL';
				break;
			case 2:
				$type = 'ALERT';
				break;
			case 3:
				$type = 'ERROR';
				break;
			case 4:
				$type = 'WARNING';
				break;
			case 5:
				$type = 'NOTICE';
				break;
			case 6:
				$type = 'INFO';
				break;
			case 7:
				$type = 'DEBUG';
				break;
			case 8:
				$type = 'CUSTOM';
				break;
			case 9:
				$type = 'SPECIAL';
				break;
			default:
				$type = 'UNKNOWN';
				break;
		}

		// Format Log Entry Context, if provided.
		if (count($context) > 0) {
			$context = ': ' . json_encode($context);
		} else {
			$context = null;
		}

		// Shorten the Log Entry Message, if too long, by trimming it from the end.
		if (isset($message[self::MAXIMUM_DATA_PACKAGE_SIZE])) {
			$message = mb_substr($message, 0, self::MAXIMUM_DATA_PACKAGE_SIZE - 3) . "...";
		}

		// Generate a Log Entry and Store it into the Database.
		$log = new Log();
		$log->applicationId = $this->applicationId;
		$log->requestId = $this->requestId;
		$log->type = $type;
		$log->message = $message;
		$log->context = $time . $context;

		// Check if Tenant was successfully identified.
		if ($log->getDI()->has('tenant')) {
			// Add reference to identified Tenant.
			$log->tenantId = $log->getDI()['tenant']->tenantId;
		} else {
			// Add reference to Unknown Tenant.
			$log->tenantId = 0;
		}

		// Check if User was successfully identified.
		if ($log->getDI()->has('user')) {
			// Add reference to identified User.
			$log->createdByUserId = $log->getDI()['user']->userId;
		} else {
			// Add reference to Anonymous User.
			$log->createdByUserId = 0;
		}

		// Return prepared Log Entry.
		return $log;
	}


	/**
	 * Formats single Log Entry and stores it into the Database.
	 *
	 * @param string  $message  Log Entry Message.
	 * @param integer $type     Log Entry Type (Severity).
	 * @param integer $time     Log Entry Timestamp (Not used. Local Database Server UTC Timestamp is used instead as it
	 *                          is more reliable to rely on a single clock in case of multiple Servers being used
	 *                          concurrently).
	 * @param array   $context  Optional Log Entry Context (Debug Backtrace, if provided).
	 */
	public function logInternal($message, $type, $time, $context = [])
	{

		// Prepare Log Entry.
		$log = $this->prepareLog($message, $type, $time, $context);

		// Try to store Log Entry into Database.
		try {
			// Warnings and Errors must be silenced, so Application can handle them appropriately and be able to
			// recover without affecting the regular logical flow.
			@$log->create();
		} catch (PDOException $exception) {
			// Log a PDOException to the PHP Error Log.
			// No need to handle PDOException here as it will be taken care of at the top level of the Application.
			error_log("PDOException: Unable to create Log Entry in Database!");
		}
	}


	/**
	 * Commits multiple Log Entries as Log Transaction and stores all of them together into the Database.
	 *
	 * @return \Phalcon\Logger\Adapter Logger Adapter.
	 */
	public function commit()
	{
		// Get the number of queued Log Entries.
		$queueSize = count($this->_queue);

		// Check if Log Transaction Queue is empty.
		if ($queueSize > 0) {
			// Construct Log Transaction Prepared SQL Statement. We have to use Raw SQL Query as PHQL (Phalcon Query
			// Language) does NOT support SQL INSERT Statements with multiple Records.
			$sql = 'INSERT INTO `log`
			        (`fk_tenant_id`, `application_id`, `request_id`, `type`, `message`, `context`, `created`,
			         `fk_created_by`)
			        VALUES';

			// Initialize Log Entry, Log Entry Counter and SQL Bind Parameters.
			// Log Entry is used to retrieve Database Connection Service.
			$log = null;
			$logEntryCounter = 0;
			$bindParameters = [];

			// Loop through all Log Entries in Log Transaction Queue.
			/** @var \Phalcon\Logger\Item $logItem */
			foreach ($this->_queue as $logItem) {
				// Prepare current Log Entry.
				/** @noinspection PhpVoidFunctionResultUsedInspection */
				$log = $this->prepareLog(
					$logItem->getMessage(),
					$logItem->getType(),
					$logItem->getTime(),
					$logItem->getContext()
				);

				// Check if current Log Entry is the first or the last Log Entry in the Log Transaction Queue.
				if ($logEntryCounter > 0 && $logEntryCounter < $queueSize) {
					// Append delimiter into SQL Query for additional Record Values.
					$sql .= ',';
				}
				// Append Values for the current Log Entry.
				$sql .= "(?, ?, ?, ?, ?, ?, UTC_TIMESTAMP(), ?)";

				// Append Bind Parameters for the current Log Entry.
				$bindParameters[] = $log->tenantId;
				$bindParameters[] = $log->applicationId;
				$bindParameters[] = $log->requestId;
				$bindParameters[] = $log->type;
				$bindParameters[] = $log->message;
				$bindParameters[] = $log->context;
				$bindParameters[] = $log->createdByUserId;

				// Increment Log Entry Counter.
				++$logEntryCounter;
			}

			// Try to store all Log Entries into Database.
			try {
				// Warnings and Errors must be silenced, so Application can handle them appropriately and be able to
				// recover without affecting the regular logical flow.
				@$log->getWriteConnection()->query($sql, $bindParameters);
			} catch (PDOException $exception) {
				// Log a PDOException to the PHP Error Log.
				// No need to handle PDOException here as it will be taken care of at the top level of the Application.
				error_log("PDOException: Unable to create Log Transaction Entries in Database!");
			}
		}
	}


	/**
	 * Closes the Database Logger.
	 *
	 * @return boolean true Always returns TRUE as Application will take care of closing Database Connection!
	 */
	public function close()
	{
		return true;
	}
}
