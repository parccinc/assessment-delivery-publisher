<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services;

use Phalcon\Exception;

/**
 * Class HttpClient
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class HttpClient
{

	/**
	 * Define all supported HTTP Request Methods.
	 */
	const GET = 'GET';
	const POST = 'POST';
	const PUT = 'PUT';
	const PATCH = 'PATCH';
	const DELETE = 'DELETE';
	const OPTIONS = 'OPTIONS';
	const HEAD = 'HEAD';

	/**
	 * @var boolean $sendHeader    Indicates if Response Header should be included in the output (@default=false).
	 */
	public $sendHeader = false;
	/**
	 * @var integer $sslVerifyPeer Indicates if HTTPS Connection should verify Peer's SSL Certificate {0|1}
	 *                             (@default=0).
	 */
	//@TODO: SSL Certificate validation should ALWAYS be performed in Production!!!
	public $sslVerifyPeer = 0;
	/**
	 * @var integer $sslVerifyHost Indicates if HTTPS Connection should verify SSL Certificate Name against its Host
	 *                             ('1' verifies existence of common name in SSL peer certificate, '2' checks existence
	 *                             of common name and also verifies that it matches the hostname provided) {0|1|2}. This
	 *                             should always be set to '2' in Production! (@default=0).
	 */
	//@TODO: SSL Certificate validation should ALWAYS be performed in Production!!!
	public $sslVerifyHost = 0;
	/**
	 * @var integer $timeout       Timeout interval in seconds (@default=60).
	 */
	public $timeout = 60;


	/**
	 * CURL API Request Helper.
	 *
	 * It makes HTTP Request using CURL Library.
	 *
	 * @param string       $url         Request URL.
	 * @param array|string $data        Request Data.
	 * @param string       $method      HTTP Request Method {'GET'|'POST'|'PUT'|'PATCH'|'DELETE'|'OPTIONS'|'HEAD'}
	 *                                  (@default='POST').
	 * @param string       $returnType  Return Type (@default='data').
	 * @param array        $httpHeaders Additional Request Headers.
	 * @param array        $fileConfig  Optional File Handler. In case of file download it should look like:
	 *                                  ['filePath' => '/PATH_TO_FILE/FILE_NAME', 'overwrite' => false]
	 *                                  If 'overwrite' is set to TRUE it will overwrite existing file, if found.
	 *                                  Otherwise, it will throw an Exception.
	 *
	 * @return Object                   Returns a Response.
	 * @throws Exception                Throws an Exception in case of an Error (i.e. if trying to overwrite an existing
	 *                                  file).
	 */
	public function request(
		$url,
		$data,
		$method = self::POST,
		$returnType = 'data',
		$httpHeaders = ['Accept: application/json'],
		$fileConfig = []
	) {

		// Try to execute HTTP Request.
		try {
			// Check if File Path is provided.
			if (!empty($fileConfig) && isset($fileConfig['filePath'])) {

				$fileName = $fileConfig['filePath'];
				$overwrite = isset($fileConfig['overwrite']) && $fileConfig['overwrite'];

				if (file_exists($fileName) && $overwrite === false) {
					throw new Exception("File '{$fileName}' already exist!");
				}
				$fileHandler = fopen($fileName, 'w+');
			}

			// Initialize CURL Handler.
			$curlHandler = curl_init($url);

			// Check which HTTP Request Method should be used.
			//@TODO: What about 'PATCH', 'OPTIONS' and 'HEAD' Requests??!
			switch ($method) {
				case 'GET':
					if ($data) {
						$url = sprintf("%s?%s", $url, http_build_query($data));
					}
					curl_setopt($curlHandler, CURLOPT_URL, $url);
					curl_setopt($curlHandler, CURLOPT_HTTPGET, true);
					break;

				case 'POST':
					curl_setopt($curlHandler, CURLOPT_POST, 1);
					if ($data) {
						curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $data);
					}
					break;

				case 'PUT':
					curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, 'PUT');
					if ($data) {
						curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $data);
					}
					break;

				case 'DELETE':
					curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, "DELETE");
					break;

				default:
					// Throw an Exception in case of unsupported Request Method.
					throw new Exception("Unsupported Request Method: {$method}");
					break;
			}

			curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);

			// Check if additional Request Headers need to be sent.
			if (!empty($httpHeaders)) {
				curl_setopt($curlHandler, CURLOPT_HTTPHEADER, $httpHeaders);
			}

			// Set additional CURL Request Options.
			curl_setopt($curlHandler, CURLOPT_HEADER, $this->sendHeader);
			curl_setopt($curlHandler, CURLOPT_TIMEOUT, $this->timeout);

			// Set SSL certificate validation.
			curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, $this->sslVerifyPeer);
			curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, $this->sslVerifyHost);

			if (isset($fileHandler)) {
				curl_setopt($curlHandler, CURLOPT_FILE, $fileHandler);
				/** @noinspection PhpUnusedParameterInspection */
				curl_setopt($curlHandler, CURLOPT_WRITEFUNCTION, function (&$curlHandler, $data) use ($fileHandler) {
					$responseLength = fwrite($fileHandler, $data);

					return $responseLength;
				});
			}

			// Execute HTTP Request.
			$result = curl_exec($curlHandler);

			// Check if File Handler is used.
			if (isset($fileHandler)) {
				// Close CURL Handler.
				curl_close($curlHandler);
				// Close File Handler!
				fclose($fileHandler);

				// Check if File was successfully saved.
				if (isset($fileName) && file_exists($fileName)) {
					return $fileName;
				} else {
					return false;
				}
			}

			// Validate if CURL Request was successful.
			if ($result === false) {
				throw new Exception('CURL ' . curl_error($curlHandler));
			}

			if ($returnType !== 'data') {
				$result = curl_getinfo($curlHandler, $returnType);
			}

			// Close CURL Handler.
			curl_close($curlHandler);

			// Try to parse Response as JSON.
			$result = json_decode($result);

			// Validate if parsed JSON Response is valid.
			//@TODO: This will not throw an Exception if "123456" is returned which is invalid in our case!
			if ($result === false || $result === null) {
				throw new Exception('JSON ' . json_last_error_msg(), json_last_error());
			}

			// Return Response.
			return $result;

		} catch (Exception $exception) {
			// Throw an Exception in case of failed HTTP Request.
			throw new Exception(
				"Error while making CURL Request to '{$url}'. Error " . $exception->getCode() . ": " .
				$exception->getMessage()
			);
		}
	}
}
