<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Services;

/**
 * TODO: Why was this not removed??!
 * Class PublishHelper
 * @package PARCC\ADP\Services
 * @author
 */
class PublishHelper
{

	/**
	 * getDbCurrentTimestamp returns the actual time stamp following the below format.
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param \Phalcon\Db\Adapter\Pdo\Mysql $connection
	 * @return string current time stamp following that format yyyy-mm-dd hh:mm:ss
	 */
	public static function getDbCurrentTimestamp($connection)
	{

		$result = $connection->query("SELECT UTC_TIMESTAMP()")->fetch();
		return $result[0];
	}
}
