<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class Student
 *
 * This is the Student Model. It contains all Student related details that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class Student extends BaseModel
{

	/**
	 * Student Properties.
	 *
	 * @var integer $studentId
	 */
	public $studentId;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var string $personalId
	 */
	public $personalId;
	/**
	 * @var string $firstName
	 */
	public $firstName;
	/**
	 * @var string $lastName
	 */
	public $lastName;
	/**
	 * @var string $dateOfBirth
	 */
	public $dateOfBirth;
	/**
	 * @var string $state
	 */
	public $state;
	/**
	 * @var string $schoolName
	 */
	public $schoolName;
	/**
	 * @var string $grade
	 */
	public $grade;
	/**
	 * @var boolean $isActive
	 */
	public $isActive;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var integer $createdByUserId
	 */
	public $createdByUserId;
	/**
	 * @var string $updatedDateTime
	 */
	public $updatedDateTime;
	/**
	 * @var integer $updatedByUserId
	 */
	public $updatedByUserId;


	/**
	 * Sets Connection to the Database and Relationship to other Models.
	 * It also joins with other Models based on defined dependencies.
	 */
	public function initialize()
	{
		// Set Primary Database Connection as Student always uses Primary Database!
		self::changeConnectionService('db');

		parent::initialize();

		// Set Relationship to other Models.
		$this->belongsTo('tenantId', 'PARCC\ADP\Models\Tenant', 'tenantId', ['alias' => 'Tenant']);
		$this->belongsTo('createdByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
		//$this->belongsTo('updatedByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
		$this->hasMany('studentId', 'TestSession', 'parentStudentId');
	}


	/**
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'student';
	}


	/**
	 * Column Mapping returns Keys as Field Names in the Database Table and Values as Application Variables.
	 *
	 * @return array Database Table Mapping to Table Columns.
	 */
	public function columnMap()
	{
		return [
			'sid' => 'studentId',
			'fk_tenant_id' => 'tenantId',
			'personal_id' => 'personalId',
			'first_name' => 'firstName',
			'last_name' => 'lastName',
			'date_of_birth' => 'dateOfBirth',
			'state' => 'state',
			'school_name' => 'schoolName',
			'grade' => 'grade',
			'active' => 'isActive',
			'created' => 'createdDateTime',
			'fk_created_by' => 'createdByUserId',
			'updated' => 'updatedDateTime',
			'fk_updated_by' => 'updatedByUserId'
		];
	}
}
