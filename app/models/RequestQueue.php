<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

use Phalcon\Mvc\Model,
	Phalcon\Db\RawValue,
	Phalcon\Mvc\Model\Validator\InclusionIn;

/**
 * Class RequestQueue
 *
 * This is the Request Queue Model. It contains all Request Queue details that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 */
class RequestQueue extends BaseModel
{

	const TYPE_TEST_PACKAGE = 'test package';
	const TYPE_ASSIGNMENT	= 'test assignment';
	const TYPE_RESULTS		= 'test results';

	const SOURCE_ACR		= 'ACR';
	const SOURCE_TD			= 'TD';
	const SOURCE_PRC		= 'PRC';
	const SOURCE_LDR		= 'LDR';

	const STATUS_SCHEDULED	= 'Scheduled';
	const STATUS_INPROGRESS	= 'InProgress';
	const STATUS_COMPLETED	= 'Completed';
	const STATUS_CANCELED	= 'Canceled';
	const STATUS_FAILED		= 'Failed';

	const ACR_STATUS_SCHEDULED		= 'Scheduled';
	const ACR_STATUS_PUBLISHED		= 'Published';
	const ACR_STATUS_UNPUBLISHED	= 'Unpublished';
	const ACR_STATUS_FAILED			= 'Failed';


	/**
	* @var integer $rqid
	*/
	public $rqid;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var integer $externalId
	 */
	public $externalId;
	/**
	 * @var string type
	 */
	public $type;
	/**
	 * @var string $source
	 */
	public $source;
	/**
	 * @var string $status
	 */
	public $status;
	/**
	 * @var string $startTime
	 */
	public $startTime;
	/**
	 * @var string $endTime
	 */
	public $endTime;
	/**
	 * @var string $statusDescription
	 */
	public $statusDescription;
	/**
	 * @var integer $priority
	 */
	public $priority;
	/**
	 * @var string $data
	 */
	public $data;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var integer $createdByUserId
	 */
	public $createdByUserId;
	/**
	 * @var string $createdDateTime
	 */
	public $updatedDateTime;

	/**
	 * @var integer $createdByUserId
	 */
	public $updatedByUserId;

	/**
	 * Constructor.
	 *
	 * This enables usage of dynamic SQL updates. SQL UPDATE statements are by default created with every column
	 * defined in the Model (full all-field SQL update). Dynamic update uses only the fields that have changed when
	 * creating the final SQL statement.
	 * In some cases this can improve the performance by reducing the traffic between the application and the database
	 * server, and this specially helps when the table has blob/text fields.
	 */
	public function initialize()
	{
		self::changeConnectionService('db2');

		parent::initialize();
		$this->belongsTo('tenantId', 'PARCC\ADP\Models\Tenant', 'tenantId', ['alias' => 'Tenant']);

		$this->useDynamicUpdate(true);

	}


	public function afterUpdate()
	{
		try {

			$connection = $this->getDI()->get($this->getDI()['defaultDb']);
			$connection->begin();

			// move to history table if the job is completed, canceled or failed.
			if (in_array($this->status, array(self::STATUS_COMPLETED, self::STATUS_CANCELED, self::STATUS_FAILED))) {
				$result = $connection->query("INSERT INTO request_queue_archive SELECT * FROM request_queue WHERE
				rqid = :id ", array('id' => $this->rqid));

				if ($result == false) {
					throw new \Exception("Inserting into request_queue_archive failed!");
				}

				$result = $connection->query("DELETE FROM request_queue WHERE rqid = :id ", array('id' => $this->rqid));
				if ($result == false) {
					throw new \Exception("Deleting record from request_queue failed!");
				}
			}

			$connection->commit();
		//TODO: This will NOT work as Transaction will throw Phalcon\Mvc\Model\Transaction\Failed Exception!!!
		} catch (Exception $ex) {
			$connection->rollback();
		}
	}

	public function notSaved()
	{
		// Obtain the flash service from the DI container
		$flash = $this->getDI()->getFlash();

		// Show validation messages
		foreach ($this->getMessages() as $message) {
			$flash->error($message);
		}
	}

	/**
	 * Independent Column Mapping.
	 */
	public function columnMap()
	{
		return array(
			'rqid'					=> 'rqid',
			'fk_tenant_id'          => 'tenantId',
			'type'					=> 'type',
			'source'				=> 'source',
			'status'				=> 'status',
			'status_description'	=> 'statusDescription',
			'priority'				=> 'priority',
			'start_time'			=> 'startTime',
			'end_time'				=> 'endTime',
			'data'					=> 'data',
			'external_id'			=> 'externalId',
			'created'		        => 'createdDateTime',
			'fk_created_by'         => 'createdByUserId',
			'updated'		        => 'updatedDateTime',
			'fk_updated_by'         => 'updatedByUserId'

		);
	}

	public function getSource()
	{
		return 'request_queue';
	}


	public static function scheduleAJob($requestBody,
		$external_tid,
		$data,
		$userType = RequestQueue::SOURCE_ACR,
		$jobType = RequestQueue::TYPE_TEST_PACKAGE,
		$controller)
	{
		// Check if a specific job exist for a specific $external_id
		$job = RequestQueue::findFirst(array(
				'columns' => '*',
				'conditions' => ' tenantId = ?1 AND createdByUserId = ?2 AND externalId = ?3 AND type = ?4',
				'bind' => [
						1 => $controller->tenant->tenantId,
						2 => $controller->user->userId,
						3 => filter_var($external_tid, FILTER_SANITIZE_STRING),
						4 => $jobType
				]
		));

		if ($job && $job->count()) {
			$controller->logger->info("Job already exists (jobId: {$job->rqid} , external id: {$job->externalId}, " .
					"insertTimeStamp: {$job->createdDateTime}, type : {$job->type}");

			// in case the externalId exists in the job queue (created by the same user) we just return it
			return ['job' => $job, 'exist' => true];
		}

		$job = new RequestQueue();

		// here we check the type of the user
		if ($controller->user->type == $userType) {
			$job->tenantId = $controller->tenant->tenantId;
			$job->source = $controller->user->type;
			$job->type = $jobType;
			$job->status = RequestQueue::STATUS_SCHEDULED;
			$job->externalId = $external_tid;
			$job->data = json_encode($data);

			// setting the job start time.
			$job->startTime = new RawValue('UTC_TIMESTAMP()');
			$job->status = RequestQueue::STATUS_INPROGRESS;

			if ($job->save() == false) {
				$controller->logger->error("Failed to save job details.");
				throw new Exception("Failed to save job details." . json_encode($job->getMessages()));
			}

			$job->refresh();
			return ['job' => $job];
		} else {
			/**
			 * @todo Support the other users like PRC
			 */
			throw new HttpException(
					'ADP-1757',
					403,
					'Access Denied.',
					'Unauthorized user'
			);
		}
	}
}
