<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class Test
 *
 * This is the Test Model. It contains all Test Battery related details that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class Test extends BaseModel
{

	/**
	 * Test Properties.
	 *
	 * @var integer $testId
	 */
	public $testId;
	/**
	 * @var integer $tenantId
	 */
	public $tenantId;
	/**
	 * @var string $externalTestId
	 */
	public $externalTestId;
	/**
	 * @var string $name
	 */
	public $name;
	/**
	 * @var string $program
	 */
	public $program;
	/**
	 * @var string $scoreReport
	 */
	public $scoreReport;
	/**
	 * @var string $subject
	 */
	public $subject;
	/**
	 * @var string $grade
	 */
	public $grade;
	/**
	 * @var string $itemSelectionAlgorithm
	 */
	public $itemSelectionAlgorithm;
	/**
	 * @var string $security
	 */
	public $security;
	/**
	 * @var string $multimedia
	 */
	public $multimedia;
	/**
	 * @var string $scoring
	 */
	public $scoring;
	/**
	 * @var string $permissions
	 */
	public $permissions;
	/**
	 * @var string $privacy
	 */
	public $privacy;
	/**
	 * @var string $description
	 */
	public $description;
	/**
	 * @var boolean $isActive
	 */
	public $isActive;
	/**
	 * @var string $createdDateTime
	 */
	public $createdDateTime;
	/**
	 * @var integer $createdByUserId
	 */
	public $createdByUserId;
	/**
	 * @var string $updatedDateTime
	 */
	public $updatedDateTime;
	/**
	 * @var integer $updatedByUserId
	 */
	public $updatedByUserId;


	/**
	 * Sets Connection to the Database and Relationship to other Models.
	 * It also joins with other Models based on defined dependencies.
	 */
	public function initialize()
	{
		// Set Primary Database Connection as Test always uses Primary Database!
		self::changeConnectionService('db');

		parent::initialize();

		// Set Relationship to other Models.
		$this->belongsTo('tenantId', 'PARCC\ADP\Models\Tenant', 'tenantId', ['alias' => 'Tenant']);
		$this->belongsTo('createdByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
		//$this->belongsTo('updatedByUserId', 'PARCC\ADP\Models\User', 'userId', ['alias' => 'User']);
		$this->hasMany('testId', 'TestForm', 'parentTestId');
	}


	/**
	 * TODO: This should be moved outside Model!!!
	 * build select to read column definition for selected column
	 * 
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $column
	 * @return array
	 */
	public function getFieldOptions($column)
	{
		$columnMap = array_flip($this->columnMap());
		
		$sql = " SHOW COLUMNS ";
		$sql .= " FROM " . $this->getSource();
		$sql .= " LIKE '" . $columnMap[$column] . "'";

		$result = current($this->getDi()->getShared('db')->fetchAll($sql));

		if (!is_array($result)) {
			$result = (array) current($result);
		}
		// user regular expression to get option list from result
		preg_match("=\((.*)\)=is", $result["Type"], $options);

		// replace single quotes
		$options = str_replace("'", "", $options[1]);

		// explode string into an array
		$options = explode(",", $options);

		return $options;
	}


	/**
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'test';
	}


	/**
	 * Column Mapping returns Keys as Field Names in the Database Table and Values as Application Variables.
	 *
	 * @return array Database Table Mapping to Table Columns.
	 */
	public function columnMap()
	{
		return [
			'tid' => 'testId',
			'fk_tenant_id' => 'tenantId',
			'external_tid' => 'externalTestId',
			'name' => 'name',
			'program' => 'program',
			'score_report' => 'scoreReport',
			'subject' => 'subject',
			'grade' => 'grade',
			'item_selection_algorithm' => 'itemSelectionAlgorithm',
			'security' => 'security',
			'multimedia' => 'multimedia',
			'scoring' => 'scoring',
			'permissions' => 'permissions',
			'privacy' => 'privacy',
			'description' => 'description',
			'active' => 'isActive',
			'created' => 'createdDateTime',
			'fk_created_by' => 'createdByUserId',
			'updated' => 'updatedDateTime',
			'fk_updated_by' => 'updatedByUserId'
		];
	}
}
