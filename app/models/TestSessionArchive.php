<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Models;

/**
 * Class TestSessionArchive
 *
 * This is the Test Session Archive Model. It contains all Archived Completed Test Battery Form Revision
 * Session/Assignment related details that are mapped to the Database.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class TestSessionArchive extends TestSession
{

	/** @noinspection PhpMissingParentCallCommonInspection
	 * Returns the Database Table linked to the Model.
	 *
	 * @return string Database Table Name.
	 */
	public function getSource()
	{
		return 'test_session_archive';
	}
}
