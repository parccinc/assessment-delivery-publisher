<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Responses;

/**
 * Class RedirectResponse
 *
 * This is the Redirect Response. It simply sends a Temporary 302 Redirect with a URL for requested File Resource.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class RedirectResponse extends BaseResponse
{

	/**
	 * Sends HTTP 302 (Temporary Move) Redirect Response. It handles any Request the same way, including HEAD Requests.
	 *
	 * @param array   $records Array of records to be returned, OR an empty array if no records are found.
	 *                         It can also hold be an Error if an Exception was thrown while trying to retrieve records.
	 * @param boolean $isError Indicates if given array contains an Error details instead of actual Data.
	 *                         Errors come from HttpExceptions.
	 */
	public function respond($records, $isError = false)
	{
		// Set the Status and Status Description in the Header.
		if ($isError && $this->useHeaderStatus) {
			// Set the Error Status and Error Status Description if necessary, based on the Application Configuration.
			$this->setStatusCode($records['statusCode'], $records['statusDescription']);
			// Set the Error Code, Error Description and Time if necessary, based on the Application Configuration.
			// Besides Header Response, this is the only exception when Error Code and Error Description are directly
			// returned in the Response Header as Response Body is omitted.
			$this->setHeader('X-Error-Code', $records['errorCode']);
			$this->setHeader('X-Error-Description', $records['errorDescription']);
			$this->setHeader('X-Time', $records['time']);
		} else {
			// Set the default Status and Status Description.
			$this->setStatusCode('200', 'OK');
		}

		// Calculate ETag, if necessary.
		if ($this->useETag) {
			$this->setEtag(md5(serialize($records)));
		}

		// Set the Redirect URL (except in case of OPTIONS Request and Errors).
		if (count($records) > 0 && $isError === false) {
			if (mb_strpos($records[0], 'http') === 0) {
				$this->redirect(
					$records[0],
					true
				);

			} else {
				$this->redirect(
					($this->getDI()->get('useHttps') === true ? 'https://' : 'http://') .
					$_SERVER['HTTP_HOST'] . $records[0],
					true
				);
			}
		}

		// Check if Response should be logged.
		if ($this->getDI()->get('config')->logger->logResponse === true) {
			// Check if Response is an Error (Exception Handler is logging all Errors).
			if (!$isError) {
				// Log the Response.
				$this->getDI()->get('logger')->debug("Redirect Response:\n" . json_encode($records));
			}
		}

		// Send the Redirect Response.
		$this->send();
	}
}
