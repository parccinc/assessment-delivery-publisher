<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Responses;

use Phalcon\Http\Response;

/**
 * Class Response
 *
 * This is the Base Response. It also disables Browser Caching.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 */
class BaseResponse extends Response
{

	/**
	 * @var boolean $useHeaderStatus Indicates if Header Status should be used in the returned Response.
	 */
	protected $useHeaderStatus = false;
	/**
	 * @var boolean $useETag Indicates if ETags should be used in the returned Response.
	 */
	protected $useETag = false;
	/**
	 * @var boolean $isHeadRequest Indicates if received Request was made using HEAD Method.
	 */
	protected $isHeadRequest = false;


	/**
	 * Constructor. It makes sure to disable Response Caching.
	 *
	 * @param string  $content           Response Body Content to be returned, OR NULL in case of HEAD Request. It can
	 *                                   also be an Error if an Exception was thrown while trying to retrieve records.
	 * @param integer $statusCode        Status Code of the Response.
	 * @param string  $statusDescription Status Description of the Response.
	 */
	public function __construct($content = null, $statusCode = null, $statusDescription = null)
	{
		parent::__construct($content, $statusCode, $statusDescription);

		// Set indicator if Header Status should be used in returned Response, based on Application Configuration.
		$this->useHeaderStatus = $this->getDI()->get('config')->app->useHeaderStatus;
		// Set indicator if ETags should be used in returned Response, based on Application Configuration.
		$this->useETag = $this->getDI()->get('config')->app->useETag;

		// Set No Caching Policy as nothing should be cached, ever!!!
		$this->setHeader('Pragma', 'no-cache');
		$this->setHeader('Pragma-Directive', 'no-cache');
		$this->setHeader('Cache-Directive', 'no-cache');
		$this->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0');
		$this->setHeader('Expires', '-1');
		$this->setHeader('Vary', '*');
	}


	/**
	 * Sets indicator if received Request was made using HEAD Method.
	 *
	 * @param  boolean      $isHeadRequest Indicates if received Request was made using HEAD Method.
	 * @return BaseResponse $this          Returns itself.
	 */
	public function setHead($isHeadRequest = true)
	{
		$this->isHeadRequest = $isHeadRequest;

		return $this;
	}
}
