<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PDOException;
use Phalcon\Db\RawValue;
use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\TestFormRevision;

/**
 * Class LoginController
 *
 * This is the RESTful Controller for handling Client's Login functionality.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Bojan Vulevic <bojan.vulevic@breaktech.com>
 *
 * @property \PARCC\ADP\Models\TestSession testSession
 */
final class LoginController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed Methods for Login Web Service and validates CORS Request.
	 * It also Grants Permission to be used only for Test Delivery.
	 *
	 * @internal param string $allowedMethods Provides a list of supported Methods by each Controller.
	 * @property-read \Phalcon\Config $this->di->config
	 */
	public function __construct()
	{
		parent::__construct('POST, OPTIONS', true);

		// Grant Access only to Test Driver Users.
		$this->validateAccess(['TD']);

		// Validate Request and retrieve Test Session.
		if ($this->request->isPost()) {
			$this->validateBrowserCORS();
		}
	}


	/**
	 * POST Request Handler. This handles Test Driver's Login.
	 *
	 * @throws HttpException In case invalid Login details are being received.
	 */
	public function login()
	{
		// Retrieve Request Body as JSON and decode it as associative array.
		$requestBody = $this->request->getJsonRawBody(true);


		// Check if Test Key was provided.
		if (empty($requestBody['testKey'])) {
			// Throw Login Exception if Test Key is missing.
			throw new HttpException('ADP-1200', 401, 'Authorization Failed.', 'Missing Test Key.');
		}
		// Retrieve Test Key from the Request Body.
		$testKey = filter_var($requestBody['testKey'], FILTER_SANITIZE_STRING);

		// Check if Student's State was provided.
		if (empty($requestBody['state'])) {
			// Throw Login Exception if Student's State is missing.
			throw new HttpException('ADP-1201', 401, 'Authorization Failed.', 'Missing State.');
		}
		// Retrieve Student's State from the Request Body.
		$state = filter_var($requestBody['state'], FILTER_SANITIZE_STRING);

		// Check if Student's Personal Identifier was provided.
		if (!isset($requestBody['studentId'])) {
			// Throw Login Exception if Student's Personal Identifier is missing.
			throw new HttpException('ADP-1202', 401, 'Authorization Failed.', 'Missing Student ID.');
		}
		// Retrieve Student's Personal Identifier from the Request Body.
		$personalId = filter_var($requestBody['studentId'], FILTER_SANITIZE_STRING);

		// Check if Student's Date of Birth was provided.
		if (!isset($requestBody['dateOfBirth'])) {
			// Throw Login Exception if Student's Date of Birth is missing.
			throw new HttpException('ADP-1203', 401, 'Authorization Failed.', 'Missing Date of Birth.');
		}
		// Retrieve Student's Date of Birth from the Request Body.
		$dateOfBirth = filter_var($requestBody['dateOfBirth'], FILTER_SANITIZE_STRING);


		// Check if Test Key is in expected format.
		// Test Key is 10-character uppercase string.
		if (!preg_match('/^[A-Z]{10}$/', $testKey)) {
			// Throw Login Exception if Test Key is in invalid format or is empty.
			throw new HttpException('ADP-1204', 401, 'Authorization Failed.', 'Invalid Test Key format.');
		}

		// Check if Student's State is in expected format.
		// State is 2-character uppercase string (abbreviation for 50 states + District of Columbia).
		if (!preg_match(
			'/^((A[LKZR])|(C[AOT])|(D[ECD])|(FL)|(GA)|(HI)|(I[DLNA])|(K[SY])|(LA)|(M[EDAINSOT])|(N[EVHJMYCD])|' .
			'(O[HKR])|(P[AT])|(RI)|(S[CD])|(T[NX])|(UT)|(V[TA])|(W[AVIY]))$/',
			$state
		)) {
			// Throw Login Exception if Student's State is in invalid format or is empty.
			throw new HttpException('ADP-1205', 401, 'Authorization Failed.', 'Invalid State.');
		}


		// Check if Test Key matches Test Assignment.
		if ($testKey !== $this->testSession->testKey) {
			// Throw Login Exception if Test Key doesn't match.
			throw new HttpException('ADP-1206', 401, 'Authorization Failed.', 'Mismatched Test Key.');
		}


		// Check if provided Test Key is for a Practice Test.
		$isPracticeTest = $this->checkIfPracticeTest($testKey);
		if ($isPracticeTest === true) {
			// Retrieve Login Configuration for Practice Tests.
			$loginConfiguration = json_decode($this->getSystemConfiguration('loginConfigurationPractice'), true);

		} else {
			// Retrieve Login Configuration for non-Practice Tests.
			$loginConfiguration = json_decode($this->getSystemConfiguration('loginConfiguration'), true);
		}

		// Check if Login Configuration contains Student' State.
		if (empty($loginConfiguration[$state])) {
			// Throw Internal Error Exception if Login Configuration doesn't contain Student's State (either due to an
			// invalid Request or missing Login Configuration for that State).
			throw new HttpException('ADP-1207', 401, 'Authorization Failed.', 'Invalid Login Configuration!');
		}


		// Check if Test Form Revision is already assigned to the Test Session, and construct PHQL (Phalcon Query
		// Language) parts related to Test Form Revision accordingly.
		if (empty($this->testSession->parentTestFormRevisionId)) {
			// Try to retrieve all Test Assignment details (Student, Test and Test Form without Test Form Revision)
			// using PHQL (Phalcon Query Language) which relies on Models.
			/** @var \Phalcon\Mvc\Model\Resultset\Simple $testAssignment */
			$testAssignment = $this->modelsManager->executeQuery(
				'SELECT
				    s.studentId,
				    s.personalId AS studentPersonalId,
				    s.dateOfBirth AS studentDateOfBirth,
				    s.state AS studentState,
				    s.firstName AS studentFirstName,
				    s.lastName AS studentLastName,
				    s.grade AS studentGrade,
				    s.schoolName AS studentSchoolName,
				    s.isActive AS studentIsActive,
				    t.testId AS testId,
				    t.name AS testName,
				    t.security AS testSecurity,
				    t.multimedia AS testMultimedia,
				    t.scoring AS testScoring,
				    t.scoreReport AS testScoreReport,
				    t.itemSelectionAlgorithm AS testItemSelectionAlgorithm,
				    t.grade AS testGrade,
				    t.description AS testDescription,
				    t.isActive AS testIsActive,
				    tf.formId AS testFormId,
				    tf.parentTestId AS testFormParentTestId,
				    tf.isActive AS testFormIsActive
				 FROM PARCC\ADP\Models\Student AS s
				 LEFT JOIN PARCC\ADP\Models\TestForm AS tf ON tf.formId = :testFormId: AND tf.tenantId = :tenantId:
				 LEFT JOIN PARCC\ADP\Models\Test AS t ON t.testId = tf.parentTestId AND t.tenantId = :tenantId:
				 WHERE s.tenantId = :studentTenantId: AND s.studentId = :studentId:
				 LIMIT 1',
				[
					'tenantId' => $this->tenant->tenantId,
					// In case Test Assignment is for a Practice Test, assigned Student belongs to Anonymous Tenant!
					'studentTenantId' => ($isPracticeTest === true ? 0 : $this->tenant->tenantId),
					'testFormId' => $this->testSession->parentTestFormId,
					'studentId' => $this->testSession->parentStudentId
				]
			);

			// Try to retrieve the first Record.
			$testAssignment = $testAssignment->getFirst();


			// Check if Test Assignment details were successfully retrieved.
			if ($testAssignment === false) {
				// Throw Login Exception if Test Assignment details were not found. This should NOT happen as Test Key
				// was already validated and matching Test Session was found.
				throw new HttpException('ADP-1208', 500, 'Internal Error.', 'Test Assignment Not Found.');
			}

		} else {
			// Try to retrieve all Test Assignment details (Student, Test, Test Form and Test Form Revision) using PHQL
			// (Phalcon Query Language) which relies on Models.
			// This only applies to non-Practice Tests as Practice Tests should never have Test Form Revision saved into
			// Database!
			/** @var \Phalcon\Mvc\Model\Resultset\Simple $testAssignment */
			$testAssignment = $this->modelsManager->executeQuery(
				'SELECT
				    s.studentId,
				    s.personalId AS studentPersonalId,
				    s.dateOfBirth AS studentDateOfBirth,
				    s.state AS studentState,
				    s.firstName AS studentFirstName,
				    s.lastName AS studentLastName,
				    s.grade AS studentGrade,
				    s.schoolName AS studentSchoolName,
				    s.isActive AS studentIsActive,
				    t.testId AS testId,
				    t.name AS testName,
				    t.security AS testSecurity,
				    t.multimedia AS testMultimedia,
				    t.scoring AS testScoring,
				    t.scoreReport AS testScoreReport,
				    t.itemSelectionAlgorithm AS testItemSelectionAlgorithm,
				    t.grade AS testGrade,
				    t.description AS testDescription,
				    t.isActive AS testIsActive,
				    tf.formId AS testFormId,
				    tf.parentTestId AS testFormParentTestId,
				    tf.isActive AS testFormIsActive,
				    tfr.revisionId AS testFormRevisionId,
				    tfr.isActive AS testFormRevisionIsActive
				 FROM PARCC\ADP\Models\Student AS s
				 LEFT JOIN PARCC\ADP\Models\TestForm AS tf ON tf.formId = :testFormId: AND tf.tenantId = :tenantId:
				 LEFT JOIN PARCC\ADP\Models\Test AS t ON t.testId = tf.parentTestId AND t.tenantId = :tenantId:
				 LEFT JOIN PARCC\ADP\Models\TestFormRevision AS tfr ON tfr.revisionId = :testFormRevisionId: AND
				                                                       tfr.tenantId = :tenantId:
				 WHERE s.tenantId = :studentTenantId: AND s.studentId = :studentId:
				 LIMIT 1',
				[
					'tenantId' => $this->tenant->tenantId,
					'studentTenantId' => $this->tenant->tenantId,
					'testFormId' => $this->testSession->parentTestFormId,
					'testFormRevisionId' => $this->testSession->parentTestFormRevisionId,
					'studentId' => $this->testSession->parentStudentId
				]
			);

			// Try to retrieve the first Record.
			$testAssignment = $testAssignment->getFirst();


			// Check if Test Assignment details were successfully retrieved.
			if ($testAssignment === false) {
				// Throw Login Exception if Test Assignment details were not found. This should NOT happen as Test Key
				// was already validated and matching Test Session was found.
				throw new HttpException('ADP-1209', 500, 'Internal Error.', 'Test Assignment Not Found.');
			}
		}


		// Retrieve Login Configuration for Student's State.
		$loginConfiguration = $loginConfiguration[$state];


		// Check if Student's State requires Student's Personal Identifier for Login.
		if ($loginConfiguration['pid'] === true) {
			// Check if Student's Personal Identifier is in expected format and is valid.
			// Student's Personal Identifier is alphanumeric string with maximum length of 30 characters.
			if (!preg_match('/^[0-9a-zA-Z]{1,30}$/', $personalId)) {
				// Throw Login Exception if Student's Personal Identifier is in invalid format or is empty.
				throw new HttpException('ADP-1210', 401, 'Authorization Failed.', 'Invalid Student ID format.');

			} elseif ($personalId !== $testAssignment['studentPersonalId']) {
				// Throw Login Exception if Student's Personal Identifier doesn't match.
				throw new HttpException('ADP-1211', 401, 'Authorization Failed.', 'Invalid Student ID.');
			}

		} elseif (!empty($personalId)) {
			// Throw Login Exception as Student's Personal Identifier is NOT expected to be received.
			throw new HttpException('ADP-1212', 401, 'Authorization Failed.', 'Unexpected Student ID.');
		}

		// Check if Student's State requires Student's Date of Birth for Login.
		if ($loginConfiguration['dob'] === true) {
			// Check if Student's Date of Birth is in expected format and is valid.
			// Student's Date of Birth is in ANSI SQL-92 format ('YYYY-MM-DD').
			if (!preg_match('/^(199\d|20\d\d)-(0\d|10|11|12)-(0\d|1\d|2\d|30|31)$/', $dateOfBirth)) {
				// Throw Login Exception if Student's Date of Birth is in invalid format or is empty.
				throw new HttpException('ADP-1213', 401, 'Authorization Failed.', 'Invalid Date of Birth format.');

			} elseif ($dateOfBirth !== $testAssignment['studentDateOfBirth']) {
				// Throw Login Exception if Student's Date of Birth doesn't match.
				throw new HttpException('ADP-1214', 401, 'Authorization Failed.', 'Invalid Date of Birth.');
			}

		} elseif (!empty($dateOfBirth)) {
			// Throw Login Exception if Student's Date of Birth is NOT expected to be received.
			throw new HttpException('ADP-1215', 401, 'Authorization Failed.', 'Unexpected Date of Birth.');
		}


		// Check if Student is Active, matches referenced State and belongs to Current Tenant.
		if ($testAssignment['studentIsActive'] === false) {
			// Throw Login Exception if Student is Deactivated.
			throw new HttpException('ADP-1216', 403, 'Access Denied.', 'Student is Deactivated.');

		} elseif ($state !== $testAssignment['studentState']) {
			// Throw Login Exception if Student's State doesn't match.
			throw new HttpException('ADP-1217', 401, 'Authorization Failed.', 'Invalid State.');
		}


		// Check if Test was successfully retrieved, is Active and belongs to Current Tenant.
		if (empty($testAssignment['testId'])) {
			// Throw Internal Error Exception if Test was not found.
			throw new HttpException('ADP-1218', 500, 'Internal Error.', 'Test Not Found.');

		} elseif ($testAssignment['testIsActive'] === false) {
			// Throw Login Exception if Test is Deactivated.
			throw new HttpException('ADP-1219', 403, 'Access Denied.', 'Test is Deactivated.');
		}


		// Check if Test Form was successfully retrieved, is Active and belongs to Current Tenant.
		if (empty($testAssignment['testFormId'])) {
			// Throw Internal Error Exception if Test Form was not found.
			throw new HttpException('ADP-1220', 500, 'Internal Error.', 'Test Form Not Found.');

		} elseif ($testAssignment['testFormIsActive'] === false) {
			// Throw Login Exception if Student was not found.
			throw new HttpException('ADP-1221', 403, 'Access Denied.', 'Test Form is Deactivated.');
		}


		// Check if Test Form Revision is already assigned to the Test Session, and construct PHQL (Phalcon Query
		// Language) parts related to Test Form Revision accordingly.
		if (empty($this->testSession->parentTestFormRevisionId)) {
			// Retrieve an Active Test Form Revision with the maximum Version (NOT the most recently published one)!
			/** @var TestFormRevision $testFormRevision */
			$testFormRevision = TestFormRevision::findFirst([
				'columns' => 'revisionId',
				'conditions' => "tenantId = ?1 AND parentTestFormId = ?2 AND isActive = 1",
				'order' => 'version DESC',
				'bind' => [
					1 => $this->tenant->tenantId,
					2 => $this->testSession->parentTestFormId
				]
			]);

			// Check if Test Form Revision was successfully retrieved.
			if ($testFormRevision === false) {
				// Throw Login Exception if no Active Test Form Revision was found.
				throw new HttpException('ADP-1222', 500, 'Internal Error.', 'Unable to Assign Test Form Revision.');
			}


			// Assign Test Form Revision to the Test Session.
			$this->testSession->parentTestFormRevisionId = $testFormRevision->revisionId;

		} else {
			// Check if Test Form Revision was successfully retrieved, is Active and belongs to Current Tenant.
			if (empty($testAssignment['testFormRevisionId'])) {
				// Throw Internal Error Exception if Test Form Revision was not found.
				throw new HttpException('ADP-1223', 500, 'Internal Error.', 'Test Form Revision Not Found.');

			} elseif ($testAssignment['testFormRevisionIsActive'] === false) {
				// Throw Login Exception if Test Form Revision is Deactivated.
				throw new HttpException('ADP-1224', 403, 'Access Denied.', 'Test Form Revision is Deactivated.');
			}
		}


		// Check if Test Session has a valid Test Status.
		if ($this->testSession->status === 'Submitted') {
			// Throw Login Exception if Test is already Submitted.
			throw new HttpException('ADP-1225', 403, 'Access Denied.', 'Test is already Submitted.');

		} elseif ($this->testSession->status === 'Completed') {
			// Throw Login Exception if Test is already Completed.
			throw new HttpException('ADP-1226', 403, 'Access Denied.', 'Test is already Completed.');

		} elseif ($this->testSession->status === 'Canceled') {
			// Throw Login Exception if Test is Canceled.
			throw new HttpException('ADP-1227', 403, 'Access Denied.', 'Test is Canceled.');

		}

		// Check if Debugging is Enabled and if Test Status should NOT be checked if it's In Progress, but only for
		// non-Practice Tests. This should NOT be used in Production and In Progress check should ALWAYS be validated!
		if (($this->config->app->debug !== true || $this->config->Login->useInProgressCheck !== false) &&
		    $isPracticeTest === false
		) {
			// Check if Test is already In Progress.
			// This validations can be skipped ONLY if Debugging is turned On and if InProgress Check if turned Off!
			if ($this->testSession->status === 'InProgress') {
				// Throw Login Exception if Test is already In Progress.
				throw new HttpException('ADP-1228', 403, 'Access Denied.', 'Test is already In Progress.');

			} elseif ((float) $this->testSession->tokenExpirationTimestamp > time() && preg_match(
				'/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/',
				$this->testSession->token
			)) {
				// Throw Login Exception if Test is not yet In Progress, but there is a valid Test Session Token which
				// hasn't expired yet (in case there was a recent successful Login and Student is currently on one of
				// the pretest pages).
				throw new HttpException('ADP-1229', 403, 'Access Denied.', 'Test is already In Progress.');
			}
		}


		// Check if Test is a Practice Test and if its Test Session Token is missing.
		// Practice Test should always use initially generated Test Session Token! Otherwise, always generate a new one.
		if ($isPracticeTest === false || empty($this->testSession->token)) {
			// Generate new GUID as a unique Test Session Token to be used as a Session ID when calling other Web
			// Services.
			$this->testSession->token = new RawValue('(SELECT uuid())');

			// Set Test Session Token Retry Counter.
			$tokenRetryCounter = 1;
			// Keep trying to generate new Test Session Token until Token Retry Count is reached (in case of failure to
			// generated a unique GUID). In general, MySQL has decent entropy which should avoid generating identical
			// GUIDs. But once too many are already being generated and with millions of generated ones, the chances for
			// generating duplicates start to increase, so this is just a safety check. Most of the time new Token
			// should be generated in the first try.
			while ($tokenRetryCounter <= (int) $this->config->Login->tokenRetryCount) {
				// Set Token Timestamp and save generated Token.
				try {
					// Try to set Token Timestamp and save generated Token.
					$this->refreshTestSessionToken($isPracticeTest);
					// Exit the loop as soon as Test Session Token is successfully generated.
					break;

				} catch (PDOException $exception) {
					// Log failed attempt to generate Test Session Token.
					$this->logger->alert("Failure to generate Test Session Token #{$tokenRetryCounter}.");

					// Check if maximum Test Session Token Retry Count has been reached.
					if ($tokenRetryCounter === (int) $this->config->Login->tokenRetryCount) {
						// Throw Login Exception in case of multiple consecutive failures to generate a unique Test
						// Session Token.
						throw new HttpException('ADP-1230', 500, 'Internal Error.', 'Unable to create Test Session.');
					}
				}

				// Increment Test Session Token Retry Counter.
				++$tokenRetryCounter;
			}
		}


		// Reload updated Test Session and related Test Assignment details.
		$this->testSession->refresh();


		// Check if Test is a Practice Test.
		if ($isPracticeTest === true) {
			// Re-assign Test Form Revision to the Test Session as it was unset prior to updating it in Database!
			/** @var TestFormRevision $testFormRevision */
			$this->testSession->parentTestFormRevisionId = $testFormRevision->revisionId;

			// Retrieve Test Driver Configuration for Practice Tests.
			$testDriverConfig = $this->getSystemConfiguration('testDriverConfigurationPractice');

		} else {
			// Retrieve Test Driver Configuration for non-Practice Tests.
			$testDriverConfig = $this->getSystemConfiguration('testDriverConfiguration');
		}


		// Send Test details and Test Driver Configuration as a Response.
		$this->respond([
			'api' => [
				'catUrl' => $this->config->Login->catUrl,
				'token' => $this->testSession->token,
				'expires' => (float) $this->testSession->tokenExpirationTimestamp,
				'time' => time() /* UTC server time. */
			],
			'config' => json_decode($testDriverConfig, true),
			'candidate' => [
				'firstName' => $testAssignment['studentFirstName'],
				'lastName' => $testAssignment['studentLastName'],
				'grade' => $testAssignment['studentGrade'],
				'school' => $testAssignment['studentSchoolName']
			],
			'test' => [
				'id' => (int) $this->testSession->parentTestFormRevisionId,
				'name' => $testAssignment['testName'],
				'secure' => $testAssignment['testSecurity'],
				'media' => $testAssignment['testMultimedia'],
				'scoring' => $testAssignment['testScoring'],
				'scoreReport' => $testAssignment['testScoreReport'],
				'itemSelection' => $testAssignment['testItemSelectionAlgorithm'],
				'grade' => $testAssignment['testGrade'],
				'description' => $testAssignment['testDescription'],
				'status' => $this->testSession->status
			],
			'tools' => [
				'lineReader' => (bool) $this->testSession->isEnabledLineReader,
				'textToSpeech' => (bool) $this->testSession->isEnabledTextToSpeech
			]
		]);
	}
}
