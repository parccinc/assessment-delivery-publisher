<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PARCC\ADP\Models\TestResults;
use PARCC\ADP\Models\TestSession;
use PARCC\ADP\Models\RequestQueue;
use PARCC\ADP\Models\TestForm;
use PARCC\ADP\Services\PublishHelper;
use PARCC\ADP\Exceptions\HttpException;
use Phalcon\Exception;

/**
 * Class ResultsExportController
 *
 * This is the RESTful Controller for Sending Results References functionality.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 *
 * @property TestSession $testSession
 */
final class ResultsExportController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed methods for Results web service and validates CORS request.
	 *
	 * @internal param string $allowedMethods Provides a list of supported methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('GET, OPTIONS', false, true);

		// Grant Access only to LDR Users.
		$this->validateAccess(['LDR']);

	}

	/**
	 * @TODO: All received parameters must be validated and sanitized!
	 * @TODO: Add Token validation and expiration!
	 *
	 * GET request handler. This is the download of Test Results.
	 *
	 * @param  string $token Test Assignment Token.
	 * @param  string $parentTestFormRevisionId Test Battery Form Revision ID.
	 * @return array                            Response contains confirmation about requested action.
	 * @throws HttpException                    In case invalid Results details are being received or Test Session Token
	 *                                          has expired.
	 */

	/**
	 * GET request handler. This is to return the last Test Results.
	 *
	 * @param int $adpTestAssignmentId
	 * @throws HttpException
	 * @throws Exception
	 */
	public function get($adpTestAssignmentId)
	{

		try {

			// Validating JSON
			$requestBody = $this->request->getJsonRawBody(true);

			$globalRequest = [
				'adpTestAssignmentId' => $adpTestAssignmentId,
				'requestBody' => json_encode($requestBody)
			];

			$job = new RequestQueue();
			$job->tenantId = $this->tenant->tenantId;
			$job->type = RequestQueue::TYPE_RESULTS;
			$job->source = $this->user->type;
			$job->status = RequestQueue::STATUS_SCHEDULED;
			$job->externalId = $adpTestAssignmentId;
			$job->data = json_encode($globalRequest);
			$job->startTime = PublishHelper::getDbCurrentTimestamp($this->db2);

			if ($job->save() == false) {
				throw new HttpException(
					'ADP-1901',
					500,
					'Internal Error.',
					'Unable to save job details.'
				);
			}

			// Validate $adpTestAssignmentId
			if (!($adpTestAssignmentId > 0 && $adpTestAssignmentId <= 4294967295)) {
				throw new HttpException(
						"ADP-1902",
						403,
						"Access Denied.",
						"Invalid value of adpTestAssignmentId ($adpTestAssignmentId)."
				);
			}

			// Retrieve referenced Test Assignment details.
			$testSession = TestSession::findFirst(
					[
							'conditions' => ' tenantId = ?1 AND testSessionId = ?2 ',
							'bind' => [
									1 => $this->tenant->tenantId,
									2 => $adpTestAssignmentId
							]
					]
			);

			if ($testSession === false) {
				throw new HttpException(
					'ADP-1903',
					404,
					'Data Not Found.',
					'Test Assignment Not Found.'
				);
			} elseif ($testSession->status == TestSession::STATUS_SCHEDULED) {
				throw new HttpException(
					"ADP-1904",
					403,
					"Access Denied.",
					"Trying to export results for a Test Assignment having a 'Scheduled' status."
				);
			}

			$job->status = RequestQueue::STATUS_INPROGRESS;
			if ($job->save() == false) {
				if ($job->save() == false) {
					throw new HttpException(
						'ADP-1901',
						500,
						'Internal Error.',
						'Unable to save job details.'
					);
				}
			}

			$data = $this->getResults($testSession);
			$this->logger->info("[ResultsExportController getResults] " . json_encode($data));

			$job->status = RequestQueue::STATUS_COMPLETED;
			$job->endTime = PublishHelper::getDbCurrentTimestamp($this->db2);

			if ($job->save() == false) {
				throw new HttpException(
					'ADP-1901',
					500,
					'Internal Error.',
					'Unable to save job details.'
				);
			}

			$this->respond($data);
		} catch (Exception $ex) {
			if (isset($job)) {
				$job->status = RequestQueue::STATUS_FAILED;
				$job->save();
			}
			throw $ex;
		}
	}

	/**
	 * getResults based on $testSession, getResults will return the most recent result references.
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param TestSession $testSession
	 * @return array
	 * @throws HttpException
	 */
	private function getResults($testSession)
	{
		/**@var $testResult TestResults */
		$testResult = TestResults::findFirst([
			'conditions' => ' tenantId = ?1 AND parentTestSessionId = ?2 AND isActive = 1',
			'order' => 'createdDateTime DESC',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $testSession->testSessionId
			]
		]);

		$resultId = null;

		if ($testResult) {
			$resultId = $testResult->testResultsId;
		} else {
			if ($testSession->status == TestSession::STATUS_CANCELED) {
				$this->logger->warning("No result found for Test Session Id " .
					"({$testSession->testSessionId}) having Status as '{$testSession->status}'");
			} elseif (in_array($testSession->status, [
				TestSession::STATUS_COMPLETED,
				TestSession::STATUS_SUBMITTED,
				TestSession::STATUS_INPROGRESS,
				TestSession::STATUS_PAUSED
			])) {
				throw new HttpException(
					"ADP-1911",
					404,
					"Data Not Found.",
					"[getResults] No result found for Test Session " .
					"Id ({$testSession->testSessionId}) having Status as '{$testSession->status}'"
				);
			} else { // unauthorized transaction // should never happen as this has been already covered previously
				throw new HttpException(
					"ADP-1912",
					403,
					"Access Denied.",
					"[getResults] Trying to export results for a Test Assignment that is not " .
					"'submitted', 'completed', 'inProgress', 'paused' or 'canceled'." .
					"(actual status: {$testSession->status})"
				);
			}
		}

		$form = TestForm::findFirst(
				[
						'conditions' => ' tenantId = ?1 AND formId = ?2 ',
						'bind' => [
								1 => $this->tenant->tenantId,
								2 => $testSession->parentTestFormId
						]
				]
		);

		if (!$form) {
			throw new HttpException(
				"ADP-1913",
				404,
				"Data Not Found.",
				"[getResults] Unable to retrieve the form for Test Session : " .
				"({$testSession->testSessionId}) having Status as '{$testSession->status}'");
		}

		return [
			'testAssignmentId' => (int)$testSession->externalTestSessionId,
			'studentId' => (int)$testSession->parentStudentId,
			'testId' => (int)$form->parentTestId,
			'testFormId' => (int)$testSession->parentTestFormId,
			'testFormRevisionId' => (int)$testSession->parentTestFormRevisionId,
			'testResultsId' => (int)$resultId,
			'startTimestamp' => $testSession->startDateTime,
			'endTimestamp' => $testSession->endDateTime,
			'status' => $testSession->status,
			'lastResultsPath' => ($testResult ? $testResult->path : null)
		];
	}
}
