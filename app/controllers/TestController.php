<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PARCC\ADP\Services;
use PARCC\ADP\Services\JSV\JSVSchema;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PARCC\ADP\Services\AWS;
use PARCC\ADP\Services\AWS\S3Exception;
use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Models\RequestQueue;
use PARCC\ADP\Models\TestContent;
use PARCC\ADP\Models\Test;
use PARCC\ADP\Models\TestForm;
use PARCC\ADP\Models\TestSession;
use PARCC\ADP\Models\TestFormRevision;
use PARCC\ADP\Services\PublishHelper;
use Phalcon\Mvc\Model;
use Phalcon\Db\RawValue;
use Phalcon\Exception;

/**
 * Class TestController
 *
 * This is the RESTful Controller for handling client's login functionality.
 *
 * @package PARCC\ADP
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 */
class TestController extends BaseController
{

	/**
	 *
	 * @var string path to the test folder.
	 */
	private $jobFolder;

	/**
	 *
	 * @var string path to the zip package.
	 */
	private $zipPackage;

	/**
	 *
	 * @var string path to the unpacked package.
	 */
	private $unpackedPackagePath;

	/**
	 * Constructor.
	 *
	 * Sets the allowed methods for Login web service.
	 *
	 * @internal param string $allowedMethods Provides a list of supported methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('POST, PATCH', false, true);

		// Grant Access only to ACR & PRC Users.
		$this->validateAccess(['ACR', 'PRC']);
	}

	/**
	 * Post process the publishing and return the response.
	 *
	 * POST request handler.
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 */
	public function post()
	{
		return $this->publish();
	}

	/**
	 * deployRevisionPackage
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param RequestQueue $job
	 * @throws Exception
	 */
	public function deployRevisionPackage($job)
	{
		$jobId = $job->rqid;
		$externalId = $job->externalId;
		$globalRequest = json_decode($job->data, true);

		$adpTestFormRevisionId = $globalRequest['adpTestFormRevisionId'];
		$contents = [];
		$test = false;

		try {
			// setting the job start time.
			$job->startTime = PublishHelper::getDbCurrentTimestamp($this->db2);
			// update the job status.
			$job->status = RequestQueue::STATUS_INPROGRESS;

			// save
			$job->save();

			if ($adpTestFormRevisionId) {
				$this->logger->info("[REPUBLISH : deployRevisionPackage] Starting Deployment of JobId ($jobId)");
				$this->logger->info("[REPUBLISH : deployRevisionPackage] Cleaning Contents from S3 for JobId ($jobId)");
				// Delete contents
				$toBeDeletedContents = TestContent::find(
						[
								'conditions' => ' tenantId = ?1 AND parentTestFormRevisionId = ?2 ',
								'bind' => [
										1 => $this->tenant->tenantId,
										2 => $adpTestFormRevisionId
								]
						]
				);

				// delete contents from AWS.
				$this->cleanUpFiles($toBeDeletedContents, true);
				// delete from DATABASE.
				$this->logger->info("[REPUBLISH : deployRevisionPackage] Cleaning Contents " .
						"from Database for JobId ($jobId)");
				foreach ($toBeDeletedContents as $content) {
					$content->delete();
				}
			} else {
				$this->logger->info("[PUBLISH : deployRevisionPackage] Starting Deployment of JobId ($jobId)");
			}

			// get the package from ACR
			$dataPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR .
					$jobId . DIRECTORY_SEPARATOR;
			$this->jobFolder = $dataPath;

			if (file_exists($this->jobFolder)) {
				throw new Exception("Job Folder ($jobId) already exists.");
			}

			$this->zipPackage = $this->getPackage($externalId, $jobId, $dataPath);

			// unpack the package
			$this->unpackedPackagePath = $this->unpackPackage($this->zipPackage, $dataPath . $externalId .
					DIRECTORY_SEPARATOR);

			// read the test.json file
			$jsonContent = file_get_contents($this->unpackedPackagePath . DIRECTORY_SEPARATOR . 'test.json');
			$testLength = strlen($jsonContent);

			$this->logger->info("Test Size: $testLength | Test Size after decoding processing: " .
				($testLength * 10) . " | Memory Limit: " .	(intval(ini_get('memory_limit')) *
								1024 *	1024));

			if (($testLength * 10) > (intval(ini_get('memory_limit')) * 1024 * 1024)) {
				$newMemory = floor(($testLength * 10) / (1024 * 1024)) + 8;
				$this->logger->info("Increasing memory limit to: {$newMemory}M");
				ini_set('memory_limit', "{$newMemory}M");
			}

			$testArray = json_decode($jsonContent, true);

			if (!isset($testArray['pub_resourceManifest']) || $testArray['pub_resourceManifest'] == null ) {
				$testArray['pub_resourceManifest'] = [];
			}

			if (!is_object($testArray) && !is_array($testArray)) {
				$this->logger->error("test.json not found or invalid for revision id: $externalId (" .
						json_last_error_msg() . ")");
				throw new Exception('Invalid Test.');
			}

			/**
			 * Parse/Validate test package : PARADS-2500 Parse/validate the test content, return error
			 * and add entry to log in case of error.
			 */
			if ($this->config->Test->useTestValidation === true) {
				$testSchema = file_get_contents(
					dirname(dirname(__DIR__)) . (string) $this->config->Test->testSchema
				);

				// Initialize JSV Schema.
				$jsvSchema = new JSVSchema("testSchema", $testSchema);
				// Check if JSON Test Schema is valid.
				if (count($jsvSchema->errors) > 0) {
					$this->logger->error("Invalid Test JSON Schema:\n" . json_encode((array) $jsvSchema->errors));
					throw new Exception("Invalid Test JSON Schema.");
				}

				// Validate JSON Test against JSON Test Schema.
				$jsv = $this->jsv->validate($testArray, $testSchema);

				// Check if Results JSON is valid.
				if ($jsv->isValid === false) {
					$this->logger->error('Invalid Test Json details : (' . json_encode($jsv->errors) . ')');
					throw new Exception('Invalid Test Json details.');
				}
			}

			/**
			 * Starting the transaction
			 */
			$transactionManager = new TransactionManager();
			// Transaction 1.
			$transaction = $transactionManager->get();
			$publishedBy =  $globalRequest["requestBody"]["publishedBy"];

			$parsedDataArray = $this->parseMetadata(
					$this->unpackedPackagePath,
					$externalId,
					$publishedBy,
					$transaction,
					$adpTestFormRevisionId);

			if ($parsedDataArray == false) {
				throw new Exception("Parsing Metadata failed.");
			}

			/**
			 * updateTestBatteryForm flag tells us if we need to make an create or
			 * an update request to update data in LDR.
			 */
			$test = $parsedDataArray['test'];
			$testForm = $parsedDataArray['testForm'];
			$testFormRevision = $parsedDataArray['testFormRevision'];
			$updateTestBatteryForm = $parsedDataArray['updateTestBatteryForm'];

			$this->logger->debug('Test: ' . json_encode($test));

			// Processing Practice Test!
			if ($test->program == 'Practice Test') {
				$practiceTestTestSession = $this->processPracticeTest($test, $testForm);
			} else {
				$this->logger->info("Skipping Practice Test Processing.");
			}

			// Committing Transaction 1.
			$transaction->commit();

			// loop through the folder create contents entries and upload content to AWS S3 including the zip package.
			$contents = $this->processContents(
					$this->unpackedPackagePath,
					$testFormRevision->revisionId,
					$this->zipPackage,
					$testArray['pub_resourceManifest']);

			if (empty($contents)) {
				$this->logger->error('Contents:' . json_encode($contents));
				throw new Exception("No contents were processed!");
			}

			/**
			 * Updating the LDR.
			 */
			if ($this->user->type == RequestQueue::SOURCE_ACR && ($test->program != 'Practice Test')) {
				// sending test battery form details LDR could be POST or PUT method depending on $updateTestBatteryForm
				if ($this->sendTestBatteryForm($test, $testForm, $updateTestBatteryForm) == false) {
					throw new Exception("Unable to send Test Battery Form");
				}
			} elseif ($test->program == 'Practice Test'){
				// @ToDo: Make a web service call to notify and send the Practice Test Test Key.
			}
			else {
				$this->logger->info("Skipping sendTestBatteryForm, Details: (User Type = " . $this->user->type .
						", Test Program = " . $test->program . ")");
			}

			// Transaction 2.
			$transaction2 = $transactionManager->get();

			// Saving
			// Enabling & Saving $est, $testForm and $testFormRevision
			$test->isActive = true;
			$testForm->isActive = true;
			$testFormRevision->isActive = true;
			$testFormRevision->updatedDateTime = new RawValue('UTC_TIMESTAMP()');

			$test->setTransaction($transaction2);
			$testForm->setTransaction($transaction2);
			$testFormRevision->setTransaction($transaction2);

			if ($test->save() == false) {
				$this->logger->error("Can't save Test.");
				$transaction2->rollback("Can't save Test.");
			}
			if ($testForm->save() == false) {
				$this->logger->error("Can't save Test Form.");
				$transaction2->rollback("Can't save Test Form.");
			}
			if ($testFormRevision->save() == false) {
				$this->logger->error("Can't save Test Form Revision.");
				$transaction2->rollback("Can't save Test Form Revision.");
			}

			// Saving contents
			foreach ($contents as $content) {
				$this->logger->debug("content: " . json_encode($content->dump()));
				$content->isActive = true;
				$content->setTransaction($transaction2);
				if ($content->save() == false) {
					$transaction2->rollback("Can't save contents.");
				}
			}

			// Committing Transaction 2.
			$transaction2->commit();

			// issue with Form Revision not capturing the changes.
			$fRevision = TestFormRevision::findFirst(
					[
							'conditions' => ' tenantId = ?1 AND revisionId = ?2 ',
							'bind' => [
									1 => $this->tenant->tenantId,
									2 => $testFormRevision->revisionId
							]
					]
			);

			$fRevision->isActive = true;
			$fRevision->save();

			// Re-Activate the Practice Test Assignment.
			if (isset($practiceTestTestSession) && ($practiceTestTestSession instanceof TestSession)) {
				$this->logger->info("Activated the practice test assignment.");

				// issue with F$practiceTestTestSession not capturing the changes.
				$testSession = TestSession::findFirst(
						[
							'conditions' => ' tenantId = ?1 AND testSessionId = ?2 ',
							'bind'  => [
								1 => $this->tenant->tenantId,
								2 => $practiceTestTestSession->testSessionId
							]
						]
				);

				$testSession->isActive = true;

				if ($testSession->save() == false) {
					$this->logger->error("Failed to save Test Session for a 'Practice Test'.");
					foreach ($testSession->getMessages() as $message) {
						$this->logger->error($message);
					}
					throw new Exception("Failed to save Test Session for a 'Practice Test'.");
				}

			}

			// setting the job end time.
			$job->status = RequestQueue::STATUS_COMPLETED;
			$job->endTime = PublishHelper::getDbCurrentTimestamp($this->db2);

			if ($job->save() == false) {
				$this->logger->error("Failed to save Job.");
				throw new Exception("Failed to save Job.");
			}

			$job->refresh();

			$this->logger->info("Updating Status  (testFormRevisionId : $externalId, Job ID : $jobId, ADP Test ID: " .
					$test->testId . ", ADP Form ID: " . $testForm->formId .
					", ADP Revision ID : $testFormRevision->revisionId , Status: " .
					RequestQueue::ACR_STATUS_PUBLISHED . " End Time: $job->endTime");
			$this->updateStatusOnAcr(
					$externalId,
					$jobId,
					$test->testId,
					$testForm->formId,
					$testFormRevision->revisionId,
					RequestQueue::ACR_STATUS_PUBLISHED,
					$job->endTime);
			// remove test folder per Bojan's request.
			if (isset($this->jobFolder)) {
				self::rrmdir($this->jobFolder);
			}
		} catch (Exception $ex) {
			$errorMessage = $ex->getMessage();
			$this->logger->error($errorMessage);

			if ($ex instanceof \PDOException) {
				$errorMessage = 'An error occurred, please check the log file.';
			}

			try {
				$cleanUpResult = $this->cleanUpFiles($contents);
				if (isset($transactionManager) && $transactionManager instanceof TransactionManager) {
					$transactionManager->rollback();
				}
			} catch (Exception $ex) {
				$this->logger->warning("An error happened while reverting changes: (" . $ex->getMessage() . ")");
			}

			$job->status = RequestQueue::ACR_STATUS_FAILED;
			$job->save();

			$testBatteryId = ($test === false) ? null : $test->testId;
			$batteryFormId = ($testForm === false) ? null : $testForm->formId;
			$formRevisionId = ($testFormRevision === false) ? null : $testFormRevision->revisionId;

			$this->updateStatusOnAcr(
					$externalId,
					$jobId,
					$testBatteryId,
					$batteryFormId,
					$formRevisionId,
					RequestQueue::ACR_STATUS_FAILED,
					PublishHelper::getDbCurrentTimestamp($this->db2),
					$errorMessage
			);
		}
	}

	/**
	 * sendTestBatteryForm send the test battery form details to LDR.
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param Test $test   The test model from which details will be sent.
	 * @param TestForm $testForm   The test form model from which details will be sent.
	 * @param boolean $updateTestBatteryForm if set to true the call will use the PUT method to update.
	 * (by default POST method is used to create a new testBatteryForm.
	 * @return boolean
	 * @throws Exception
	 */
	public function sendTestBatteryForm(Test $test, TestForm $testForm, $updateTestBatteryForm = false)
	{

		try {
			// getting the HttpClient from the service.
			/* @var $httpClient \PARCC\ADP\Services\HttpClient */
			$httpClient = $this->httpClient;
			// get the ldr token
			$token = $this->getToken($this->tenant->tenantId);

			$this->logger->info("[PUBLISH : sendTestBatteryForm] Sending TestBatteryForm to LDR (testBatteryId:" .
					$test->testId . ", testBatteryFormName: " . $testForm->name .
					", testBatteryFormId: " . $testForm->formId . ")");

			$ldrUrl = $this->di['config']->ldr->{$this->tenant->tenantId}->host . '/test-battery-form';

			$headers = [
				'Token:' . $token
			];

			$data['testBatteryId'] = $test->testId;

			$data['testBatteryName'] = $test->name;
			$data['testBatterySubject'] = $test->subject;
			$data['testBatteryGrade'] = $test->grade;
			$data['testBatteryProgram'] = $test->program;
			$data['testBatterySecurity'] = $test->security;
			$data['testBatteryPermissions'] = $test->permissions;
			$data['testBatteryScoring'] = $test->scoring;
			$data['testBatteryDescription'] = $test->description;

			$data['testBatteryFormId'] = $testForm->formId;
			$data['testBatteryFormName'] = $testForm->name;

			$method = $httpClient::POST;
			if ($updateTestBatteryForm) {
				$method = $httpClient::PUT;
			}

			$res = $httpClient->request($ldrUrl, json_encode($data), $method, 'data', $headers);
			$this->logger->info("[sendTestBatteryFrom result] : " . json_encode($res));

			if (isset($res) && isset($res->result) && mb_strtolower($res->result) == 'success') {
				$this->logger->info("[sendTestBatteryFrom result] Update Status on LDR was Successful : " .
						json_encode($res));
				return true;
			} else {
				$this->logger->error("[sendTestBatteryFrom result] Update Status on LDR Failed details : " .
						json_encode($res));
				return false;
			}
		} catch (Exception $ex) {
			$this->logger->error($ex->getMessage());
			return false;
		}
	}

	/**
	 * processPracticeTest
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param $test
	 * @param $testForm
	 *
	 * @return bool|TestSession|Model
	 * @throws Exception
	 */
	private function processPracticeTest($test, $testForm)
	{
		$testSession = false;

		// Processing Practice Test!
		if ($test->program == 'Practice Test') {
			$this->logger->info("[processPracticeTest] Started]");

			$testSession = TestSession::findFirst([
					'conditions' => ' tenantId = ?1 AND parentTestFormId = ?2 ',
					'order' => ' createdDateTime DESC ',
					'bind' => [
							1 => $this->tenant->tenantId,
							2 => $testForm->formId
					]
			]);

			if ($testSession) {
				// if the Test Session exists for this Form Id use this one.
				$testSession->isActive = false;
				$testSession->save();
				$this->logger->info("[processPracticeTest] Deactivated existing test assignment.");
			} else {
				// if this is a first time creation, I create a new test session and set a PRACTICE Access Code
				// and set the anonymous student.
				$this->logger->info("[processPracticeTest] Creating a new Test Assignment.");

				$testSession = new TestSession();
				$testSession->tenantId = $this->tenant->tenantId;
				$testSession->parentStudentId = 0;
				$testSession->parentTestFormId = $testForm->formId;
				$testSession->isEnabledLineReader = 1;
				$testSession->isEnabledTextToSpeech = 1;
				$testSession->status = TestSession::STATUS_SCHEDULED;
				$testSession->isActive = false;

				$iteration = 0;
				$recordCreated = false;

				do {
					try {
						if (property_exists($this->config->app, 'practiceTestKeyPrefix')) {
							$testSession->testKey =
									$this->generateAccessCode($this->config->app->practiceTestKeyPrefix);
						} else {
							$this->logger->warning("Configuration entry is not set for
							test[practiceTestKeyPrefix]");
							$testSession->testKey = $this->generateAccessCode();
						}
						$recordCreated = $testSession->save();
					} catch (\PDOException $ex) {
						$this->logger->warning('Trying to generate a new unique Access Code: ' . $ex->getMessage());
					}
					$iteration++;
				} while ($recordCreated == false && $iteration < (int)$this->config->Login->tokenRetryCount);

				if ($iteration == $this->config->Login->tokenRetryCount) {
					$this->logger->error("Unable to find test session for practice test! (form id:
						{$testForm->formId})");
					throw new Exception("Unable to find test session for practice test!");
				}
				$this->logger->info("[processPracticeTest] Deactivated new test assignment.");
			}
		}
		$this->logger->info("[processPracticeTest] Ended");
		return $testSession;
	}

	/**
	 * generateAccessCode (aka Test Key)
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $prePendingString
	 * @param int $maxLengthPrePendingString
	 * @param int $accessCodeLength
	 * @return bool|string
	 */
	private function generateAccessCode($prePendingString = 'PRACT',
	                                    $maxLengthPrePendingString = 5,
	                                    $accessCodeLength = 10)
	{
		// Making sure the pre-pending string is all UpperCase.
		$prePendingString = strtoupper($prePendingString);
		// Getting the Length of the pre pending string.
		$prePendingStringLength = strlen($prePendingString);
		if ($prePendingStringLength > $maxLengthPrePendingString) {
			$this->logger->error("[generateAccessCode] Pre-pending String ($prePendingString) is longer than
			$maxLengthPrePendingString characters!");
			return false;
		}
		// Allowed Characters are A-Z
		$allowedCharacters = range('A', 'Z');
		$accessCode = "";
		// Construct the remaining Test Key.
		for ($i = 0; $i < ($accessCodeLength - $prePendingStringLength); $i++) {
			$accessCode .= $allowedCharacters[mt_rand(0, count($allowedCharacters) - 1)];
		}
		// Return the Concatenation of the pre-pending string and the generated Test Key.
		return $prePendingString . $accessCode;
	}

	/**
	 * getToken
	 *
 	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param  integer $tenantId
	 * @return string/false returns the token if successful, false otherwise.
	 * @throws Exception
	 */
	private function getToken($tenantId)
	{
		// getting the HttpClient from the service.
		/* @var $httpClient \PARCC\ADP\Services\HttpClient */
		$httpClient = $this->httpClient;

		// initializing the token
		$token = null;

		if (extension_loaded('apc')) { // Check if Apc extension is enabled and use it.
			$token = $this->apcCache->get("ldr-token-$tenantId",
					$this->di['config']->ldr->{$this->tenant->tenantId}->tokenLifetime);
		} else {
			$this->logger->alert("Apc extension is not loaded. Please make sure to enable Apc extension.");
		}

		if ($token === null) {

			$ldrUrl = $this->di['config']->ldr->{$this->tenant->tenantId}->host . '/session-token';

			$result = $httpClient->request($ldrUrl, json_encode(
					[
						"clientName" => $this->di['config']->ldr->{$this->tenant->tenantId}->username,
						"password" => $this->di['config']->ldr->{$this->tenant->tenantId}->password
					]));

			if (isset($result->token)) {
				$token = $result->token;
				if (extension_loaded('apc')) {
					$this->apcCache->save(
							"ldr-token-$tenantId",
							$token,
							$this->di['config']->ldr->{$this->tenant->tenantId}->tokenLifetime
					);
					$this->logger->info("[LDR session-token] Caching Token : $token");
				}
			} else {
				throw new Exception("Unable to get Session Token from LDR. (error details : " . json_encode($result));
			}
		}

		return $token;
	}

	/**
	 * sendTestBattery  uses PUT method to update Test Battery Properties in the LDR side.
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $data
	 * @return boolean
	 */
	public function sendTestBattery($data)
	{
		try {
			/* @var $httpClient \PARCC\ADP\Services\HttpClient */
			$httpClient = $this->httpClient;
			// get the ldr token
			$token = $this->getToken($this->tenant->tenantId);
			$this->logger->info("[sendTestBattery] Sending TestBattery Properties to LDR (" . json_encode($data) . ")");

			$ldrUrl = $this->di['config']->ldr->{$this->tenant->tenantId}->host . '/test-battery';

			$headers =[
				'Token:' . $token
			];

			$res = $httpClient->request($ldrUrl, json_encode($data), $httpClient::PUT, 'data', $headers);

			$this->logger->info("[sendTestBattery result] : " . json_encode($res));

			if (isset($res) && isset($res->result) && mb_strtolower($res->result) == 'success') {
				$this->logger->info("[sendTestBattery result] Update Status on LDR was Successful : " .
						json_encode($res));
				return true;
			} else {
				$this->logger->error("[sendTestBattery result] Update Status on LDR Failed details : " .
						json_encode($res));
				return false;
			}
		} catch (Exception $ex) {
			$this->logger->error($ex->getMessage());
			return false;
		}
	}

	/**
	 * cleanUpFiles
	 *
	 * @param $contents
	 * @param bool|false $cleanUpAwsFilesOnly
	 * @return bool
	 */
	public function cleanUpFiles($contents, $cleanUpAwsFilesOnly = false)
	{
		try {
			$revisionId = null;
			$contentBucket = $this->di['config']->s3->contentBucket;

			if (!empty($contents)) {
				foreach ($contents as $content) {
					if (is_null($revisionId)) {
						$revisionId = $content->parentTestFormRevisionId;
					}
					// remove from AWS !
					$key = $this->di['config']->s3->contentPrefix . '/' . $content->parentTestFormRevisionId .
							'/' . $content->path;

					try {
						$this->s3->deleteObject($contentBucket, $key);
					} catch (S3Exception $ex) {
						$this->logger->error("[AWS] Unable to delete (" . $key . ")");
					}

					// try to delete .gz version of the file either it exists or not.
					try {
						$this->s3->deleteObject($contentBucket, $key . '.gz');
					} catch (S3Exception $ex) {
						$this->logger->debug("[AWS] Unable to delete (" . $key .  ".gz)");
					}
				}
			}

			// No need to remove the zip package as it would be overridden in the republishing process.
			if ($cleanUpAwsFilesOnly) {
				return true;
			}

			/**
			 * Delete the test zipPackage from the AWS S3 PACKAGES Bucket.
			 */
			if (!is_null($revisionId)) {
				$contentArchiveKey = $this->di['config']->s3->contentArchivePrefix . '/' . $revisionId . '/' .
						preg_replace("/.*(i[0-9]{12,25}.[zZ][iI][pP])$/", "$1", $this->zipPackage);
				$contentArchiveBucket = $this->di['config']->s3->contentArchiveBucket;

				try {
					$this->s3->deleteObject($contentArchiveBucket, $contentArchiveKey);
					$this->logger->info("Test zip package ($contentArchiveKey) deleted successfully " .
							"from Packages bucket (" . $contentArchiveBucket . ") AWS S3.");
				} catch (S3Exception $ex) {
					$this->logger->error("Unable to delete zip package ($contentArchiveKey) from Packages bucket " .
							"(" . $contentArchiveBucket . ") AWS S3 server.");
				}
			} else {
				$this->logger->warning("Unable to retrieve the Revision ID, skipping archive removal.");
			}

			// remove created files.
			if (isset($this->zipPackage)) {
				unlink($this->zipPackage);
			}
			// remove unpacked zip package.
			if (isset($this->unpackedPackagePath)) {
				self::rrmdir($this->unpackedPackagePath);
			}
			// remove test folder.
			if (isset($this->jobFolder)) {
				self::rrmdir($this->jobFolder);
			}
			return true;
		} catch (Exception $ex) {
			$this->logger->error("An error happened while cleaning up files (" . $ex->getMessage() . ")");
			return false;
		}
	}

	/**
	 * rrmdir delete folders and contents recursively
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $dir folder path.
	 * @return boolean
	 */
	static public function rrmdir($dir)
	{
		$result = true;
		foreach (glob($dir . '/*') as $file) {
			if (is_dir($file)) {
				$result &= self::rrmdir($file);
			} else {
				unlink($file);
			}
		}

		$result &= @rmdir($dir);
		return (boolean) $result;
	}

	/**
	 * processContents
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param	string $unpackedPackagePath path to the unpacked test package.
	 * @param   int $testBatteryFormRevisionId.
	 * @param   string $zipPackage path to the zip package.
	 * @param   array $pubManifestArray array that contains a list of files to be checked.
	 *
	 * @return  array $contents array of TestContent
	 * @throws Exception
	 */
	public function processContents($unpackedPackagePath,
	                                $testBatteryFormRevisionId,
	                                $zipPackage,
	                                $pubManifestArray = [])
	{
		$contentBucket = $this->di['config']->s3->contentBucket;

		$objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($unpackedPackagePath),
				\RecursiveIteratorIterator::SELF_FIRST);
		// excludedList is an array that has a list
		$excludedList = ['.', '..', 'metadata.json'];
		$contents = [];

		if (!extension_loaded('fileinfo')) {
			$this->logger->error("fileinfo extension not loaded.");
			throw new Exception("fileinfo extension not loaded.");
		}

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0';
		$headers['Expires'] = '0';
		$pubManifestFilesToBeChecked = [];
		foreach ($objects as $name => $object) {

			if (is_file($name) && (!in_array($object->getFileName(), $excludedList))) {

				// make sure that all files that will be uploaded and processed
				// are found in test.json pub_resourceManifest
				// Check both ways
				// 1 way (early check)
				if ($object->getFileName() != 'test.json') {

					$pubManifestFilesToBeChecked[] = $this->di['config']->Test->resourcePrefix . $object->getFileName();

					if (!in_array($this->di['config']->Test->resourcePrefix .
							$object->getFileName(), $pubManifestArray)
					) {
						$this->logger->error(" {$object->getFileName()} " .
								"Not found in pub_Manifest array for revisionId : $testBatteryFormRevisionId");

						if ($this->di['config']->Test->strictFileManifestCheck) {
							throw new Exception(" {$object->getFileName()} Not found in pub_Manifest.");
						}
					}
				}

				$key = $this->di['config']->s3->contentPrefix . '/' . $testBatteryFormRevisionId . '/'
						. $object->getFileName();
				$content = new TestContent();
				$content->parentTestFormRevisionId = $testBatteryFormRevisionId;

				$this->logger->debug("AWS Upload of ($key)");
				if (preg_match('/^.*\.json$/', mb_strtolower($name))) {
					$contentType = 'application/json';
				} elseif (preg_match('/^.*\.css$/', mb_strtolower($name))) {
					$contentType = 'text/css';
				} elseif (preg_match('/^.*\.js$/', mb_strtolower($name))) {
					$contentType = 'text/javascript';
				} elseif (preg_match('/^.*\.woff$/', mb_strtolower($name))) {
					$contentType = 'application/x-font-woff';
				} elseif (preg_match('/^.*\.otf$/', mb_strtolower($name))) {
					$contentType = 'application/x-font-otf';
				} elseif (preg_match('/^.*\.ttf$/', mb_strtolower($name))) {
					$contentType = 'application/x-font-ttf';
				} elseif (preg_match('/^.*\.eot$/', mb_strtolower($name))) {
					$contentType = 'application/vnd.ms-fontobject';
				} elseif (preg_match('/^.*\.ico$/', mb_strtolower($name))) {
					$contentType = 'image/x-icon';
				} elseif (preg_match('/^.*\.(htm|html)$/', mb_strtolower($name))) {
					$contentType = 'text/html';
				} elseif (preg_match('/^.*\.xhtml$/', mb_strtolower($name))) {
					$contentType = 'application/xhtml+xml';
				} elseif (preg_match('/^.*\.txt$/', mb_strtolower($name))) {
					$contentType = 'text/plain';
				} elseif (preg_match('/^.*\.xml$/', mb_strtolower($name))) {
					$contentType = 'text/xml';
				} elseif (preg_match('/^.*\.(svg|svgz)$/', mb_strtolower($name))) {
					$contentType = 'image/svg+xml';
				} else {
					$contentType = finfo_file($finfo, $name);
				}

				$headers['Content-Type'] = $contentType;

				try {
					$this->s3->uploadObjectFile($name,
							$contentBucket,
							$key,
							(preg_match('/^.*(svgz)$/', mb_strtolower($name)) ? ($headers +
									['Content-Encoding' => 'gzip']) : ($headers))
					);
					if (function_exists('gzencode')) {
						if (preg_match('/^.*(json|css|js|htm|html|xhtml|xml|txt|ttf|otf|eot|woff|svg|ico)$/',
								mb_strtolower($name)))
						{
							$temp = tempnam(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data', 'gz_');
							if ($temp == false) {
								$this->logger->error("Unable to get a tmp file to gzip ($name).");
								throw new Exception("Unable to get a tmp file.");
							}

							$temp = $this->gzipFile($name,
									$temp,
									$this->config->Test->gzipChunkSize,
									$this->config->Test->gzipCompressionLevel);
							if ($temp == false) {
								$this->logger->error("Unable to gzip the file ($name).");
								throw new Exception("Unable to gzip the file.");
							}

							try {
								$this->s3->uploadObjectFile($temp,
										$contentBucket,
										$key . '.gz',
										$headers + ['Content-Encoding' => 'gzip']);
							} catch (S3Exception $ex) {
								$this->logger->warning("Unable to upload gzipped version of ($name) to AWS S3 server.");
							}
							unlink($temp);
						}
					} else {
						$this->logger->warning('function gzencode does not exist.');
					}
					$content->path = $object->getFileName();
					$content->tenantId = $this->tenant->tenantId;
					$content->createdByUserId = $this->user->userId;
					$content->isActive = false;
					if ($object->getFileName() == 'test.json') {
						$content->isDefault = 1;
					} else {
						$content->isDefault = 0;
					}
				} catch (S3Exception $ex) {
					throw new Exception("Unable to upload ($name) to AWS S3 server.");
				}

				$contents[] = $content;
				$this->logger->info("File ($name) copied successfully to destinations.");
			} else {
				$this->logger->debug("Unsatisfied condition for $name (No upload to AWS)");
			}
		}
		finfo_close($finfo);

		// Checking and making sure files in the package matches what is in the pub manifest array in test.json
		// way 2
		if (count(array_diff($pubManifestArray, $pubManifestFilesToBeChecked)) != 0) {
			$this->logger->error("pub_Manifest properties and files found in the package are not matching for
			revisionId :
			$testBatteryFormRevisionId ,  Details: (test.json pubManifest:" . json_encode($pubManifestArray) .
			", files found in the package: " . json_encode($pubManifestFilesToBeChecked) . ")");

			if ($this->di['config']->Test->strictFileManifestCheck) {
				throw new Exception(" pub_Manifest properties and files found in the package are not matching!");
			}
		}

		// Uploading the zip package to AWS S3  Content Archive bucket.
		$contentArchiveKey = preg_replace("/.*(i[0-9]{12,25}.[zZ][iI][pP])$/", "$1", $zipPackage);
		$contentArchiveBucket = $this->di['config']->s3->contentArchiveBucket;

		try {
			$this->s3->uploadObjectFile($zipPackage,
					$contentArchiveBucket,
					$this->di['config']->s3->contentArchivePrefix . '/' . $testBatteryFormRevisionId . '/' .
					$contentArchiveKey);
			$this->logger->info("Test zip package ($zipPackage) uploaded successfully to AWS S3 Packages bucket (" .
					$contentArchiveBucket . ")");
		} catch (S3Exception $ex) {
			$this->logger->error("Unable to upload zip package ($zipPackage) to AWS S3 server.");
			throw new Exception("Unable to upload zip package ($zipPackage) to AWS S3 server.");
		}

		return $contents;
	}

	/**
	 * parseMetadata
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $unpackedPackagePath
	 * @param string $testFormRevisionId
	 * @param string $publishedBy
	 * @param \Phalcon\Mvc\Model\TransactionInterface &$transaction
	 * @param integer/null $adpTestFormRevisionId
	 * @return mixed array|bool
	 * @throws Exception
	 * @throws HttpException
	 */
	public function parseMetadata($unpackedPackagePath, $testFormRevisionId, $publishedBy, &$transaction,
	                              $adpTestFormRevisionId = null)
	{

		// flag to be used to detect if we are doing a create (POST) or an update (PUT) call,  to send the test
		// battery form.
		$updateTestBatteryForm = false;

		$metadatafile = $unpackedPackagePath . DIRECTORY_SEPARATOR . 'metadata.json';

		if (!file_exists($metadatafile)) {
			$this->logger->error("Unable to find metadatafile for testFormRevisionId: $testFormRevisionId");
			throw new Exception("Unable to find metadatafile for testFormRevisionId: $testFormRevisionId");
		}

		if (!is_readable($metadatafile)) {
			$this->logger->error("Unable to read metadatafile for testFormRevisionId: $testFormRevisionId");
			throw new Exception("Unable to read metadatafile for testFormRevisionId: $testFormRevisionId");
		}

		$metaDataContent = file_get_contents($metadatafile);
		$metadataArray = json_decode($metaDataContent, true);

		if (!is_object($metadataArray) && !is_array($metadataArray)) {
			$this->logger->error("metadata.json not found or is invalid.");
			throw new Exception("testSchema.json not found or is invalid.");
		}

		try {
			// Key Validation
			// Making sure Battery indexes exist.
			$test = new Test();
			$batteryIndexes = ["id", "name", "program", "grade", "itemSelectionAlgorithm", "security", "scoreReport",
					"multimedia", "scoring", "permissions", "privacy", "description"];
			foreach ($batteryIndexes as $index) {
				if (!isset($metadataArray["battery"][$index])) {
					$transaction->rollback("Battery $index not found.");
				}

				if (in_array($index, ["subject", "program", "grade", "itemSelectionAlgorithm", "security",
						"scoreReport", "multimedia", "scoring", "permissions", "privacy"])) {
					if (!in_array($metadataArray["battery"][$index], $test->getFieldOptions($index))) {
						$transaction->rollback("Battery Form $index value ({$metadataArray["battery"][$index]}) not
						authorized!");
					}
				}
			}

			// Making sure form indexes exist.
			$formIndexes = ["id", "name", "testName", "testId"];
			foreach ($formIndexes as $index) {
				if (!isset($metadataArray["form"][$index])) {
					$transaction->rollback("Battery Form $index not found.");
				}
			}

			// Making sure revision indexes exist.
			$revisionIndexes = ["id", "version", "compiledBy", "compiledDateTime"];
			foreach ($revisionIndexes as $index) {
				if (!isset($metadataArray["revision"][$index])) {
					$transaction->rollback("Battery Form Revision $index not found.");
				}
			}

			// Check the pattern for the ids.
			foreach ([ $metadataArray["battery"]["id"], $metadataArray["form"]["testId"],
					         $metadataArray["form"]["id"],  $metadataArray["revision"]["id"]] as $value) {
				if (!preg_match('/^i[0-9]{12,25}$/', $value)) {
					$transaction->rollback("'$value' does not match the ID format.");
				}
			}

			$batteryValidation = new \Phalcon\Validation();

			$batteryValidation->add('description', new \Phalcon\Validation\Validator\StringLength(
				[
				"messageMaximum" => 'Description should not exceed 4096 characters',
				"max" => 4096
			]));

			/** should we allow empty names ? * */
			$batteryValidation->add('name', new \Phalcon\Validation\Validator\StringLength(
				[
				"messageMaximum" => "Test Battery's name should not exceed 100 characters",
				"max" => 100,
				"messageMinimum" => "Test Battery's name should at least be 1 character",
				"min" => 1,
			]));

			$errors = $batteryValidation->validate($metadataArray["battery"]);
			if (count($errors)) {
				$this->logger->error("Battery validation failed: " . json_encode($errors));
				$transaction->rollback("Battery name or description does not meet the validation requirements.");
			}

			// The below function checks if the tenantId, name, grade and subject already exist and throws an
			// exception if that is the case.
			$this->checkIfNameGradeSubjectExists(
					$metadataArray["battery"]["name"],
					$metadataArray["battery"]["grade"],
					$metadataArray["battery"]["subject"],
					$metadataArray["battery"]["id"]
			);

			$formValidation = new \Phalcon\Validation();
			/** should we allow empty names ? * */
			$formValidation->add('name', new \Phalcon\Validation\Validator\StringLength(
				[
				"messageMaximum" => 'Test Battery Form name should not exceed 100 characters',
				"max" => 20,
				"messageMinimum" => 'Test Battery Form name should at least be 1 character',
				"min" => 1,
			]));
			/** should we allow empty names ? * */
			$formValidation->add('testName', new \Phalcon\Validation\Validator\StringLength(
				[
				"messageMaximum" => 'Form Test Name should not exceed 100 characters',
				"max" => 100,
				"messageMinimum" => 'Form Test Name should at least be 1 character',
				"min" => 1,
			]));

			$errors = $formValidation->validate($metadataArray["form"]);
			if (count($errors)) {
				$this->logger->error("Form validation failed: " . json_encode($errors));
				$transaction->rollback("Form name or testName does not meet the validation requirements.");
			}

			if (!ctype_digit($metadataArray['revision']['version'])) {
				$transaction->rollback("revision version should be a numeric.");
			}
			if (!ctype_digit($metadataArray['revision']['compiledDateTime'])) {
				$transaction->rollback("revision compiledDateTime should be numeric.");
			}

			$revisionValidation = new \Phalcon\Validation();
			$revisionValidation->add('compiledBy', new \Phalcon\Validation\Validator\StringLength(
				[
				"messageMaximum" => 'compiledBy should not exceed 100 characters',
				"max" => 100,
				"messageMinimum" => 'compiledBy should at least be 1 character',
				"min" => 1,
			]));

			$errors = $revisionValidation->validate($metadataArray['revision']);
			if (count($errors)) {
				$this->logger->error("Revision validation failed: " . json_encode($errors));
				$transaction->rollback("compiledBy does not meet the validation requirements.");
			}

			//	check if test exist, if it does, we re-use it.
			$test = Test::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND externalTestId = ?2 AND createdByUserId = ?3',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $metadataArray["battery"]["id"],
						3 => $this->user->userId
					]
			]);

			if ($test && $test->count()) {
				$updateTestBatteryForm = true;
				$this->logger->info("Reusing existing Test: " . json_encode($test->dump()));
				$test->setTransaction($transaction);
			} else {
				$test = new Test();
				$test->setTransaction($transaction);
				$test->tenantId = $this->tenant->tenantId;
				$test->program = $metadataArray["battery"]["program"]; // This property should not be updated
				$test->isActive = false;
			}

			$test->createdByUserId = $this->user->userId;
			$test->createdDateTime = new RawValue('UTC_TIMESTAMP()');

			$test->externalTestId = $metadataArray["battery"]["id"];
			$test->grade = $metadataArray["battery"]["grade"];
			$test->itemSelectionAlgorithm = $metadataArray["battery"]["itemSelectionAlgorithm"];
			$test->scoreReport = $metadataArray["battery"]["scoreReport"];
			$test->multimedia = $metadataArray["battery"]["multimedia"];
			$test->name = $metadataArray["battery"]["name"];
			$test->permissions = $metadataArray["battery"]["permissions"];
			$test->privacy = $metadataArray["battery"]["privacy"];

			$test->security = $metadataArray["battery"]["security"];
			$test->scoring = $metadataArray["battery"]["scoring"];
			$test->subject = $metadataArray["battery"]["subject"];
			$test->description = $metadataArray["battery"]["description"];

			if ($test->save() == false) {
				$transaction->rollback("Can't save Test :" . json_encode($test->getMessages()));
			}

			$this->logger->info("Creating a new test: " . json_encode($test->dump()));

			// check form uniqueness
			$testForm = TestForm::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND parentTestId = ?2 AND name = ?3 AND externalFormId != ?4 ',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $test->testId,
						3 => $metadataArray["form"]["name"],
						4 => $metadataArray["form"]["id"]
					]
			]);

			if ($testForm && $testForm->count()) {
				throw new HttpException('ADP-1709',
						403,
						'Access Denied.',
						'Form name already exist for the actual battery. (Battery Id :' .
						$metadataArray["battery"]["id"] . ' | Form Name: ' . $metadataArray["form"]["name"] . ')');
			}

			// check if form exist, if it does, we re-use it.
			$testForm = TestForm::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND externalFormId = ?2 AND createdByUserId = ?3',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $metadataArray["form"]["id"],
						3 => $this->user->userId
					]
			]);

			if ($testForm && $testForm->count()) {
				$this->logger->info("Reusing existing Test Form : " . json_encode($testForm->dump()));
				$testForm->setTransaction($transaction);
			} else {
				$updateTestBatteryForm = false;
				$testForm = new TestForm();
				$testForm->setTransaction($transaction);
				$testForm->isActive = false;
				$testForm->tenantId = $this->tenant->tenantId;
			}

			// form details
			$testForm->createdDateTime = new RawValue('UTC_TIMESTAMP()');

			$testForm->createdByUserId = $this->user->userId;
			$testForm->parentTestId = $test->testId;
			$testForm->name = $metadataArray["form"]["name"];
			$testForm->formTestName = $metadataArray["form"]["testName"];
			$testForm->externalFormId = $metadataArray["form"]["id"];
			$testForm->externalFormTestId = $metadataArray["form"]["testId"];

			if ($testForm->save() == false) {
				$transaction->rollback("Can't save Test Form :" . json_encode($testForm->getMessages()));
			}

			// check if testFormRevision exist (created by the same user, then rollback and cancel everything!
			$testFormRevision = TestFormRevision::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND externalRevisionId = ?2 AND createdByUserId = ?3',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $metadataArray["revision"]["id"],
						3 => $this->user->userId
					]
			]);

			if ($testFormRevision && $testFormRevision->count() && is_null($adpTestFormRevisionId)) {
				$this->logger->info("Revision already published from the same source: " .
						json_encode($testFormRevision->dump()));
				$transaction->rollback('Revision already published from the same source.');
			}

			if ($testFormRevision && $testFormRevision->count()) {

//				// if adpTestFormRevisionId does not match the revisionId then we stop processing
// and we return an error.
//				if ($testFormRevision->revisionId != $adpTestFormRevisionId) {
//					$this->logger->error("Revision Id ({$testFormRevision->revisionId})
// Does not match adpTestFormRevisionId ({$adpTestFormRevisionId})");
//					$transaction->rollback('Revision found does not match received argument.');
//				}

				$this->logger->info("Republishing & Reusing existing Test Form revision : " .
						json_encode($testFormRevision->dump()));
			} else {
				// revision details
				$testFormRevision = new TestFormRevision();
				$testFormRevision->tenantId = $this->tenant->tenantId;
			}

			$testFormRevision->setTransaction($transaction);
			$testFormRevision->createdByUserId = $this->user->userId;
			$testFormRevision->parentTestFormId = $testForm->formId;
			$testFormRevision->isActive = false;

			$testFormRevision->externalRevisionId = $metadataArray["revision"]["id"];
			$testFormRevision->compiledBy = $metadataArray["revision"]["compiledBy"];
			$testFormRevision->compiledDateTime = date('Y-m-d H:i:s', $metadataArray["revision"]["compiledDateTime"]);
			$testFormRevision->version = $metadataArray["revision"]["version"];

			$testFormRevision->publishedDateTime = new RawValue('UTC_TIMESTAMP()');
			$testFormRevision->publishedBy = $publishedBy;

			$this->logger->info("Test Form Revision : " . json_encode($testFormRevision->toArray()));

			if ($testFormRevision->save() == false) {
				$transaction->rollback("Can't save Test Form Revision :" . json_encode($testFormRevision->getMessages
						()));
			}

			return ['test' => $test, 'testForm' => $testForm, 'testFormRevision' => $testFormRevision,
					'updateTestBatteryForm' => ($updateTestBatteryForm || $adpTestFormRevisionId)];
		} catch (Phalcon\Mvc\Model\Transaction\Failed $e) {
			$this->logger->error('Failed to save Test, reason: ', $e->getMessage());
			return false;
		}
	}

	/**
	 * unpackPackage
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string		$zipPackagePath  Path of the zip package.
	 * @param string		$destinationPath		 the revision id of the package (will be used
	 *                                               to create a folder where to extract the package).
	 *
	 * @return string		path where package will be extracted.
	 * @throws Exception	in case of error while extracting the zip package.
	 */
	public function unpackPackage($zipPackagePath, $destinationPath)
	{
		$zip = new \ZipArchive;
		$return = $zip->open($zipPackagePath);
		if ($return === TRUE) {
			if (!file_exists($destinationPath)) {
				mkdir($destinationPath, 0777);
			}
			$zip->extractTo($destinationPath);
			$zip->close();
		} else {
			$this->logger->error("Unable to open the test package for testFormRevisionId : $destinationPath !" .
					" (return code: $return)");
			throw new Exception("Unable to open the test package for testFormRevisionId : $destinationPath ! " .
					"(return code: $return)");
		}
		return $destinationPath;
	}

	/**
	 * getHmacSignature
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $apiUrl
	 * @param string $user
	 * @param string $password
	 * @param string $secret
	 * @return string
	 */
	public function getHmacSignature($apiUrl, $user, $password, $secret)
	{
		$timestamp = time();
		$nonce = rand(0, 99999);
		$hash = (string)$apiUrl . round(($timestamp / 1000)) . $nonce;

		$signature = hash_hmac('sha256', $hash, $secret);
		$this->logger->debug("Secret : $secret | ApiUrl : $apiUrl | Timestamp : $timestamp | " .
				"Nonce : $nonce | Hash : $hash | Signature : $signature");

		$signature = 'Authentication: ' . base64_encode($timestamp . ':' . $nonce . ':' . base64_encode($signature));
		$this->logger->debug("Signature:" . $signature);
		return $signature;
	}

	/**
	 * getPackage
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param string $testFormRevisionId
	 * @param integer $jobId
	 * @param string $destinationPath
	 * @return Object
	 * @throws Exception
	 */
	public function getPackage($testFormRevisionId, $jobId, $destinationPath)
	{
		if (!property_exists($this->di['config']->acr, $this->user->userId)) {
			$this->logger->error("[getPackage] Configuration entry is missing for ACR user ({$this->user->userId})");
			throw new Exception("Configuration entry is missing for ACR user");
		}

		if (!file_exists($destinationPath)) {
			mkdir($destinationPath, 0777, true);
		}

		$data['testFormRevisionId'] = $testFormRevisionId;
		$data['adpPublisherRequestId'] = $jobId;

		$this->logger->debug("[OUTGOING DOWNLOAD PACKAGE REQUEST] " . json_encode($data));

		$fileName = $destinationPath . "$testFormRevisionId.zip";

		if (file_exists($fileName)) {
			$this->logger->error("[getPackage] File ($fileName) already exist!");
			throw new Exception("File ($fileName) already exist!");
		}

		$url = $this->di['config']->acr->{$this->user->userId}->host . '/taoTestBatteries/Publish/downloadPackage';

		$headers = [
			'Authorization: Basic ' . base64_encode($this->di['config']->acr->{$this->user->userId}->username . ":" .
					$this->di['config']->acr->{$this->user->userId}->password),
			'Content-Type:application/x-www-form-urlencoded',
			$this->getHmacSignature('/taoTestBatteries/Publish/downloadPackage',
					$this->di['config']->acr->{$this->user->userId}->username,
					$this->di['config']->acr->{$this->user->userId}->password,
					$this->di['config']->acr->{$this->user->userId}->secret)
		];

		$fileConfig = ['filePath' => $fileName, 'overwrite' => false];

		/* @var $httpClient \PARCC\ADP\Services\HttpClient */
		$httpClient = $this->httpClient;
		return $httpClient->request($url, json_encode($data), $httpClient::POST, 'data', $headers, $fileConfig);
	}

	/**
	 * updateStatusOnAcr
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $testFormRevisionId ACR id.
	 * @param integer $jobId
	 * @param $testBatteryId
	 * @param $testBatteryFormId
	 * @param integer $testBatteryFormRevisionId testFormRevisionId ADP id.
	 * @param string $status
	 * @param integer $jobEndTime
	 * @param string $errorDescription
	 * @return bool
	 * @throws Exception
	 */
	public function updateStatusOnAcr($testFormRevisionId,
	                                  $jobId,
	                                  $testBatteryId,
	                                  $testBatteryFormId,
	                                  $testBatteryFormRevisionId,
	                                  $status,
	                                  $jobEndTime = NULL,
	                                  $errorDescription = NULL)
	{
		try {
			$data['testFormRevisionId'] = $testFormRevisionId;
			$data['adpPublisherRequestId'] = $jobId;
			$data['adpTestBatteryId'] = $testBatteryId;
			$data['adpFormId'] = $testBatteryFormId;
			$data['adpFormRevisionId'] = $testBatteryFormRevisionId;
			$data['publishedTimestamp'] = $jobEndTime;
			$data['status'] = $status;
			if (!empty($errorDescription)) {
				$data['errorDescription'] = $errorDescription;
			}

			if (!property_exists($this->di['config']->acr, $this->user->userId)) {
				$this->logger->error("[getPackage] Configuration entry is missing for ACR user ({$this->user->userId})");
				throw new Exception("Configuration entry is missing for ACR user");
			}

			$this->logger->debug("[OUTGOING CONFIRMATION REQUEST] " . json_encode($data));

			$url = $this->di['config']->acr->{$this->user->userId}->host . '/taoTestBatteries/Publish/confirm';

			$headers = [
				'Authorization: Basic ' . base64_encode($this->di['config']->acr->{$this->user->userId}->username .
						":" . $this->di['config']->acr->{$this->user->userId}->password),
				'Content-Type:application/x-www-form-urlencoded',
				$this->getHmacSignature('/taoTestBatteries/Publish/confirm',
						$this->di['config']->acr->{$this->user->userId}->username,
						$this->di['config']->acr->{$this->user->userId}->password,
						$this->di['config']->acr->{$this->user->userId}->secret)
			];

			/* @var $httpClient \PARCC\ADP\Services\HttpClient */
			$httpClient = $this->httpClient;

			$res = $httpClient->request($url, json_encode($data), $httpClient::POST, 'data', $headers);

			if (isset($res) && isset($res->success) && $res->success == 'true') {
				$this->logger->info("Update Status on [$url] was Successful: " . json_encode($res));
				return true;
			} else {
				$this->logger->info("Update Status on [$url] Failed details : " . json_encode($res));
				return false;
			}
		} catch (Exception $ex) {
			$this->logger->error($ex->getMessage());
			return false;
		}
	}

	/**
	 * PUT request handler.
	 * This request is handling the test re-publishing
	 *
	 * @see http://docs.adp.parcc-ads.breaktech.org/#api-ADP_Publisher-Test_Re_Publishing
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param $adpTestFormRevisionId
	 * @throws Exception
	 * @throws HttpException
	 * @return array Response contains all requested records.
	 */
	public function put($adpTestFormRevisionId)
	{
		$this->logger->info("[TEST RE-PUBLISHING : Re-publishing Test " .
				"(adpTestFormRevisionId: $adpTestFormRevisionId)]");
		$this->publish($adpTestFormRevisionId);
	}

	/**
	 * PATCH request handler.
	 * This request is handling the activation/deactivation of a form revision
	 *
	 * @see http://docs.adp.parcc-ads.breaktech.org/#api-ADP_Publisher-Test_Activate_Deactivate
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param integer $adpTestFormRevisionId
	 * @throws Exception
	 * @throws HttpException
	 *
	 */
	public function testActivation($adpTestFormRevisionId)
	{
		$errors = [];
		$validBooleanValues = ['true', 'false', 'TRUE', 'FALSE', true, false, 0, 1];

		$this->logger->info('[TEST PATCH : Activating/Deactivating a Test.]');
		try {

			// Validating JSON
			$requestBody = $this->request->getJsonRawBody(true);

			$validation = new \Phalcon\Validation();
			$validation->add('testFormRevisionId', new \Phalcon\Validation\Validator\PresenceOf(array(
				'message' => 'testFormRevisionId is missing!'
			)));

			$validation->add('testFormRevisionId', new \Phalcon\Validation\Validator\Regex(array(
				'pattern' => '/^i[0-9]+$/',
				'message' => 'The revision id format is invalid'
			)));

			$validation->add('publishedBy', new \Phalcon\Validation\Validator\Regex(array(
				'pattern' => '/^[A-Za-z0-9- \.]{1,45}$/',
				'message' => 'The publishedBy format is invalid'
			)));

			$errors = $validation->validate($requestBody);

			if (!isset($requestBody['active']) || !in_array($requestBody['active'], $validBooleanValues, true)) {
				$errors[] = 'active index not set or has an invalid value (' . $requestBody['active'] . ')';
			}
			$requestBody['active'] = filter_var($requestBody['active'], FILTER_VALIDATE_BOOLEAN);

			if (!preg_match("/^[0-9]+$/", $adpTestFormRevisionId) &&
					$adpTestFormRevisionId > 0 &&
					$adpTestFormRevisionId <= 4294967295) {
				$errors[] = 'adpTestFormRevisionId  is not a valid unsigned integer';
			}

			if (count($errors)) {
				foreach ($errors as $error) {
					$this->logger->error($error);
				}
				throw new HttpException(
						'ADP-1753',
						403,
						'Access Denied.',
						'There were validation errors.');
			}

			$this->logger->debug("[Publish/Response]" . $this->response->getContent());
			$globalRequest = [
					'adpTestFormRevisionId' => $adpTestFormRevisionId,
					'requestBody' => ($requestBody)
			];
			$result = RequestQueue::scheduleAJob(
					$requestBody,
					$adpTestFormRevisionId,
					$globalRequest,
					RequestQueue::SOURCE_ACR,
					RequestQueue::TYPE_TEST_PACKAGE,
					$this
			);

			$job = $result['job'];
			if (isset($result['exist'])) {
				return $this->respond([
					'requestId' => $job->rqid,
					'publishedTimestamp' => $job->createdDateTime,
					'status' => RequestQueue::ACR_STATUS_SCHEDULED
					]
				);
			}

			$testFormRevision = TestFormRevision::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId =  ?1 AND revisionId = ?2 AND  externalRevisionId = ?3 AND
					createdByUserId = ?4',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $adpTestFormRevisionId,
						3 => $requestBody['testFormRevisionId'],
						4 => $this->user->userId
					]
				]
			);

			if (!$testFormRevision) {
				throw new HttpException(
						'ADP-1756',
						404,
						'Data Not Found.',
						"Unable to find testFormRevision (testFormRevisionId:" .
						" {$requestBody['testFormRevisionId']}, adpTestFormRevisionId: $adpTestFormRevisionId)");
			}

			$isActive = (($requestBody['active'] === 'false' ||
					$requestBody['active'] === false ||
					$requestBody['active'] === 0) ? false : true);

			if ($testFormRevision->isActive != $isActive) {
				$testFormRevision->updatedByUserId = $this->user->userId;
				$testFormRevision->isActive = $isActive;

				if (!$testFormRevision->save()) {
					$this->logger->error("Failed to save testFormRevision.");
					throw new Exception("Failed to save testFormRevision.");
				}
				$this->logger->info("testFormRevision active flag is now set to " . $requestBody['active']);
			} else {
				$this->logger->info("testFormRevision active flag already set to " . $requestBody['active']);
			}
			/**
			 * END ACTIVATING/DEACTIVATING PROCESSING
			 */
			$response = [
				'requestId' => $job->rqid,
				'publishedTimestamp' => $job->createdDateTime,
				'status' => RequestQueue::ACR_STATUS_SCHEDULED
			];

			// setting the job end time.
			$job->endTime = PublishHelper::getDbCurrentTimestamp($this->db);
			$job->status = RequestQueue::STATUS_COMPLETED;
			if ($job->save() == false) {
				$this->logger->error("Failed to save job details.");
				throw new Exception("Failed to save job details.");
			}

			$this->respond($response);
		} catch (Exception $ex) {

			if (isset($job)) {
				$job->status = RequestQueue::STATUS_FAILED;
				$job->save();
			}
			//Create a response
			$this->logger->error("Error happened while saving JOB details : " . $ex->getTraceAsString());
			if ($ex instanceof HttpException) {
				throw $ex;
			}
			throw new HttpException(
					'ADP-1706',
					403,
					'Access Denied.',
					$ex->getMessage());
		}
	}

	/**
	 * testPatch
	 *
	 * @param integer $adpTestId
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @throws HttpException
	 * @throws \PARCC\ADP\ControllersException
	 * @throws Exception
	 */
	public function testPatch($adpTestId)
	{
		$validBooleanValues = ['true', 'false', 'TRUE', 'FALSE', true, false, 0, 1];
		$couldReturnException = true;

		$this->logger->info("[PATCH TEST ATTRIBUTES] adpTestId: $adpTestId");
		// Validating JSON
		$requestBody = $this->request->getJsonRawBody(true);
		$this->logger->info("[PATCH TEST ATTRIBUTES] requestBody: " . json_encode($requestBody));

		// JSON body could not be parsed, throw exception
		if ($requestBody === null) {
			throw new HttpException(
					'ADP-1760',
					412,
					'Invalid Request.',
					'The data sent to the server is not a valid JSON object.');
		}

		try {
			if (!isset($requestBody['batteryId']))  {

				throw new HttpException(
						'ADP-1761',
						403,
						'Access Denied',
						'batteryId property is missing.');
			}

			if (!preg_match('/^i[0-9]{12,25}$/', $requestBody['batteryId'])) {
				throw new HttpException(
						'ADP-1762',
						403,
						'Access Denied',
						'Battery Id wrong format (Battery Id: ' . $requestBody['batteryId'] . ')');
			}

			$batteryId = $requestBody['batteryId'];

			$test = Test::findFirst(
					[
							'columns' => '*',
							'conditions' => ' tenantId = ?1 AND testId = ?2 AND  externalTestId = ?3',
							'bind' => [
									1 => $this->tenant->tenantId,
									2 => $adpTestId,
									3 => $batteryId
							]
					]
			);

			if (!$test) {
				throw new HttpException(
						'ADP-1763',
						404,
						'Data Not Found.',
						'Unable to find battery for Battery Id : ' .
						$requestBody['batteryId'] . ' and AdpTestId : '. $adpTestId );
			}

			if (isset($requestBody['isActive'])) {
				if (!in_array($requestBody['isActive'], $validBooleanValues, true)) {
					throw new HttpException(
							'ADP-1764',
							403,
							'Access Denied.',
							'isActive property has an invalid value (' . $requestBody['isActive'] . ')');
				}
				$requestBody['isActive'] = filter_var($requestBody['isActive'], FILTER_VALIDATE_BOOLEAN);
			}


			$notAllowedAttributes = ['program', 'scoreReport', 'itemSelectionAlgorithm'];
			$allowedAttributes = ["name", "subject", "grade", "security", "multimedia", "scoring",
					"permissions", "privacy", "isActive", "description"];
			$fieldOptionsAttributes = ["subject", "grade", "security", "multimedia", "scoring",
					"permissions", "privacy"];

			foreach ($requestBody as $index => $value) {
				if (in_array($index, $notAllowedAttributes)) {
					throw new HttpException(
							'ADP-1765',
							403,
							'Access Denied.',
							"Battery $index not allowed for edits!");
				}

				if (in_array($index, $fieldOptionsAttributes)) {
					if (!in_array($value, $test->getFieldOptions($index))) {
						throw new HttpException(
								'ADP-1766',
								403,
								'Access Denied.',
								"Battery $index value ({$value}) not authorized!");
					}
				}

				if (in_array($index, ['description', 'name'])) {
					$batteryValidation = new \Phalcon\Validation();
					if (isset($requestBody['description'])) {
						$batteryValidation->add('description', new \Phalcon\Validation\Validator\StringLength(
							array(
							"messageMaximum" => 'Description should not exceed 4096 characters',
							"max" => 4096
						)));
					}
					if (isset($requestBody['name'])) {
						$batteryValidation->add('name', new \Phalcon\Validation\Validator\StringLength(
							array(
							"messageMaximum" => "Test Battery's name should not exceed 100 characters",
							"max" => 100,
							"messageMinimum" => "Test Battery's name should at least be 1 character",
							"min" => 1,
						)));
					}
					$errors = $batteryValidation->validate($requestBody);
					if (count($errors)) {
						throw new HttpException(
								'ADP-1767',
								403,
								'Access Denied.',
								"Battery validation failed: " . json_encode($errors)
						);
					}
				}
			}

			$name = isset($requestBody["name"]) ? $requestBody["name"] : $test->name;
			$grade = isset($requestBody["grade"]) ? $requestBody["grade"] : $test->grade;
			$subject = isset($requestBody["subject"]) ? $requestBody["subject"] : $test->subject;

			// The below function checks if the tenantId, name, grade and subject already exist and throws an
			// exception if that is the case.
			$this->checkIfNameGradeSubjectExists($name, $grade, $subject, $batteryId);

			$globalRequest = ['adpTestId' => $adpTestId , 'requestBody' => json_encode($requestBody)];

			// scheduling a job.
			$result = RequestQueue::scheduleAJob(
					$requestBody,
					$batteryId,
					$globalRequest,
					RequestQueue::SOURCE_ACR,
					RequestQueue::TYPE_TEST_PACKAGE,
					$this
			);

			$job = $result['job'];
			if (isset($result['exist'])) {
				return $this->respond([
					'requestId' => $job->rqid,
					'publishedTimestamp' => $job->createdDateTime,
					'status' => RequestQueue::ACR_STATUS_SCHEDULED
					]
				);
			}

			$data = [];

			// saving new details
			$this->logger->info("[PATCH TEST ATTRIBUTES] START");
			foreach ($requestBody as $index => $value) {
				if (in_array($index, $allowedAttributes)) {
					$this->logger->info("[PATCH TEST ATTRIBUTES] $index : (old: " . $test->{$index} .
							", new: {$value})");
					if ($test->{$index} != $value) {
						$data['testBattery' . ucfirst($index)] = $value;
						$test->{$index} = $value;
					}
				}
			}

			$success = true;

			if (count($data)){
				if ($this->user->type == RequestQueue::SOURCE_ACR && $test->program != 'Practice Test') {
					$data['testBatteryId'] = $test->testId;
					$success = $this->sendTestBattery($data);
				}
			}

			if(!$success){
				$this->logger->error('[PATCH TEST ATTRIBUTES] Unable to send Test Battery');
				throw new HttpException(
						'ADP-1769',
						500,
						'Internal Error.',
						'[PATCH TEST ATTRIBUTES] Unable to send Test Battery'
				);
			}

			$this->logger->info("[PATCH TEST ATTRIBUTES] END");
			$test->save();
			$response = [
				'requestId' => $job->rqid,
				'publishedTimestamp' => $job->createdDateTime,
				'status' => RequestQueue::ACR_STATUS_SCHEDULED
			];

			$this->respond($response);
			$couldReturnException = false;

			// setting the job end time.
			$job->endTime = PublishHelper::getDbCurrentTimestamp($this->db);
			$job->status = RequestQueue::STATUS_COMPLETED;
			if ($job->save() == false) {
				$this->logger->error("Failed to save job details.");
				throw new Exception("Failed to save job details.");
			}
		} catch (Exception $ex) {
			if (isset($job)) {
				$job->status = RequestQueue::STATUS_FAILED;
				$job->save();
			}

			//Create a response
//			$this->logger->error("Error happened while saving JOB details : " . $ex->getTraceAsString());

			if ($couldReturnException) {
				if ($ex instanceof HttpException) {
					throw $ex;
				}
				throw new HttpException(
						'ADP-1770',
						500,
						'Internal Error.',
						$ex->getMessage());
			}
		}
	}


	/**
	 * checkIfNameGradeSubjectExists
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param string $name
	 * @param string $grade
	 * @param string $subject
	 * @param string $batteryId
	 * @throws HttpException
	 */
	public function checkIfNameGradeSubjectExists($name, $grade, $subject, $batteryId)
	{
		/** search and see if a test with the same name/grade/subject already exist in the same group
		 * 1st group ("Practice Test" or "Quiz"), the 2nd group (Any other type of test) */
		$conditions = [];
		$conditions[] = " tenantId = ?1 AND name = ?2 AND grade = ?3 AND subject = ?4 AND externalTestId != ?5 " .
				"AND program NOT IN ('Practice Test','Quiz Test') ";
		$conditions[] = " tenantId = ?1 AND name = ?2 AND grade = ?3 AND subject = ?4 AND externalTestId != ?5 " .
				"AND program IN ('Practice Test','Quiz Test') ";


		foreach ($conditions as $condition) {
			$existingTest = Test::findFirst([
					'columns' => '*',
					'conditions' => $condition,
					'bind' => [
							1 => $this->tenant->tenantId,
							2 => $name,
							3 => $grade,
							4 => $subject,
							5 => $batteryId
					]
			]);

			if ($existingTest) {
				throw new HttpException(
						'ADP-1708',
						403,
						'Access Denied.',
						"The combination of name:{$name}/grade:" .
						"{$grade}/subject:{$subject} already exists."
				);
			}
		}
	}



	/**
	 * OPTIONS request handler.
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @return array Response contains all requested records.
	 */
	public function options()
	{
		return $this->respond([]);
	}

	/**
	 * publish
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $adpTestFormRevisionId
	 * @return void
	 * @throws HttpException
	 * @throws Exception
	 */
	public function publish($adpTestFormRevisionId = NULL)
	{
		ignore_user_abort(true);
		set_time_limit(0);

		// a flag used to prohibit sending more than one response.
		$couldReturnException = true;

		// array of errors.
		$errors = [];
		if ($adpTestFormRevisionId) {
			if (!preg_match("/^[0-9]+$/", $adpTestFormRevisionId) &&
					$adpTestFormRevisionId > 0 &&
					$adpTestFormRevisionId <= 4294967295) {
				throw new HttpException(
						'ADP-1703',
						403,
						'Access Denied.',
						'adpTestFormRevisionId  is not a valid unsigned integer'
				);
			}

			$this->logger->info('[PUBLISH : Starting the Re-publishing process.]');
		} else {
			$this->logger->info('[PUBLISH : Starting the publishing process.]');
		}

		try {
			// Validating JSON
			$requestBody = $this->request->getJsonRawBody(true);


			$validation = new \Phalcon\Validation();
			$validation->add('testFormRevisionId', new \Phalcon\Validation\Validator\PresenceOf(array(
				'message' => 'testFormRevisionId is missing!'
			)));
			$validation->add('testFormRevisionId', new \Phalcon\Validation\Validator\Regex(array(
				'pattern' => '/^i[0-9]+$/',
				'message' => 'The revision id format is invalid'
			)));

			$validation->add('publishedBy', new \Phalcon\Validation\Validator\Regex(array(
				'pattern' => '/^[A-Za-z0-9- \.]{1,45}$/',
				'message' => 'The publishedBy format is invalid'
			)));

			$errors = $validation->validate($requestBody);

			if (count($errors)) {
				throw new HttpException(
						'ADP-1703',
						403,
						'Access Denied.',
						'There were validation errors.'
				);
			}

			$globalRequest = [
					'adpTestFormRevisionId' => $adpTestFormRevisionId,
					'requestBody' => ($requestBody)
			];

			// scheduling a job.
			$testFormRevisionId = filter_var($requestBody['testFormRevisionId'], FILTER_SANITIZE_STRING);

			$result = RequestQueue::scheduleAJob(
					$requestBody,
					$testFormRevisionId,
					$globalRequest,
					RequestQueue::SOURCE_ACR,
					RequestQueue::TYPE_TEST_PACKAGE,
					$this
			);

			$job = $result['job'];
			if (isset($result['exist'])) {
				return $this->respond([
					'requestId' => $job->rqid,
					'publishedTimestamp' => $job->createdDateTime,
					'status' => RequestQueue::ACR_STATUS_SCHEDULED
					]
				);
			}

			$this->logger->debug("[Publish/Response]" . $this->response->getContent());

			// buffer all upcoming output
			ob_start();

			$response = [
				'requestId' => $job->rqid,
				'publishedTimestamp' => $job->createdDateTime,
				'status' => RequestQueue::ACR_STATUS_SCHEDULED
			];

			$this->respond($response);
			$couldReturnException = false;

			// get the size of the output
			$size = ob_get_length();

			// send headers to tell the browser to close the connection
			header("Content-Encoding: none");
			header("Content-Length: $size");
			header('Connection: close');

			$this->logger->info("ob_get_level: " . ob_get_level());

			// flush all output
			while(@ob_end_flush());
			flush();

			//  Trigger the handling of the jobs. (for now we launch right after returning the response,
			// in the future, it will be a scheduled job processing.
			$this->deployRevisionPackage($job);
		} catch (Exception $ex) {
			//Create a response
			$this->logger->error("Error happened while saving JOB details : " . $ex->getTraceAsString());
			if ($couldReturnException) {
				if ($ex instanceof HttpException) {
					throw $ex;
				}
				throw new HttpException(
						'ADP-1707',
						500,
						'Internal Error.',
						$ex->getMessage()
				);
			}
		}
	}

	public function cancel_publishing()
	{
		//Create a response
//		$response = new \PARCC\ADP\Responses\JsonResponse();
	}

	/**
	 * Compress a file using GZip algorithm with low memory consumption.
	 *
	 * @param  string  $source           Path to file that should be compressed.
	 * @param  string  $destination		 Path to the destination file,
	 *                                   if not provided it would be the $source appended by '.gz'.
	 * @param  integer $chunkSize		 The chunk size default value is 0.5MB (@default= 524288).
	 * @param  integer $compressionLevel GZip compression level (@default=9).
	 *
	 * @return string/false              $destination if provided as a parameter or $source with ".gz" appended
	 *                                   (if $destination not provided) if successful, OR false if operation fails.
	 */
	private function gzipFile($source, $destination = null, $chunkSize = 524288, $compressionLevel = 9)
	{
		if (is_null($destination)) {
			$destination = $source . '.gz';
		}

		$error = false;
		$fpOut = gzopen($destination, 'wb' . $compressionLevel);

		if ($fpOut !== false) {
			$fpIn = fopen($source, 'rb');

			if ($fpIn !== false) {
				while (!feof($fpIn)) {
					gzwrite($fpOut, fread($fpIn, $chunkSize));
				}

				fclose($fpIn);
			} else {
				$error = true;
			}

			gzclose($fpOut);
		} else {
			$error = true;
		}

		if ($error) {
			return false;
		} else {
			return $destination;
		}
	}
}
