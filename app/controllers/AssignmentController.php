<?php
/*
 * This file is part of ADP.
 *
 * ADP is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * ADP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with ADP. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Copyright © 2015 Breakthrough Technologies, LLC
 */

namespace PARCC\ADP\Controllers;

use PARCC\ADP\Models\Test;
use PARCC\ADP\Models\TestSession;
use PARCC\ADP\Models\TestForm;
use PARCC\ADP\Models\Student;
use PARCC\ADP\Models\RequestQueue;
use PARCC\ADP\Models\TestResults;
use PARCC\ADP\Models\TestResultsArchive;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Db\RawValue;
use PARCC\ADP\Exceptions\HttpException;
use PARCC\ADP\Services\AWS\S3Exception;
use Phalcon\Exception;
use PDOException;

/**
 * Class AssignmentController
 *
 * This is the RESTful Controller for handling assignment creation, updating and deactivation.
 *
 * @package PARCC\ADP\Controllers
 * @version v2.0.0
 * @license Proprietary owned by PARCC. Copyright © 2015 Breakthrough Technologies, LLC
 * @author  Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 */
class AssignmentController extends BaseController
{

	/**
	 * Constructor.
	 *
	 * Sets the allowed methods for Assessment web service and validates CORS request.
	 *
	 * @internal param string $allowedMethods Provides a list of supported methods by each Controller.
	 */
	public function __construct()
	{
		parent::__construct('POST, PATCH, DELETE, OPTIONS', false, true);
		// Grant Access only to LDR & PRC Users.
		$this->validateAccess(['LDR', 'PRC']);
	}

	/**
	 * POST request handler. This Request Creates/Updates a test assignment.
	 * LDR is the only user supposed to call this web service.
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param bool|false $adpTestAssignmentId
	 * @throws HttpException
	 * @throws Exception
	 * @internal  Error code interval ADP-1801 -> ADP-1809 & (ADP-1824)
	 */
	public function post($adpTestAssignmentId = false)
	{
		// a flag used to prohibit sending more than one response.
		$couldReturnException = true;

		try {

			$this->logger->info('[ASSIGNMENT : Starting the Creation of assignment.]');

			// Validating JSON
			$requestBody = $this->request->getJsonRawBody(true);

			// request validation
			$this->validateRequest($requestBody, $adpTestAssignmentId);

			// Early check on TestKey  (if test is set)
			if (isset($requestBody['test'])) {
				$testSession = TestSession::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND testKey = ?1 ',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $requestBody['test']['testKey']
					]
				]);

				if (!$adpTestAssignmentId && $testSession) {
					// test key exist on creation we return an error, on update PDOException will be thrown.
					$this->logger->error('Test Key already exists.');
					throw new HttpException(
						'ADP-1824',
						412,
						'Invalid Request.',
						'Test Key already exists.');
				}
			}

			// Check if a specific job exist for a specific testAssignmentId
			$job = RequestQueue::findFirst([
				'columns' => '*',
				'conditions' => ' tenantId = ?1 AND type = ?2 AND source = ?3  AND externalId = ?4 ',
				'bind' => [
					1 => $this->tenant->tenantId,
					2 => RequestQueue::TYPE_ASSIGNMENT,
					3 => RequestQueue::SOURCE_LDR,
					4 => $requestBody['testAssignmentId']
				]
			]);

			if ($job && $job->count()) {
				$this->logger->info("Job already exists (jobId: {$job->rqid} ,type : " .
					RequestQueue::TYPE_ASSIGNMENT .
					", external id: {$job->externalId}, insertTimeStamp: {$job->createdDateTime}");

				$testSession = TestSession::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND externalTestSessionId = ?2 ',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $requestBody['testAssignmentId']
					]
				]);

				if ($testSession) {
					// in case the externalId exists in the job queue (created by the same user) we just return it
					return $this->respond([
						'testAssignmentId' => (int) $testSession->testSessionId,
						'createdTimestamp' => $testSession->createdDateTime,
						'status' => $testSession->status
					]);
				} else { // in case there is a job but no testSession found!
					$this->logger->error('Unable to find testSession. (requestBody["testAssignmentId"] = '
						. $requestBody['testAssignmentId'] . ')');
					throw new HttpException(
						'ADP-1807',
						404,
						'Data Not Found.',
						'Unable to find testSession. (requestBody["testAssignmentId"] = '
						. $requestBody['testAssignmentId'] . ')');
				}
			}
			/**
			 * @var RequestQueue
			 */
			$job = new RequestQueue();

			// here we check the type of the user
			if ($this->user->type == RequestQueue::SOURCE_LDR) {
				$job->tenantId = $this->tenant->tenantId;
				$job->source = $this->user->type;
				$job->type = RequestQueue::TYPE_ASSIGNMENT;
				$job->status = RequestQueue::STATUS_SCHEDULED;
				$job->externalId = $requestBody['testAssignmentId'];
				$job->data = json_encode($requestBody);
				$job->createdByUserId = $this->user->userId;

				$job->startTime = new RawValue('UTC_TIMESTAMP()');
				$job->status = RequestQueue::STATUS_INPROGRESS;

				if ($job->save() == false) {
					$this->logger->error('Failed to save job details.');
					throw new Exception("Failed to save job details.");
				}

				$job->refresh();

				// starting a transaction
				$transactionManager = new TransactionManager();
				$transaction = $transactionManager->get();

				try {
					$student = $this->processStudent($requestBody, $transaction);
					$testSession = $this->processTest($requestBody, $adpTestAssignmentId, $transaction);
					$transaction->commit();
				} catch(PDOException $ex) {
					$transaction->rollback($ex->getMessage());
				}

				$job->endTime = new RawValue('UTC_TIMESTAMP()');
				$job->status = RequestQueue::STATUS_COMPLETED;
				if ($job->save() == false) {
					$this->logger->error('Failed to save job details.');
					throw new Exception("Failed to save job details.");
				}
				$job->refresh();

				$response = [
					"testAssignmentId" => (int) $testSession->testSessionId,
					"status" => $testSession->status
				];

				if ($adpTestAssignmentId) { // on update send the updatedTimestamp else send createdTimestamp
					$response["updatedTimestamp"] = $testSession->updatedDateTime;
				} else {
					$response["createdTimestamp"] = $testSession->createdDateTime;
				}

				// return response and then do further processing (archiving) if condition is met.
				ignore_user_abort(true);
				set_time_limit(0);
				ob_start();
				if (ob_get_contents()) {
					ob_clean();
				}
				// tentative
				$s3 = clone $this->s3;
				$logger = clone $this->logger;
				$config = clone $this->config;

				$this->respond($response);
				$couldReturnException = false;

				// get the size of the output
				$size = ob_get_length();

				// send headers to tell the browser to close the connection
				header("Content-Encoding: none");
				header("Content-Length: $size");
				header('Connection: close');

				// flush all output
				while (@ob_end_flush()) ;
//				@ob_flush();
				flush();

				// Launch archiving after returning response, test is completed or canceled.
				if (in_array($testSession->status, [TestSession::STATUS_COMPLETED, TestSession::STATUS_CANCELED])) {
					if (!$this->launchArchiveProcess($testSession, $config, $s3, $logger)) {
						$this->logger->warning("Archiving Process failed for testSession
						id: ({$testSession->testSessionId})");
					} else {
						$this->logger->info("Archiving Process succeeded for testSession
						id: ({$testSession->testSessionId})");
					}
				}
			}
			// else is handled by  validateAccess function.
		} catch (Exception $ex) {
			if (isset($job)) {
				$job->status = RequestQueue::STATUS_FAILED;
				if ($job->save() == false) {
					$this->logger->error('Failed to save job details.');
					throw new Exception("Failed to save job details.");
				}
				$job->refresh();
			}

			$this->logger->error("[ASSIGNMENT] Error : {$ex->getMessage()},
								Line: {$ex->getLine()}, File: {$ex->getFile()}");

			if ($ex instanceof HttpException) {
				throw $ex;
			}

			$errorMessage = $ex->getMessage();
			if ($ex instanceof PDOException) {
				$errorMessage = 'An error occurred, please check the log file.';
				if (isset($transactionManager) && $transactionManager instanceof TransactionManager) {
					$transactionManager->rollback();
					$this->logger->error("[ASSIGNMENT] rolling back all active transactions!");
				}
			}

			if (isset($couldReturnException) && $couldReturnException) {
				throw new HttpException(
					'ADP-1809',
					500,
					'Internal Error.',
					$errorMessage
				);
			}
		}
	}

	/**
	 * build select to read column definition for selected column
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param string $database
	 * @param string $sourceTable
	 * @param string $column
	 * @return array|mixed
	 */
	private function getFieldOptions($database, $sourceTable, $column)
	{
		$sql = " SHOW COLUMNS ";
		$sql .= " FROM " . $sourceTable;
		$sql .= " LIKE '" . $column . "'";

		$result = current($this->getDI()->getShared($database)->fetchAll($sql));

		if (!is_array($result)) {
			$result = (array)current($result);
		}
		// user regular expression to get option list from result
		preg_match("=\((.*)\)=is", $result["Type"], $options);

		// replace single quotes
		$options = str_replace("'", "", $options[1]);

		// explode string into an array
		$options = explode(",", $options);

		return $options;
	}

	/**
	 * PUT request handler.
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @throws HttpException
	 * @param $adpTestAssignmentId
	 * @return array Response contains all requested records.
	 */
	public function patch($adpTestAssignmentId)
	{
		return $this->post($adpTestAssignmentId);
	}

	/**
	 * DELETE request handler.
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @param $adpTestAssignmentId
	 * @throws HttpException
	 * @throws \PARCC\ADP\Models\Exception
	 * @throws \PARCC\ADP\Models\HttpException
	 */
	public function delete($adpTestAssignmentId)
	{
		// Job Management, Job exits or does not exist.
		$this->logger->info('[ASSIGNMENT DELETE : Starting of assignment deletion.]');

		// Validating JSON
		$requestBody = $this->request->getJsonRawBody(true);

		// Request Validation.
		$this->validateStudent($requestBody);
		$this->validateTestAssignment($requestBody);

		$testAssignmentId = $requestBody['testAssignmentId'];
		$studentId = $requestBody['student']['studentId'];

		// Validate adpTestAssignmentId.
		if (!$this->is32BitUnsignedInteger($adpTestAssignmentId)) {
			throw new HttpException(
				'ADP-1850',
				412,
				'Invalid Request.',
				'Missing adpTestAssignmentId in the request.'
			);
		}

		$globalRequest = ['adpTestAssignmentId' => $adpTestAssignmentId, 'requestBody' => json_encode($requestBody)];

		// scheduling a job.
		$result = RequestQueue::scheduleAJob(
			$requestBody,
			$testAssignmentId,
			$globalRequest,
			RequestQueue::SOURCE_LDR,
			RequestQueue::TYPE_ASSIGNMENT,
			$this
		);

		/* @var $job RequestQueue */
		$job = $result['job'];

		if (isset($result['exist'])) {
			throw new HttpException('ADP-1851', 403, 'Access Denied.', '[ASSIGNMENT DELETE] Job already exists.');
		}

		// Get assignment.
		/* @var $testSession TestSession */
		$testSession = TestSession::findFirst([
			'columns' => '*',
			'conditions' => ' tenantId = ?1 AND testSessionId = ?2 ',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $adpTestAssignmentId
			]
		]);

		if ($testSession == false) {
			// Throw an error if assignment does not exist.
			$job->status = RequestQueue::STATUS_FAILED;
			$job->save();
			throw new HttpException("ADP-1852", 404, "Data Not Found.", "[ASSIGNMENT DELETE] Unable to find a " .
				"testSession to delete.");
		} else if ($testSession->createdByUserId != $this->user->userId) {
			// Throw an error if assignment was created by a different user.
			$job->status = RequestQueue::STATUS_FAILED;
			$job->save();
			throw new HttpException("ADP-1853", 403, "Access Denied.", "[ASSIGNMENT DELETE] Test Session does not " .
				"belong to actual user.");
		} else if ($testSession->parentStudentId != $studentId) {
			// Throw an error if assignment belong to a different user.
			$job->status = RequestQueue::STATUS_FAILED;
			$job->save();
			throw new HttpException("ADP-1854", 403, "Access Denied.", "[ASSIGNMENT DELETE] Test Session does not " .
				"belong to actual student.");
		} else if ($testSession->externalTestSessionId != $testAssignmentId) {
			// Throw an error if assignment does not have the right test assignment id.
			$job->status = RequestQueue::STATUS_FAILED;
			$job->save();
			throw new HttpException("ADP-1855", 403, "Access Denied.", "[ASSIGNMENT DELETE] Test Session does not " .
				"have the right test assignment Id.");
		}

		$program = $testSession->TestForm->Test->program;

		if ($program == 'Practice Test') {
			$job->status = RequestQueue::STATUS_FAILED;
			$job->save();
			throw new HttpException('ADP-1856', 403, 'Access Denied.', '[ASSIGNMENT DELETE] Status not allowed.');
		}

		$this->logger->info("[ASSIGNMENT DELETE] TestSessionId:($adpTestAssignmentId) | StudentId:($studentId) |" .
			" Status:({$testSession->status}) | Program:($program) | externalTestSessionId:($testAssignmentId)");

		// Check Assignment status.
		if ($testSession->status == TestSession::STATUS_SCHEDULED) {

			// Delete Assignment.
			if ($testSession->delete()) {
				$this->logger->info("[ASSIGNMENT DELETE] Assignment deleted successfully.");
			} else {
				// Throw exception in case assignment status is not valid.
				$job->status = RequestQueue::STATUS_FAILED;
				$job->save();
				throw new HttpException('ADP-1857',
					500,
					'Internal Error.',
					'[ASSIGNMENT DELETE] Unable to delete assignment.');
			}

		} else {
			// Throw exception in case assignment status is not valid.
			$job->status = RequestQueue::STATUS_FAILED;
			$job->save();
			throw new HttpException('ADP-1858', 403, 'Access Denied.', '[ASSIGNMENT DELETE] Status not allowed.');
		}

		// Check if Student has other assignments.
		$testAssignments = TestSession::find([
			'columns' => '*',
			'conditions' => ' tenantId = ?1 AND parentStudentId = ?2 ',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $studentId
			]
		]);

		if ($studentId == 0) {
			$this->logger->warning("[ASSIGNMENT DELETE] Skipping deletion of Anonymous Student record.");
		} else {
			// Delete the student if no other test assignments belong to him.
			if ($testAssignments->count() == 0) {
				$student = Student::findFirst(['columns' => '*',
					'conditions' => ' tenantId = ?1 AND studentId = ?2 ',
					'bind' => [
						1 => $this->tenant->tenantId,
						2 => $studentId
					]
				]);

				if ($student) {
					// Delete student.
					if ($student->delete() == false) {
						$this->logger->error("[ASSIGNMENT DELETE] Unable to delete student.");
					} else {
						$this->logger->info("[ASSIGNMENT DELETE] Student deleted successfully.");
					}
				} else {
					$this->logger->warning("[ASSIGNMENT DELETE] Student record was not found for" .
						"StudentId ($studentId).");
				}
			} else {
				$this->logger->info("[ASSIGNMENT DELETE] Skipping Student deletion (Student has other assignments)");
			}
		}

		$job->status = RequestQueue::STATUS_INPROGRESS;
		$job->save();

		$job = RequestQueue::findFirst(['columns' => '*',
			'conditions' => ' tenantId = ?1 AND rqid = ?2 ',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $job->rqid
			]
		]);

		$updateDateTime = $job->updatedDateTime;

		$job->status = RequestQueue::STATUS_COMPLETED;
		$job->save();

		$this->logger->info('[ASSIGNMENT DELETE : End of assignment deletion.]');
		return $this->respond([
			"testAssignmentId" => $adpTestAssignmentId,
			"updatedTimestamp" => $updateDateTime,
			"status" => "Deleted"
		]);
	}

	/**
	 * OPTIONS request handler.
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 *
	 * @return array Response contains all requested records.
	 */
	public function options()
	{
		return $this->respond($this->request->getJsonRawBody(true));
	}

	/**
	 * processTest
	 *
	 * @internal  Error code interval ADP-1825 -> ADP-1829
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @param integer /false $adpTestAssignmentId
	 * @param \Phalcon\Mvc\Model\TransactionInterface $transaction
	 * @return TestSession
	 * @throws HttpException
	 */
	private function processTest($requestBody, $adpTestAssignmentId, $transaction)
	{
		$this->logger->info('[ASSIGNMENT TEST PROCESSING] Starting Test Processing.');
		// ending testSession processing
		$condition = ' tenantId = ?1 AND externalTestSessionId = ?2 ';
		$bind = [
				1 => $this->tenant->tenantId,
				2 => filter_var($requestBody['testAssignmentId'], FILTER_SANITIZE_STRING)];

		if ($adpTestAssignmentId) {
			$condition = ' tenantId = ?1 AND externalTestSessionId = ?2 AND testSessionId = ?3';
			$bind[3] = filter_var($adpTestAssignmentId, FILTER_SANITIZE_STRING);
		}

		$testSession = TestSession::findFirst([
			'columns' => '*',
			'conditions' => $condition,
			'bind' => $bind
		]);

		if (!$testSession && $adpTestAssignmentId) {
			throw new HttpException(
				'ADP-1825',
				404,
				'Data Not Found.',
				'[ASSIGNMENT PATCH] Unable to find testSession for update.'
			);
		}

		// if a POST call and testSession is not found, then we create one.
		if ($testSession == false && !$adpTestAssignmentId) {
			$this->logger->info('[ASSIGNMENT POST] Creating a new testSession.');
			$testSession = new TestSession();
		} elseif ($testSession && $adpTestAssignmentId) {
			$this->logger->info('[ASSIGNMENT PATCH] Reusing existing testSession
			(testSessionId : ' . $testSession->testSessionId . ')');
		}

		// testSession
		$testSession->setTransaction($transaction);

		if (isset($requestBody['test'])) {// if test exists in the request.
			// check if formId belongs to test id and just save formId, if not throw an exception.
			$testForm = TestForm::findFirst([
					'columns' => '*',
					'conditions' => ' tenantId = ?1 AND formId = ?2 AND parentTestId = ?3 ',
					'bind' => [
							1 => $this->tenant->tenantId,
							2 => $requestBody['test']['testFormId'],
							3 => $requestBody['test']['testId']]
			]);

			if (!$testForm) {
				throw new HttpException(
					'ADP-1827',
					500,
					'Internal Error.',
					'Test Form does not belong to Test Battery. (testId:'
					. $requestBody['test']['testId'] . ', testFormId:' . $requestBody['test']['testFormId'] . ')'
				);
			}
		}

		// we make sure the student record exists or else we throw an exception.
		$student = Student::findFirst([
				'columns' => '*',
				'conditions' => ' tenantId = ?1 AND studentId = ?2 ',
				'bind' => [
						1 => $this->tenant->tenantId,
						2 => $requestBody['student']['studentId']]
		]);

		if (count($requestBody['student']) == 1 && !$student) {
			throw new HttpException(
				'ADP-1828',
				404,
				'Data Not Found.',
				'Unable to find student. (studentId: ' . $requestBody['student']['studentId'] . ')'
			);
		}

		// Starting testSession processing
		if ($adpTestAssignmentId) { // PATCH
			$testSession->updatedByUserId = $this->user->userId;
			$testSession->parentStudentId = $requestBody['student']['studentId'];
			if (isset($requestBody['test'])) {
				$testSession->parentTestFormId = $requestBody['test']['testFormId'];
				$testSession->testKey = $requestBody['test']['testKey'];

				// optional accommodation properties
				if (isset($requestBody['test']['enableLineReader'])) {
					$testSession->isEnabledLineReader = $requestBody['test']['enableLineReader'];
				}

				if (isset($requestBody['test']['enableTextToSpeech'])) {
					$testSession->isEnabledTextToSpeech = $requestBody['test']['enableTextToSpeech'];
				}
			}
			$testSession->isActive = true;
			$testSession->updatedDateTime = new RawValue('UTC_TIMESTAMP()');

			/**
			 * Status handling:
			 *
			 * InProgress    -> Paused        (delete the token "reset")
			 * Submitted    -> Paused        (delete the token "re-opening")
			 *
			 * Submitted    -> Completed    (triggers result export)
			 *
			 * Scheduled    -> Scheduled    (delete the token "reset")
			 * Paused        -> Paused        (delete the token "reset")
			 *
			 * Scheduled    \
			 * InProgress    => Canceled    (triggers result export)
			 * Paused       /
			 *
			 */
			if (isset($requestBody['testStatus'])) {
				if ($requestBody['testStatus'] != $testSession->status) {

					if (($requestBody['testStatus'] == TestSession::STATUS_PAUSED &&
							!in_array($testSession->status, [
								TestSession::STATUS_INPROGRESS,
								TestSession::STATUS_SUBMITTED,
								TestSession::STATUS_PAUSED])
						) ||
						(in_array($requestBody['testStatus'], [
							TestSession::STATUS_SCHEDULED,
							TestSession::STATUS_INPROGRESS,
							TestSession::STATUS_SUBMITTED])
						) ||
						($requestBody['testStatus'] == TestSession::STATUS_COMPLETED &&
							$testSession->status != TestSession::STATUS_SUBMITTED
						) ||
						($requestBody['testStatus'] == TestSession::STATUS_CANCELED &&
							!in_array($testSession->status, [
								TestSession::STATUS_SCHEDULED,
								TestSession::STATUS_INPROGRESS,
								TestSession::STATUS_PAUSED])
						)
					) {
						$transaction->rollback("Unauthorized status change (actual:" . $testSession->status .
							", new status: " . $requestBody['testStatus'] . ")");
					}

					if ($requestBody['testStatus'] == TestSession::STATUS_PAUSED) {
						// delete the token
						$testSession->token = null;
					}

					// updating the status
					$testSession->status = $requestBody['testStatus'];
				} elseif (in_array($requestBody['testStatus'], [
					TestSession::STATUS_SCHEDULED,
					TestSession::STATUS_PAUSED
				])
				) {
					// delete the token
					$testSession->token = null;
				} else {
					$transaction->rollback("Unauthorized status change (actual:" . $testSession->status .
						", new status: " . $requestBody['testStatus'] . ")");
				}
			}
		} else { //POST
			$testSession->tenantId = $this->tenant->tenantId;
			$testSession->externalTestSessionId = $requestBody['testAssignmentId'];
			$testSession->parentStudentId = $requestBody['student']['studentId'];

			$testSession->createdByUserId = $this->user->userId;
			$testSession->parentTestFormId = $requestBody['test']['testFormId'];
			$testSession->testKey = $requestBody['test']['testKey'];
			$testSession->isActive = true;
			$testSession->status = TestSession::STATUS_SCHEDULED;
			$testSession->createdDateTime = new RawValue('UTC_TIMESTAMP()');

			// optional accommodation properties
			if (isset($requestBody['test']['enableLineReader'])) {
				$testSession->isEnabledLineReader = $requestBody['test']['enableLineReader'];
			}

			if (isset($requestBody['test']['enableTextToSpeech'])) {
				$testSession->isEnabledTextToSpeech = $requestBody['test']['enableTextToSpeech'];
			}
		}

		if ($testSession->save() == false) {
			$this->logger->error('Failed to save testSession details.');
			$transaction->rollback('Failed to save testSession details.');
		}

		$testSession->refresh();
		$this->logger->info('[ASSIGNMENT TEST PROCESSING] Ending Test Processing.');
		return $testSession;
		// ending testSession processing
	}

	/**
	 * processStudent
	 *
	 * @internal  Error code interval ADP-1819
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @param \Phalcon\Mvc\Model\TransactionInterface $transaction
	 * @return Student
	 * @throws HttpException
	 */
	private function processStudent($requestBody, $transaction)
	{
		$this->logger->info('[ASSIGNMENT STUDENT PROCESSING] Starting Student Processing.');

		// Student Processing. [Start]
		$student = Student::findFirst([
				'columns' => '*',
				'conditions' => ' tenantId = ?1 AND studentId = ?2 ',
				'bind' => [
						1 => $this->tenant->tenantId,
						2 => $requestBody['student']['studentId']
				]
		]);

		$this->logger->info('[ASSIGNMENT] Student Processing : (' . json_encode($requestBody['student']) . ')');

		if (count($requestBody['student']) == 1) {
			// we make sure the student record exists or else we throw an exception.
			if (!$student) {
				throw new HttpException(
					'ADP-1819',
					404,
					'Data Not Found.',
					'Unable to find student. (studentId: ' .
					$requestBody['student']['studentId'] . ')'
				);
			}
			$this->logger->info('[ASSIGNMENT STUDENT PROCESSING] Ending Student Processing
			(Student exists no updates needed).');
			// No need to save the student we just make sure it exists.
			return $student;
		} else { // in case we have many properties to set for the student (create or update)
			$studentCreate = false;
			if (!$student) {
				$student = new Student();
				$student->tenantId = $this->tenant->tenantId;
				$studentCreate = true;
			}

			// we update the student records here.
			$student->setTransaction($transaction);

			foreach ($requestBody['student'] as $key => $value) {
				if (in_array($key, [
						'studentId',
						'personalId',
						'firstName',
						'lastName',
						'dateOfBirth',
						'state',
						'schoolName',
						'grade']
				)
				) {
					$student->{$key} = $value;
				} else {
					$this->logger->warning("[Student Create/Update] Skipping key: $key , value: $value");
				}
			}

			// setting the active flag and the creator/updater
			$student->isActive = true;
			if ($studentCreate) {
				$student->createdByUserId = $this->user->userId;
				$student->createdDateTime = new RawValue('UTC_TIMESTAMP()');
				$this->logger->info('[ASSIGNMENT] Creating new Student.');
			} else {
				$student->updatedByUserId = $this->user->userId;
				$student->updatedDateTime = new RawValue('UTC_TIMESTAMP()');
				$this->logger->info('[ASSIGNMENT] Updating existing Student.');
			}

			if ($student->save() == false) {
				$this->logger->error('Failed to save student details.');
				$transaction->rollback('Failed to save student details.');
			}
		}

		$this->logger->info('[ASSIGNMENT STUDENT PROCESSING] Ending Student Processing.');
		return $student;
		// Student Processing. [End]
	}

	/**
	 * validateRequest
	 *
	 * @internal  Error code interval ADP-1802 -> ADP-1805
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @param bool|false $adpTestAssignmentId
	 * @throws HttpException On validation failure an exception is being thrown.
	 */
	private function validateRequest($requestBody, $adpTestAssignmentId = false)
	{
		// validation of required properties
		$this->logger->info('[ASSIGNMENT VALIDATION] Starting validation of required properties.');

		// check the request structure and make sure the correct keys are present.
		if (!$adpTestAssignmentId) {
			$this->logger->info('[ASSIGNMENT VALIDATION] POST: Assignment Create Call.');

			foreach (['testAssignmentId', 'test', 'student'] as $index) {
				if (!isset($requestBody[$index])) {
					throw new HttpException(
						"ADP-1803",
						412,
						"Invalid Request.",
						"Missing index ($index) in the request."
					);
				}
			}
		} else {
			$this->logger->info('[ASSIGNMENT VALIDATION] PATCH: Assignment Update Call.');

			if (!$this->is32BitUnsignedInteger($adpTestAssignmentId)) {
				throw new HttpException(
					'ADP-1804',
					412,
					'Invalid Request.',
					'Missing adpTestAssignmentId in the request.'
				);
			}

			// check all 4 possible request bodies
			$keys = array_keys($requestBody);
			if (0 !== count(array_diff([
					'testAssignmentId',
					'student'
				], $keys)) && /* Update Student Only */
				0 !== count(array_diff([
					'testAssignmentId',
					'testStatus',
					'student'
				], $keys)) && /* Update Test Status Only */
				0 !== count(array_diff([
					'testAssignmentId',
					'test',
					'student'
				], $keys)) && /* Update Test Only */
				0 !== count(array_diff([
					'testAssignmentId',
					'test',
					'testStatus',
					'student'], $keys))
			) { /* Update everything */
				throw new HttpException(
					"ADP-1805",
					412,
					"Invalid Request.",
					"PATCH request is missing some indexes (request keys: " .
					implode(',', $keys) . ")"
				);
			}
		}

		$this->validateTestAssignment($requestBody);
		$this->validateStudent($requestBody);

		if (isset($requestBody['test'])) {
			$this->validateTest($requestBody);
		}
		if (isset($requestBody['testStatus'])) {
			$this->validateTestStatus($requestBody);
		}
	}

	/**
	 * validateTestAssignment TestAssignment Validation
	 *
	 * @internal  Error code interval ADP-1806
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @throws HttpException
	 */
	private function validateTestAssignment($requestBody)
	{
		$this->logger->info('[ASSIGNMENT VALIDATION] Starting Test Assignment validation.');
		// Check and make sure the input is a 32 bit integer between 1 and 4294967295
		if (!isset($requestBody['testAssignmentId']) ||
			!$this->is32BitUnsignedInteger($requestBody['testAssignmentId'])
		) {
			throw new HttpException(
				'ADP-1806',
				403,
				'Access Denied.',
				"Properties need to be an unsigned integer greater than 0 : ['testAssignmentId'] = " .
				$requestBody['testAssignmentId']
			);
		}
		$this->logger->info('[ASSIGNMENT VALIDATION] Ending Test Assignment validation.');
	}

	/**
	 * validateTestStatus validate the test status.
	 *
	 * @internal  Error code interval ADP-1830
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @throws HttpException
	 */
	private function validateTestStatus($requestBody)
	{
		$this->logger->info('[ASSIGNMENT VALIDATION] Starting Test Status validation.');
		// Validating the status
		if (!in_array($requestBody['testStatus'], $this->getFieldOptions('db', 'test_session', 'status'))) {
			$this->logger->error("['status'] is not valid (status:" . $requestBody['testStatus'] . ")");
			throw new HttpException(
				'ADP-1830',
				403,
				"Access Denied.",
				"['status'] is not valid (status:" . $requestBody['testStatus'] . ")"
			);
		}
		$this->logger->info('[ASSIGNMENT VALIDATION] Ending Test Status validation.');
	}

	/**
	 * validateStudent validate the student details.
	 *
	 * @internal  Error code interval ADP-1810 -> ADP-1819
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @throws HttpException
	 */
	private function validateStudent($requestBody)
	{
		$this->logger->info('[ASSIGNMENT VALIDATION] Starting Student properties validation.');
		// student validation
		if (!isset($requestBody['student']['studentId'])) {
			$this->logger->error("Missing index [student][studentId] in the request.");
			throw new HttpException(
				'ADP-1810',
				412,
				'Invalid Request.',
				'Missing index [student][studentId] in the request.'
			);
		}

		// check and make sure the input is a 32 bit integer between 1 and 4294967295
		if (!$this->is32BitUnsignedInteger($requestBody['student']['studentId'])) {
			throw new HttpException(
				'ADP-1811',
				403,
				'Access Denied.',
				"Properties need to be an unsigned integer greater than 0 : ['student']['studentId'] =  " .
				$requestBody['student']['studentId']
			);
		}

		// Student Optional Properties
		if (isset($requestBody['student']['state'])) {
			if (!in_array($requestBody['student']['state'], $this->getFieldOptions('db', 'student', 'state'))) {
				throw new HttpException(
					'ADP-1812',
					403,
					'Access Denied.',
					"Invalid ['student']['state'] = " . $requestBody['student']['state']
				);
			}
		}

		if (isset($requestBody['student']['grade'])) {
			if (!in_array($requestBody['student']['grade'], $this->getFieldOptions('db', 'student', 'grade'))) {
				throw new HttpException(
					'ADP-1813',
					403,
					"Access Denied.",
					"Invalid ['student']['grade'] = " . $requestBody['student']['grade']
				);
			}
		}

		if (isset($requestBody['student']['dateOfBirth'])) {
			if (preg_match("/^\d{4}-\d{2}-\d{2}$/", $requestBody['student']['dateOfBirth'])) {
				$dateParts = explode('-', $requestBody['student']['dateOfBirth']);
				if (!checkdate($dateParts[1], $dateParts[2], $dateParts[0])) {
					throw new HttpException(
						'ADP-1814',
						403,
						'Access Denied.',
						"Invalid ['student']['dateOfBirth'] = " .
						$requestBody['student']['dateOfBirth']
					);
				}
			} else {
				throw new HttpException(
					"ADP-1815",
					403,
					"Access Denied.",
					"Invalid Format ['student']['dateOfBirth'] = " . $requestBody['student']['dateOfBirth']
				);
			}
		}

		// based on specs from PARCC
		if (isset($requestBody['student']['schoolName'])) {
			if (!preg_match("/^[a-zA-Z0-9- \.()'`\/\\\\&#+!:]{1,60}$/", $requestBody['student']['schoolName'])) {
				throw new HttpException(
					"ADP-1816",
					403,
					"Access Denied.",
					"['student']['schoolName'] should not be empty or longer than 60 characters = " .
					$requestBody['student']['schoolName']
				);
			}
		}

		// based on specs from PARCC
		foreach (['firstName', 'lastName'] as $index) {
			if (isset($requestBody['student'][$index])) {
				if (!preg_match("/^[a-zA-Z0-9- \.']{1,35}$/", $requestBody['student'][$index])) {
					throw new HttpException(
						"ADP-1817",
						403,
						"Access Denied.",
						"['student'][$index] should not be empty or longer than 35 characters = " .
						$requestBody['student'][$index]
					);
				}
			}
		}

		// based on specs from PARCC
		if (isset($requestBody['student']['personalId'])) {
			if (!preg_match("/^[A-Z0-9]{1,30}$/", $requestBody['student']['personalId'])) {
				throw new HttpException(
					"ADP-1818",
					403,
					"Access Denied.",
					"['student']['personalId'] doesn't match the pattern = " . $requestBody['student']['personalId']
				);
			}
		}
		$this->logger->info('[ASSIGNMENT VALIDATION] Ending Student properties validation.');
	}

	/**
	 * ValidateTest validate the test details.
	 *
	 * @internal  Error code interval ADP-1820 -> ADP-1832
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param array $requestBody
	 * @throws HttpException
	 */
	private function validateTest($requestBody)
	{
		$this->logger->info('[ASSIGNMENT VALIDATION] Starting Test properties validation.');
		// validation of Test object
		foreach (['testId', 'testFormId', 'testKey'] as $index) {
			if (!isset($requestBody['test'][$index])) {
				$this->logger->error("Missing index [test][$index] in the request.");
				throw new HttpException(
					"ADP-1820",
					412,
					"Invalid Request.",
					"Missing index [test][$index] in the request."
				);
			}
		}

		if (!$this->is32BitUnsignedInteger($requestBody['test']['testId'])) {
			throw new HttpException(
				"ADP-1821",
				403,
				"Access Denied.",
				"Properties need to be an unsigned integer greater than 0 Invalid ['test']['testId'] =  " .
				$requestBody['test']['testId']
			);
		}

		if (!$this->is32BitUnsignedInteger($requestBody['test']['testFormId'])) {
			throw new HttpException(
				"ADP-1822",
				403,
				"Access Denied.",
				"Properties need to be an unsigned integer greater than 0 Invalid ['test']['testFormId'] = " .
				" | ['test']['testFormId'] = " . $requestBody['test']['testFormId']
			);
		}

		// Make sure the test key is a valid string 10 uppercase characters that do not start with PRACT
		if (!preg_match("/^(?!PRACT)([A-Z]{10})$/", $requestBody['test']['testKey'])) {
			throw new HttpException(
				"ADP-1823",
				403,
				"Access Denied.",
				"Invalid ['test']['testKey'] = " . $requestBody['test']['testKey']
			);
		}

		$validBooleanValues = ['true', 'false', 'TRUE', 'FALSE', true, false, 0, 1];
		// Validate optional accommodation enableLineReader properties
		if (isset($requestBody['test']['enableLineReader'])) {
			if (!in_array($requestBody['test']['enableLineReader'], $validBooleanValues, true)) {
				throw new HttpException(
					"ADP-1831",
					403,
					"Access Denied.",
					"enableLineReader index has an invalid value ({$requestBody['test']['enableLineReader']}"
				);
			}

			$requestBody['test']['enableLineReader'] = filter_var($requestBody['test']['enableLineReader'],
				FILTER_VALIDATE_BOOLEAN);
		}

		// Validate optional accommodation enableTextToSpeech properties
		if (isset($requestBody['test']['enableTextToSpeech'])) {
			if (!in_array($requestBody['test']['enableTextToSpeech'], $validBooleanValues, true)) {
				throw new HttpException(
					"ADP-1832",
					403,
					"Access Denied.",
					"enableTextToSpeech index has an invalid value ({$requestBody['test']['enableTextToSpeech']})"
				);
			}

			$requestBody['test']['enableTextToSpeech'] = filter_var($requestBody['test']['enableTextToSpeech'],
				FILTER_VALIDATE_BOOLEAN);
		}

		$this->logger->info('[ASSIGNMENT VALIDATION] Ending Test properties validation.');
	}

	/**
	 * is32BitUnsignedInteger
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $var
	 * @return boolean true if valid, false otherwise.
	 */
	private function is32BitUnsignedInteger($var)
	{
		return (preg_match("/^[0-9]+$/", $var) && $var > 0 && $var <= 4294967295);
	}

	/**
	 * launchArchiveProcess
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param TestSession $testSession
	 * @param $config
	 * @param object $s3
	 * @param $logger
	 * @throws S3Exception
	 * @throws HttpException
	 * @return boolean
	 */
	public function launchArchiveProcess($testSession, $config, $s3, $logger)
	{
		// error_reporting(E_ALL & ~E_NOTICE);
		$return = false;
		$keys = [];

		// initializing the $deleteRecordsFromDatabase flag.
		$deleteRecordsFromDatabase = true;
		$countOfResultsWithStartedSuffix = 0;
		$countOfResultsWithPausedSuffix = 0;
		$countOfResultsWithResumedSuffix = 0;
		$countOfResultsWithSubmittedSuffix = 0;
		$countOfRegularResultsFiles = 0;

		// [Archive All files start!] Get all files found in s3, archive them and upload them to the archive folder.
		try {

			$revertedTestSession = strrev($testSession->testSessionId);
			$status = strtolower($testSession->status);

			$keys = $s3->getObjectList(
					$config->s3->resultsBucket,
					$config->s3->resultsPrefix . '/' . $revertedTestSession,
					$config->s3->resultsPrefix . '/' . $revertedTestSession
			);

			// Keep the valid keys only.
			foreach ($keys as $k => $v) {
				if (preg_match('#/$#', $k)) {
					unset($keys[$k]);
				}
			}
		} catch (S3Exception $ex) {
			$logger->error($ex->getMessage());
		}

		if (count($keys) > 0) {
			$return = false;
			$logger->info("[launchArchiveProcess] Archiving all files and creating a zip package.");

			$zipFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $revertedTestSession .
					'.' . $status . '.' .  $this->tenant->tenantId . '.zip';

			while (file_exists($zipFile)) {
				$zipFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR .
						$revertedTestSession . '_'  . $status . rand(0, 100) . '.' . $this->tenant->tenantId . '.zip';
			}

			try {
				$s3->getObjectDetails(
						$config->s3->resultsArchiveBucket,
						$config->s3->resultsArchivePrefix . '/' . $revertedTestSession . '.' . $status . '.' .
						$this->tenant->tenantId . '.zip'
				);
			} catch (S3Exception $ex) {
				$logger->error("[launchArchiveProcess] Zip Package exist on s3 or was already uploaded to S3
				 ({$config->s3->resultsArchivePrefix}/$revertedTestSession.$status.{$this->tenant->tenantId}.zip')
				Details: (" . $ex->getMessage() . ")");
				return $return;
			}

			//create the archive
			$zip = new \ZipArchive();
			if ($zip->open($zipFile, \ZipArchive::CREATE) !== true) {
				$logger->error();
				throw new HttpException(
					'ADP-1914',
					500,
					'Internal Error.',
					'[launchArchiveProcess] Unable to open zip file'
				);
			}

			foreach ($keys as $key => $v) {
				// Try to download Test Results from AWS S3.
				try {
					$file = $s3->downloadObject(
						(string)$config->s3->resultsBucket, (string)$config->s3->resultsPrefix . '/' . $key
					);
					$zip->addFromString($key, $file->body);
				} catch (S3Exception $exception) {
					$logger->error("[launchArchiveProcess] Unable to find file '{$config->s3->resultsPrefix}/{$key}'
					in S3 (details: " . $exception->getMessage() . ")");
					$deleteRecordsFromDatabase = false;
					continue;
				}
			}
			$zip->close();

			// ZipArchive::CHECKCONS will enforce additional consistency checks to make sure the zip package is valid.
			$res = (integer)$zip->open($zipFile, \ZipArchive::CHECKCONS);
			switch ($res) {
				case \ZipArchive::ER_NOZIP :
					$logger->error("not a zip archive. error code ($res)");
					return $return;
					break;
				case \ZipArchive::ER_INCONS :
					$logger->error("[launchArchiveProcess] consistency check failed. error code ($res)");
					return $return;
					break;
				case \ZipArchive::ER_CRC :
					$logger->error("[launchArchiveProcess] checksum failed. error code ($res)");
					return $return;
					break;
			}
			$zip->close();

			try {
				$s3->uploadObjectFile($zipFile,
						$config->s3->resultsArchiveBucket,
						"{$config->s3->resultsArchivePrefix}/" .
						"{$revertedTestSession}.{$status}.{$this->tenant->tenantId}.zip"
				);
			} catch (S3Exception $ex) {
				$logger->error("[launchArchiveProcess] Unable to upload
				{$revertedTestSession}.{$status}.{$this->tenant->tenantId}.zip package to AWS.
				(details:" . $ex->getMessage() . ")");
				return $return;
			}

			try {
				$s3->getObjectDetails($config->s3->resultsArchiveBucket,
						"{$config->s3->resultsArchivePrefix}/".
						"{$revertedTestSession}.{$status}.{$this->tenant->tenantId}.zip"
				);
			} catch (S3Exception $ex) {
				$logger->error("[launchArchiveProcess] Unable to verify the existence of the zip package in S3
				({$config->s3->resultsArchivePrefix}/{$revertedTestSession}.{$status}." .
				"{$this->tenant->tenantId}.zip) Details: (" . $ex->getMessage() . ")");
				return $return;
			}

			$logger->info("[launchArchiveProcess] Successfully uploaded 	{$testSession->testSessionId}
			.{$status}.{$this->tenant->tenantId}.zip) to AWS.");
			// delete the zip package
			unlink($zipFile);
		} else {
			$logger->warning("No s3 files to archive.");
		}
		// [Archive All files end!]

		/**
		 * 1. Download results from results bucket in S3,
		 * 2. zip results,
		 * 3. upload to results archive bucket.
		 * 4. delete results except the last or most recent result.
		 */
		$testResults = TestResults::find([
			'conditions' => ' tenantId = ?1 AND parentTestSessionId = ?2 AND isActive = 1',
			'order' => 'createdDateTime DESC',
			'bind' => [
				1 => $this->tenant->tenantId,
				2 => $testSession->testSessionId
			]
		]);

		if ($testResults->count() == 0) {
			if ($testSession->status == TestSession::STATUS_COMPLETED) {
				$logger->error("[launchArchiveProcess] Canceling the archiving process not enough results
				(Test Session ID: {$testSession->testSessionId},
				Test Session Status: {$testSession->status}, results count: {$testResults->count()})");
			} else {
				$logger->warning("[launchArchiveProcess] Canceling the archiving process not enough results
				(Test Session ID: {$testSession->testSessionId},
				Test Session Status: {$testSession->status}, results count: {$testResults->count()})");
			}
			return $return;
		}

		if ($testResults->count() == 1) {
			if ($testSession->status == TestSession::STATUS_COMPLETED) {
				$logger->error("[launchArchiveProcess] Submitted Test Assignment should have at least 2 results,
				only 1 result found (Test Session ID: {$testSession->testSessionId},
				Test Session Status: {$testSession->status}, results count: {$testResults->count()})");
				$deleteRecordsFromDatabase = false;
			} else {
				$logger->warning("[launchArchiveProcess] Canceling the archiving process not enough results
				(Test Session ID: {$testSession->testSessionId}, Test Session Status: {$testSession->status},
				results count: {$testResults->count()})");
			}
		}

		// Make sure the count matches or else no records will get deleted!
		$logger->info("S3 keys count: " . count($keys) . " | Test Results Count: " . $testResults->count());
		if (count($keys) != $testResults->count()) {
			$deleteRecordsFromDatabase = false;
			$logger->error("Count does not match! No records will be deleted!");
		}

		// Keep the valid keys only.
		foreach ($keys as $k => $v) {
			if (!preg_match('/\.json$/', $k)) {
				unset($keys[$k]);
			}
		}

		foreach ($testResults as $result) {
			if (preg_match('/started\.json$/', strtolower($result->path))) {
				$countOfResultsWithStartedSuffix++;
			} else if (preg_match('/paused\.json$/', strtolower($result->path))) {
				$countOfResultsWithPausedSuffix++;
			} else if (preg_match('/resumed\.json$/', strtolower($result->path))) {
				$countOfResultsWithResumedSuffix++;
			} else if (preg_match('/submitted\.json$/', strtolower($result->path))) {
				$countOfResultsWithSubmittedSuffix++;
			} else {
				$countOfRegularResultsFiles++;
			}

			// Try to download Test Results from AWS S3.
			try {
				$s3->downloadObject(
					(string)$config->s3->resultsBucket, (string)$config->s3->resultsPrefix . '/' . $result->path
				);
			} catch (S3Exception $exception) {
				$logger->error("[launchArchiveProcess] Unable to find file
				'{$config->s3->resultsPrefix}/{$result->path}' in S3 (details: " . $exception->getMessage() . ")");
				$deleteRecordsFromDatabase = false;
				continue;
			}
		}

		$logger->info("[launchArchiveProcess STATISTICS] TestSession ID ({$testSession->testSessionId})
		Results Count:  Started : $countOfResultsWithStartedSuffix | Paused : $countOfResultsWithPausedSuffix | " .
			"Resumed : $countOfResultsWithResumedSuffix | Submitted : $countOfResultsWithSubmittedSuffix | " .
			"Regular : $countOfRegularResultsFiles");

		// here we check and make sure, if the test is completed, to find 1 .started.json and 1 .submitted.json records.
		if ($testSession->status == TestSession::STATUS_COMPLETED) {
			if ($countOfResultsWithStartedSuffix != 1 || $countOfResultsWithSubmittedSuffix != 1) {
				$logger->alert("[launchArchiveProcess] Unexpected count of Started/Submitted records " .
					"(Started: $countOfResultsWithStartedSuffix, " .
					"Submitted: $countOfResultsWithSubmittedSuffix) expected count is 1.");
			}
		}

		// this is the part where I need to clean S3 and leave the last result file.
		$doNotDeleteResult = $testResults->getFirst();

		foreach ($testResults as $result) {
			if (strtotime($result->createdDateTime) > strtotime($doNotDeleteResult->createdDateTime)) {
				$logger->warning("[launchArchiveProcess] Detecting a result (testResultId: {$result->testResultsId}, " .
					"createDateTime: {$result->createdDateTime}) that has a more recent createDateTime than " .
					"(testResultId: {$doNotDeleteResult->testResultsId}, " .
					"createDateTime: {$doNotDeleteResult->createdDateTime})");
				return $return;
			}
		}

		// We only delete records from database, if file count matches between S3 and Database
		if ($deleteRecordsFromDatabase) {
			foreach ($testResults as $result) {
				/* @var $result TestResults */
				if ($result->testResultsId != $doNotDeleteResult->testResultsId) {
					try {
						$s3->deleteObject($config->s3->resultsBucket, $config->s3->resultsPrefix . '/' . $result->path);
						$logger->info("[launchArchiveProcess] '$result->path' has been successfully removed from s3");
					} catch (Exception $ex) {
						$logger->warning("[launchArchiveProcess] removing '$result->path'  from s3 failed.");
					}

					// This is the part where I need to move record to history table in database.
					$resultHistory = new TestResultsArchive();
					// copy result object
					$resultHistory->cloneObject($result);
					// save a backup
					$resultHistory->save();
					// delete the record from results
					$result->delete();
				}
			}

			$return = true;
			/**
			 * Making sure that there is only one result left, getBucket was not retuning results for some reason.
			 */
			try {
				$keys = $s3->getObjectList($config->s3->resultsBucket, $config->s3->resultsPrefix . '/' .
					strrev($doNotDeleteResult->parentTestSessionId), $config->s3->resultsPrefix . '/' .
					strrev($doNotDeleteResult->parentTestSessionId));
			} catch (S3Exception $ex) {
				$logger->error($ex->getMessage());
			}

			if (count(preg_grep('#' . $doNotDeleteResult->path . '$#', array_keys($keys))) == 0) {
				$return = false;
				$logger->error("[launchArchiveProcess] Unable to find most recent active result in result bucket.");
			}

			if (count($keys) > 2) {
				$return = false;
				$logger->error("[launchArchiveProcess] There are left over files  in s3.");
			}
		} else {
			$logger->warning("[launchArchiveProcess] Skipping Database records deletion, " .
				"as S3 and database data do not match for TestSession ({$testSession->testSessionId})");
		}

		return $return;
	}
}
